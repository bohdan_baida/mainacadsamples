﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirlineInfo

{
    class Program
    {

        static void Main(string[] args)
        {
            int x;
            int dcnt = 0;
            int acnt = 0;
            
            try
            {
                do
                {
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine("1. Departures ");
                    Console.WriteLine("2. Arrivals");
                    Console.WriteLine("3. Emergency");
                    try
                    {
                        x = (int)uint.Parse(Console.ReadLine());
                        switch (x)
                        {
                            case 1:
                                Departure(dcnt);
                                Console.WriteLine("");
                                dcnt++;
                                break;
                            case 2:
                                Arrivals(acnt);
                                Console.WriteLine("");
                                acnt++;
                                break;
                            case 3:
                                EmergencyInfo();
                                Console.WriteLine("");
                                break;
                            default:
                                Console.WriteLine("Alert!Exit");
                                break;

                        }
                    }
                    catch(Exception e)
                    {
                        Console.WriteLine("Error" + e.Message);
                    }
                    Console.WriteLine("Press Esc for end / Press any key to  continue :");

                }
                while (Console.ReadKey().Key != ConsoleKey.Escape);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        static void Departures()
        {
            Console.WriteLine();
        }
        #region EmergencyInfo
         static void EmergencyInfo()
        {
            Console.WriteLine(@"Abou Emergency");
            Console.WriteLine("In case of fire, specially trained people are involved in the evacuation.");
            Console.WriteLine("Please follow their directions and do not do anything alone.");
            Console.WriteLine("At the airport itself, evacuation passes through the main exits.");
            Console.WriteLine("The main thing - do not panic!");
        }
        #endregion

    }
}
