﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirportInfo
{
    struct AirportArrival
    {
        public int FlightNumber { get; set; }
        public string ArriveFrom { get; set; }
        public DateTime ArriveTime { get; set; }
        public string Airline { get; set; }
        public Gates Gate  { get; set; }
        public  int Terminal  { get; set; }
        public FlStatus FlightStatus { get; set; }
        public AirportArrival(int flightnumber, string ArriveFrom, DateTime ArrivalTime, string airline, Gates gate, Terminal terminal, FlStatus flightStatus)
        {
            FlightNumber = flightnumber;
            ArriveFrom = ArriveFrom;
            ArriveTime = ArrivalTime;
            Airline = airline;
            gate = gate;
            terminal = terminal;
            FlightStatus = flightStatus;



        }
        public void Display()
        {
            Console.WriteLine($"{FlightNumber} | {ArriveFrom.PadRight(8)} | {ArriveTime:dd.MM.yyy HH:mm} | {Airline} | {Gate} | {Terminal} | {FlightStatus}");
        }
    }
}
