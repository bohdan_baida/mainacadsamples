﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AirportInfo
{
     struct AirportDeparture
     {
        public int FlightNumber { get; set; }
        public string DepartureFrom { get; set; }
        public DateTime DepartureTime { get; set; }
        public string Airline { get; set; }
        public Gates Gate { get; set; }
        public int Terminal { get; set; }
        public FlStatus FlightStatus { get; set; }
        public AirportDeparture(int flightnumber, string DepartureFrom, DateTime DepartureTime, string airline, Gates gate, Terminal terminal, FlStatus flightStatus)
        {
            FlightNumber = flightnumber;
            DepartureFrom = DepartureFrom;
            DepartureTime = DepartureTime;
            Airline = airline;
            gate = gate;
            terminal = terminal;
            FlightStatus = flightStatus;



        }
        public void Display()
        {
            Console.WriteLine($"{FlightNumber} | {DepartureFrom.PadRight(8)} | {DepartureTime:dd.MM.yyy HH:mm} | {Airline} | {Gate} | {Terminal} | {FlightStatus}");
        }
    }

}
