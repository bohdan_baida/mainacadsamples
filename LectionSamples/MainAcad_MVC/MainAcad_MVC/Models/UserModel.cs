﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MainAcad_MVC.Models
{
    public class UserModel
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }
    }
}