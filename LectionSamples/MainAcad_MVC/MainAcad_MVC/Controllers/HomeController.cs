﻿using MainAcad_MVC.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MainAcad_MVC.Controllers
{
    public class HomeController : Controller
    {

        public ActionResult Index()
        {
            var users = new List<UserModel>()
            {
                new UserModel()
                {
                    Id=1,
                    FirstName ="Bohdan",
                    LastName = "Baida"
                },
                new UserModel()
                {
                    Id=1,
                    FirstName ="Ivan",
                    LastName = "Petrov"
                }
            };
            return View(users);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult GetJson()
        {
            var user = new UserModel()
            {
                Id = 1,
                FirstName = "Bohdan",
                LastName = "Baida"
            };

            var userJson = JsonConvert.SerializeObject(user);

            return Json(userJson, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetFile()
        {
            var path = Server.MapPath("~/Files/1.jpg");
            using (StreamReader sr = new StreamReader(path))
            {
                return File(sr.BaseStream, "image/jpg", fileDownloadName: "qwq.jpg");
            }
            //return File(path, "image/jpg", fileDownloadName: "qwq.jpg");
        }

        public ActionResult GetAlcohol(int age)
        {
            if (age<18)
            {
                //return new HttpStatusCodeResult(403);//Forbiden
                return RedirectToAction("GetMilk", "Home");
                //return RedirectToRoute("~/Views/GetMilk");
                //return Redirect(@"https://stackoverflow.com/questions/11795737/content-response-type-image-png");
            }
            //return RedirectPermanent();
            return View();
        }
    }
}