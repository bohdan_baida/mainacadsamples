﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainAcad_1_2
{
    class Program
    {
        static void Main(string[] args)
        {
            byte numberOfPages = new byte();
            byte numberOfStudents = 12;
            byte leftStudents = 4;
            byte arrivedStudents = leftStudents;
            Console.WriteLine($"Value of variable: {numberOfPages}");
            Console.WriteLine(numberOfStudents);
            Console.WriteLine(sizeof(byte));
            Console.WriteLine(byte.MinValue);
            Console.WriteLine(byte.MaxValue);
            Console.WriteLine(numberOfStudents - leftStudents);
            checked
            {
                byte studentsAfterArrival = (byte)(numberOfStudents + arrivedStudents);
            }
            unchecked
            {
                byte studentsAfterArrival = (byte)(numberOfStudents + arrivedStudents);
            }
            Console.WriteLine(numberOfStudents + arrivedStudents);
            //Integer Numbers
            //int
            //long
            //uint
            //ulong
            //short
            //ushort
            //Real numbers
            //decimal
            //float
            //double
            //Boolean
            //bool 
            //Characters
            //char

            string helloAlert = null;
            string helloAlert1 = "Hello";
            string hello2;
            string hello3 = new string(new char[] { 'a','b'});

            int? a = null;
            int? b = null;
            Nullable<int> c = null;
            Console.WriteLine(a != b);
            Console.Read();

            const int constA = 10;
        }
    }
}
