﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainAcad_2_3
{
    class Program
    {
        static void Main(string[] args)
        {
            var p1 = new Point(1, 1);
            var p2 = new Point(2, 2);
            var p3 = Point.Add(p1, p2);
            var p4 = p1.Add(p2);
            var p5 = p1 + p2;
            var p6 = !p1;
            //if (p1)
            //{
            //    Console.WriteLine("upper");
            //}
            //if (!p6)
            //{
            //    Console.WriteLine("lower");
            //}

            //var pointList = new PointsList();
            //pointList.Add(p1);
            //pointList.Add(p2);
            //pointList.Add(p3);
            //var pp = pointList[1];
            //var pp1 = pointList[11];

            //var p7 = p1 + 4;
            ////var p8 = 4 + p1;// should add second operator '+' overload
            //var p8 = ++p1;

            //Console.WriteLine(p1 >= p2);
            //Console.WriteLine(p2 >= p1);
            //Console.WriteLine(p1 <= p2);

            var pp1 = new Point(1, 1);
            var pp2 = new Point(1, 1);
            //Console.WriteLine(pp1 == pp2);
            //Console.WriteLine(pp1 != pp2);
            //Console.WriteLine(pp1.GetHashCode());
            //Console.WriteLine(pp2.GetHashCode());
            //Console.WriteLine(pp1 & pp2);
            Point3D p3d = pp1;
            Point p2d = (Point)p3d;
            Console.Read();
        }
    }
}
