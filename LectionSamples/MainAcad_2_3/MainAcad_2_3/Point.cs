﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainAcad_2_3
{
    public class Point
    {
        public int X { get; set; }

        public int Y { get; set; }

        public Point(int x, int y)
        {
            X = x;
            Y = y;
        }

        public static Point Add(Point p1, Point p2)
        {
            return new Point(p1.X + p2.X, p1.Y + p2.Y);
        }

        public Point Add(Point p)
        {
            return new Point(this.X + p.X, this.Y + p.Y);
        }

        public static Point operator +(Point p1, Point p2)
        {
            return Add(p1, p2);
        }

        public static Point operator !(Point p)
        {
            return new Point(-p.X, -p.Y);
        }
        public static bool operator true(Point p)
        {
            return p.Y >= 0;
        }
        public static bool operator false(Point p)
        {
            return p.Y < 0;
        }

        public static Point operator +(Point p1, int p2)
        {
            return new Point(p1.X + p2, p1.Y + p2);
        }

        public static Point operator ++(Point p)
        {
            return new Point(p.X + 1, p.Y + 1);
        }

        public static bool operator >=(Point p1, Point p2)
        {
            var p1Len = p1.X * p1.X + p1.Y * p1.Y;
            var p2Len = p2.X * p2.X + p2.Y * p2.Y;
            return p1Len >= p2Len;
        }

        public static bool operator <=(Point p1, Point p2)
        {
            var p1Len = p1.X * p1.X + p1.Y * p1.Y;
            var p2Len = p2.X * p2.X + p2.Y * p2.Y;
            return p1Len <= p2Len;
        }

        public override int GetHashCode()
        {
            return this.X << 10 + this.Y;
            //return (val.row.value()<<10) + val.col.value();
        }

        public override bool Equals(object obj)
        {
            return this.GetHashCode() == obj.GetHashCode();
        }

        public static bool operator ==(Point p1, Point p2)
        {
            return p1.GetHashCode() == p2.GetHashCode();
        }
        public static bool operator !=(Point p1, Point p2)
        {
            return p1.GetHashCode() != p2.GetHashCode();
        }
        //HW: Implement >,< operators

        public static bool operator &(Point p1, Point p2)
        {
            return p1.Y >= 0 && p2.Y >= 0;
        }

        //public static explicit operator Point3D(Point p)
        //{
        //    return new Point3D(p.X, p.Y, 0);

        //}

        public static implicit operator Point3D(Point p)
        {
            return new Point3D(p.X, p.Y, 0);

        }
    }
}
