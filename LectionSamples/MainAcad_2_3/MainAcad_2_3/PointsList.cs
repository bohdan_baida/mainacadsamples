﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainAcad_2_3
{
    public class PointsList
    {
        private Point[] array;

        public Point this[uint i]
        {
            get
            {
                if (i >= array.Length)
                {
                    return null;
                }
                return array[i];
            }
            //set
            //{

            //}
        }

        public PointsList()
        {
            array = new Point[0];
        }

        public void Add(Point p)
        {
            var list = array.ToList();
            list.Add(p);
            array = list.ToArray();
        }

        public void Remove(Point p)
        {
            var list = array.ToList();
            var point = array.FirstOrDefault(c => c.X == p.X && c.Y == p.Y);
            if (point == null)
            {
                return;
            }
            list.Remove(point);
            array = list.ToArray();
        }
    }
}
