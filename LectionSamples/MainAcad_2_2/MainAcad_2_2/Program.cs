﻿using MainAcad_2_2.NewProgram;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainAcad_2_2
{
    static class Program
    {
        static Random rand = new Random();

        static void Main(string[] args)
        {
            //Console.WriteLine(args[0]);
            //Console.WriteLine(args[1]);
            //var sum1 = Sum(1, 2, 3, 4, 5);
            //var sum2 = Sum(1, 2);
            //var arr = new int[] { 1, 2, 3 };
            //var sum3 = Sum(arr);

            //var point = new Point(1, 1);
            //ChangePoint(point);
            //ChangePoint(point, false);
            //ChangePoint(point, true, 1);
            //ChangePoint(point, increaser: 1);//named parameter


            //var accrualValue1 = -10m;
            //var accrualValue2 = 10m;
            //ValidateAccrual(ref accrualValue1);
            //ValidateAccrual(ref accrualValue2);

            ////ref, out
            //var a = "10";
            //int a_num;
            //int.TryParse(a, out a_num);

            //bool a1, a2;
            //if (TryParse("true", out a1))
            //{
            //    Console.WriteLine(a1);
            //}
            //if(!TryParse("фолс",out a2))
            //{
            //    Console.WriteLine("Not parsed");
            //}

            //var point = new Point(1, 1);
            //ChangePoint2(ref point);

            //Visualize(point);
            //Console.WriteLine("a");

            string a = "113235423";
            ChangeString(a);
            Console.WriteLine(a);

            var point = new Point(1, 1);
            var point1 = new Point(1,1)
            {
                X = 2,
                Y = 2
            };

            DoSmth();
            GC.Collect();

            //Conditions in break points
            //for (int i = 0; i < 1000; i++)
            //{
            //    Console.WriteLine(i);
            //}
            Console.Read();
        }

        static void DoSmth()
        {
            Console.WriteLine("Hello World!");
            var program1 = new Program1();

        }

        static int GetRandomNumber()
        {
            return rand.Next();
        }

        static Point GetRandomPoint()
        {
            return new Point(rand.Next(), rand.Next());
        }

        static int Add(int x, int y)
        {
            return x + y;
        }

        static void Visualize(Point p)
        {
            Console.WriteLine($"({p.X},{p.Y})");
        }
        static void AddToPoint(Point p, int value)
        {
            p.X += value;
            p.Y += value;
        }

        static Point AddToPoint(Point p1, Point p2)
        {
            return new Point(p1.X + p2.X, p1.Y + p2.Y);
        }

        static bool Compare(Point p1, Point p2)
        {
            if (p1 == null || p2 == null)
            {
                return false;
            }

            return p1.X == p2.X && p2.Y == p2.Y;
        }

        static void Visualize1(Point x)
        {
            if (x == null)
                return;

            Visualize(x);
        }

        static int Sum(params int[] parameters)
        {
            return parameters.Sum();
        }

        static void ChangePoint(
            Point p = null,
            bool autoSave = true,
            int increaser = 0//, 
                             //int a = 1 + 1
            )
        //optional parameters
        {
            p.X = 1;

            p.X += increaser;
            p.Y += increaser;

            if (!autoSave)
                return;

            //ctx.SaveChanges();
        }


        static void ValidateAccrual(ref decimal accrualValue)
        {
            accrualValue = accrualValue < 0
                ? 0
                : accrualValue;
        }

        static bool TryParse(string valueString, out bool value)
        {
            var result = true;

            switch (valueString.ToLower())
            {
                case "true":
                    result = true;
                    value = true;
                    break;
                case "false":
                    result = true;
                    value = false;
                    break;
                default:
                    result = false;
                    value = false;
                    break;
            }
            return result;

        }

        static void ChangePoint(Point x)
        {
            x.X += 1;
            x.Y += 1;
        }

        static void ChangePoint1(Point x)
        {
            x = new Point(x.X + 1, x.Y + 1);
        }

        static void ChangePoint2(ref Point x)
        {
            x = new Point(x.X + 1, x.Y + 1);
        }

        static void ChangeString(string a)
        {
            //!!! string is immutable. 
            //!!!Use "ref" to change original value
            a = "dwswqsws";
        }

        static int Add1(int x, int y)
        {
            return x + y;
        }
        static int Add1(ref int x1, int y1)
        {
            return x1 + y1 ;
        }

    }
}
