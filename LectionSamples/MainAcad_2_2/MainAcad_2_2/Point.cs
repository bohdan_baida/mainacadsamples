﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainAcad_2_2
{
    public class Point
    {
        public int X { get; set; }
        public int Y { get; set; }

        public Point()
        {

        }
        public Point(int x, int y)
        {
            X = x;
            Y = y;
        }
    }
}
