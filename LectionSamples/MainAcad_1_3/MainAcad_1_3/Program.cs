﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainAcad_1_3
{
    class Program
    {
        static void Main(string[] args)
        {
            //ariphmetical + - * / % ++ --
            var a = 8;
            var b = 4;
            //int c = a + b; //6
            //int d = a + b++;
            //int e = a + ++b;
            //int f = a + --b;//7
            //int g = a + b--;//7
            int h = a + b * b;
            int h1 = (a + b) * b;
            int h3 = a * b / a;
            int h4 = b / a;//0!

            double c = 5;
            double d = 10;
            var h5 = c / d;// 0.5

            decimal c1 = 5.544m;
            decimal c2 = 2.000m;
            decimal c3 = 1;
            c3 = c1 / c2;

            double d1 = 1;
            double d2 = d1 / 0;
            var d3 = double.IsInfinity(d2);
            double d4 = 0;
            double d5 = 0;
            double d6 = d4 / d5;
            var d7 = double.IsNaN(d6);

            int e1 = 1;
            int e2 = 0;
            //int e3 = e1 / e2;
            int e4 = e1 * e2;

            var e5 = 10;
            var e6 = e5++;//10

            float e7 = 10;
            var e8 = e7 is double;
            var e9 = e7 is float;

            var f1 = 1;
            var f2 = 2;
            var f3 = f1 == f2;
            var f4 = f1 != f2;

            Nullable<int> x1 = 1;
            var x2 = x1 ?? 3;
            Nullable<int> x3 = null;
            int x4 = x3 ?? 0;

            bool x5 = false;
            bool x6 = !x5;
            x5 = !x5;

            var q1 = 2;
            var q2 = 3;
            char q4 = (char)(100 + 1);
            var q3 = q1 == q2 ? "yes" : "no";

            q1 += 10;
            //q1 = q1 + 10;
            q1 -= 10;
            q1 *= 10;
            q1 /= 10;
            q1 -= q1;
            q2 %= 2;
            //q2 = q2 % 2;

            var q5 = 10;
            q5 = ~q5;

            byte q6 = 32 >> 2;
            int aa = 1, bb = 2;
            int a11 = (2 + 2) * 2;
            byte a12 = (byte)12;


            int a13 = 11;
            Console.WriteLine($"begining of method");
            if (a13 == 1)
            {
                Console.WriteLine($"{nameof(a13)} == 1");
                Console.WriteLine($"{nameof(a13)} == 1");
            }
            else
                Console.WriteLine($"{nameof(a13)} != 1");

            Console.WriteLine($"end of method");

            int i = 4;
            switch (i)
            {
                case 1:
                    Console.WriteLine($"{nameof(i)} == 1");
                    break;//goto case 3;
                case 2:
                    Console.WriteLine($"{nameof(i)} == 2");
                    break;
                case 3:
                    Console.WriteLine($"{nameof(i)} == 3");
                    break;
                default:
                    Console.WriteLine($"{nameof(i)} is not matched cases");
                    break;
            }

            for (int ii = 0; ii <= 5; ii += 2)
            {
                Console.WriteLine($"{nameof(ii)} is {ii}");

            }
            //for (; ; )
            //{
            //    Console.WriteLine(1);

            //}
            aa = 2;
            Console.WriteLine("while");

            while (aa != 2)
            {
                aa += 1;
                Console.WriteLine(aa);
            }
            Console.WriteLine("do while");
            var aaa = 2;
            do
            {
                //aaa += 1;
                Console.WriteLine(aaa);

            } while (aaa != 2);

            Console.WriteLine("foreach");

            var arr = new int[] { 1, 2, 3, 4, 5, 6, 7, 11, 123, 325 };
            foreach (var item in arr)
            {
                Console.WriteLine(item);

            }
            for (i = 0; i < arr.Length; i++)
            {
                Console.WriteLine(arr[i]);
            }

            Console.ReadKey();
        }
    }
}
