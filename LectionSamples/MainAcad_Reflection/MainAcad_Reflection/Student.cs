﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainAcad_Reflection
{
    public class Student
    {
        int id;

        public Student()
        {

        }

        public Student(string name)
        {
            Name = name;
        }

        public Student(string name, int id)
        {
            Name = name;
            Id = id;
        }
        public int Id
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
            }
        }

        public string Name { get; set; }

        public void SetName(string name)
        {
            Name = name;
        }

        private void DecrementId()
        {
            id = id - 1;
        }
    }
}
