﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace MainAcad_Reflection
{
    class Program
    {
        static void Main(string[] args)
        {
            //Assembly
            var assembly = Assembly.GetCallingAssembly();
            var types = assembly.GetTypes();
            foreach (var type in types)
            {
                Console.WriteLine(type.Name);
            }
            //Console.ReadLine();

            //Type
            var student = new Student();
            var studentType = student.GetType();
            //var methods = studentType.GetMethods(BindingFlags.NonPublic | BindingFlags.Instance);

            //MethodInfo
            var method = studentType.GetMethod(
                "DecrementId",
                BindingFlags.Instance | BindingFlags.NonPublic);
            var method1 = studentType.GetMethod(
                "SetName");
            method.Invoke(student, new object[] { });
            method1.Invoke(student, new object[] { "Ira" });

            //Activator -- Fabric method, abstract fabric
            var newStudent = (Student)Activator.CreateInstance(studentType, "Ira", 3);

            //PropertyInfo
            var property = studentType.GetProperty("Name");
            var studName = (string)property.GetValue(newStudent);
            property.SetValue(newStudent, "Vika");
            var newStudName = (string)property.GetValue(newStudent);
            var ctors = studentType.GetConstructors();


        }

    }
}
