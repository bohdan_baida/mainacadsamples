﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainAcad_Reflection
{
    public class Car
    {
        public decimal Price { get; set; }
        public string Model { get; set; }
        public int Millage => millage;

        private int millage;

        public Car(int millage)
        {
            this.millage = millage;
        }
        public Car(string model, decimal price )
        {
            Model =model;
            Price = price;
            millage = 0;
        }

        public void SetPrice(decimal price)
        {
            Price = price;
        }
    }
}
