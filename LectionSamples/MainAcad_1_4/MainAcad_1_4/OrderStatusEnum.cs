﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainAcad_1_4
{
    public enum OrderStatus : byte
    {
        Accepted = 1,
        Approved,
        OnDelivery,
        Delivered
    }
}
