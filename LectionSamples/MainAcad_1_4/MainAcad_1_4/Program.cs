﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainAcad_1_4
{
    class Program
    {
        //enum OrderStatus
        //{
        //    Accepted = 1,
        //    Approved,
        //    OnDelivery,
        //    Delivered
        //}

        //structure
        struct Point
        {
            public int X { get; }
            public int Y { get; }

            //constructor
            public Point(int x, int y)
            {
                X = x;
                Y = y;
            }

            public override string ToString()
            {
                return $"({X},{Y})";
            }

        }
        static void Main(string[] args)
        {
            int[] arr = new int[5];
            //int[] arr = new int[5] { 1, 2, 3, 4, 5 };
            //int[] arr = new int[] { 1, 2, 3, 4, 5 };
            //for (int i = 0; i < arr.Length; i++)
            //{
            //    Console.WriteLine(arr[i]);
            //}

            //int[] arr1 = new int[1000];
            //for (int i = 0; i < arr1.Length; i++)
            //{
            //    arr1[i] = i + 1;
            //}
            //foreach (var item in arr1) //підходить для виведення та не для зміни
            //{
            //    Console.WriteLine(item);
            //}
            //Console.Read();

            //MULTIDIMENSIONAL ARRAYS

            //int[,] dim2Array = new int[8, 8];
            //Console.CursorSize = 2;
            //for (int i = 0; i < dim2Array.GetLength(0); i++)
            //{
            //    for (int j = 0; j < dim2Array.GetLength(1); j++)
            //    {
            //        dim2Array[i, j] = (i + 1) * (j + 1);
            //    }
            //    Console.WriteLine();
            //}
            //for (int i = 0; i < dim2Array.GetLength(0); i++)
            //{
            //    for (int j = 0; j < dim2Array.GetLength(1); j++)
            //    {
            //        Console.Write(dim2Array[i, j]);
            //    }
            //    Console.WriteLine();
            //}
            //Console.Read();


            //JAGGED ARRAY
            //int[][] jaggedArray = new int[2][];
            //jaggedArray[0] = new int[2] { 1, 2 };
            //jaggedArray[1] = new int[] { 1, 2, 3 };
            //for (int i = 0; i < jaggedArray.Length; i++)
            //{
            //    foreach (var item in jaggedArray[i])
            //    {
            //        Console.Write(item);
            //    }
            //    Console.WriteLine();
            //}
            //Console.Read();

            //SEARCH
            //int[] arr_s = new int[] { 3,2,6,8,2,7,9,3,1,2,8};
            //bool isExists = false;
            //const int element = 4;
            //foreach (var item in arr_s)
            //{
            //if (item == element)
            //{
            //isExists = true;
            //break;
            //}
            //}

            //int minElement = arr_s[0];
            //foreach (var item in arr_s)
            //{
            //if (item < minElement)
            //minElement = item;
            //}

            //int maxElement = arr_s[0];
            //foreach (var item in arr_s)
            //{
            //if (item > maxElement)
            //maxElement = item;
            //}


            //Console.WriteLine(isExists);
            //Console.WriteLine(minElement);
            //Console.WriteLine(maxElement);

            //Console.WriteLine(arr_s.Contains(4));
            //Console.WriteLine(arr_s.Min());
            //Console.WriteLine(arr_s.Max());

            //Console.Read();

            //COPY
            //int[] arr1 = new int[5] { 1, 2, 3, 4, 5 };
            //int[] arr2 = new int[6];
            //for (int i = 0; i < arr1.Length; i++)
            //{
            //    arr2[i] = arr1[i];
            //}
            //arr2[5] = 6;
            //foreach (var item in arr2)
            //{
            //    Console.WriteLine(item);
            //}
            //Console.Read();

            //SORT
            //int[] arr_s = new int[] { 3, 2, 6, 8, 2, 7, 9, 3, 1, 2, 8 };
            ////var sortedArray = arr_s.OrderBy(x => x);
            //Array.Sort(arr_s);
            ////quick sort
            ////bubble sort
            //foreach (var item in arr_s)
            //{
            //    Console.WriteLine(item);
            //}

            //COMPARE
            //int[] arr_1 = new int[] { 3, 2, 6, 8, 2, 7, 9, 2, 1, 2, 8 };
            //int[] arr_2 = new int[] { 3, 2, 6, 8, 2, 7, 9, 3, 1, 2, 8 };
            //if (arr_1.Length != arr_2.Length)
            //{
            //    Console.WriteLine("different");
            //}
            //else
            //{
            //    bool isEquals = true;
            //    for (int i = 0; i < arr_1.Length; i++)
            //    {
            //        if (arr_1[i] != arr_2[i])
            //            isEquals = false;
            //    }
            //    Console.WriteLine(isEquals ? "equals" : "different");
            //}

            //boxing - unboxing... danger
            //object[] arr_obj = new object[3];
            //arr_obj[0] = "qaqaq";
            //arr_obj[1] = 12;
            //arr_obj[2] = false;
            //foreach (var item in arr_obj)
            //{
            //    Console.WriteLine(item);
            //}

            //OrderStatus orderStatus = OrderStatus.Accepted;
            //orderStatus = OrderStatus.Approved;

            //Console.WriteLine(orderStatus);
            //Console.WriteLine((int)orderStatus);


            int x1 = 1;
            int y1 = 3;

            int x2 = 2;
            int y2 = 5;

            var point1 = new Point(1,3);
            var point2 = new Point(2, 5);

            Console.WriteLine(point1);
            Console.WriteLine(point2);

            Console.Read();

        }
    }
}
