﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Main_Acad_Multithreading
{
    public class Toilet
    {
        private int numberOfSits;

        Semaphore semaphore = new Semaphore(2, 2);

        public List<Person> people;

        public int NumberOfSits => numberOfSits;

        public Toilet(int numberOfSits)
        {
            this.numberOfSits = numberOfSits;
        }

        public void LetNewPersonIn(object person)
        {
            var pers = (Person)person;
            semaphore.WaitOne();
            Console.WriteLine($"Person with time {pers.TimeOfShitting} started");
            Thread.Sleep(pers.TimeOfShitting);
            Console.WriteLine($"Person with time {pers.TimeOfShitting} finished");
            semaphore.Release();

        }

    }

    public class Person
    {
        private int timeOfShitting;
        public int TimeOfShitting => timeOfShitting;
        public Person(int timeOfShitting)
        {
            this.timeOfShitting = timeOfShitting;
        }
    }
}
