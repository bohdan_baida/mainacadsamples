﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
namespace Main_Acad_Multithreading
{
    class Program
    {
        static int i = 1;
        static object _lock = new object();
        static Mutex mutex = new Mutex();
        static void Main(string[] args)
        {
            bool createdNew;
            var mutex = new Mutex(true, "appTest", out createdNew);
            if (!createdNew)
            {
                throw new Exception("Can`t run 2 instances");
            }
            //ThreadStart
            //ThreadStart threadStart = new ThreadStart(Counter);
            //ParameterizedThreadStart parameterizedThreadStart
            //    = new ParameterizedThreadStart(Counter);
            //Thread thread = new Thread(threadStart);
            //Console.WriteLine("Begin of operation");
            //thread.Start();
            //parameterizedThreadStart.Invoke(20);
            //Console.WriteLine("End of operation");

            //var current = Thread.CurrentThread;
            //Console.WriteLine(current.ThreadState);
            //Console.WriteLine(current.Priority);
            //Console.WriteLine(current.Name);
            //Console.WriteLine(current.ManagedThreadId);

            //for (int i = 0; i < 3; i++)
            //{
            //    ThreadStart threadStart = new ThreadStart(Counter);
            //    Thread thread = new Thread(threadStart);
            //    thread.Start();
            //}

            //Queue<Person> people = new Queue<Person>();
            //people.Enqueue(new Person(3000));
            //people.Enqueue(new Person(4000));
            //people.Enqueue(new Person(2000));
            //people.Enqueue(new Person(5000));

            //Toilet toilet = new Toilet(2);
            //while (people.Count != 0)
            //{
            //    var nextPerson = people.Dequeue();
            //    ParameterizedThreadStart threadStart 
            //        = new ParameterizedThreadStart(toilet.LetNewPersonIn);
            //    Thread thread = new Thread(threadStart);
            //    thread.Start(nextPerson);
            //}

            Console.WriteLine("Begin");
            Task.Run(() =>
            {
                for (int i = 0; i < 10; i++)
                {
                    Console.WriteLine(i);
                }
            }).Wait();
            Task.Run(() => Counter());
            var result = Task.Run(() =>
            {
                int sum = 0;
                for (int i = 0; i < 10; i++)
                {
                    sum += i;
                }
                return sum;
            });
            var intResult = result.Status;
            Console.WriteLine("End");
            Console.Read();

        }

        static void Counter()
        {
            throw new Exception();
            //Monitor.Enter(_lock);
            //lock (_lock)
            //{
            mutex.WaitOne();
            i = 1;
            while (i != 11)
            {
                Console.WriteLine(i);
                i++;
            }
            mutex.ReleaseMutex();
            //}
            //Monitor.Exit(_lock);
        }

        static void Counter(object j)
        {
            int counter = (int)j;
            for (int i = 1; i <= counter; i++)
            {
                Console.WriteLine(i);
            }
        }
    }
}
