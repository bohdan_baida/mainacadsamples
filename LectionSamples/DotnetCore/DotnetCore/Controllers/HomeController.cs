﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DotnetCore.Models;

namespace DotnetCore.Controllers
{
    //Microsoft.EntityFrameworkCore.SqlServer
    public class HomeController : Controller
    {
        TaskContext taskContext;

        public HomeController(TaskContext taskContext)
        {
            this.taskContext = taskContext;
        }
        public IActionResult Index()
        {

            return View(taskContext.ManagerTasks.ToList());
        }
        public IActionResult Task(int id)
        {

            return View(taskContext.ManagerTasks.Find(id));
        }


        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public IActionResult CreateTask()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> CreateTask(ManagerTask managerTask)
        {
            taskContext.ManagerTasks.Add(managerTask);
            await taskContext.SaveChangesAsync();

            return RedirectToAction("Index");
        }
    }
}
