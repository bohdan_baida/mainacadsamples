﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DotnetCore.Models
{
    public class ManagerTask
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string Header { get; set; }

        public string Description { get; set; }
    }

    public class TaskContext : DbContext
    {
        public TaskContext(DbContextOptions<TaskContext> contextOptions)
            : base(contextOptions)
        {

        }
        public DbSet<ManagerTask> ManagerTasks { get; set; }
    }
}
