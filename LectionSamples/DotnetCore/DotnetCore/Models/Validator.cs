﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DotnetCore.Models
{
    public class Validator : IValidator
    {
        public string Validate()
        {
            return "Validated";
        }
    }
    public interface IValidator
    {
        string Validate();
    }
}
