﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainAcad_3_1
{
    class Program
    {
        static void Main(string[] args)
        {
            //unsafe
            //{
            //    int* a;
            //}
            //Match("qwery", "qwerty1");

            //List<Random> list = new List<Random>();
            //while (true)
            //{
            //    var random = new Random();
            //    list.Add(random);
            //}
            //try
            //{
            //    Console.WriteLine(Fact(10));

            //}
            //catch (Exception ex)
            //{

            //    throw;
            //}
            while (true)
            {
                try
                {
                    var a = int.Parse(Console.ReadLine());
                    var b = int.Parse(Console.ReadLine());
                    var c = a / b;
                    Console.WriteLine(c);

                    int i = int.MaxValue;
                    checked
                    {
                        i++;
                    }


                }
                //bad idea
                //catch (Exception)
                //{
                //    Console.WriteLine("Unexpected exception");

                //}
                catch (DivideByZeroException)
                {
                    Console.WriteLine("Divide by zero");

                }
                catch (FormatException /*ex*/)
                {

                    Console.WriteLine("Please type the number");
                }
                catch (Exception)
                {
                    Console.WriteLine("Unexpected exception");
                }
                finally
                {

                }

                //for security issues as most
                //try
                //{
                //}
                //finally
                //{

                //}

                //try
                //{
                //    try
                //    {

                //    }
                //    catch (Exception)
                //    {

                //        throw;
                //    }
                //}
                //catch (Exception)
                //{

                //    throw;
                //}

                //catch (Exception)
                //{
                //    if (e is DivideByZeroException)
                //    {
                //        Console.WriteLine("Divide by zero");
                //    }
                //    if (e is FormatException)
                //    {
                //        Console.WriteLine("Please type the number");
                //    }
                //}
            }

            
        }

        public static int Fact(int n)
        {
            if (n == 0)
                return 1;

            return n * Fact(n - 1);
        }
        public static void Match(string password, string passwordConfirm)
        {
            try
            {
                if (password != passwordConfirm)
                {
                    throw new PasswordException("Password and password confirm is not matched",password, passwordConfirm);
                }
            }
            catch (PasswordException ex)
            {
                throw ex;
            }
            finally
            {
                password = string.Empty;
                passwordConfirm = string.Empty;
            }
           
        }
    }
}
