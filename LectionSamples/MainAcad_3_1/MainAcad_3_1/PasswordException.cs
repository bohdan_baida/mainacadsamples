﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainAcad_3_1
{
    /// <summary>  
    ///  This class performs an important function.  
    /// </summary> 
    class PasswordException : ApplicationException
    {
        private const string defaultMessage = "Password is invalid";
        public PasswordException()
            : base(defaultMessage)
        {

        }
        public PasswordException(string message)
            : base(message)
        {

        }
        public PasswordException(string message, string password, string passwordConfirm)
            : base(message)
        {
            this.Data.Add(nameof(password), password);
            this.Data.Add(nameof(passwordConfirm), passwordConfirm);

        }
    }
}
