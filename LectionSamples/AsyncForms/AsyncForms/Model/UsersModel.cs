﻿using AsyncForms.Model.EF;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AsyncForms.Model
{
    public class UsersModel
    {
        BlogContext context = new BlogContext();
        public UsersModel()
        {

        }

        public IReadOnlyCollection<User> GetUsers()
        {
            return context.Users
                .AsNoTracking()
                .ToList();
        }

        public async Task<IReadOnlyCollection<User>> GetUsersAsync()
        {
            return await context.Users
                .AsNoTracking()
                .ToListAsync();
        }

        public User GetUser(int id)
        {
            return context.Users
                .Find(id);
        }
        public async Task<User> GetUserAsync(int id)
        {
            return await context.Users
                .FindAsync(id);
        }

        public async Task SaveChangesAsync()
        {

            await context.SaveChangesAsync();
        }

        public async Task RemoveUserAsync(int Id)
        {
            User user = await context.Users.FindAsync(Id);
            context.Users.Remove(user);
            await context.SaveChangesAsync();
        }
    }
}
