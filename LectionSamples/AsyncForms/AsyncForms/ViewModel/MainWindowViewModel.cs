﻿using AsyncForms.Model;
using AsyncForms.Model.EF;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AsyncForms.ViewModel
{
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        UsersModel model = new UsersModel();

        public event PropertyChangedEventHandler PropertyChanged;

        public IReadOnlyCollection<User> Users { get; set; }
        public Command UserSelectCommand { get; set; }
        public Command SaveCommand { get; set; }
        public Command RemoveCommand { get; set; }

        public User CurrentUser { get; set; }

        public MainWindowViewModel()
        {
            UserSelectCommand = new Command(async (obj) =>
            {
                var us = (User)obj;
                CurrentUser = await model.GetUserAsync(us.Id);
                PropertyChanged?
                    .Invoke(this, new PropertyChangedEventArgs("CurrentUser"));
            });
            SaveCommand = new Command(async (obj) =>
            {
                await model.SaveChangesAsync();
                CurrentUser = await model.GetUserAsync(CurrentUser.Id);
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("CurrentUser"));
            });
            RemoveCommand = new Command(async (obj) =>
            {
                if (CurrentUser != null)
                {
                    await model.RemoveUserAsync(CurrentUser.Id);
                    await RefreshMainFormAsync();

                }
            });
            RefreshMainFormAsync();
        }

        private async Task RefreshMainFormAsync()
        {
            Users = await model.GetUsersAsync();
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Users"));

            var user = Users.FirstOrDefault();
            if (user != null)
            {
                CurrentUser = await model.GetUserAsync(user.Id);
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("CurrentUser"));
            }
        }
    }
}
