﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Messagess
{

    public class AddOrderMessage
    {
        public Order Order { get; set; }
    }

    public class AddOrerResponseMessage
    {
        public bool IsSuccess { get; set; }
    }

    public class CompleteOrderMessage
    {
        public int OrderId { get; set; }
    }

    public class Order
    {
        public int Id { get; set; }

        public string Acceptor { get; set; }

        public string DeliveryAddress { get; set; }

        public DateTime OrderDate { get; set; }

        public bool IsProcessed { get; set; }

        public override string ToString()
        {
            return $"Id:{Id}\n" +
                $"Acceptor:{Acceptor}\n" +
                $"DeliveryAddress:{DeliveryAddress}\n" +
                $"OrderDate:{OrderDate}";
        }
    }
}
