﻿using EasyNetQ;
using Messagess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace DeliveryService.WindowsService
{
    public partial class Service1 : ServiceBase
    {
        static IBus bus;

        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            bus = RabbitHutch.CreateBus("host=localhost");

            bus.RespondAsync<AddOrderMessage, AddOrerResponseMessage>(ProcessOrder);
            bus.Subscribe<CompleteOrderMessage>(Guid.NewGuid().ToString(), CompleteOrder);

            Console.WriteLine("Service is statred");
            Console.Read();
        }

        static async Task<AddOrerResponseMessage> ProcessOrder(AddOrderMessage message)
        {
            try
            {
                Console.WriteLine(message.Order.ToString());
                return new AddOrerResponseMessage()
                {
                    IsSuccess = true
                };
            }
            catch (Exception)
            {

                return new AddOrerResponseMessage() { IsSuccess = false };
            }

        }

        static void CompleteOrder(CompleteOrderMessage completeOrder)
        {
            Console.WriteLine($"Order {completeOrder.OrderId} is completed");
        }
        protected override void OnStop()
        {
        }
    }
}
