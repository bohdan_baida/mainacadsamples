﻿using EasyNetQ;
using Messagess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Topshelf;
using System.IO;

namespace DeliveryService
{
    public class Program
    {
        static IBus bus;
        static void Main(string[] args)
        {
            ConfigureService.Configure();
            //Console.WriteLine("Service is statred");
            //Console.Read();
        }

        static async Task<AddOrerResponseMessage> ProcessOrder(AddOrderMessage message)
        {
            try
            {
                using (StreamWriter streamWriter = new StreamWriter(@"C:\file_orders.txt"))
                {
                    streamWriter.WriteLine(message.Order.ToString());
                }
                return new AddOrerResponseMessage()
                {
                    IsSuccess = true
                };
            }
            catch (Exception)
            {

                return new AddOrerResponseMessage() { IsSuccess = false };
            }
           
        }

        static void CompleteOrder(CompleteOrderMessage completeOrder)
        {
            Console.WriteLine($"Order {completeOrder.OrderId} is completed");
        }

        internal static class ConfigureService
        {
            internal static void Configure()
            {
                HostFactory.Run(configure =>
                {
                    configure.Service<MyService>(service =>
                    {
                        service.ConstructUsing(s => new MyService());
                        service.WhenStarted(s => s.Start());
                        service.WhenStopped(s => s.Stop());
                    });
                    //Setup Account that window service use to run.  
                    configure.RunAsLocalSystem();
                    configure.SetServiceName("MyWindowServiceWithTopshelf");
                    configure.SetDisplayName("MyWindowServiceWithTopshelf");
                    configure.SetDescription("My .Net windows service with Topshelf");
                });
            }
        }

        public class MyService
        {
            public void Start()
            {
                bus = RabbitHutch.CreateBus("host=localhost");

                bus.RespondAsync<AddOrderMessage, AddOrerResponseMessage>(ProcessOrder);
                bus.Subscribe<CompleteOrderMessage>(Guid.NewGuid().ToString(), CompleteOrder);
            }
            public void Stop()
            {
                // write code here that runs when the Windows Service stops.  
            }
        }
    }



}
