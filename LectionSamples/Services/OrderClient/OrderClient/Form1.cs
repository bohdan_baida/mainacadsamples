﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EasyNetQ;
using Messagess;
namespace OrderClient
{
    public partial class Form1 : Form
    {
        IBus bus;
        public Form1()
        {
            InitializeComponent();
            bus = RabbitHutch.CreateBus("host=localhost");
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private async void button1_Click(object sender, EventArgs e)
        {
            var order = new Order()
            {
                Id = int.Parse(tbId.Text),
                Acceptor = tbAcceptor.Text,
                DeliveryAddress = tbDeliveryAddress.Text,
                OrderDate = dtDate.Value

            };

            var message = new AddOrderMessage()
            {
                Order = order
            };

            var result = await bus.RequestAsync<AddOrderMessage, AddOrerResponseMessage>(message);

            MessageBox.Show($"Result:{result.IsSuccess}");
        }
    }
}
