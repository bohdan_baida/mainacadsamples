﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace HashManager
{
    public class PasswordHashManager
    {
        private const string salt1 = "my super puper salt";
        private const string salt2 = "my ordinary salt";

        public string HashPassword(string password)
        {
            var passwordWithSalt = salt1 + password + salt2;

            var sha256 = new SHA256Managed();
            var bytesHash = sha256.ComputeHash(
                Encoding.Unicode.GetBytes(passwordWithSalt));

            return Convert.ToBase64String(bytesHash);
        }

        public bool ValidatePassword(string password, string hashFromDb)
        {
            var passwordHash = HashPassword(password);
            return passwordHash == hashFromDb;
        }
    }
}
