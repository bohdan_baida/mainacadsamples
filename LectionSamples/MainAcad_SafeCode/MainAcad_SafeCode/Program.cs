﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HashManager;

namespace MainAcad_SafeCode
{
    class Program
    {
        static void Main(string[] args)
        {
            unsafe
            {
                int a = 10;
                //int* a_pointer = &a;
                //Console.WriteLine((int)a_pointer);
                //Increment(a_pointer);
                //Console.WriteLine(a);

                //Point point = new Point()
                //{
                //    X = 1,
                //    Y = 1
                //};


                //Console.WriteLine($"{point.X},{point.Y}");

                //int* arr = stackalloc int[10];
                //for (int i = 0; i < 10; i++)
                //{
                //    arr[i] = i + 1;
                //}
                //for (int i = 0; i < 10; i++)
                //{
                //    Console.WriteLine(arr[i]);
                //}

                //int aa = 10;
                //int* arr1 = &aa;
                //for (int i = 0; i < 10000; i++)
                //{
                //    arr1[i] = i + 1;
                //}
                //for (int i = 0; i < 100; i++)
                //{
                //    Console.WriteLine(arr1[i]);
                //}
                var hashManager = new PasswordHashManager();
                var password = "Qwerty1@";
                var hash = hashManager.HashPassword(password);
                Console.WriteLine(hash);

                //Authentication
                Console.WriteLine(hashManager.ValidatePassword(password, hash));
                //Autherization
                Console.Read();
            }
        }

        static unsafe void Increment(int* a)
        {
            *a = *a + 10;
        }
        static unsafe void Increment(Point* a)
        {
            a->X++;
            a->Y++;
        }

        struct Point
        {
            public int X { get; set; }
            public int Y { get; set; }
        }
    }
}
