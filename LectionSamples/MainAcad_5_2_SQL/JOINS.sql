select * from Customers
select * from Orders

--insert into Orders
--select 1,GETDATE(),'Kyiv, Konyeva,5'

--insert into Orders
--select Customers.Id, GETDATE(), Customers.Adress
--from Customers
--where id=3

--JOINS:
--INNER JOIN
--LEFT JOIN
--RIGHT JOIN
--CROSS JOIN
SELECT FirstName,LastName,Adress,OrderDate
FROM Customers c
	INNER JOIN Orders o on c.Id = o.CustomerId
SELECT FirstName,LastName,Adress,OrderDate
FROM Customers c
	LEFT JOIN Orders o on c.Id = o.CustomerId
SELECT FirstName,LastName,Adress,OrderDate
FROM Orders o
	RIGHT JOIN Customers c on c.Id = o.CustomerId

SELECT *
FROM Customers c
	Cross join Orders o