--SSMS - SqlServerManagementStudio
-- SQL (MySql,T-SQL,PL-SQL)
-- DDL(data description language)
-- DML(data manipulation language)
---------------------------------------------------
-- CONSISTENCY CHECKS
-- DATABASES TABLES,CONSTRAINT, VIEWS,TRIGGERS, STORED PROCESDURES, FUNCTIONS

CREATE DATABASE MainAcadOrderService

USE MainAcadOrderService

CREATE TABLE Customers
(
	Id INT IDENTITY(1,1),
	FirstName varchar(100),
	LastName varchar(150),
	Age int,
	Adress varchar(max),
	IsActive bit,

	CONSTRAINT PK_Customers_Id PRIMARY KEY(Id),
	CONSTRAINT Age_More_Then_Zero CHECK (Age>0 and Age<150)
)

CREATE TABLE Orders
(
	Id INT IDENTITY(1,1),
	CustomerId int,
	OrderDate datetime,
	DeliveryAdress varchar(max),
	CONSTRAINT PK_Orders_Id PRIMARY KEY(Id),
	CONSTRAINT FK_Orders_Customers FOREIGN KEY(CustomerId) REFERENCES Customers(Id)
)

CREATE TABLE Goods
(
	Id int IDENTITY(1,1),
	GoodsName varchar(500),
	Price decimal(18,2),
	Quantity int,
	IsActive bit,

	CONSTRAINT PK_Goods_Id PRIMARY KEY(Id),
)

CREATE TABLE OrderItems
(
	Id INT IDENTITY(1,1),
	OrderId int,
	GoodsId int,
	Price decimal(18,2),
	Quantity int,
	CONSTRAINT PK_OrderItems_Id PRIMARY KEY(Id),
	CONSTRAINT FK_OrderItems_Orders FOREIGN KEY(OrderId) REFERENCES Orders(Id) ON DELETE CASCADE,
	CONSTRAINT FK_OrderItems_Goods FOREIGN KEY(GoodsId) REFERENCES Goods(Id)
)


------------

--INT
--decimal(8,2)
--******.**
--real
--bit(1,0)
--char
--char(10)
--varchar(10)
--nchar(10)
--nvarchar(10)