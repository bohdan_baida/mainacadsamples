--DML (Create,Read,Update,Delete)
--CRUD
USE MainAcadOrderService

INSERT INTO Customers(FirstName, LastName, Age,Adress)
VALUES('Bohdan', 'Baida', 22, 'Kyiv, Konyeva,5')
INSERT INTO Customers(FirstName, LastName, Age,Adress)
VALUES('Alex', 'Ivanov', 18, 'Kyiv, Vasylkivska,5')
INSERT INTO Customers(FirstName, LastName, Age,Adress)
VALUES('Nazar', 'Palko', 22, 'Kyiv, Sechenova,5')
INSERT INTO Customers(FirstName, LastName, Age,Adress)
VALUES('Alex', 'Nazarov', 22, 'Kyiv, Sechenova,5')


SELECT * FROM Customers
SELECT FirstName,LastName FROM Customers WHERE Age = 23
--ORDER, GROUP, FILTER
SELECT *
FROM Customers
ORDER BY Adress,FirstName, Age

SELECT Adress,Age, COUNT(*) [PersonCount],AVG(Age) [AvarageAge]
FROM Customers
WHERE Age > 10
GROUP BY Adress,Age
having avg(Age)>=18
order by PersonCount


UPDATE Customers SET Age=13 WHERE ID= 5

DELETE FROM Customers 
WHERE ID=2