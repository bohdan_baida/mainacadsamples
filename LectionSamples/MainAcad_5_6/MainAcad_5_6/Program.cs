﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainAcad_5_6
{
    class Program
    {
        static void Main(string[] args)
        {
            var retriever = new QuestionsRetriever();

            //retriever.FillData();
            //retriever.DisplayQuestions();

            //retriever.AddNewQuestion();
            //retriever.RemoveQuestion(4);
            //retriever.RemoveQuestion(5);
            //retriever.RemoveQuestion(6);
            //retriever.UpdateQuestion(3, "bla bla bla?");

            //retriever.DisplayQuestions();

            //var questionId = retriever.AddQuestion("How is the weather?");
            //retriever.AddAnswer(questionId, "Fine", false);
            //retriever.AddAnswer(questionId, "Awful", false);
            //retriever.AddAnswer(questionId, "SO so", true);
            //retriever.Commit();


            TestSystemEntities context = new TestSystemEntities();
            foreach (var item in context.Questions)
            {
                Console.WriteLine($"{item.Id}.{item.QuestionText}");
                foreach (var ans in item.Answers)
                {
                    Console.WriteLine($"\t- {ans.AnswerText}");

                }
            }
            Console.Read();
        }
    }
}
