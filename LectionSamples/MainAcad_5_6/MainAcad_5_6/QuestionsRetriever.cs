﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace MainAcad_5_6
{
    public class QuestionsRetriever
    {
        private string connectionString;
        private DataSet dataSet;
        private SqlDataAdapter dataAdapter;
        private SqlTransaction transaction;
        SqlConnection sqlConnection;

        public QuestionsRetriever()
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["TesterConnection"].ConnectionString;
            dataSet = new DataSet();
            sqlConnection = sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();
            transaction = sqlConnection.BeginTransaction();

        }

        public void FillData()
        {
            var query = "select * from Questions";
            dataAdapter = new SqlDataAdapter(query, sqlConnection);
            dataAdapter.Fill(dataSet);
            dataSet.Tables[0].PrimaryKey = new DataColumn[] { dataSet.Tables[0].Columns["Id"] };

        }

        public void DisplayQuestions()
        {
            foreach (DataRow row in dataSet.Tables[0].Rows)
            {
                Console.WriteLine($"{row["Id"]}.{row["QuestionText"]}");
                //var answersQuery = $"select * from Answers where questionid = {row["Id"]}";
            }
        }

        public void AddNewQuestion()
        {
            var row = dataSet.Tables[0].NewRow();
            row["QuestionText"] = "How the weather today?";
            dataSet.Tables[0].Rows.Add(row);

            var commandBuilder = new SqlCommandBuilder(dataAdapter);
            dataAdapter.Update(dataSet);
            dataSet.Clear();
            dataAdapter.Fill(dataSet);
        }

        public void RemoveQuestion(int id)
        {
            var row = dataSet.Tables[0].Rows.Find(id);
            row.Delete();
            var commandBuilder = new SqlCommandBuilder(dataAdapter);
            dataAdapter.Update(dataSet);
            dataSet.Clear();
            dataAdapter.Fill(dataSet);
        }

        public void UpdateQuestion(int id, string newText)
        {
            var row = dataSet.Tables[0].Rows.Find(id);
            row["QuestionText"] = newText;

            var commandBuilder = new SqlCommandBuilder(dataAdapter);

            dataAdapter.Update(dataSet.Tables[0]);
            dataSet.Clear();
            dataAdapter.Fill(dataSet);
        }

        public int AddQuestion(string text)
        {
            try
            {
                var command = new SqlCommand(
                $"INSERT INTO Questions(QuestionText) output inserted.Id values('{text}')",
                sqlConnection,
                transaction
                );
                var id = (int)command.ExecuteScalar();
                return id;
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                throw;
            }
            
        }
        public void AddAnswer(int questionId, string text, bool isCorrect)
        {
            try
            {
                int correct = isCorrect ? 1 : 0;
                var command = new SqlCommand(
                $"insert into Answers(QuestionId, AnswerText, IsCorrect) values({questionId},'{text}',{correct})",
                sqlConnection,
                transaction
                );
                command.ExecuteNonQuery();
            }
            catch (Exception)
            {
                transaction.Rollback();
                throw;
            }
        }

        public void Commit()
        {
            transaction.Commit();
            sqlConnection.Close();
        }
    }
}
