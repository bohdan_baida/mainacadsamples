﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainAcad_1_6
{
    //static class
    public static class MathOperations
    {
        //const(static field)
        public static readonly double Pi;

        static MathOperations()
        {
            Pi = 3.14;
        }
        //static method
        public static double CircleLength(double radius)
        {
            return 2 * Pi * radius;
        }

        public static double CircleSquare(double radius)
        {
            return Pi * radius * radius;
        }

        public static double CircleVolume(double radius)
        {
            return (4 / 3) * Pi * Math.Pow(radius, 3);
        }


    }
}
