﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainAcad_1_6
{
    public class PussyAnimal
    {
        /*protected*/
        private Species species;

        public string Name { get; set; }

        public Species Species
        {
            get
            {
                return species;
            }
            //set
            //{
            //    if (value != Species.Dog)
            //    {
            //        species = value;
            //    }
            //}
        }

        public PussyAnimal(Species species, string name) : this(species)
        {
            Name = name;
        }

        public PussyAnimal(Species species)
        {
            this.species = species;
        }

        public virtual void Feed(string food)
        {
            Console.Write($"Your animal was feeded!\n");
        }

    }

    public enum Species
    {
        Dog,
        Cat,
        Rat,
        Hamster
    }
}
