﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainAcad_1_6
{
    public class Cat : PussyAnimal
    {
        Selection selection;

        public Selection Selection
        {
            get
            {
                return selection;
            }
        }

        public Cat(Selection selection) : base(Species.Cat)
        {
            this.selection = selection;
        }

        public void ScratchSofa()
        {
            Console.WriteLine("Sofa succesfully scratched");
        }

        public override  void Feed(string food)
        {
            Console.WriteLine($"Mew mew! Good {food}\n");
            base.Feed(food);
        }
    }

    public enum Selection
    {
        British,
        Egyptian,
        MainKun
    }
}
