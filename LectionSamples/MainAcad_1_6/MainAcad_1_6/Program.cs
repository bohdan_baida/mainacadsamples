﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainAcad_1_6
{
    class Program
    {
        static void Main(string[] args)
        {
            PussyAnimal pussyAnimal = new PussyAnimal(Species.Cat);
            //pussyAnimal.Species = Species.Dog;
            pussyAnimal.Feed("pizza");

            Cat cat = new Cat(Selection.MainKun);
            cat.Feed("valerianka");
            cat.ScratchSofa();

            Dog Bob = new Dog("Bob");
            Dog Bob1 = new Dog();
            //public - available anywhere
            //protected - available in the class and inherited classes
            //private - available only in the class where it was defined
            Bob.Feed("bone");

            //
            PussyAnimal[] animals = new PussyAnimal[4];
            animals[0] = cat;
            animals[1] = Bob;
            animals[2] = pussyAnimal;
            animals[3] = new Hamster();
            //Liskov substitution
            //virtual - override
            foreach (var animal in animals)
            {
                animal.Feed("pizza");
            }

            var point = new Point { X = 1, Y = 2 };
            var point1 = point;
            point1.X = 2;
            point = ChangePoint(point);
            SetNames(animals);

            //fields
            //constants
            var point2 = new Point();
            //point2.Pi_field - instance variable
            //Point.Pi - class variable(static variables)
            Console.WriteLine(MathOperations.CircleSquare(3));
            Console.WriteLine(MathOperations.CircleLength(3));

            Console.Read();
        }
        private static Point ChangePoint(Point p)
        {
            p.X = 3;
            return p;
        }

        //single responsibility
        private static void SetNames(PussyAnimal[] animals)
        {
            foreach (var animal in animals)
            {
                animal.Name = "Bobik";
            }
        }
    }
}
