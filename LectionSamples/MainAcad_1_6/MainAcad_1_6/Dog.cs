﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainAcad_1_6
{
    public class Dog : PussyAnimal
    {
        public Dog() : base(Species.Dog)
        {

        }
        public Dog(string name) : base(Species.Dog, name)
        {

        }

        public void Gav()
        {
            Console.WriteLine("Gav Gav!");
        }

        public void TakeLeg()
        {
            Console.WriteLine($"Good dog! Good {Name}");
        }
        public override void Feed(string food)
        {
            Console.WriteLine($"Gav gav! Good {food}\n");
            base.Feed(food);
        }
    }
}
