﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigInt
{
    public class BigInt
    {
        private int[] numbers;

        public int Lenght => numbers.Length;

        public int[] NumbersArray => numbers;

        public BigInt()
        {
            numbers = new int[0];
        }

        public int this[int i] => numbers[i];

        public BigInt(string number)
        {
            if (!number.Any(c => char.IsNumber(c)))
                throw new ArgumentException($"{number} is not a number");

            numbers = number.Select(c => int.Parse(c.ToString())).ToArray();
        }


        public static BigInt operator +(BigInt number1, BigInt number2)
        {
            int[] bigger, lower;
            if (number1.Lenght > number2.Lenght)
            {
                bigger = new int[number1.NumbersArray.Length];
                lower = new int[number2.NumbersArray.Length];
                number1.NumbersArray.CopyTo(bigger, 0);
                lower = number2.NumbersArray;
            }
            else
            {
                bigger = new int[number2.NumbersArray.Length];
                lower = new int[number1.NumbersArray.Length];
                bigger = number2.NumbersArray;
                lower = number1.NumbersArray;
            }

            Array.Reverse(bigger);
            Array.Reverse(lower);

            int reminder = 0;
            int[] result = new int[bigger.Length + 1];
            for (int i = 0; i < bigger.Length; i++)
            {
                if (i < lower.Length)
                {
                    var sum = bigger[i] + lower[i] + reminder;
                    reminder = 0;
                    if (sum < 9)
                    {
                        result[i] = sum;
                    }
                    else
                    {
                        //10,11,12,..,18
                        reminder++;
                        result[i] = sum % 10;
                    }
                }
                else
                {
                    result[i] = bigger[i] + reminder;
                    reminder = 0;
                }
            }
            result[result.Length - 1] = reminder;
            Array.Reverse(result);
            
            return new BigInt(CastToString(result));



        }

        public override string ToString()
        {
            return CastToString(numbers);
        }

        public static BigInt operator -(BigInt number1, BigInt number2)
        {
            throw new NotImplementedException();
        }
        public static BigInt operator *(BigInt number1, BigInt number2)
        {
            throw new NotImplementedException();
        }
        public static BigInt operator /(BigInt number1, BigInt number2)
        {
            throw new NotImplementedException();
        }

        private static string CastToString(int[] array)
        {
            var stringResult = "";
            for (int i = 0; i < array.Length; i++)
            {
                if (i == 0 && array[i] == 0)
                {
                    continue;
                }
                stringResult += array[i];
            }

            return stringResult;
        }

    }
}
