﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourierService
{
    public class Order
    {
        public DateTime DeliveryDate { get; set; }

        public string DeliveryAddress { get; set; }

        public decimal Weight { get; set; }
    }
}
