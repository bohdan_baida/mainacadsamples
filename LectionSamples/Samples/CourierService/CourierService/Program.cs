﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourierService
{
    class Program
    {
        static void Main(string[] args)
        {
            Office office = new Office();

            Warehouse warehouse = new Warehouse();
            Random random = new Random();

            for (int i = 0; i < 20; i++)
                warehouse.AddCourier(new Courier(random.Next(3, 6)));

            office.OnOrderAccept += warehouse.AddOrder;

            for (int i = 0; i < 30; i++)
                office.AcceptOrder(new Order()
                {
                    DeliveryAddress = "Kiev, Sechenova 6",
                    DeliveryDate = DateTime.Now,
                    Weight = random.Next(1, 4)
                });

            var couriers = warehouse.EquipCouriers();

            couriers.ForEach(c => Console.WriteLine(c.ToString()));

            Console.Read();
        }
    }
}
