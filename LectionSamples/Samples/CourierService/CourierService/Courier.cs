﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourierService
{
    public class Courier
    {
        private List<Order> orders;

        public decimal MaxWeight { get; set; }

        public decimal AvailableLimit => MaxWeight - orders.Sum(c => c.Weight);


        public Courier(decimal maxWeight)
        {
            MaxWeight = maxWeight;
            orders = new List<Order>();
        }

        public bool AddOrder(Order order)
        {
            if (AvailableLimit < order.Weight)
                return false;

            orders.Add(order);
            return true;
        }

        public override string ToString()
        {
            return $"Courier with {MaxWeight} max weight " +
                $"has {orders.Count} orders " +
                $"with {orders.Sum(c => c.Weight)} total";
        }
    }
}
