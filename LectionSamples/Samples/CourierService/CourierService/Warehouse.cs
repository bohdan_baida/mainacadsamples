﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourierService
{
    public class Warehouse
    {
        private List<Order> orders;
        private List<Courier> couriers;

        public Warehouse()
        {
            orders = new List<Order>();
            couriers = new List<Courier>();
        }

        public void AddCourier(Courier courier)
        {
            couriers.Add(courier);
        }

        public void AddOrder(Order order)
        {
            orders.Add(order);
        }

        public List<Courier> EquipCouriers()
        {
            List<Courier> result = new List<Courier>();

            var couriersQueue = new Queue<Courier>(couriers.OrderByDescending(c => c.MaxWeight));
            orders = orders.OrderByDescending(c => c.Weight).ToList();
            while (orders.Count != 0)
            {
                if (couriersQueue.Count == 0)
                    return result;

                var courier = couriersQueue.Dequeue();
                couriers.Remove(courier);

                while (courier.AvailableLimit > 0)
                {
                    var maxOrderToAdd = orders
                        .Where(c => c.Weight <= courier.AvailableLimit)
                        .FirstOrDefault();

                    if (maxOrderToAdd == default(Order))
                        break;

                    courier.AddOrder(maxOrderToAdd);
                    orders.Remove(maxOrderToAdd);
                }

                result.Add(courier);
            }

            return result;
        }
    }
}
