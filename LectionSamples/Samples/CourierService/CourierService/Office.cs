﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourierService
{
    public class Office
    {
        public event Action<Order> OnOrderAccept;

        public void AcceptOrder(Order order)
        {
            //some office logic
            OnOrderAccept(order);
        }
    }
}
