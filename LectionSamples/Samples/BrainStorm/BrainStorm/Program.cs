﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrainStorm
{
    class Program
    {
        static void Main(string[] args)
        {
            //ref, out
            int a;
            
            Point c = new Point()
            {
                X = 1,
                Y = 2
            };
            TryGetValue(out a);
            TryGetValue1(ref a);
            Bla(ref c);

            Console.WriteLine($"{c.X} {c.Y}");
            //reference by reference
            //IEnumerable<Point> coll = new List<Point>();
            //ICollection<Point> coll1 = new List<Point>();
            //IList<Point> coll2 = new List<Point>();
            Animal dog = new Dog();
            dog.Voice();
            //dog.LegsCount1
            //явна реалізація інетрфейсу

            A1 aa = new A();
            A2 aa1 = aa as A2;
            A2 aa2 = dog as A2;
            A2 aa3 = (A2)dog;

            aa.A();

            var a_str = "bla";
            var b_str = "BLA";
            Console.WriteLine(a_str == b_str);
            Console.WriteLine(a_str.ToLower() == b_str.ToLower());
            Console.WriteLine(string.Compare(a_str, b_str, StringComparison.OrdinalIgnoreCase));

            Console.Read();

            //Exception
            //SystemException ApplicationException
            //IDisposable
            //using (resource)
            //{

            //}

            int a1 = 1;
            long a2 = a1;
            int a3 = (int)a2;

            try
            {

            }
            catch (ApplicationException exApp)
            {

            }
            catch (Exception ex)
            {

                throw;
            }
            finally
            {

            }
        }
        static void Bla(ref Point a)
        {
            a = new Point()
            {
                X = 3,
                Y = 3
            };
        }

        static void TryGetValue(out int a)
        {
            a = 10;
        }
        static void TryGetValue1(ref int a)
        {
            a = 1;
        }
    }
    class Point
    {
        public int X { get; set; }
        public int Y { get; set; }
    }

    public abstract class Animal
    {
        public const int LegsCount = 4;

        public readonly int LegsCount1;

        public abstract void Voice();

        public Animal()
        {
            LegsCount1 = 1;

        }
        public void Feed()
        {
        }
    }

    public sealed class Dog : Animal
    {
        public override void Voice()
        {
            Console.WriteLine("Gav");
        }
    }

    interface A1
    {
        void A();
    }
    interface A2
    {
        void A();
    }

    class A : A1, A2
    {
        void A1.A()
        {

        }
        void A2.A()
        {

        }

    }


}
