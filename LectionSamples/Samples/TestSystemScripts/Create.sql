use master;
create database TestSystem
go
use TestSystem;
create table Questions(
Id int not null identity(1,1) primary key,
QuestionText varchar(1000) not null
)
create table Answers(
Id int not null identity(1,1) primary key,
AnswerText varchar(1000) not null,
QuestionId int not null,
IsCorrect bit not null,
foreign key(QuestionId) references Questions(Id)
)