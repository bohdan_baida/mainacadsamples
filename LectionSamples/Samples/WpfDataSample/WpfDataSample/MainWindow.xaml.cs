﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfDataSample.Class;

namespace WpfDataSample
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public CatsModel CatsModel { get; set; }
        //Зробити так, щоб кіт при редагуванні мінявся при натисканні кнопки Зберегти
        //Ввести Id для котів
        //При виборі кота зі списку копіювати його(new Cat(){....})
        //При збереженні переприсвоїти свойства оригінального кота свойствами копії
        public MainWindow()
        {
            InitializeComponent();

             var Cats = new List<Cat>();
            Cats.Add(new Cat()
            {
                Name = "Tom",
                Age = 2,
                Price = 1000,
                ImagePath = @"Images/cat.jpg",
                Species = new Species()
                {
                    Name = "British",
                    Description = "Super rare british cat. Ultra expensive"
                }
            });
            Cats.Add(new Cat()
            {
                Name = "Barsik",
                Age = 10,
                Price = 2000,
                ImagePath = @"Images/cat2.jpg",
                Species = new Species()
                {
                    Name = "Persian",
                    Description = "Super rare persian cat. Ultra expensive"
                }
            });
            Cats.Add(new Cat()
            {
                Name = "Puffy",
                Age = 2,
                Price = 500,
                ImagePath = @"Images/cat3.jpg",
                Species = new Species()
                {
                    Name = "Prosto kot",
                    Description = "Super rare prosto cat. Ultra expensive"
                }
            });

            DataContext = new CatsModel();
            CatsModel = new CatsModel()
            {
                Cats = new ObservableCollection<Cat>(Cats),
                CurrentCat = null

            };
        }

      
        
    }
    
    
        private void ListBox_SelectionChanged(object sender,SelectionChangedEventArgs e )
        {
        CatsModel.CurrentCat = (Cat)e.AddedItems[0];
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var newCat = new Cat()
            {
                Species = new Species()
            };
            CatsModel.Cats.Add(newCat);
            CatsModel.CurrentCat = newCat;
        }
    }
    public class CatsModel : INotifyPropertyChanged
    {
        private Cat currentCat;
        public ObservableCollection<Cat> Cats { get; set; }
        public Cat CurrentCat
        {
            get { return currentCat; }
            set
            {
                currentCat = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("currentCat"));
                }
            }
    }
        public event PropertyChangedEventHandler PropertyChanged;
    }

    public interface INotifyPropertyChanged
    {
    }
}
