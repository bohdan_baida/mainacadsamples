﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace WpfDataSample.Class
{
    public class Cat : INotifyPropertyChanged
    {
        private string name;
        public int age;

        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                OnPropertyChanged();

            }
        }

        public int Age
        {
            get { return age; }
            set
            {
                age = value;
                OnPropertyChanged();
            }
        }

        public string ImagePath { get; set; }

        public Species Species { get; set; }

        public decimal Price { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged([CallerMemberName]string propertyName = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }

    public class Species
    {
        public string Name { get; set; }

        public string Description { get; set; }
    }
}
