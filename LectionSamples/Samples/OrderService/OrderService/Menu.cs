﻿using OrderService.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderService
{
    class Menu
    {
        public static List<Category> categories;

        public static Category GetCategory(string name)
        {
            //categories.Where(c => c.ParentCategory == parCateg);
            return categories.FirstOrDefault(c => c.CategoryName == name);
        }
        public static Category GetCategory(int id)
        {
            //categories.Where(c => c.ParentCategory == parCateg);
            return categories.FirstOrDefault(c => c.Id == id);
        }
        public static void CategoryMenu()
        {
            int x = -1;
            int parentid = 0;
            bool exit = false;

            try
            {
                do
                {
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.Clear();
                    //Console.OutputEncoding = Encoding.UTF8;
                    //var list = categories.Select(c => (c.ParentCategory.Id == parentid) || (parentid == 0 && c.ParentCategory == null));
                    var list = (from c in categories
                               where (parentid == 0 && c.ParentCategory == null) || (c.ParentCategory?.Id == parentid)
                               select new { Name = c.CategoryName, Id = c.Id }).ToList();

                    for (int i = 0; i < list.Count; i++)
                        Console.WriteLine($"{i}. {list[i]}");

                    Console.WriteLine("0. Exit       <");
                    try
                    {
                        Console.Write("Enter menu: ");
                        x = (int)uint.Parse(Console.ReadLine());
                        Console.WriteLine();
                        if (x >= 1 && x <= list.Count())
                        {
                            var cat = GetCategory(list.ElementAt(x - 1).Id);
                            parentid = cat.Id;
                        }
                        else if (x == 0 && parentid != 0)
                        {
                            var cat = GetCategory(parentid);
                            if (cat.ParentCategory != null)
                            {
                                parentid = cat.ParentCategory.Id;
                            }
                            else
                            {
                                parentid = 0;
                            }
                        }
                        else if (x == 0 && parentid == 0)
                        {
                            exit = true;
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Error" + e.Message);
                    }
                    Console.ForegroundColor = ConsoleColor.DarkYellow;
                    //Console.WriteLine("Press Spacebar to exit; press any key to continue");
                    Console.ForegroundColor = ConsoleColor.White;
                }
                while (!exit);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}

