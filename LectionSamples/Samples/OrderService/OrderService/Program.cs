﻿using OrderService.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Visualizer;

namespace OrderService
{
    class Program
    {

        static void Main(string[] args)
        {
            //1. Вибір товарів(навігація по категоріях + пошук)
            //var good1 = new Good()
            //{
            //    Name = "Asus ZenBook 1",
            //    Price = 30000,
            //    Category = CategoriesHepler.GetCategory("Планшеты")
            //};
            //goods.Add(good1);
            Console.OutputEncoding = Encoding.UTF8;


            while (true)
            {
                var command = Console.ReadLine();
                switch (command)
                {
                    case "1":
                        var orderProcessor = new OrderProcessor();
                        orderProcessor.BeginProcessOrder();
                        break;
                    default:
                        break;
                }
            }
        }

        

        
    }
}
