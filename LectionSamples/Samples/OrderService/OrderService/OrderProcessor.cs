﻿using OrderService.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Visualizer;

namespace OrderService
{
    public class OrderProcessor
    {
        Order order;
        enum OrderProcessorMode
        {
            None = 1,
            SelectCategory = 2,
            SelectGood = 3
        }
        OrderProcessorMode orderProcessorMode = OrderProcessorMode.SelectCategory;

        public OrderProcessor()
        {
            order = new Order();
        }
        public void BeginProcessOrder()
        {
            while (true)
            {
                Console.Clear();
                ShowOrderDetails();
                Console.WriteLine("Please add goods to order:");
                BeginAddGood();

            }
        }
        private void BeginAddGood()
        {
            ShowRootCategories();
            string choise = string.Empty;
            while (choise != "q")
            {
                choise = Console.ReadLine();

                int choiseInt;
                if (!int.TryParse(choise, out choiseInt))
                    continue;

                switch (orderProcessorMode)
                {
                    case OrderProcessorMode.SelectCategory:
                        ShowNextCategories(choiseInt);
                        break;
                    case OrderProcessorMode.SelectGood:
                        var good = GoodsHelper.GetGood(choiseInt);
                        Console.WriteLine("Please input quantity:");
                        var quantity = Console.ReadLine();
                        order.AddGood(new OrderItem()
                        {
                            GoodId = good.Id,
                            GoodsName = good.Name,
                            Price = good.Price,
                            Quantity = int.Parse(quantity)
                        });
                        orderProcessorMode = OrderProcessorMode.SelectCategory;
                        return;
                    case OrderProcessorMode.None:
                    default:
                        return;
                }
            }
        }

        private void ShowRootCategories()
        {
            var rootCategories = CategoriesHepler.GetRootCategories();
            VisualizeTable<Category>(rootCategories);

        }
        private void ShowNextCategories(int categoryId)
        {
            var childCategories = CategoriesHepler.GetChildCategories(categoryId);
            if (childCategories.Count != 0)
            {
                VisualizeTable(childCategories);
            }
            else
            {
                orderProcessorMode = OrderProcessorMode.SelectGood;
                VisualizeTable(GoodsHelper.GetGoods(categoryId));
            }
        }
        private void VisualizeTable<T>(List<T> items)
        {
            IVisualizer visualizer = new Visualizer<T>(items);
            visualizer.Visualize();
        }

        private void ShowOrderDetails()
        {
            if (order.Empty)
            {
                Console.WriteLine("Your order contains no goods");
                return;
            }
            Console.WriteLine("Your order contains:");
            VisualizeTable(order.Goods);
        }
    }
}
