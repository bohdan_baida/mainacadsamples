﻿using OrderService.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderService
{
    public static class GoodsHelper
    {
        static List<Good> goods = new List<Good>();

        static GoodsHelper()
        {
            var good1 = new Good()
            {
                Name = "Asus ZenBook 1",
                Price = 30000,
                Category = CategoriesHepler.GetCategory("Asus")
            };
            goods.Add(good1);
        }
        public static List<Good> GetGoods()
        {
            return goods;
        }
        public static List<Good> GetGoods(int categoryId)
        {
            return goods.Where(c => c.Category.Id == categoryId).ToList();
        }

        public static Good GetGood(int id)
        {
            return goods.FirstOrDefault(c => c.Id == id);
        }
    }
}
