﻿using OrderService.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderService
{
    public static class CategoriesHepler
    {
        public static List<Category> categories;
        static CategoriesHepler()
        {
            categories = new List<Category>();
            var baseCategoryNotebook = new Category(1, "Ноутбуки и компютеры", null);
            var baseCategorySmartPhone = new Category(2, "Смартфони", null);
            categories.Add(baseCategoryNotebook);
            categories.Add(baseCategorySmartPhone);

            var categoryNotebook = new Category(3, "Ноутбуки", baseCategoryNotebook);
            var categoryTablet = new Category(4, "Планшеты", baseCategoryNotebook);
            var categoryEbooks = new Category(5, "Електронні книги", baseCategoryNotebook);

            var categoryAsus = new Category(6, "Asus", categoryNotebook);
            var categoryApple = new Category(7, "Apple", categoryNotebook);
            var categoryEbook = new Category(8, "Кольорові", categoryTablet);

            categories.Add(categoryNotebook);
            categories.Add(categoryTablet);
            categories.Add(categoryEbooks);
            categories.Add(categoryAsus);
            categories.Add(categoryApple);
            categories.Add(categoryEbook);
        }

        public static Category GetCategory(string name)
        {
            //categories.Where(c => c.ParentCategory == parCateg);
            return categories.FirstOrDefault(c => c.CategoryName == name);
        }

        public static List<Category> GetChildCategories(int categoryId)
        {
            return categories
                .Where(c => c.ParentCategory?.Id == categoryId)
                .ToList();
        }

        public static List<Category> GetRootCategories()
        {
            return categories
                .Where(c => c.ParentCategory == null)
                .ToList();
        }
    }
}
