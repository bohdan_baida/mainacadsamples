﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderService.Entities
{
    public class Order
    {
        List<OrderItem> goods;

        public Order()
        {
            goods = new List<OrderItem>();
        }

        public List<OrderItem> Goods => goods;

        public bool Empty => goods.Count == 0;

        public void AddGood(OrderItem good)
        {
            goods.Add(good);
        }
    }
}
