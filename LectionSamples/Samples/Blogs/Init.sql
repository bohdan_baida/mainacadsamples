use master 
create database Blogs
go
use Blogs
go

create table Users(
Id int not null primary key identity(1,1),
FirstName varchar(max),
LastName varchar(Max),
Username varchar(max) not null,
PasswordHash varchar(max) not null
)

create table Blogs(
Id int not null primary key identity(1,1),
BlogName varchar(max) not null,
BlogDescription varchar(max)
)

create table UserBlogs(
UserId int not null,
BlogId int not null
primary key(UserId,BlogId),
foreign key (UserId) references dbo.Users(Id),
foreign Key (BlogId) references dbo.Blogs(Id)
)

create table Posts(
Id int not null primary key identity(1,1),
BlogId int not null,
CreatedBy int not null,
PostHeader varchar(max) not null,
PostBody varchar(max) not null,
foreign key (CreatedBy) references dbo.Users(Id),
foreign Key (BlogId) references dbo.Blogs(Id)
)

create table Comments(
Id int not null primary key identity(1,1),
PostId int not null,
UserId int not null,
CommentText varchar(max) not null,
foreign key (UserId) references dbo.Users(Id),
foreign Key (PostId) references dbo.Posts(Id)

)