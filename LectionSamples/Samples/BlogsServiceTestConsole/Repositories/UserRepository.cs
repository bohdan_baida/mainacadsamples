﻿using DAL;
using Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositories
{
    public class UserRepository : BaseRepository , IUserRepository
    {

        public void AddUser(User user)
        {
            context.Users.Add(user);
            context.SaveChanges();
        }

        public void BlockUser(int userId)
        {
            var user = context.Users.Single(c => c.Id == userId);
            user.IsBlocked = true;
            context.SaveChanges();
        }
        
        public User GetUser(int userId)
        {
            return context.Users.Where(c => c.Id == userId).First();
        }

        public IEnumerable<User> GetUsers()
        {
            return context.Users.ToList();
        }

        public void UpdateUser(User user)
        {
            var dbUser = context.Users.Find(user.Id);
            dbUser.FirstName = user.FirstName;
            dbUser.LastName = user.LastName;
            dbUser.IsBlocked = user.IsBlocked;

            context.SaveChanges();
            //var dbEntity = context.Entry(user);
            //dbEntity.CurrentValues.SetValues(user);
        }
    }
}
