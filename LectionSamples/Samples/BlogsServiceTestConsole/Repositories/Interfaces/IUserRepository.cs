﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositories.Interfaces
{
    public interface IUserRepository
    {
        void AddUser(User user);
        void BlockUser(int userId);
        User GetUser(int userId);
        IEnumerable<User> GetUsers();
        void UpdateUser(User user);

    }
}
