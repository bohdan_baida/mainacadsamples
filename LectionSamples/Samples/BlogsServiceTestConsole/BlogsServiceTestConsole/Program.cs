﻿using BusinessLogic;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogsServiceTestConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            UserBO userBO = new UserBO();
            var user = new User();
            Console.WriteLine("Type FirstName:");
            user.FirstName = Console.ReadLine();

            Console.WriteLine("Type LastName:");
            user.LastName = Console.ReadLine();

            Console.WriteLine("Type Username:");
            user.Username = Console.ReadLine();

            Console.WriteLine("Type Password:");
            user.PasswordHash = Console.ReadLine();

            userBO.CreateUser(user);

            Console.Read();
        }
    }
    //1. Add changes to database(if needed)
    //2. Add changes to DAL(if needed)
    //3. Add changes to repository
    //4. Add changes to business logic
    //5. Add changes to UI

    //HW!
    //1. Move classes User,Post,Blog,Comment to separated library(dll)
    //2. Add references for newly created library to all layers of application
    //3. Visualize list of user in console
    //4. Add options to create new user and edit old user in console.
}
