﻿using BlogsClasses;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Input;

namespace UserManagementApp.Model
{
    public class SignInPageModel : INotifyPropertyChanged
    {
        private User currentUser;
        public User CurrentUser
        {
            get { return currentUser; }
            set
            {
                currentUser = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("currentUser"));
                }
            }
        }

        public SignInPageModel()
        {
            currentUser = new User();
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }

    public class LoginPageModel : INotifyPropertyChanged
    {
        private User currentUser;
        public User CurrentUser
        {
            get { return currentUser; }
            set
            {
                currentUser = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("currentUser"));
                }
            }
        }

        public LoginPageModel()
        {
            currentUser = new User();
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }

    public class Command : ICommand
    {
        private Action<object> execute;
        private Func<object, bool> canExecute;

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public Command(Action<object> execute, Func<object, bool> canExecute = null)
        {
            this.execute = execute;
            this.canExecute = canExecute;

            if (canExecute == null)
            {
                canExecute = obj => true;
            }
        }

        public bool CanExecute(object parameter)
        {
            return this.canExecute == null || this.canExecute(parameter);
        }

        public void Execute(object parameter)
        {
            this.execute(parameter);
        }
    }

}