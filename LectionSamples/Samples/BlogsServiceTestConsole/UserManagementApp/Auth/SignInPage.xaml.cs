﻿using BisinessLogic;
using BlogsClasses;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using UserManagementApp.Model;

namespace UserManagementApp.Auth
{
    /// <summary>
    /// Interaction logic for SignInPage.xaml
    /// </summary>
    public partial class SignInPage : Window
    {
        SignInPageModel formModel = new SignInPageModel();
        public SignInPage()
        {
            InitializeComponent();
            DataContext = formModel;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            CreateUser();
        }

        private void Password_PasswordChanged(object sender, RoutedEventArgs e)
        {
            formModel.CurrentUser.PasswordHash = ((PasswordBox)sender).Password;
        }
        private void CreateUser()
        {
            UserBO userBO = new UserBO();

            var userModel = formModel.CurrentUser;
            User newUser = new User()
            {
                FirstName = userModel.FirstName,
                LastName = userModel.LastName,
                Username = userModel.Username,
                PasswordHash = userBO.HashPassword(userModel.PasswordHash)
            };

            userBO.CreateUser(newUser);

            MessageBox.Show("User is created");
        }

 
    }
}
