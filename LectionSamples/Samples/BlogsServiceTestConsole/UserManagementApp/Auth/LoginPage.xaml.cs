﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using UserManagementApp.Model;
using BlogsClasses;
using BisinessLogic;

namespace UserManagementApp.Auth
{
    /// <summary>
    /// Interaction logic for LoginPage.xaml
    /// </summary>
    public partial class LoginPage : Window
    {
        LoginPageModel formModel = new LoginPageModel();
        public LoginPage()
        {
            InitializeComponent();
            DataContext = formModel;
        }

        private void Button_Login_Click(object sender, RoutedEventArgs e)
        {
            UserBO userBO = new UserBO();

            if (userBO.CheckUserPassword(
                formModel.CurrentUser.Username,
                formModel.CurrentUser.PasswordHash))
            {
                MessageBox.Show("Password is correct!");
            }
            else
            {
                MessageBox.Show("Password is not correct!");
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            SignInPage signInPage = new SignInPage();
            signInPage.Show();
            this.Close();
        }


    }
}
