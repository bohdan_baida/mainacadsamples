﻿using BusinessLogic;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebUIApp.Controllers
{
    public class HomeController : Controller
    {
        UserBO userBO;
        public HomeController()
        {
            userBO = new UserBO();
        }
        public ActionResult Index()
        {
            if (HttpContext.Request.Cookies.Count == 0)
            {
                HttpContext.Response.Cookies.Add(new HttpCookie("role", "admin"));
            }
            var users = userBO.GetUsers();
            return View(users);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var user = userBO.GetUser(id);
            return View(user);
        }

        [HttpPost]
        public ActionResult Edit(User user)
        {
            userBO.UpdateUser(user);
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(User user)
        {
            userBO.CreateUser(user);
            return RedirectToAction("Index");
        }

    }
}