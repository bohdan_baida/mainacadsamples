﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebUIApp.Helpers
{
    public static class TableHelper
    {
        public static MvcHtmlString RenderTable<T>(this HtmlHelper helper, IEnumerable<T> elements)
            where T : class
        {
            TagBuilder table = new TagBuilder("table");
            table.AddCssClass("table table-striped");

            var head = new TagBuilder("thead");
            var row = new TagBuilder("tr");
            var props = typeof(T).GetProperties()
                .Where(c=>c.PropertyType.IsPrimitive || c.PropertyType == typeof(string));
            foreach (var property in props)
            {
                var column = new TagBuilder("td");
                column.InnerHtml = property.Name;

                row.InnerHtml += column.ToString();
            }
            head.InnerHtml += row.ToString();

            var body = new TagBuilder("tbody");
            foreach (var element in elements)
            {
                var tblRow = new TagBuilder("tr");
                foreach (var property in props)
                {
                    var tblCol = new TagBuilder("td");
                    tblCol.InnerHtml = property.GetValue(element).ToString();
                    tblRow.InnerHtml += tblCol.ToString();
                }
                body.InnerHtml += tblRow.ToString();
            }
            table.InnerHtml = head.ToString();
            table.InnerHtml += body.ToString();

            return new MvcHtmlString(table.ToString());
            
        }
    }
}