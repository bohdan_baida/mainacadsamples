﻿CREATE FUNCTION [dbo].[GetUserBlogs]
(
	@userId int
)
RETURNS @returntable TABLE
(
	BlogName varchar(max),
	PostsCount int
)
AS
BEGIN
	INSERT @returntable
	SELECT b.BlogName [BlogName],
		count(p.Id) [PostsCount]
	FROM UserBlogs ub
		join Blogs b on b.Id = ub.BlogId
		left join Posts p on p.BlogId = b.Id
	where ub.userid = @userid
	group by b.BlogName

	RETURN
END
