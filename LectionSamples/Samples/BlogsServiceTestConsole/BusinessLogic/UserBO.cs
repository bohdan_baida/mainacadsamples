﻿using BusinessLogic.Services;
using DAL;
using Repositories;
using Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public class UserBO
    {
        IUserRepository userRepository;
        ISmsService smsService;
        public UserBO()
        {
            userRepository = new UserRepository();
            smsService = new SmsService();
        }

        public IEnumerable<User> GetUsers()
        {
            return userRepository.GetUsers();
        }

        public User GetUser(int id)
        {
            return userRepository.GetUser(id);
        }

        public void UpdateUser(User user)
        {
            userRepository.UpdateUser(user);
        }

        //Dependency injection
        //Constructor injection
        public UserBO(IUserRepository userRepository, ISmsService smsService)
        {
            this.userRepository = userRepository;
            this.smsService = smsService;
        }

        public void CreateUser(User user)
        {
            userRepository.AddUser(user);
        }

        //Moq
        public void BlockUser(int userid)
        {
            var user = userRepository.GetUser(userid);
            if (user.Posts.Any())
            {
                userRepository.BlockUser(userid);
                smsService.Notify(userid, "Your user account is blocked");
            }
            else
            {
                throw new ApplicationException("Can`t block user with posts");
            }
        }

    }
}
