﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Main_Acad_Attributes_UnitTests.Attributes
{
   public static class ReflectionHelper
    {
        public static MemberInfo GetMember(Type type, string member)
        {
            return type.GetMember(member).First();
        }
       
    }
}
