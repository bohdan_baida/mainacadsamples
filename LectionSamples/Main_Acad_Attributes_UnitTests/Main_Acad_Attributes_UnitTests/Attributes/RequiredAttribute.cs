﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Main_Acad_Attributes_UnitTests.Attributes
{
    [AttributeUsage( AttributeTargets.Property | AttributeTargets.Field)]
    public class RequiredAttribute : Attribute
    {
        public bool Validate(string value)
        {
            return string.IsNullOrWhiteSpace(value);
        }
    }
}
