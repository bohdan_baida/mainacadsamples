﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
namespace Main_Acad_Attributes_UnitTests.Attributes
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public class RangeValidationAttribute : Attribute
    {
        public int MinValue { get; set; }
        public int MaxValue { get; set; }
        public RangeValidationAttribute(int minValue, int maxValue)
        {
            MinValue = minValue;
            MaxValue = maxValue;
        }

        public bool Validate(int value)
        {
            return value <= MaxValue && value >= MinValue;
        }
    }
}
