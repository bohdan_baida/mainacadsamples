﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Main_Acad_Attributes_UnitTests.MockExample
{
    public class UserAccountService
    {
        public void BlockUserAccount(int userId)
        {
            Console.WriteLine($"User {userId} is blocked");
        }
    }
}
