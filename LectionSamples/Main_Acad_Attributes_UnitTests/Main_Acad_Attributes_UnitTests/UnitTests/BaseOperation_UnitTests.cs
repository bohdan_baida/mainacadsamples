﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using FluentAssertions;

namespace Main_Acad_Attributes_UnitTests.UnitTests
{
    public class BaseOperation_UnitTests
    {
        BaseOperations target;

        public BaseOperation_UnitTests()
        {
            target = new BaseOperations();
        }

        [Fact]
        public void Divide_When_SecondArgIsZero_Then_ShouldThrowException()
        {
            // Arrange
            const decimal a = 100500;
            const decimal b = 0;

            // Act
            Action action = () => target.Divide(a, b);

            // Assert
            action.Should()
                .Throw<DivideByZeroException>();
        }

        [Fact]
        public void Divide_When_SecondArgIsNotZero_Then_ShouldReturnResult()
        {
            // Arrange
            const decimal a = 10;
            const decimal b = 5;
            const decimal expected = 2;

            // Act
            var result = target.Divide(a, b);

            // Assert
            result.Should().BeOfType(typeof(decimal));
            result.Should().Be(expected);
        }

        [Theory]
        [InlineData(Developers.AstonMartin, Counties.UK)]
        [InlineData(Developers.Mini, Counties.UK)]
        [InlineData(Developers.Linkoln, Counties.USA)]
        [InlineData(Developers.Ford, Counties.USA)]
        [InlineData(Developers.Audi, Counties.Germany)]
        [InlineData(Developers.BMW, Counties.Germany)]
        public void GetCountry_When_DeveloperCorrect_Then_ShouldReturnCountry(
            Developers developer,
            Counties country
            )
        {
            // Arrange

            // Act
            var result = target.GetCountry(developer);

            //Assert
            result.Should().Be(country);
        }

        [Fact]
        public void ValidateStudent_When_AgeLessThen5_Then_ShuldReturnFalse()
        {
            // Arrange
            const bool expected = false;
            var student = new Student()
            {
                Age = 4
            };

            // Act
            var result = target.ValidateStudent(student);

            // Assert
            result.Should().Be(expected);
        }
    }
}
