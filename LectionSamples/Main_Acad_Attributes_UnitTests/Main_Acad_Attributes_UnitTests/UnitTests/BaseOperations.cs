﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Main_Acad_Attributes_UnitTests.UnitTests
{
    public class BaseOperations
    {
        public decimal Divide(decimal a, decimal b)
        {
            return a / b;
        }

        public Counties GetCountry(Developers dev)
        {
            switch (dev)
            {
                case Developers.Audi:
                case Developers.BMW:
                    return Counties.Germany;

                case Developers.Ford:
                case Developers.Linkoln:
                    return Counties.USA;

                case Developers.AstonMartin:
                case Developers.Mini:
                    return Counties.UK;

                default:
                    throw new ArgumentException($"Value {dev} is not defined");
            }
        }

        public bool ValidateStudent(Student student)
        {
            if (student.Age < 5)
            {
                return false;
            }

            if (student.Form > 11 || student.Form < 1)
            {
                return false;
            }

            if (string.IsNullOrWhiteSpace(student.FirstName))
            {
                return false;
            }

            if (string.IsNullOrWhiteSpace(student.LastName))
            {
                return false;
            }

            return true;
        }
    }
}
