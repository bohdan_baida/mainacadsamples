﻿using Main_Acad_Attributes_UnitTests.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Main_Acad_Attributes_UnitTests
{
    public class Student
    {
        private int form { get; set; }
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        public int Age { get; set; }

        [RangeValidation(1, 11)]
        public int Form
        {
            get
            {
                return form;
            }
            set
            {
                var member = ReflectionHelper.GetMember(this.GetType(), "Form");
                var attribute = (RangeValidationAttribute)Attribute.GetCustomAttribute(member, typeof(RangeValidationAttribute));
                if (attribute.Validate(value))
                {
                    form = value;
                }
                else
                {
                    throw new ArgumentException($"value should be between {attribute.MinValue} and {attribute.MaxValue}");
                }
            }
        }
    }
}
