﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Structural.Wrapper
{
    public interface IPrinter
    {
        void Print(string document);
    }
    public interface IScanner
    {
        string Scan();
    }

    public class Printer : IPrinter
    {
        public void Print(string document)
        {
            Console.WriteLine(document);
        }
    }
    public class Scanner : IScanner
    {
        public string Scan()
        {
            return "scanned document";
        }
    }

    public class PrinterScanner : IPrinter, IScanner
    {
        public IScanner Scanner { get; private set; }
        public IPrinter Printer { get; private set; }
        public PrinterScanner(IScanner scanner, IPrinter printer)
        {
            Scanner = scanner;
            Printer = printer;
        }

        public string Scan()
        {
            return Scanner.Scan();
        }

        public void Print(string document)
        {
            Printer.Print(document);
        }
    }
}
