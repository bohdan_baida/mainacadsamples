﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Structural.Facade
{
    public class AviaCompany
    {
        public void BookTicket()
        {
            Console.WriteLine("Ticket was booked");
        }
    }
}
