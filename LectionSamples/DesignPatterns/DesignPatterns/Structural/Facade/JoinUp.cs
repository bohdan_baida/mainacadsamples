﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Structural.Facade
{
    public class JoinUp
    {
        AviaCompany aviaCompany;
        ExcursionAgency excursionAgency;
        HotelService hotelService;

        public JoinUp(AviaCompany aviaCompany, ExcursionAgency excursionAgency, HotelService hotelService)
        {
            this.hotelService = hotelService;
            this.excursionAgency = excursionAgency;
            this.aviaCompany = aviaCompany;
        }

        public void BuyAIPackage()
        {
            hotelService.BookRoom();
            aviaCompany.BookTicket();
        }

        public void BuyExcursionPackage()
        {
            hotelService.BookRoom();
            aviaCompany.BookTicket();
            excursionAgency.BuyExcursionPackage();
        }
        public void BuyWildPackage()
        {
            aviaCompany.BookTicket();
        }
    }
}
