﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Structural.Facade
{
    public class ExcursionAgency
    {
        public void BuyExcursionPackage()
        {
            Console.WriteLine("Excursion package was buught");
        }
    }
}
