﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Structural.Adapter
{
    public class BabushkaTV : IThinSocket
    {
        public void On()
        {
            Console.WriteLine("Thin socket tv on");
        }
    }

    public class VnuchokTV : IWideSocket
    {
        public void On()
        {
            Console.WriteLine("Wide socket tv on");
        }
    }

    public interface IWideSocket
    {
        void On();
    }
    public interface IThinSocket
    {
        void On();
    }

    public class WideToThinAdapter : IWideSocket
    {
        IThinSocket thinSocket;
        public WideToThinAdapter(IThinSocket thinSocket)
        {
            this.thinSocket = thinSocket;
        }

        public void On()
        {
            thinSocket.On();
        }
    }

    public class Socket
    {
        public void Put(IWideSocket wide)
        {
            wide.On();
        }
    }
}
