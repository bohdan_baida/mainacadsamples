﻿using DesignPatterns.Behavioral.ChainOfResponsibility;
using DesignPatterns.Behavioral.Command;
using DesignPatterns.Creational.AbstractFabric;
using DesignPatterns.Creational.Builder;
using DesignPatterns.Creational.FabricMethod;
using DesignPatterns.Creational.Prototype;
using DesignPatterns.Creational.Singleton;
using DesignPatterns.SOLID;
using DesignPatterns.Structural.Adapter;
using DesignPatterns.Structural.Wrapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DesignPatterns
{
    class Program
    {
        static void Main(string[] args)
        {
            // Singleton
            //FileReader.Instance.Read();

            // Prototype
            //Task task = new Task();
            //var task1 = task.Clone();

            //Builder
            //var pc = new GamingPCBuilder().Build();
            //var pc1 = new BusinessPCBuilder().Build();

            // AbstractFactory IOC
            //var factory = new TeddyFactory();
            //var bear = factory.GetBear();
            //var bunny = factory.GetBunny();
            //Console.WriteLine(bear);
            //Console.WriteLine(bunny);
            //Console.Read();

            // Fabric method
            //var logger = LoggerFactory.GetLogger(Loggers.DB);
            //logger.LogMethodExecution("Main");

            // Wrapper
            //PrinterScanner printerScanner = new PrinterScanner(new Scanner(), new Printer());
            //printerScanner.Scan();
            //printerScanner.Print("safdas");

            //printerScanner.Printer.Print("dsadas");
            //printerScanner.Scanner.Scan();

            // Adapter
            //Socket socket = new Socket();

            //VnuchokTV wideSocketTV = new VnuchokTV();
            //socket.Put(wideSocketTV);

            //WideToThinAdapter thinSocketTV = new WideToThinAdapter(new BabushkaTV());
            //socket.Put(thinSocketTV);

            // Command
            //var consoleLogger = LoggerFactory.GetLogger(Loggers.Console);
            //var dbLogger = LoggerFactory.GetLogger(Loggers.DB);
            //var tcpLogger = LoggerFactory.GetLogger(Loggers.TCP);


            //consoleLogger.LogMethodExecution("sadas");
            //dbLogger.LogMethodExecution("sadas");
            //tcpLogger.LogMethodExecution("sadas");

            //LoggingService loggingService = new LoggingService();
            //loggingService.AddLoger(consoleLogger);
            //loggingService.AddLoger(dbLogger);
            //loggingService.AddLoger(tcpLogger);

            //ILogger logger = (ILogger)loggingService;
            //logger.LogMethodExecution("sdcsad");

            // Chain of responsibility
            //var firstPerson =
            //    new Visitor("Masha", Dishes.Soup,
            //        new Visitor("Kate", Dishes.Steak,
            //            new Visitor("Ivan", Dishes.Vodka,
            //                new Visitor("Kolya", Dishes.Salad))));

            //firstPerson.GetOrder(Dishes.Vodka);
            //firstPerson.GetOrder(Dishes.Salad);
            //firstPerson.GetOrder(Dishes.Soup);
            //firstPerson.GetOrder(Dishes.Steak);


            //SOLID
            //Single Responsibility
            //Open-Closed
            //Liskov Substitution
            //Interface Segregation
            //Dependency Inversion
            Trianle trianle = new Trianle();
            Console.WriteLine(trianle.GetShapeName());
            Console.Read();
        }
    }
}
