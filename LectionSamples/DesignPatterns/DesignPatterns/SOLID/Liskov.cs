﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.SOLID.Liskov
{

    class Square
    {
        public int A { get; set; }

        public int GetArea()
        {
            return A * A;
        }
    }

    class Rectangle : Square
    {
        public int B { get; set; }

        public int GetArea()
        {
            return A * B;
        }
    }
}
