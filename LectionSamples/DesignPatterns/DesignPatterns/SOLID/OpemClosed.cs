﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.SOLID
{
    //public enum ShapeType
    //{
    //    Rectangle,
    //    Circle,
    //    Triangle,
    //    Square,
    //    Romb
    //}

    public abstract class Shape
    {
        //public abstract ShapeType ShapeType { get; }

        public abstract string GetShapeName();
    }
    public class Trianle : Shape
    {
        //public override ShapeType ShapeType => ShapeType.Triangle;

        public override string GetShapeName()
        {
            return "trian";
        }
    }
    public class Circle : Shape
    {
        //public override ShapeType ShapeType => ShapeType.Circle;
        public override string GetShapeName()
        {
            return "circ";
        }
    }
    public class Rectanle : Shape
    {
        //public override ShapeType ShapeType => ShapeType.Rectangle;

        public override string GetShapeName()
        {
            return "rect";
        }
    }
    public class Square : Shape
    {
        //public override ShapeType ShapeType => ShapeType.Square;

        public override string GetShapeName()
        {
            return "square";
        }
    }

    public class Romb : Shape
    {
        //public override ShapeType ShapeType => throw new NotImplementedException();

        public override string GetShapeName()
        {
            return "romb";
        }
    }
}
