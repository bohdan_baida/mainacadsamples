﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Creational.AbstractFabric
{
    public abstract class AbstractFabric
    {
        public abstract Bear GetBear();

        public abstract Bunny GetBunny();
    }

    public class WoodenFactory : AbstractFabric
    {
        public override Bear GetBear()
        {
            return new WoodenBear();
        }

        public override Bunny GetBunny()
        {
            return new WoodenBunny();
        }
    }
    public class TeddyFactory : AbstractFabric
    {
        public override Bear GetBear()
        {
            return new TeddyBear();
        }

        public override Bunny GetBunny()
        {
            return new TeddyBunny();
        }
    }

    public abstract class Bear
    {
        public abstract ToyType ToyType { get; }

        public override string ToString()
        {
            return $"I am {ToyType} bear";
        }
    }

    public class TeddyBear : Bear
    {
        public override ToyType ToyType => ToyType.Teddy;

        
    }


    public class WoodenBear : Bear
    {
        public override ToyType ToyType => ToyType.Wooden;

    }

    public abstract class Bunny
    {
        public abstract ToyType ToyType { get; }

        public override string ToString()
        {
            return $"I am {ToyType} bunny";
        }
    }

    public class TeddyBunny : Bunny
    {
        public override ToyType ToyType => ToyType.Teddy;


    }


    public class WoodenBunny : Bunny
    {
        public override ToyType ToyType => ToyType.Wooden;

    }


    public enum ToyType
    {
        Teddy,
        Wooden
    }
}
