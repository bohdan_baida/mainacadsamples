﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Creational.Prototype
{
    public class Task
    {
        public int Id { get; set; }

        public string Header { get; set; }

        public string Description { get; set; }

        public User User { get; set; }

        public Task Clone()
        {
            //Shallow copy
            var clone  = (Task)this.MemberwiseClone();

            clone.Id++;

            return clone;
        }


        //public Task DeepCopy()
        //{
        //    return new Task()
        //    {
        //        Id = this.Id + 1,
        //        Header = this.Header,
        //        Description = this.Description,
        //        User = new User()
        //        {
        //            Name = this.User.Name
        //        }
        //    };
        //}
    }

    public class User
    {
        public string Name { get; set; }
    }
}
