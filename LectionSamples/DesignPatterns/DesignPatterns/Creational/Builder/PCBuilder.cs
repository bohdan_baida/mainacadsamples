﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Creational.Builder
{
    public abstract class PCBuilder
    {
        public PC Build()
        {
            PC pC = new PC();
            AddRAM(pC);
            AddMemory(pC);
            AddProcessor(pC);

            return pC;
        }

        protected abstract void AddRAM(PC pC);
        protected abstract void AddProcessor(PC pC);
        protected abstract void AddMemory(PC pC);
    }

    public class GamingPCBuilder : PCBuilder
    {
        protected override void AddMemory(PC pC)
        {
            pC.SpaceAmount = 1000;
            pC.DriveType = DriveType.SSD;
        }

        protected override void AddProcessor(PC pC)
        {
            pC.ProcessorGeneration = "Intel Core i9";
        }

        protected override void AddRAM(PC pC)
        {
            pC.RAMSize = 12;
        }
    }

    public class BusinessPCBuilder : PCBuilder
    {
        protected override void AddMemory(PC pC)
        {
            pC.SpaceAmount = 500;
            pC.DriveType = DriveType.HDD;
        }

        protected override void AddProcessor(PC pC)
        {
            pC.ProcessorGeneration = "Intel Core i5";
        }

        protected override void AddRAM(PC pC)
        {
            pC.RAMSize = 8;
        }
    }
}
