﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Creational.Builder
{
    public class PC
    {
        public string ProcessorGeneration { get; set; }

        public int RAMSize { get; set; }

        public int SpaceAmount { get; set; }

        public DriveType DriveType { get; set; }
    }

    public enum DriveType
    {
        HDD,
        SSD
    }
}
