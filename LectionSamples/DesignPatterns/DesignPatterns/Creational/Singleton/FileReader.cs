﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DesignPatterns.Creational.Singleton
{
    //Thread safe Singleton
    public class FileReader
    {
        MemoryStream stream;
        static object _lock = new object();

        private static FileReader fileReader;

        //Mock
        public static FileReader Instance
        {
            get
            {
                lock (_lock)
                {
                    if (fileReader == null)
                    {
                        fileReader = new FileReader();
                    }
                    return fileReader;
                }
               
            }
        }

        private FileReader()
        {
            Initialize();
        }

        private void Initialize()
        {
            Thread.Sleep(3000);
        }

        public string Read()
        {
            return "";
        }
    }
}
