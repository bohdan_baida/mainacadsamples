﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Creational.FabricMethod
{
    // Log4Net, Serilog
    public class TCPLogger : ILogger
    {
        public void LogException(Exception exception)
        {
            Console.WriteLine($"[TCP] {exception.Message}");
        }

        public void LogMethodExecution(string methodInfo)
        {
            Console.WriteLine($"[TCP] {methodInfo}");
        }
    }
}
