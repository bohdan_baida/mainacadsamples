﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Creational.FabricMethod
{
    public class DBLogger : ILogger
    {
        public void LogException(Exception exception)
        {
            Console.WriteLine($"[DB] {exception.Message}");
        }

        public void LogMethodExecution(string methodInfo)
        {
            Console.WriteLine($"[DB] {methodInfo}");
        }
    }
}
