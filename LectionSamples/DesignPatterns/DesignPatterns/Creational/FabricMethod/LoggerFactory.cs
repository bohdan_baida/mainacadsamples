﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Creational.FabricMethod
{
    public static class LoggerFactory
    {
        public static ILogger GetLogger(Loggers loggerType)
        {
            switch (loggerType)
            {
                case Loggers.TCP:
                    return new TCPLogger();
                case Loggers.DB:
                    return new DBLogger();
                case Loggers.Console:
                    return new ConsoleLogger();
                default:
                    throw new ArgumentException("Logger type is invalid");
            }
        }
    }
}
