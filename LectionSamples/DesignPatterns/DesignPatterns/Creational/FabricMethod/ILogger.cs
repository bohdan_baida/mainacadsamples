﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Creational.FabricMethod
{
    public interface ILogger
    {
        void LogException(Exception exception);

        void LogMethodExecution(string methodInfo);
    }
}
