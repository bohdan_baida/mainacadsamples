﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Behavioral.ChainOfResponsibility
{
    public enum Dishes
    {
        Steak,
        Vodka,
        Salad,
        Soup
    }

    public class Visitor
    {
        public Dishes dish;

        public string Name { get; }

        Visitor neighbour;

        public Visitor(string name, Dishes dish, Visitor neighbour = null)
        {
            Name = name;
            this.dish = dish;
            this.neighbour = neighbour;
        }

        public void GetOrder(Dishes dishDelivered)
        {
            if (this.dish == dishDelivered)
            {
                Console.WriteLine($"I {Name} got {dish}");
            }
            else
            {
                neighbour?.GetOrder(dishDelivered);
            }

        }
    }
}
