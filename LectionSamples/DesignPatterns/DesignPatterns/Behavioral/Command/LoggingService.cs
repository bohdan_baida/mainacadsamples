﻿using DesignPatterns.Creational.FabricMethod;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns.Behavioral.Command
{
    public class LoggingService : ILogger
    {
        List<ILogger> loggers;
        public LoggingService()
        {
            loggers = new List<ILogger>();
        }

        public void AddLoger(ILogger logger)
        {
            loggers.Add(logger);
        }

        public void LogException(Exception exception)
        {
            foreach (var logger in loggers)
            {
                logger.LogException(exception);
            }
        }

        public void LogMethodExecution(string method)
        {
            foreach (var logger in loggers)
            {
                logger.LogMethodExecution(method);
            }
        }
    }
}
