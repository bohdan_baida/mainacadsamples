use master 
create database Test
go
use Test
create table JobPositions(
	Id int primary key,
	Name varchar(max)
)

create table Employees(
	Id int primary key identity(1,1),
	FirstName varchar(max),
	LastName varchar(max),
	HireDate date,
	FireDate date,
	JobPositionId int,
	BaseSalary decimal(18,2),
	constraint Employees_JobPositionId_JobPositions_Id foreign key(JobPositionId) references JobPositions(Id)
)

create table Calandar(
	Id int primary key identity(1,1),
	[Date] date,
	IsWorking bit

)

create table EmployeeWorkDays(
	Id int primary key identity(1,1),
	CalandarId int,
	EmployeeId int,
	constraint EmployeeWorkDays_EmployeeId_Employees_Id foreign key(EmployeeId) references Employees(Id),
	constraint EmployeeWorkDays_CalandarId_Calandar_Id foreign key (CalandarId) references Calandar(Id)
)



create table EmployeeSalaries(
	Id int primary key identity(1,1),
	EmployeeId int,
	[Year] int,
	[Month] int,
	Amount decimal(18,2),
	constraint EmployeeSalaries_EmployeeId_Employees_Id foreign key(EmployeeId) references Employees(Id)
)
