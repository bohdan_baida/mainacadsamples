﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterviewExample.OOP
{
    public interface ISpy
    {
        void Kill();
    }
    public interface ISoldier
    {
        void Kill();
    }

    public class JamesBond //: ISpy,ISoldier 
    {

    }
}
