﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterviewExample.OOP
{
    public class TestString
    {
        public string Pluralize(string basicString, int count)
        {
            var result = string.Empty;
            for (int i = 0; i < count; i++)
            {
                result += basicString;
            }
            return result;
        }

        public int GetWordsCount(string text)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<string> GetUniqueWords(string text)
        {
            throw new NotImplementedException();
        }
    }

    public static class StringExtensions
    {
        public static bool Contains(this string basic, params string[] items)
        {
            throw new NotImplementedException();
        }
    }
}
