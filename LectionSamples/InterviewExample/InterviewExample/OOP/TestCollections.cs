﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterviewExample.OOP
{
    public class TestCollections
    {

        public IEnumerable FillCollectionAndPrint()
        {
            var collection = new ArrayList();

            for (int i = 0; i < 1000; i++)
            {
                for (int j = 0; j < 1000; j++)
                {
                    collection.Add(new Point()
                    {
                        X = i,
                        Y = j
                    });
                }
            }

            foreach (var item in collection)
            {
                var point = (Point)item;
                Console.WriteLine($"({point.X},{point.Y})");
            }

            return collection;
        }

        public IReadOnlyCollection<CustomerReportItem> GetCustomerOrdersReport(
            IEnumerable<Order> orders,
            DateTime from,
            DateTime to)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Order> GetOldestOrders(IEnumerable<Order> orders)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Customer> GetCustomersWithMaxOrders(IEnumerable<Order> orders)
        {
            throw new NotImplementedException();
        }
    }

    public struct Point
    {
        public int X { get; set; }
        public int Y { get; set; }

    }

    public class Order
    {
        public int Id { get; set; }

        public int CustomerId { get; set; }

        public DateTime OrderDate { get; set; }

        public decimal TotalPrice { get; set; }
    }

    public class Customer
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }

    public class CustomerReportItem
    {
        public string CustomerName { get; set; }

        public int OrdersCount { get; set; }

        public decimal TotalAmount { get; set; }
    }


}
