﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterviewExample.OOP
{
    public class TestDBContext
    {
        public IOrderedQueryable<int> QueryableTest(IQueryable<Order> DbContext)
        {
            var items = DbContext.Where(c => c.OrderDate > DateTime.Now);
            items = items.Where(c => c.TotalPrice > 100500);

            var group = items.GroupBy(c => c.CustomerId)
                .Select(c => c.Key)
                .OrderBy(c => c);

            return group;
        }
    }
}
