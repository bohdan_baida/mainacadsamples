﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterviewExample.OOP
{
    public class TestVirt
    {
        public TestVirt()
        {
            Do();
        }

        public virtual void Do()
        {
            Console.WriteLine("Base Do()");
        }
    }

    public class TestVirt1 : TestVirt
    {
        public override void Do()
        {
            Console.WriteLine("Derived 1 Do()");
        }
    }

    public class TestVirt2 : TestVirt
    {
        public virtual new void Do()
        {
            Console.WriteLine("Derived 2 Do()");
        }
    }

    public class TestVirt3 : TestVirt2
    {
        public override void Do()
        {
            Console.WriteLine("Derived 3 Do()");
        }
    }
}
