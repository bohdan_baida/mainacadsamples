﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test
{
    class Program
    {
        static int i = 0;
        //static object _lock = new object();

        static void Main(string[] args)
        {
            for (int i = 0; i < 3; i++)
            {
                Task.Run(() => Counter());
            }
            Console.Read();
        }

        static void Counter()
        {

            //lock (_lock)
            //{
                i = 1;
                while (i != 11)
                {
                    Console.WriteLine(i);
                    i++;
                }
            //}
        }
    }
}
