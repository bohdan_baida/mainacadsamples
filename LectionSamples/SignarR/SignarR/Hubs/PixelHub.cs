﻿using DAL;
using Microsoft.AspNet.SignalR;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace SignarR.Hubs
{
    public class PixelHub : Hub
    {
        PixelContext context;

        public PixelHub()
        {
            context = new PixelContext();
        }
        public override Task OnConnected()
        {
            var pixels = context.Pixels.ToList();
            var pixelsJson = JsonConvert.SerializeObject(pixels);

            this.Clients.Caller.drawAllPointsWithinAllTimeForUser(pixelsJson);

            return base.OnConnected();
        }

        public void AddPixelServer(int x, int y, string color)
        {
            context.Pixels.Add(new Pixel()
            {
                X = x,
                Y = y,
                DateTime = DateTime.Now,
                Color = color
            });

            context.SaveChanges();

            this.Clients.All.addPixel(x, y, color);
        }

        public void ProcessMessage(string userName, string messageText)
        {
            this.Clients.All.renderMessage(userName, messageText);

        }

    }
}