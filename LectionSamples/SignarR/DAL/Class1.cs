﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class Pixel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int X { get; set; }

        public int Y { get; set; }

        public string Color { get; set; }

        public DateTime DateTime { get; set; }
    }

    public class PixelContext : DbContext
    {
        public PixelContext()
            : base("PixelConnection")
        {
        }

        public DbSet<Pixel> Pixels { get; set; }
    }
}
