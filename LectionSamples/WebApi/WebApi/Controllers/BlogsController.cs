﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using WebApi.Entities;

namespace WebApi.Controllers
{
    public class BlogsController : ApiController
    {
        BlogsContext blogsContext = new BlogsContext();

        public BlogsController()
        {

        }

        public async Task<IEnumerable<Blog>> GetBlogs()
        {
            return await blogsContext.Blogs.ToListAsync();
        }

        public async Task<Blog> GetBlog(int id)
        {
            //return Task.Run(() => blogsContext.Blogs.FindAsync(id));
            return await blogsContext.Blogs.FindAsync(id);
        }

        [HttpPut]
        public async Task AddBlog(Blog blog)
        {
            blogsContext.Blogs.Add(blog);

            await blogsContext.SaveChangesAsync();
        }

        [HttpPost]
        public async Task Update(Blog blog)
        {
            var blogFromDb = await blogsContext.Blogs.FindAsync(blog.Id);

            blogFromDb.BlogDescription = blog.BlogDescription;
            blogFromDb.BlogName = blog.BlogName;

            await blogsContext.SaveChangesAsync();
        }
        [HttpDelete]
        public async Task DeleteBlog([FromUri]int blogId)
        {
            var blogdel = await blogsContext.Blogs.FindAsync(blogId);
            blogsContext.Blogs.Remove(blogdel);
            await blogsContext.SaveChangesAsync();
        }

        // AJAX(asynchronus java script and xml)
           
        // $.post("http://localhost:52098/api/blogs",{
        //     "Id":1,
        // },(result) => {
        // console.log(result);
        // });
           
        // $.post("http://localhost:52098/api/blogs",{
        //     "Id":1,
        // "BlogName":"dsadsa",
        // "BlogDescription":"dsfsdfsdfdsfsd"
        // },(result) => {
        // console.log(result);
        // });

        // Swagger - automatic api documentation generator
    }
}
