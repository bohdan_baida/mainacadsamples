namespace WebApi.Entities
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class BlogsContext : DbContext
    {
        public BlogsContext()
            : base("name=BlogsContext")
        {
        }

        public virtual DbSet<C__RefactorLog> C__RefactorLog { get; set; }
        public virtual DbSet<Blog> Blogs { get; set; }
        public virtual DbSet<Comment> Comments { get; set; }
        public virtual DbSet<Post> Posts { get; set; }
        public virtual DbSet<UserBlog> UserBlogs { get; set; }
        public virtual DbSet<User> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Blog>()
                .Property(e => e.BlogName)
                .IsUnicode(false);

            modelBuilder.Entity<Blog>()
                .Property(e => e.BlogDescription)
                .IsUnicode(false);

            modelBuilder.Entity<Blog>()
                .HasMany(e => e.Posts)
                .WithRequired(e => e.Blog)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Blog>()
                .HasMany(e => e.UserBlogs)
                .WithRequired(e => e.Blog)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Comment>()
                .Property(e => e.CommentText)
                .IsUnicode(false);

            modelBuilder.Entity<Comment>()
                .HasMany(e => e.Comments1)
                .WithOptional(e => e.Comment1)
                .HasForeignKey(e => e.ParentCommentId);

            modelBuilder.Entity<Post>()
                .Property(e => e.PostHeader)
                .IsUnicode(false);

            modelBuilder.Entity<Post>()
                .Property(e => e.PostBody)
                .IsUnicode(false);

            modelBuilder.Entity<Post>()
                .HasMany(e => e.Comments)
                .WithRequired(e => e.Post)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .Property(e => e.FirstName)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.LastName)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.Username)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.PasswordHash)
                .IsUnicode(false);
        }
    }
}
