use Uber

select * from SegmentLengths
--avg, sum, count, max, min
--rank, row_number, dense_rank
select end_junction_id, sum(len) 
from SegmentLengths
group by end_junction_id

--WINDOWED FUNCTIONS
--select ROW_NUMBER() over ( partition by [start_junction_id] order by len),*
--from SegmentLengths
--select *, sum(len) over ( partition by [start_junction_id] order by len),*
--from SegmentLengths

create table Employee(
Id int identity(1,1) primary key,
FName varchar(max),
SName varchar(max),
Salary decimal(18,2)
)

insert into Employee
select 'B','B',10000
insert into Employee
select 'A','A',10000
insert into Employee
select 'C','C',7000

select * 
from Employee
where Salary = (select max(salary) from Employee)