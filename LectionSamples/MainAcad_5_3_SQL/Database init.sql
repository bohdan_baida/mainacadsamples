--use master
--create database Contraband
--go
use Contraband
create table Shops(
ShopId int primary key identity(1,1),
ShopName varchar(300)
)

create table [Ports](
PortId int primary key identity(1,1),
PortName varchar(300)
)

create table Suppliers(
SupplierId int primary key identity(1,1),
SupplierName varchar(300)
)
create table Supplier_Port_Shop(
SupplierId int,
PortId int,
ShopId int,
primary key(SupplierId,PortId,ShopId),
foreign key (SupplierId) references Suppliers(SupplierId),
foreign key (PortId) references [Ports](PortId),
foreign key (ShopId) references Shops(ShopId)
)