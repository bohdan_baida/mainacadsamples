﻿select s.ShopId,
	s.ShopName,
	COUNT(distinct sps.SupplierId)
from Shops s
	join Supplier_Port_Shop sps on sps.ShopId = s.ShopId
group by s.ShopId,s.ShopName

--1НФ - всі атрибути є атомарними
-- 1 | 2 | 1,2,3,5 |
-- 2НФ - у вашому відношенні немає залежностей від частини ключа
-- 3НФ - у вашому відношенні немає залежностей між неключовими атрибутами
-- НФБК - ліва частина кожної залежності є ключем
