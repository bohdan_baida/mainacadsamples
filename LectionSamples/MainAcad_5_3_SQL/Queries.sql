select * from Shops
select * from Ports
select * from Suppliers
select * from Supplier_Port_Shop

select distinct ShopName
from Shops inner join Supplier_Port_Shop on Shops.ShopID = Supplier_Port_Shop.ShopId
--where Supplier_Port_Shop.PortId = 1 or Supplier_Port_Shop.PortId = 2
where Supplier_Port_Shop.PortId in (1,2)

select *
from Shops
where exists(
	select * from Supplier_Port_Shop 
	where Supplier_Port_Shop.PortId=1 and Supplier_Port_Shop.ShopId = Shops.ShopId
)

select *
from Shops
where not exists(
	select * from Supplier_Port_Shop 
	where Supplier_Port_Shop.SupplierId=3 and Supplier_Port_Shop.ShopId = Shops.ShopId
)

