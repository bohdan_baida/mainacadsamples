﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainAcad_1_5
{
    public class ColaResult
    {
        public int BottlesCount { get; }
        public decimal MoneyBack { get; }

        public ColaResult(int bottlesCount, decimal moneyBack)
        {
            BottlesCount = bottlesCount;
            MoneyBack = moneyBack;
        }
    }
}
