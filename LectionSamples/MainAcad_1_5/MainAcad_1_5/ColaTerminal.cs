﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainAcad_1_5.Terminals
{
    public class ColaTerminal //internal by default
    {
        private int bottlesCount;
        private decimal bottlePrice;

        private string password = "qwerty";

        public int BottlesCount
        {
            get
            {
                return bottlesCount;
            }
        }
        public decimal BottlePrice
        {
            get
            {
                return bottlePrice;
            }
        }
        //public int BottlesCount => bottlesCount;

        public ColaTerminal(int bottlesCount, decimal bottlePrice)
        {
            this.bottlePrice = bottlePrice;
            this.bottlesCount = bottlesCount;
        }

        public ColaResult Buy(decimal amount, int countToBuy)
        {
            var availableBottles = Math.Min(bottlesCount, countToBuy);
            if (availableBottles == 0)
            {
                return new ColaResult(0, amount);
            }
            var totalAmount = availableBottles * bottlePrice;
            if (totalAmount < amount)
            {
                bottlesCount -= countToBuy;
                return new ColaResult(countToBuy, amount - totalAmount);
            }

            var availableToBuy = Math.Truncate(amount / bottlePrice);
            bottlesCount -= availableBottles;
            return new ColaResult((int)availableToBuy, amount - (availableToBuy * bottlePrice));
        }

        public void ChangePrice(decimal newPrice, string password)
        {
            if (CheckPassword(password))
            {
                this.bottlePrice = newPrice;
            }
        }

        public void AddBotles(int bottlesToAdd, string password)
        {
            if (CheckPassword(password))
            {
                this.bottlesCount += bottlesToAdd; ;
            }
        }

        private bool CheckPassword(string password)
        {
            return this.password == password;
        }

    }
}

