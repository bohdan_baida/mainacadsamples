﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using MainAcad_1_5.Terminals;

namespace MainAcad_1_5
{
    class Program
    {
        static void Main(string[] args)
        {
            //string str = "Hello!";
            //char[] arr = new char[] { 'H', 'e', 'l', 'l', 'o', '!' };
            //string str1 = new string(arr);
            //String str2 = "Hello!";
            //str2 = "Hello1!";
            ////str2[1] = 'q'; //not allowed

            //str = "qwert\twewqewq\nxzczx"; // just string
            //Console.WriteLine(str);
            //str = @"qwert\twewqewq\nxzczx";//verbatium string
            //Console.WriteLine(str);
            //str = $"asdsad {str1} dsadas"; //interpolated string
            //Console.WriteLine(str);
            //str2 += "!!";
            //Console.WriteLine(str2 + str2); //concatenation(додавання строк, або склеювання)

            //foreach (var symbol in str2)
            //{
            //    Console.WriteLine(symbol);

            //}
            //for (int i = 0; i < str2.Length; i++)
            //{
            //    Console.WriteLine(str2[i]);

            //}

            //var phone = new Phone()
            //{
            //    Manufacturer = "Apple",
            //    ModelName = "Iphone 5S"
            //};
            //Console.WriteLine(phone);

            //var input = Console.ReadLine();
            //int a = (int)input; //don`t do it!!!
            //int a = Convert.ToInt32(input);
            //int b = int.Parse(input);
            //int c;
            //if (int.TryParse(input, out c))
            //{
            //    Console.WriteLine(c);
            //}
            //else
            //{
            //    Console.WriteLine("Number not valid");

            //}
            //int.TryParse(input, out c);
            //var a = (char)Console.Read();
            //var b = Console.ReadLine();
            //var c = Console.ReadKey();
            //if (c.Key == ConsoleKey.Tab)
            //{
            //    Console.WriteLine("Tab pressed");
            //}

            //while (true)
            //{
            //    var random = new Random();
            //    var frequency = random.Next(10, 5000);
            //    var duration = random.Next(100, 400);
            //    var interval = random.Next(100, 400);
            //    Thread.Sleep(interval);
            //    Console.Beep(frequency, duration);
            //}
            //Console.BackgroundColor = ConsoleColor.Green;
            //Console.ForegroundColor = ConsoleColor.Red;
            //Console.WriteLine("Bla bla bla");
            //Console.WindowWidth = 15;
            //Console.WindowHeight = 15;
            //Console.BufferWidth = 20;
            //Console.BufferHeight = 20;
            //for (int i = 0; i < 10; i++)
            //{
            //    for (int j = 0; j < 10; j++)
            //    {
            //        Console.Write("1");
            //    }
            //    //Console.WriteLine();
            //}
            ColaTerminal colaTerminal = new ColaTerminal(10, 5);
            Console.WriteLine($"{colaTerminal.BottlesCount} is available");
            Console.WriteLine($"The price is:{colaTerminal.BottlePrice}");

            Console.WriteLine("Enter bottles count");
            var bottlesCount = int.Parse(Console.ReadLine());

            Console.WriteLine("Enter money");
            var money = decimal.Parse(Console.ReadLine());

            var result = colaTerminal.Buy(money, bottlesCount);
            Console.WriteLine($"You bought {result.BottlesCount} bottles and got money back {result.MoneyBack}");

            colaTerminal.AddBotles(10, "qwerty");
            colaTerminal.ChangePrice(7m, "qwerty");
            var result1 = colaTerminal.Buy(money, bottlesCount);
            Console.WriteLine($"You bought {result1.BottlesCount} bottles and got money back {result1.MoneyBack}");

            Console.Read();
        }

    }

    struct Phone
    {
        public string ModelName { get; set; }
        public string Manufacturer { get; set; }

        public override string ToString()
        {
            return $"{Manufacturer} {ModelName}";
        }
    }
}
