﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types.ReplyMarkups;

namespace BarBot
{
    class Program
    {
        static TelegramBotClient botClient = new TelegramBotClient("1028355854:AAEUJydk9WQTdHzkOQZ9NbCj-nZpiipk9hg");
        static BarRepository repository = new BarRepository();
        static Random random = new Random();
        static void Main(string[] args)
        {
            botClient.OnMessage += BotClient_OnMessage;
            botClient.OnCallbackQuery += BotClient_OnCallbackQuery;
            botClient.StartReceiving();

            Console.Read();
        }

        private static async void BotClient_OnCallbackQuery(object sender, Telegram.Bot.Args.CallbackQueryEventArgs e)
        {
            var user = repository.GetUser(e.CallbackQuery.Data);
            repository.ProcessOrder(user);

            await botClient.SendTextMessageAsync(
                      new Telegram.Bot.Types.ChatId(user.ChatId.Value), $"Ваше замовлення завершено!");
        }

        private static Drink GetRandomDrink(IEnumerable<Drink> drinks)
        {

            return drinks.OrderBy(c => random.Next()).First();
        }

        private static async void BotClient_OnMessage(object sender, Telegram.Bot.Args.MessageEventArgs e)
        {
            try
            {


                var drinks = repository.GetDrinks();
                var usualDrinks = drinks.Where(c => !c.DrinkName.Contains("Special") && !c.DrinkName.Contains("Random"));

                var user = repository.GetUser(e.Message.From.Username);
                if (user == null)
                {
                    user = repository.AddUser(e.Message.From.Username, e.Message.Chat.Id);
                }

                if (e.Message.Text == "/start")
                {
                    foreach (var drink in usualDrinks)
                    {
                        var result = await botClient.SendPhotoAsync(
                            new Telegram.Bot.Types.ChatId(e.Message.Chat.Id),
                            new Telegram.Bot.Types.InputFiles.InputOnlineFile(new Uri(drink.ImagePath)),
                            caption: $"{drink.DrinkName}");
                    }

                    List<KeyboardButton>[] btns = new List<KeyboardButton>[10]{
                    new List<KeyboardButton>(),
                    new List<KeyboardButton>(),
                    new List<KeyboardButton>(),
                    new List<KeyboardButton>(),
                    new List<KeyboardButton>(),
                    new List<KeyboardButton>(),
                    new List<KeyboardButton>(),
                    new List<KeyboardButton>(),
                    new List<KeyboardButton>(),
                    new List<KeyboardButton>()

                };

                    int i = 0;
                    int ii = 0;
                    foreach (var drink in drinks.OrderBy(c=>c.Id!=10).ThenBy(c=>c.Id==11))
                    {
                        if (ii == 4)
                        {
                            i++;
                            ii = 1;
                        }
                        btns[i].Add(new KeyboardButton()
                        {
                            Text = drink.DrinkName
                        });
                        ii++;
                    }
                    var keyboard = new ReplyKeyboardMarkup(btns.Select(c => c.ToArray()).ToArray());
                    await botClient.SendTextMessageAsync(
                         new Telegram.Bot.Types.ChatId(e.Message.Chat.Id), "Оберіть напій",
                         replyMarkup: keyboard);

                    //var keyboard = new InlineKeyboardMarkup()
                }

                if (usualDrinks.Select(c => c.DrinkName).Contains(e.Message.Text))
                {
                    var drinkToAdd = usualDrinks.First(c => c.DrinkName.Contains(e.Message.Text));
                    if (user.UserDrinks.Any(c => c.IsAccepted == false))
                    {
                        await botClient.SendTextMessageAsync(
                            new Telegram.Bot.Types.ChatId(e.Message.Chat.Id), $"Ваше попереднє замовлення ще не завершено");
                    }
                    else
                    {
                        repository.AddUserDrink(user, drinkToAdd);
                        var admin = repository.GetAdmin();
                        await botClient.SendTextMessageAsync(
                            new Telegram.Bot.Types.ChatId(e.Message.Chat.Id), $"Замовлення прийнято");

                        var inline = new InlineKeyboardMarkup(
                            new InlineKeyboardButton()
                            {
                                CallbackData = $"{user.UserName}",
                                Text = $"Process"

                            });
                        await botClient.SendTextMessageAsync(
                            new Telegram.Bot.Types.ChatId(admin.ChatId.Value),
                            $"New order {e.Message.Text} from {e.Message.From.FirstName + " " + e.Message.From.LastName}",
                            replyMarkup: inline);
                    }
                }

                if (e.Message.Text == "Random")
                {
                    var drinkToAdd = GetRandomDrink(usualDrinks);
                    if (user.UserDrinks.Any(c => c.IsAccepted == false))
                    {
                        await botClient.SendTextMessageAsync(
                            new Telegram.Bot.Types.ChatId(e.Message.Chat.Id), $"Ваше попереднє замовлення ще не завершено");
                    }
                    else
                    {
                        repository.AddUserDrink(user, drinkToAdd);
                        var admin = repository.GetAdmin();
                        await botClient.SendTextMessageAsync(
                            new Telegram.Bot.Types.ChatId(e.Message.Chat.Id), $"Замовлення прийнято");

                        var inline = new InlineKeyboardMarkup(
                            new InlineKeyboardButton()
                            {
                                CallbackData = $"{user.UserName}",
                                Text = $"Process"

                            });
                        await botClient.SendTextMessageAsync(
                            new Telegram.Bot.Types.ChatId(admin.ChatId.Value),
                            $"New random order {drinkToAdd.DrinkName} from {e.Message.From.FirstName + " " + e.Message.From.LastName}",
                            replyMarkup: inline);
                    }
                }
                if (e.Message.Text == "Special offer")
                {
                    var drinkToAdd = drinks.First(c => c.DrinkName.Contains(e.Message.Text));

                    if (user.UserDrinks.Any(c => c.IsAccepted == false))
                    {
                        await botClient.SendTextMessageAsync(
                            new Telegram.Bot.Types.ChatId(e.Message.Chat.Id), $"Ваше попереднє замовлення ще не завершено");
                        return;
                    }
                    if (repository.CheckSpecialOffer())
                    {
                        await botClient.SendTextMessageAsync(
                             new Telegram.Bot.Types.ChatId(e.Message.Chat.Id), $"Пощастить наступного разу)");
                        return;

                    }
                    else
                    {
                        repository.AddUserDrink(user, drinkToAdd);
                        var admin = repository.GetAdmin();
                        await botClient.SendTextMessageAsync(
                            new Telegram.Bot.Types.ChatId(e.Message.Chat.Id), $"Special offer в студію! Пора в клубас:)");

                        var inline = new InlineKeyboardMarkup(
                            new InlineKeyboardButton()
                            {
                                CallbackData = $"{user.UserName}",
                                Text = $"Process"

                            });
                        await botClient.SendTextMessageAsync(
                            new Telegram.Bot.Types.ChatId(admin.ChatId.Value),
                            $"New {drinkToAdd.DrinkName} from {e.Message.From.FirstName + " " + e.Message.From.LastName}",
                            replyMarkup: inline);
                    }
                }

                if (e.Message.Text == "/statistics")
                {
                    var userDrinks = repository.GetUserDrinksStats(user);
                    var result = "";
                    foreach (var userDrink in userDrinks)
                    {
                        result += userDrink.Key + " : " + userDrink.Value + "шт.\n";
                    }
                    await botClient.SendTextMessageAsync(
                          new Telegram.Bot.Types.ChatId(e.Message.Chat.Id), $"{result}");

                }
            }
            catch (Exception)
            {

            }
        }
    }
}
