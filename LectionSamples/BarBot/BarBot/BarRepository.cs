﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BarBot
{
    public class BarRepository
    {
        BarModel barModel = new BarModel();
        Random random = new Random();
        public User GetUser(string userName)
        {
            return barModel.Users.FirstOrDefault(c => c.UserName == userName);
        }

        public User GetAdmin()
        {
            return barModel.Users.FirstOrDefault(c => c.IsAdmin == true);
        }


        public User AddUser(string userName, long chatId)
        {
            barModel.Users.Add(new User()
            {
                TelegramId = "1488",
                UserName = userName,
                PersonalTask = "",
                PersonalTaskDone = true,
                ChatId = chatId,
                IsAdmin = false
            });
            barModel.SaveChanges();

            return GetUser(userName);
        }

        public List<Drink> GetDrinks()
        {
            return barModel.Drinks.ToList();
        }

        public void ProcessOrder(User user)
        {
            foreach (var item in user.UserDrinks)
            {
                item.IsAccepted = true;
            }
            barModel.SaveChanges();
        }
        public void AddUserDrink(User user, Drink drink)
        {
            user.UserDrinks.Add(new UserDrink()
            {
                UserId = user.Id,
                DrinkId = drink.Id,
                IsAccepted = false
            });
            barModel.SaveChanges();
        }

        public bool CheckSpecialOffer()
        {
            return barModel.UserDrinks.Where(c => c.DrinkId == 11).Count() >= 2;
        }

        public Dictionary<string, int> GetUserDrinksStats(User user)
        {
            return user.UserDrinks
                .GroupBy(c => c.Drink.DrinkName)
                .Select(c => new
                {
                    Key = c.Key,
                    Count = c.Count()
                })
                .ToDictionary(c => c.Key, c => c.Count);
                
        }


    }
}
