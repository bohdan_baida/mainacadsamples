namespace BarBot
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class BarModel : DbContext
    {
        public BarModel()
            : base("name=BarModel")
        {
        }

        public virtual DbSet<Drink> Drinks { get; set; }
        public virtual DbSet<UserDrink> UserDrinks { get; set; }
        public virtual DbSet<User> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Drink>()
                .Property(e => e.DrinkName)
                .IsUnicode(false);

            modelBuilder.Entity<Drink>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<Drink>()
                .Property(e => e.ImagePath)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.TelegramId)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.UserName)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.PersonalTask)
                .IsUnicode(false);
        }
    }
}
