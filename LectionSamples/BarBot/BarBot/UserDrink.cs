namespace BarBot
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class UserDrink
    {
        public int Id { get; set; }

        public int? UserId { get; set; }

        public int? DrinkId { get; set; }

        public bool? IsAccepted { get; set; }

        public virtual Drink Drink { get; set; }

        public virtual User User { get; set; }
    }
}
