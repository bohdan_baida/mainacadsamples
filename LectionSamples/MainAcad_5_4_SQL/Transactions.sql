--select * from Orders
--select * from OrderItems
--exec spRemoveNonValidOrders '2019-10-02'

alter PROCEDURE spRemoveNonValidOrders
	@Date date
AS
BEGIN
	--delete OrderItems 
	--where OrderId in(
	--	select o.Id
	--	from Orders o
	--	where CONVERT(date, o.OrderDate) = @Date
	--		AND o.DeliveryAdress is null or o.DeliveryAdress= ''
	--)
	set transaction isolation level read uncommitted
	
	begin try
		begin transaction

		delete oi
		from OrderItems oi
			join Orders o on o.Id = oi.OrderId
		where CONVERT(date, o.OrderDate) = @Date
				AND o.DeliveryAdress is null or o.DeliveryAdress= ''
		
		delete o
		from Orders o
		where CONVERT(date, o.OrderDate) = @Date
			AND (o.DeliveryAdress is null or o.DeliveryAdress= '')  and o.Id/0=1
			commit;

	end try
	begin catch 
	if @@TRANCOUNT>0
	begin
		rollback tran;
	end
	end catch
END
GO
