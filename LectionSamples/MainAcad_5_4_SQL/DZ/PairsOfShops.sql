use contraband

--select * from Suppliers
--select * from Ports
--select * from Shops
--select * from Supplier_Port_Shop

select distinct s1.ShopName [S1], s2.ShopName [S2]
from Shops s1, Shops s2
where not exists (
	select * 
	from Supplier_Port_Shop sps1 
	where s1.ShopId = sps1.ShopId
		and not exists (
			select * 
			from Supplier_Port_Shop sps2 
			where sps2.ShopId = s2.ShopId
				and sps2.SupplierId = sps1.SupplierId
		)	
)
and not exists (
	select * 
	from Supplier_Port_Shop sps2 
	where s2.ShopId = sps2.ShopId
		and not exists (
			select * 
			from Supplier_Port_Shop sps1
			where sps1.ShopId = s1.ShopId
				and sps1.SupplierId = sps2.SupplierId
		)
)
and s1.ShopId != s2.ShopId

