CREATE TRIGGER Trg_Update_Log_Orders
   ON  dbo.Orders
   AFTER Update
AS 
BEGIN
insert into history.Orders(PKId, CustomerId,DeliveryAdress,OrderDate,ActionType)
select inserted.Id, 
	inserted.CustomerId,
	inserted.DeliveryAdress
	,inserted.OrderDate,
	'UPDATE' from inserted
END
GO