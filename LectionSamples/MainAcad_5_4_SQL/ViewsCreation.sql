CREATE VIEW [dbo].[vShopsSuppliers1]
AS
SELECT        dbo.Shops.ShopId, dbo.Shops.ShopName, dbo.Suppliers.SupplierId, dbo.Suppliers.SupplierName
FROM            dbo.Shops INNER JOIN
                         dbo.Supplier_Port_Shop ON dbo.Shops.ShopId = dbo.Supplier_Port_Shop.ShopId INNER JOIN
                         dbo.Suppliers ON dbo.Supplier_Port_Shop.SupplierId = dbo.Suppliers.SupplierId
GO
