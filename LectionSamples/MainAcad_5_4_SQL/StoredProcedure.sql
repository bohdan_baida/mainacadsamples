--CREATE 
ALTER PROCEDURE spOrdersForCustomer
	@CustomerId int
AS
BEGIN
	SELECT 
		o.Id, 
		c.FirstName,
		c.LastName,
		o.OrderDate,
		o.DeliveryAdress,
		SUM(distinct oi.GoodsId) [UniqueItemsCount],
		SUM(oi.Price) [TotalSum]
	from Orders o
		join Customers c on c.Id = o.CustomerId
		join OrderItems oi on oi.OrderId = o.Id
	where o.CustomerId = @CustomerId
	group by c.FirstName,c.LastName,o.OrderDate,o.DeliveryAdress,o.Id
END