USE [master]
GO
/****** Object:  Database [MainAcadOrderService]    Script Date: 10/9/2019 9:31:42 PM ******/
CREATE DATABASE [MainAcadOrderService]
GO
ALTER DATABASE [MainAcadOrderService] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [MainAcadOrderService] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [MainAcadOrderService] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [MainAcadOrderService] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [MainAcadOrderService] SET ARITHABORT OFF 
GO
ALTER DATABASE [MainAcadOrderService] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [MainAcadOrderService] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [MainAcadOrderService] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [MainAcadOrderService] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [MainAcadOrderService] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [MainAcadOrderService] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [MainAcadOrderService] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [MainAcadOrderService] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [MainAcadOrderService] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [MainAcadOrderService] SET  ENABLE_BROKER 
GO
ALTER DATABASE [MainAcadOrderService] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [MainAcadOrderService] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [MainAcadOrderService] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [MainAcadOrderService] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [MainAcadOrderService] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [MainAcadOrderService] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [MainAcadOrderService] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [MainAcadOrderService] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [MainAcadOrderService] SET  MULTI_USER 
GO
ALTER DATABASE [MainAcadOrderService] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [MainAcadOrderService] SET DB_CHAINING OFF 
GO
ALTER DATABASE [MainAcadOrderService] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [MainAcadOrderService] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [MainAcadOrderService] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [MainAcadOrderService] SET QUERY_STORE = OFF
GO
USE [MainAcadOrderService]
GO
/****** Object:  Schema [history]    Script Date: 10/9/2019 9:31:42 PM ******/
CREATE SCHEMA [history]
GO
/****** Object:  UserDefinedFunction [dbo].[fGetOrderAmount]    Script Date: 10/9/2019 9:31:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fGetOrderAmount]
(
	@OrderId int
)
RETURNS decimal
AS
BEGIN
	declare @OrderAmount decimal(18,2) = 0

	select @OrderAmount = sum(Quantity*Price)
	from OrderItems
	where OrderItems.OrderId = @OrderId

	RETURN @orderAmount
END
GO
/****** Object:  Table [dbo].[Customers]    Script Date: 10/9/2019 9:31:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customers](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [varchar](100) NOT NULL,
	[LastName] [varchar](150) NOT NULL,
	[Age] [int] NULL,
	[Adress] [varchar](max) NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_Customers_Id] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Goods]    Script Date: 10/9/2019 9:31:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Goods](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[GoodsName] [varchar](500) NULL,
	[Price] [decimal](18, 2) NULL,
	[Quantity] [int] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_Goods_Id] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderItems]    Script Date: 10/9/2019 9:31:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderItems](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[OrderId] [int] NULL,
	[GoodsId] [int] NULL,
	[Price] [decimal](18, 2) NULL,
	[Quantity] [int] NULL,
 CONSTRAINT [PK_OrderItems_Id] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Orders]    Script Date: 10/9/2019 9:31:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Orders](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NULL,
	[OrderDate] [datetime] NULL,
	[DeliveryAdress] [varchar](max) NULL,
 CONSTRAINT [PK_Orders_Id] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [history].[Orders]    Script Date: 10/9/2019 9:31:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [history].[Orders](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PKId] [int] NOT NULL,
	[CustomerId] [int] NULL,
	[OrderDate] [datetime] NULL,
	[DeliveryAdress] [varchar](max) NULL,
	[ActionType] [nchar](20) NULL,
	[ActionDate] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Customers] ON 

INSERT [dbo].[Customers] ([Id], [FirstName], [LastName], [Age], [Adress], [IsActive]) VALUES (1, N'Bohdan', N'Baida', 22, N'Kyiv, Konyeva,5', 1)
INSERT [dbo].[Customers] ([Id], [FirstName], [LastName], [Age], [Adress], [IsActive]) VALUES (3, N'Nazar', N'Palko', 23, N'Kyiv, Sechenova,5', 1)
INSERT [dbo].[Customers] ([Id], [FirstName], [LastName], [Age], [Adress], [IsActive]) VALUES (4, N'Alex', N'Ivanov', 18, N'Kyiv, Vasylkivska,5', 1)
INSERT [dbo].[Customers] ([Id], [FirstName], [LastName], [Age], [Adress], [IsActive]) VALUES (5, N'Alex', N'Nazarov', 13, N'Kyiv, Sechenova,5', 1)
SET IDENTITY_INSERT [dbo].[Customers] OFF
SET IDENTITY_INSERT [dbo].[Goods] ON 

INSERT [dbo].[Goods] ([Id], [GoodsName], [Price], [Quantity], [IsActive]) VALUES (1, N'Apple IPhone 11XXXXL', CAST(12000.00 AS Decimal(18, 2)), 12, 1)
INSERT [dbo].[Goods] ([Id], [GoodsName], [Price], [Quantity], [IsActive]) VALUES (2, N'Nuawei P20', CAST(10000.00 AS Decimal(18, 2)), 12, 1)
INSERT [dbo].[Goods] ([Id], [GoodsName], [Price], [Quantity], [IsActive]) VALUES (3, N'Asus ZenPhone', CAST(14000.00 AS Decimal(18, 2)), 12, 1)
INSERT [dbo].[Goods] ([Id], [GoodsName], [Price], [Quantity], [IsActive]) VALUES (4, N'Xiaomi Mi10', CAST(17000.00 AS Decimal(18, 2)), 12, 1)
SET IDENTITY_INSERT [dbo].[Goods] OFF
SET IDENTITY_INSERT [dbo].[OrderItems] ON 

INSERT [dbo].[OrderItems] ([Id], [OrderId], [GoodsId], [Price], [Quantity]) VALUES (2, 2, 1, CAST(10000.00 AS Decimal(18, 2)), 4)
INSERT [dbo].[OrderItems] ([Id], [OrderId], [GoodsId], [Price], [Quantity]) VALUES (5, 2, 4, CAST(14000.00 AS Decimal(18, 2)), 4)
INSERT [dbo].[OrderItems] ([Id], [OrderId], [GoodsId], [Price], [Quantity]) VALUES (7, 2, 3, CAST(17000.00 AS Decimal(18, 2)), 3)
INSERT [dbo].[OrderItems] ([Id], [OrderId], [GoodsId], [Price], [Quantity]) VALUES (9, 1, 2, CAST(12000.00 AS Decimal(18, 2)), 1)
SET IDENTITY_INSERT [dbo].[OrderItems] OFF
SET IDENTITY_INSERT [dbo].[Orders] ON 

INSERT [dbo].[Orders] ([Id], [CustomerId], [OrderDate], [DeliveryAdress]) VALUES (1, 1, CAST(N'2019-10-02T21:24:30.320' AS DateTime), N'Irpin')
INSERT [dbo].[Orders] ([Id], [CustomerId], [OrderDate], [DeliveryAdress]) VALUES (2, 3, CAST(N'2019-10-02T21:25:49.690' AS DateTime), N'Kyiv, Sechenova,5')
INSERT [dbo].[Orders] ([Id], [CustomerId], [OrderDate], [DeliveryAdress]) VALUES (3, 1, CAST(N'2019-10-02T05:25:49.690' AS DateTime), N'wererwe')
INSERT [dbo].[Orders] ([Id], [CustomerId], [OrderDate], [DeliveryAdress]) VALUES (9, 1, CAST(N'2019-10-11T05:25:49.690' AS DateTime), NULL)
INSERT [dbo].[Orders] ([Id], [CustomerId], [OrderDate], [DeliveryAdress]) VALUES (10, 1, CAST(N'2019-10-11T05:25:49.690' AS DateTime), NULL)
SET IDENTITY_INSERT [dbo].[Orders] OFF
SET IDENTITY_INSERT [history].[Orders] ON 

INSERT [history].[Orders] ([Id], [PKId], [CustomerId], [OrderDate], [DeliveryAdress], [ActionType], [ActionDate]) VALUES (7, 3, 1, CAST(N'2019-10-02T21:25:49.690' AS DateTime), N'Vasilkiv', N'UPDATE              ', CAST(N'2019-10-09T21:16:12.017' AS DateTime))
INSERT [history].[Orders] ([Id], [PKId], [CustomerId], [OrderDate], [DeliveryAdress], [ActionType], [ActionDate]) VALUES (8, 3, 1, CAST(N'2019-10-02T21:25:49.690' AS DateTime), N'fsd\', N'UPDATE              ', CAST(N'2019-10-09T21:16:21.920' AS DateTime))
INSERT [history].[Orders] ([Id], [PKId], [CustomerId], [OrderDate], [DeliveryAdress], [ActionType], [ActionDate]) VALUES (9, 3, 1, CAST(N'2019-10-02T21:25:49.690' AS DateTime), N'wererwe', N'UPDATE              ', CAST(N'2019-10-09T21:16:28.323' AS DateTime))
INSERT [history].[Orders] ([Id], [PKId], [CustomerId], [OrderDate], [DeliveryAdress], [ActionType], [ActionDate]) VALUES (10, 3, 1, CAST(N'2019-10-02T04:25:49.690' AS DateTime), N'wererwe', N'UPDATE              ', CAST(N'2019-10-09T21:17:04.617' AS DateTime))
INSERT [history].[Orders] ([Id], [PKId], [CustomerId], [OrderDate], [DeliveryAdress], [ActionType], [ActionDate]) VALUES (11, 9, 1, NULL, NULL, N'INSERT              ', CAST(N'2019-10-09T21:28:40.290' AS DateTime))
INSERT [history].[Orders] ([Id], [PKId], [CustomerId], [OrderDate], [DeliveryAdress], [ActionType], [ActionDate]) VALUES (12, 9, 1, NULL, NULL, N'UPDATE              ', CAST(N'2019-10-09T21:28:49.207' AS DateTime))
INSERT [history].[Orders] ([Id], [PKId], [CustomerId], [OrderDate], [DeliveryAdress], [ActionType], [ActionDate]) VALUES (13, 10, 1, CAST(N'2019-10-11T05:25:49.690' AS DateTime), NULL, N'INSERT              ', CAST(N'2019-10-09T21:28:57.023' AS DateTime))
SET IDENTITY_INSERT [history].[Orders] OFF
ALTER TABLE [dbo].[Customers] ADD  CONSTRAINT [DF_Customers_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
ALTER TABLE [dbo].[OrderItems]  WITH CHECK ADD  CONSTRAINT [FK_OrderItems_Goods] FOREIGN KEY([GoodsId])
REFERENCES [dbo].[Goods] ([Id])
GO
ALTER TABLE [dbo].[OrderItems] CHECK CONSTRAINT [FK_OrderItems_Goods]
GO
ALTER TABLE [dbo].[OrderItems]  WITH CHECK ADD  CONSTRAINT [FK_OrderItems_Orders] FOREIGN KEY([OrderId])
REFERENCES [dbo].[Orders] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[OrderItems] CHECK CONSTRAINT [FK_OrderItems_Orders]
GO
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD  CONSTRAINT [FK_Orders_Customers] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customers] ([Id])
GO
ALTER TABLE [dbo].[Orders] CHECK CONSTRAINT [FK_Orders_Customers]
GO
ALTER TABLE [dbo].[Customers]  WITH CHECK ADD  CONSTRAINT [Age_More_Then_Zero] CHECK  (([Age]>(0) AND [Age]<(150)))
GO
ALTER TABLE [dbo].[Customers] CHECK CONSTRAINT [Age_More_Then_Zero]
GO
/****** Object:  StoredProcedure [dbo].[spOrdersForCustomer]    Script Date: 10/9/2019 9:31:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--CREATE 
CREATE PROCEDURE [dbo].[spOrdersForCustomer]
	@CustomerId int
AS
BEGIN
	SELECT 
		o.Id, 
		c.FirstName,
		c.LastName,
		o.OrderDate,
		o.DeliveryAdress,
		SUM(distinct oi.GoodsId) [UniqueItemsCount],
		SUM(oi.Price) [TotalSum]
	from Orders o
		join Customers c on c.Id = o.CustomerId
		join OrderItems oi on oi.OrderId = o.Id
	where o.CustomerId = @CustomerId
	group by c.FirstName,c.LastName,o.OrderDate,o.DeliveryAdress,o.Id
END
GO
/****** Object:  StoredProcedure [dbo].[spRemoveNonValidOrders]    Script Date: 10/9/2019 9:31:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--select * from Orders
--select * from OrderItems
--exec spRemoveNonValidOrders '2019-10-02'
CREATE PROCEDURE [dbo].[spRemoveNonValidOrders]
	@Date date
AS
BEGIN
	--delete OrderItems 
	--where OrderId in(
	--	select o.Id
	--	from Orders o
	--	where CONVERT(date, o.OrderDate) = @Date
	--		AND o.DeliveryAdress is null or o.DeliveryAdress= ''
	--)

	
	begin try
		begin transaction

		delete oi
		from OrderItems oi
			join Orders o on o.Id = oi.OrderId
		where CONVERT(date, o.OrderDate) = @Date
				AND o.DeliveryAdress is null or o.DeliveryAdress= ''
		
		delete o
		from Orders o
		where CONVERT(date, o.OrderDate) = @Date
			AND (o.DeliveryAdress is null or o.DeliveryAdress= '')  and o.Id/0=1
			commit;

	end try
	begin catch 
	if @@TRANCOUNT>0
	begin
		rollback tran;
	end
	end catch
END
GO
USE [master]
GO
ALTER DATABASE [MainAcadOrderService] SET  READ_WRITE 
GO
