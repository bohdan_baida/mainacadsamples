alter TRIGGER Trg_Insert_Log_Orders
   ON  dbo.Orders
   AFTER INSERT, UPDATE
AS 
BEGIN
if (select count(*) from deleted)>0
begin
insert into history.Orders(PKId, CustomerId,DeliveryAdress,OrderDate,ActionType)
select deleted.Id, 
	deleted.CustomerId,
	deleted.DeliveryAdress
	,deleted.OrderDate,
	'UPDATE' from deleted
end
else
begin
insert into history.Orders(PKId, CustomerId,DeliveryAdress,OrderDate,ActionType)
select inserted.Id, 
	inserted.CustomerId,
	inserted.DeliveryAdress
	,inserted.OrderDate,
	'INSERT' from inserted
end
END
GO
