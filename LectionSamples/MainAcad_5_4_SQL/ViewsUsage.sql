--Views
--Sequences
--Indexes

select * from Ports
select * from Suppliers
select * from Shops
select * FROM Supplier_Port_Shop

declare @supplId int =1
select distinct s.ShopId,s.ShopName,spl.SupplierId, spl.SupplierName
from Shops s
	join Supplier_Port_Shop sps on sps.ShopId = s.ShopId
	join Suppliers spl on spl.SupplierId = sps.SupplierId
where spl.SupplierId = @supplId

declare @shop int =1
select distinct s.ShopId,s.ShopName,spl.SupplierId, spl.SupplierName
from Shops s
	join Supplier_Port_Shop sps on sps.ShopId = s.ShopId
	join Suppliers spl on spl.SupplierId = sps.SupplierId
where s.ShopId = @shop

select * from vShopsSuppliers1 ss
where ss.ShopId = @shop