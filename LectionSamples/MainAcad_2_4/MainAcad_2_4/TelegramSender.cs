﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainAcad_2_4
{
    public class TelegramSender : ISender
    {
        public void NotifyDeliveryTime()
        {
            Console.WriteLine("Telegram delivery time notify");
        }

        public void NotifyOrderAccept()
        {
            Console.WriteLine("Telegram order accept notify");
        }
    }
}
