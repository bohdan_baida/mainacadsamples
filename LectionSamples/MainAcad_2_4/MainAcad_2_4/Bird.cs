﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainAcad_2_4
{
    public class Bird
    {
        public virtual/*abstract*/ void Voice()
        {
            Console.WriteLine("Bird is singing");
        }

    }

    public class Duck : Bird
    {
        public new virtual void Voice()
        {
            Console.WriteLine("Kra kra!");
        }

        public void Swim()
        {

        }
    }
    public class DonaldDuck :Duck
    {
        public override void Voice()
        {
            Console.WriteLine("Money money!");
        }
    }
}
