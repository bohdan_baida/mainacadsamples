﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainAcad_2_4
{
    public class ViberSender : ISender
    {
        public void NotifyDeliveryTime()
        {
            Console.WriteLine("Viber delivery time notify");
        }

        public void NotifyOrderAccept()
        {
            Console.WriteLine("Viber order accept notify");
        }
    }
}
