﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainAcad_2_4
{
    public class Fish
    {
        private MAW maw;

        public Fish()
        {
            maw = new MAW();
        }

        //nested class
        class MAW
        {
            public void ChangeDensity(DensityChangeTypeEnum densityType)
            {
                //complicated logic
                Console.WriteLine("Density was changed!");
            }
        }
        enum DensityChangeTypeEnum
        {
            Low = 1,
            Up
        }

        public void GoDeep()
        {
            maw.ChangeDensity(DensityChangeTypeEnum.Up);
            
        }

        public void GoUp()
        {
            maw.ChangeDensity(DensityChangeTypeEnum.Low);
        }
    }
}
