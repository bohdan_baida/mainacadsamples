﻿using MainAcad_2_4.Cards.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainAcad_2_4.Cards
{
    public class CardSpades : Card
    {
        public override Suits Suite => Suits.Spades;

        public CardSpades(Prices price) : base(price)
        {

        }
    }
}
