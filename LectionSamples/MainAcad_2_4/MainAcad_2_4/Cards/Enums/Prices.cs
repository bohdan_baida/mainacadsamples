﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainAcad_2_4.Cards.Enums
{
    public enum Prices
    {
        Two = 2,
        Three = 3,
        Four = 4,
        Five = 5,
        Six = 6,
        Seven = 7,
        Eight = 8,
        Nine = 9,
        Ten = 10,
        Jack = 11,
        Queen = 12,
        King = 13,
        Ace = 14
    }

    public static class PriceHelper
    {
        public static string GetPrice(Prices price)
        {
            switch (price)
            {
                case Prices.Two:
                    return "2";
                case Prices.Three:
                    return "3";
                case Prices.Four:
                    return "4";
                case Prices.Five:
                    return "5";
                case Prices.Six:
                    return "6";
                case Prices.Seven:
                    return "7";
                case Prices.Eight:
                    return "8";
                case Prices.Nine:
                    return "9";
                case Prices.Ten:
                    return "10";
                case Prices.Jack:
                    return "J";
                case Prices.Queen:
                    return "Q";
                case Prices.King:
                    return "K";
                case Prices.Ace:
                    return "A";
                default:
                    throw new System.Exception(); ;
            }
        }
    }
}
