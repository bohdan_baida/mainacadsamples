﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainAcad_2_4.Cards.Enums
{
    public enum Suits
    {
        Clubs = 1,
        Hearts,
        Diamonds,
        Spades
    }

    public static class SuitesHelper
    {
        public static string GetEmoji(Suits suite)
        {
            switch (suite)
            {
                case Suits.Spades:
                    return "♠";
                case Suits.Hearts:
                    return "♥";
                case Suits.Clubs:
                    return "♣";
                case Suits.Diamonds:
                    return "♦";
                default:
                    throw new System.Exception();
            }
        }
    }
}
