﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MainAcad_2_4.Cards.Enums;

namespace MainAcad_2_4.Cards
{
    public class CardClubs : Card
    {
        public override Suits Suite => Suits.Clubs;

        public CardClubs(Prices price) : base(price)
        {

        }
    }
}
