﻿using MainAcad_2_4.Cards.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainAcad_2_4.Cards
{
    public class CardDiamonds : Card
    {
        public override Suits Suite => Suits.Diamonds;

        public CardDiamonds(Prices price) : base(price)
        {

        }
    }
}
