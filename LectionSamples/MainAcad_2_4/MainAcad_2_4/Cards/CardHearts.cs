﻿using MainAcad_2_4.Cards.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainAcad_2_4.Cards
{
    public class CardHearts : Card
    {
        public override Suits Suite => Suits.Hearts;

        public CardHearts(Prices price) : base(price)
        {

        }
    }
    //OK
    //public class CardHearts1 : CardHearts
    //{
    //    public CardHearts1(Prices price) : base(price)
    //    {

    //    }
    //}
}
