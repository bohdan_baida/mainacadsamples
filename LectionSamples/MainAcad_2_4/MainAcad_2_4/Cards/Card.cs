﻿using MainAcad_2_4.Cards.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainAcad_2_4.Cards
{
    public abstract class Card
    {
        private Prices price;
        public Prices Price => price;

        public abstract Suits Suite { get; }

        //public abstract void Bla();

        public Card(Prices price)
        {
            this.price = price;
        }

        public override string ToString()
        {
            return $"{PriceHelper.GetPrice(Price)}{SuitesHelper.GetEmoji(Suite)}";
        }
    }
}
