﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.IO.Compression;

namespace MainAcad_5_1
{
    class Program
    {
        static void Main(string[] args)
        {
            //File
            //var file = new FileInfo("C:\bla.txt");
            //var files = Directory.GetFiles(@"C:\temp");
            //foreach (var path in files)
            //{
            //    File.WriteAllText(path, "Hello world 1!");
            //}

            //File.WriteAllText(@"C:\temp\1.txt", "hello");


            //Console.WriteLine(Path.GetDirectoryName(@"C:\temp\1.txt"));
            //Console.WriteLine(Path.GetExtension(@"C:\temp\1.txt"));

            //var basePath = @"C:\temp";
            //for (int i = 0; i < 10; i++)
            //{
            //    File.WriteAllText(Path.Combine(basePath, $"{i}.txt"), "hello");

            //}

            //FileStream fileStream = new FileStream(@"C:\temp\1.txt", FileMode.Append);
            //using (StreamWriter streamWriter = new StreamWriter(@"C:\temp\1.txt"))
            //{
            //    for (int i = 0; i < 50000; i++)
            //    {
            //        streamWriter.WriteLine(i);
            //    }
            //}
            //List<string> list = new List<string>();
            //using (StreamReader streamReader = new StreamReader(@"C:\temp\1.txt"))
            //{
            //    while (!streamReader.EndOfStream)
            //    {
            //        list.Add(streamReader.ReadLine());

            //    }
            //}

            //var result = "";
            //var timeString = DateTime.Now;
            //foreach (var item in list)
            //{
            //    result += item;
            //}
            //var diffString = DateTime.Now - timeString;
            //Console.WriteLine(diffString);

            //StringBuilder stringBuilder = new StringBuilder();
            //var timeStringB = DateTime.Now;

            //foreach (var item in list)
            //{
            //    stringBuilder.AppendLine(item);
            //}
            //var res = stringBuilder.ToString();
            //var diffStringB = DateTime.Now - timeStringB;
            //Console.WriteLine(diffStringB);

            //Console.WriteLine("end");
            //Console.Read();

            //var console = Console.Out;

            //List<Tuple<string, string, string>> questions = new List<Tuple<string, string, string>>();
            //using (StreamReader streamReader = new StreamReader(@"C:\temp\ds.txt", encoding: Encoding.UTF8))
            //{
            //    while (!streamReader.EndOfStream)
            //    {
            //        var question = streamReader.ReadLine();
            //        var answer1 = streamReader.ReadLine();
            //        var answer2 = streamReader.ReadLine();

            //        questions.Add(new Tuple<string, string, string>(question, answer1, answer2));
            //    }
            //}
            //Console.OutputEncoding = Encoding.UTF8;
            //questions.ForEach(c =>
            //{
            //    Console.WriteLine(c.Item1);
            //    Console.WriteLine($"1 - {c.Item2}");
            //    Console.WriteLine($"2 - {c.Item3}");
            //    var choice = int.Parse(Console.ReadLine());
            //    if (choice == 1)
            //    {
            //        Console.WriteLine("Krasawa");
            //    }
            //    else
            //    {
            //        Console.WriteLine("Idi v peklo grishnik");

            //    }

            //});


            //using (FileStream outStream = File.Create(@"C:\temp\1.gz"))
            //{
            //    using (GZipStream gZipStream = new GZipStream(outStream, CompressionMode.Compress))
            //    {
            //        using (FileStream fileStream = new FileStream(@"C:\temp\1.txt", FileMode.Open))
            //        {
            //            fileStream.CopyTo(gZipStream);

            //        }
            //    }
            //}

            //using (FileStream fileStream = new FileStream(@"C:\temp\1.gz", FileMode.Open))
            //{
            //    using (GZipStream gZipStream = new GZipStream(fileStream, CompressionMode.Decompress))
            //    {
            //        using (StreamReader streamReader = new StreamReader(gZipStream))
            //        {
            //            var str = streamReader.ReadToEnd();
            //        }
            //    }
            //}

            //FileInfo fileInfo = new FileInfo();

            //var b = new String(new char[] { '1', 'q' });
            //b = string.Intern(b);
            //var c = string.IsInterned(b);
            //var a = "1q";
            //var c1 = string.IsInterned(a);

            //Console.WriteLine(a == b);
            //Console.WriteLine(string.Equals(a,b));
            //Console.WriteLine(string.ReferenceEquals(a, b));

            var str = string.Format("{0},{1},{2}", 1, 2, 3);

            var arr = str.Split(new char[] { ',' }).ToList();
            
            var s1 = "\"";
            Console.WriteLine(s1);
            Console.Read();


        }

    }
}
