﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainAcad_4_2
{
    public class Response
    {
        public IEnumerable<string> Errors { get; set; }
        public string Status { get; set; }
    }

    public class Response<T> : Response
        where T : class, new()
    {
        public T Data { get; set; }
    }
}
