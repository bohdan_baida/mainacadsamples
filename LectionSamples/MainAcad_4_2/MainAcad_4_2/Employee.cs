﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainAcad_4_2
{
    public class Employee: IDictionaryConvertable<string, string>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Department { get; set; }
        public decimal Salary { get; set; }

        public Dictionary<string, string> ToDictionary()
        {
            //Reflection
            var result = new Dictionary<string, string>();
            var properties = typeof(Employee).GetProperties(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance);
            foreach (var prop in properties)
            {
                result.Add(prop.Name, prop.GetValue(this).ToString());
            }
            return result;
        }
    }
}
