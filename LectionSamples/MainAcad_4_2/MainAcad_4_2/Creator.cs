﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainAcad_4_2
{
    public static class Creator
    {
        public static T Create<T>()
        //where T : IComparable<T>
        {
            return Activator.CreateInstance<T>();
        }
        public static int Compare<T>(T t1, T t2)
            where T : IComparable<T>
        {
            var isEquals = t1.CompareTo(t2);
            return isEquals;
        }

        public static TOut Cast<TIn, TOut>(TIn item)
        {
            return default(TOut);

        }


    }
}
