﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainAcad_4_2
{
    public interface IDictionaryConvertable<TKey, TValue>
    {
        Dictionary<TKey, TValue> ToDictionary();
    }
}
