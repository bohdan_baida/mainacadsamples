﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainAcad_4_2
{
    class Program
    {
        public delegate void Show<T>(T message);

        static void Main(string[] args)
        {
            #region NON GENERIC
            ArrayList arrayList = new ArrayList();
            arrayList.Add(121.21);
            arrayList.Add(2.12);
            arrayList.Add(1212.1212);
            arrayList.Remove(2);
            foreach (var item in arrayList)
            {
                var itm = (double)item;
            }

            Stack stack = new Stack();
            Queue queue = new Queue();

            queue.Enqueue("Order 1");
            queue.Enqueue("Order 2");
            queue.Enqueue("Order 3");
            queue.Enqueue("Order 4");
            while (queue.Count != 0)
            {
                Console.WriteLine(queue.Dequeue());
            }

            stack.Push("Book 1");
            stack.Push("Book 2");
            stack.Push("Book 3");
            stack.Push("Book 4");
            stack.Push("Book 5");
            while (stack.Count != 0)
            {
                Console.WriteLine(stack.Pop());
            }

            Hashtable hashtable = new Hashtable();
            hashtable.Add("1", new string[] { "One", "two" });
            hashtable.Add("lastName", "Two");
            hashtable.Add("2", "Three");
            hashtable.Add("4", "Three");

            var element = hashtable["1"];

            BitArray bitArray = new BitArray(12);
            #endregion



            var customList = new CustomList<Employee>();
            var empl = Creator.Create<Employee>();
            var res1 = Creator.Compare(2, 2);
            var res2 = Creator.Compare(2, 3);
            var res3 = Creator.Compare(3, 2);


            List<int> list = new List<int>();
            list.Add(1);
            list.Add(2);
            list.Add(3);
            list.Add(4);
            list.Find(c => c > 2);
            list.First(c => c == 3);
            list.FirstOrDefault(c => c == 3);
            list.Single(c => c == 3);
            list = list.OrderBy(c => c).ToList();
            var a = list.First();

            List<Employee> employees = new List<Employee>();
            employees.Add(new Employee()
            {
                FirstName = "Bohdan",
                LastName = "Baida",
                Department = "IT",
                Salary = 100
            });
            employees.Add(new Employee()
            {
                FirstName = "Alex",
                LastName = "Ivanov",
                Department = "Sales",
                Salary = 200
            });
            employees.Add(new Employee()
            {
                FirstName = "Alex",
                LastName = "Alexandrov",
                Department = "IT",
                Salary = 300
            });
            employees.Add(new Employee()
            {
                FirstName = "Ivan",
                LastName = "Alexandrov",
                Department = "Sales",
                Salary = 400
            });
            employees.Add(new Employee()
            {
                FirstName = "Ivan",
                LastName = "Ivanov",
                Department = "Support",
                Salary = 300
            });


            var groupedCollection = employees
                .GroupBy(c => c.Department)
                .Select(c => new
                {
                    Department = c.Key,
                    EmployeesCount = c.Count(),
                    AverageSalary = c.Average(x => x.Salary)
                });

            var sortedCollection = employees.OrderBy(c => c.Salary);
            var totalSalary = employees.Sum(c => c.Salary);
            var maxSalary = employees.Max(c => c.Salary);
            var minSalary = employees.Min(c => c.Salary);

            foreach (var item in groupedCollection)
            {
                Console.WriteLine($"{item.Department}:{item.EmployeesCount}, {item.AverageSalary}");
            }

            Queue<Employee> employeesQueue = new Queue<Employee>();
            Stack<Employee> employeesStack = new Stack<Employee>();
            employeesQueue.Enqueue(new Employee());
            var next = employeesQueue.Dequeue();
            employeesStack.Push(new Employee());
            var nextFromStack = employeesStack.Pop();

            Dictionary<int, string> dictionary = new Dictionary<int, string>();
            dictionary.Add(1, "One");
            dictionary.Add(2, "Two");
            var value = dictionary[2];
            var emplGroup = employees
                .GroupBy(c => c.Department)
                .ToDictionary(c => c.Key, x => x.ToList());

            LinkedList<string> employeesLinkedList = new LinkedList<string>();
            employeesLinkedList.AddFirst("Player 1");
            employeesLinkedList.AddAfter(employeesLinkedList.Find("Player 1"), "Player 2");
            employeesLinkedList.AddLast("Player 3");
            employeesLinkedList.AddFirst("Player 4");
            var element1 = employeesLinkedList.Find("Player 1");

            ObservableCollection<Employee> col = new ObservableCollection<Employee>();
            Show<int> show = (xx) => Console.WriteLine($"{xx},{element1}");
            show(121);
            Show<string> showStr = (xxx) => Console.WriteLine($"{xxx} str");
            showStr("fsdf");

            IDictionaryConvertable<string, string> empl11 = new Employee()
            {
                FirstName = "A",
                LastName = "B",
                Department = "C",
                Salary = 100
            };
            var d = empl11.ToDictionary();
            Console.Read();

        }
    }
}
