﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainAcad_4_3
{
    class Program
    {
        static void Main(string[] args)
        {
            IEnumerable<int> enumerableCollection = new List<int>();
            ICollection<int> icollectionCollection = new List<int>();
            IReadOnlyCollection<int> readOnlyCollection = new List<int>();
            IList<int> ilistCollection = new List<int>();

            foreach (var item in enumerableCollection)
            {
                Console.WriteLine(item);
            }
            //not allowed
            //for (int i = 0; i < enumerableCollection.Count(); i++)
            //{
            //    Console.WriteLine(enumerableCollection[i]);
            //}
            foreach (var item in icollectionCollection)
            {
                Console.WriteLine(item);

            }
            //not allowed
            //for (int i = 0; i < icollectionCollection.Count(); i++)
            //{
            //    Console.WriteLine(icollectionCollection[i]);
            //}
            foreach (var item in ilistCollection)
            {
                Console.WriteLine(item);
            }
            for (int i = 0; i < ilistCollection.Count(); i++)
            {
                Console.WriteLine(ilistCollection[i]);
            }

            var sortedList = new SortedList<ComparableObject, string>();
            sortedList.Add(new ComparableObject() { X = 2 }, "1");
            sortedList.Add(new ComparableObject() { X = 3 }, "2");
            sortedList.Add(new ComparableObject() { X = 1 }, "2");
            var dictionary = new Dictionary<ComparableObject, string>();
            dictionary.Add(new ComparableObject() { X = 2 }, "1");
            dictionary.Add(new ComparableObject() { X = 2 }, "1");
            dictionary.Add(new ComparableObject() { X = 3 }, "1");

            //var obj  = dictionary[new ComparableObject() { X = 2 }];
            foreach (var item in sortedList)
            {
                Console.WriteLine(item.Value);
            }

            Queue<int> queue = new Queue<int>();
            Stack<int> stack = new Stack<int>();

        }
    }
}
