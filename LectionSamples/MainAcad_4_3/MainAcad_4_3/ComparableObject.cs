﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainAcad_4_3
{
    public class ComparableObject : IComparable<ComparableObject>
    {
        public int X { get; set; }

        public int CompareTo(ComparableObject other)
        {
            if (this.X == other.X)
                return 0;

            if (this.X > other.X)
                return 1;

            return -1;
        }
    }
}
