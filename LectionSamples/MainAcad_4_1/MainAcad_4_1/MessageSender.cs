﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainAcad_4_1
{
    public class MessageSender
    {
        public delegate void Message();
        public delegate void MessageWithParam(string message);
        public delegate int MathOperation(int a, int b);

        public void Send()
        {
            ShowHello();
            //instanse of custom delegate
            Message message = ShowWorld;
            message += ShowHello;
            message += ShowWorld;
            message();
            message.Invoke();


            MessageWithParam messageWithParam = ShowMessage;
            messageWithParam += ShowMessage;
            messageWithParam("Hello with param");

            MathOperation mathOperation = Add;
            mathOperation += Sub;
            var a = mathOperation(2, 5);
            Console.WriteLine(a);

            Message message1 = ShowHello, message2 = ShowHello;
            var message3 = message1 + message2;
            message1 -= ShowHello;
            message1?.Invoke();//? - check if null, if yes - break
            Invoke(message3);

        }
        public void ShowHello()
        {
            Console.WriteLine("Hello");
        }
        public void ShowWorld()
        {
            Console.WriteLine("world!");
        }
        public void ShowMessage(string message)
        {
            Console.WriteLine(message);
        }
        public int Add(int a, int b)
        {
            return a + b;
        }
        public int Sub(int a, int b)
        {
            return a - b;
        }
        public void Invoke(Message del)
        {
            del();
        }
    }
}
