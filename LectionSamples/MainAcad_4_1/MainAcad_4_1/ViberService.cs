﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainAcad_4_1
{
    public class ViberService
    {
        public void SendViberMessage(decimal sum, OperationTypeEnum operationType)
        {
            var opType = Enum.GetName(typeof(OperationTypeEnum), operationType);
            Console.WriteLine($"{opType}: {sum} USD.Viber");
        }
    }
}
