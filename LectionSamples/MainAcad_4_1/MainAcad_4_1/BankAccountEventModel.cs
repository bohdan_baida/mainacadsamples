﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainAcad_4_1
{
    public class BankAccountEventModel
    {
        private decimal amount;

        public event Notify OnBankOperation;

        public BankAccountEventModel(decimal amount)
        {
            this.amount = amount;
        }

        public void Refil(decimal sum)
        {
            if (sum < 0)
                return;

            amount += sum;
            OnBankOperation?.Invoke(sum, OperationTypeEnum.Refil);
        }
        public void Spend(decimal sum)
        {
            if (sum < 0 || sum > amount)
                return;

            amount -= sum;
            OnBankOperation?.Invoke(sum, OperationTypeEnum.Spend);
        }

    }
}
