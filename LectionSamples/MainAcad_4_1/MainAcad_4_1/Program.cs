﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainAcad_4_1
{
    delegate void MathOperation(int a, int b);
    delegate int MathOperationRet(int a, int b);
    delegate void PrintPersonInfo(Client person);
    delegate Person GetPersonInfo();

    class Program
    {
        static void Main(string[] args)
        {
            //MessageSender messageSender = new MessageSender();
            //messageSender.Send();

            BankAccount account = new BankAccount(1000);
            BankAccountEventModel bankAccount = new BankAccountEventModel(1000);
            SmsService smsService = new SmsService();
            ViberService viberService = new ViberService();
            //account.Subscribe(smsService.SendSms);
            //account.Subscribe(viberService.SendViberMessage);
            //account.Spend(100);
            //account.Spend(50);
            //account.Refil(300);

            //bankAccount.OnBankOperation += smsService.SendSms;
            //bankAccount.OnBankOperation += viberService.SendViberMessage;
            //bankAccount.OnBankOperation -= viberService.SendViberMessage;
            //bankAccount.OnBankOperation += delegate (decimal sum, OperationTypeEnum opType)
            //{
            //    Console.WriteLine($"Anonymus method subscriber");
            //};
            //bankAccount.OnBankOperation
            //    += (sum, opType) => Console.WriteLine($"Anonymus method subscriber");
            //bankAccount.Spend(100);
            //bankAccount.Spend(50);
            //bankAccount.Refil(300);

            //MathOperation mathOperation = (a, b) => Console.WriteLine(a + b);
            //mathOperation += (a, b) => Console.WriteLine(a - b);
            //mathOperation += (a, b) => Console.WriteLine(a * b);
            //mathOperation += (a, b) => Console.WriteLine(a / b);
            //mathOperation(4, 2);

            //Делегат коваріантний відносно вхідних параметрів
            //Делегат контрваріантний відносно вихідного параметра
            PrintPersonInfo printPerson = PrintClientInfoForPerson;
            printPerson += PrintClientInfoForClient;

            printPerson(new Client()
            {
                ClientId = "aqsqaq",
                FirstName = "BBR"
            });

            GetPersonInfo getPersonInfo = GetClient;
            getPersonInfo += GetPerson;


            var arr = new int[] { 1, 2, 3, 4, 5 };

            Predicate<int> predicate = c => c % 5 == 0;
            Func<int, int, bool> func = (a, b) => a == b;
            Func<int, bool> func1 = a => a % 2 == 0;
            Action action = () => Console.WriteLine("DS");
            Action<int, int> action1 = (a, b) => Console.WriteLine(a + b);

            for (int i = 0; i < 100; i++)
            {
                Console.WriteLine($"{i},{predicate(i)}");
            }
            arr.Where(func1);

            Console.Read();

        }

        public static void PrintClientInfoForClient(Client client)
        {
            Console.WriteLine($"{client.ClientId};{client.FirstName}");
        }
        public static void PrintClientInfoForPerson(Person client)
        {
            Console.WriteLine($"{client.FirstName}");

        }
        public static Client GetClient()
            => new Client() { ClientId = "wdas", FirstName = "dsad" };
        public static Person GetPerson()
            => new Client() { ClientId = "wdas", FirstName = "dsad" };


        //public static Client GetClient()
        //{
        //    return new Client() { ClientId = "wdas", FirstName = "dsad" };
        //}

    }


    class Person
    {
        public string FirstName { get; set; }
    }

    class Client : Person
    {
        public string ClientId { get; set; }
    }

}
