﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainAcad_4_1
{
    public delegate void Notify(decimal sum, OperationTypeEnum operationType);

    public class BankAccount
    {
        private decimal amount;
        private Notify subscribers;

        //int a;
        //public int MyProperty
        //{
        //    get
        //    {
        //        return a;
        //    }
        //    set
        //    {
        //        a = value;
        //    }
        //}

        public BankAccount(decimal amount)
        {
            this.amount = amount;
        }

        public void Subscribe(Notify notify)
        {
            subscribers += notify;
        }
        public void Unsubscribe(Notify notify)
        {
            subscribers -= notify;

        }

        public void Refil(decimal sum)
        {
            if (sum < 0)
                return;

            amount += sum;
            subscribers?.Invoke(sum, OperationTypeEnum.Refil);
        }
        public void Spend(decimal sum)
        {
            if (sum < 0 || sum > amount)
                return;

            amount -= sum;
            subscribers?.Invoke(sum, OperationTypeEnum.Spend);
        }
    }

    public enum OperationTypeEnum
    {
        Refil = 1,
        Spend = 2
    }
}
