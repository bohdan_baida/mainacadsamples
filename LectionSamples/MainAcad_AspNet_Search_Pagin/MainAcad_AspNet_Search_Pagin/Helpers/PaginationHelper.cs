﻿using MainAcad_AspNet_Search_Pagin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace MainAcad_AspNet_Search_Pagin.Helpers
{
    public static class PaginationHelper
    {
        public static MvcHtmlString PaginatioFor(this HtmlHelper html, PageInfo pageInfo, UrlHelper helper)
        {
            StringBuilder result = new StringBuilder();
            for (int i = 0; i < pageInfo.NumberOfPages; i++)
            {
                var linkBuilder = new TagBuilder("a");
                linkBuilder.AddCssClass("btn");
                linkBuilder.SetInnerText((i + 1).ToString());
                linkBuilder.MergeAttribute("href", helper.Action("IndexPagin", "Home", new { page = i + 1 }));
                if (i + 1 == pageInfo.CurrentPage)
                {
                    linkBuilder.AddCssClass("btn-primary");
                }
                result.AppendLine(linkBuilder.ToString());
            }
            return new MvcHtmlString(result.ToString());
        }
    }
}