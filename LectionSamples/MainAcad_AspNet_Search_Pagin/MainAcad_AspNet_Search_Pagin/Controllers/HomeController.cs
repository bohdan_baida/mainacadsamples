﻿using MainAcad_AspNet_Search_Pagin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MainAcad_AspNet_Search_Pagin.Controllers
{
    public class HomeController : Controller
    {
        const int PageSize = 2;
        List<User> users;
        public HomeController()
        {
            users = new List<User>()
            {
                new User()
                {
                    FirstName = "Peter",
                    LastName = "Alexandrov",
                    Roles = new string[]
                    {
                         "supervisor"
                    },
                    Login= "petr"
                },
                new User()
                {
                    FirstName = "Bohdan",
                    LastName = "Baida",
                    Roles = new string[]
                    {
                        "admin", "supervisor", "manager"
                    },
                    Login= "blindBeaver"
                },
                new User()
                {
                    FirstName = "Ruslan",
                    LastName = "Baida",
                    Roles = new string[]
                    {
                        "admin", "manager"
                    },
                    Login= "rusl1997"
                },
                new User()
                {
                    FirstName = "Alex",
                    LastName = "Ivanov",
                    Roles = new string[]
                    {
                         "manager"
                    },
                    Login= "alexa"
                }
            };
        }
        public ActionResult Index(string searchString = null)
        {
            if (string.IsNullOrWhiteSpace(searchString))
            {
                return View(users);
            }

            ViewBag.SearchString = searchString;

            var trimedString = searchString.Trim().ToUpper();
            Func<User, bool> comparer = (User u)
                => u.FirstName.ToUpper().Contains(trimedString) ||
                    u.LastName.ToUpper().Contains(trimedString) ||
                    u.Login.ToUpper().Contains(trimedString) ||
                    u.Roles.Any(x => x.ToUpper().Contains(trimedString));


            return View(users.Where(c => comparer(c)).ToList());

        }
        public ActionResult IndexPagin(int page = 1)
        {
            var filteredUsers = users
                .Skip((page - 1) * PageSize)
                .Take(PageSize)
                .ToList();

            return View(new PaginationModel()
            {
                Users = filteredUsers,
                PageInfo = new PageInfo()
                {
                    TotalCount = users.Count(),
                    PageSize = PageSize,
                    CurrentPage = page
                }
            });
        }


    }
}