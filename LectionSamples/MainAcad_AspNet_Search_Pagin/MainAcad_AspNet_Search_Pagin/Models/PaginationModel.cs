﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MainAcad_AspNet_Search_Pagin.Models
{
    public class PaginationModel
    {
        public List<User> Users { get; set; }

        public PageInfo PageInfo { get; set; }
    }
}