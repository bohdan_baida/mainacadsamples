﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MainAcad_AspNet_Search_Pagin.Models
{
    public class User
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string[] Roles { get; set; }

        public string Login { get; set; }
    }
}