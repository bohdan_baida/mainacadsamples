﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MainAcad_AspNet_Search_Pagin.Models
{
    public class PageInfo
    {
        public int TotalCount { get; set; }

        public int PageSize { get; set; }

        public int CurrentPage { get; set; }

        public int NumberOfPages
            => (int)Math.Ceiling(((decimal)TotalCount) / PageSize);
    }
}