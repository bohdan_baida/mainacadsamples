﻿using System.Web;
using System.Web.Mvc;

namespace MainAcad_AspNet_Search_Pagin
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
