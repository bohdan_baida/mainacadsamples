﻿namespace MainAcad_5_7.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PassportData_Added : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PassportDatas",
                c => new
                    {
                        UserId = c.Int(nullable: false),
                        PassportSeries = c.String(nullable: false),
                        PassportNumber = c.String(nullable: false),
                        ProvidedBy = c.String(nullable: false),
                        Date = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.UserId)
                .ForeignKey("dbo.Users", t => t.UserId)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PassportDatas", "UserId", "dbo.Users");
            DropIndex("dbo.PassportDatas", new[] { "UserId" });
            DropTable("dbo.PassportDatas");
        }
    }
}
