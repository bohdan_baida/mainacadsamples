﻿namespace MainAcad_5_7.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Blogs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        BlogName = c.String(nullable: false),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Posts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        BlogId = c.Int(nullable: false),
                        CreatedBy = c.Int(nullable: false),
                        PostHeader = c.String(),
                        PostBody = c.String(),
                        OnDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Blogs", t => t.BlogId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.CreatedBy, cascadeDelete: true)
                .Index(t => t.BlogId)
                .Index(t => t.CreatedBy);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(nullable: false),
                        LastName = c.String(nullable: false),
                        Username = c.String(nullable: false),
                        PasswordHash = c.String(nullable: false),
                        IsBlocked = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UserBlogs",
                c => new
                    {
                        User_Id = c.Int(nullable: false),
                        Blog_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.User_Id, t.Blog_Id })
                .ForeignKey("dbo.Users", t => t.User_Id, cascadeDelete: true)
                .ForeignKey("dbo.Blogs", t => t.Blog_Id, cascadeDelete: true)
                .Index(t => t.User_Id)
                .Index(t => t.Blog_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Posts", "CreatedBy", "dbo.Users");
            DropForeignKey("dbo.UserBlogs", "Blog_Id", "dbo.Blogs");
            DropForeignKey("dbo.UserBlogs", "User_Id", "dbo.Users");
            DropForeignKey("dbo.Posts", "BlogId", "dbo.Blogs");
            DropIndex("dbo.UserBlogs", new[] { "Blog_Id" });
            DropIndex("dbo.UserBlogs", new[] { "User_Id" });
            DropIndex("dbo.Posts", new[] { "CreatedBy" });
            DropIndex("dbo.Posts", new[] { "BlogId" });
            DropTable("dbo.UserBlogs");
            DropTable("dbo.Users");
            DropTable("dbo.Posts");
            DropTable("dbo.Blogs");
        }
    }
}
