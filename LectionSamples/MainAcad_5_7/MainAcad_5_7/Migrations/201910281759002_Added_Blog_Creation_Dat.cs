﻿namespace MainAcad_5_7.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Added_Blog_Creation_Dat : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Blogs", "DateOfCreation", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Blogs", "DateOfCreation");
        }
    }
}
