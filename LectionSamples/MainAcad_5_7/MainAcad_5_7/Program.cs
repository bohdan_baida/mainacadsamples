﻿using MainAcad_5_7.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace MainAcad_5_7
{
    class Program
    {
        //1. Open Package Manager Console(Tools/NuGet Package Manager/...)
        //2. enable-migrations
        //3. add-migration <migration name>
        //4. update-database
        static void Main(string[] args)
        {
            BlogContext context = new BlogContext();
            //var user = context.Users
            //    .Single(c => c.Username == "bohdan");
            //user.Blogs.Add(new Blog()
            //{
            //    BlogName = "Kak bit samim krutim na svoyey gallere",
            //    Description = "Dich",
            //    DateOfCreation = DateTime.Now
            //});
            //context.SaveChanges();

            //eager loading(!INCLUDE!)
            //lazy loading
            foreach (var user in context.Users/*.Include(c => c.Blogs)*/)
            {
                Console.WriteLine($"{user.Username}");
                foreach (var blog in user.Blogs)
                {
                    Console.WriteLine($"\t{blog.BlogName} {blog.DateOfCreation}");
                }
            }
            Console.Read();
        }
    }
}
