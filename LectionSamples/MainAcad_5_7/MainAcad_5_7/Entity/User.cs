﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MainAcad_5_7.Entity
{
    public class User
    {
        public User()
        {
            Posts = new HashSet<Post>();
            Blogs = new HashSet<Blog>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        public string Username { get; set; }

        [Required]
        public string PasswordHash { get; set; }

        [Required]
        public bool IsBlocked { get; set; }

        public virtual ICollection<Post> Posts { get; set; }

        public virtual ICollection<Blog> Blogs { get; set; }

        public virtual PassportData PassportData { get; set; }
    }
}
