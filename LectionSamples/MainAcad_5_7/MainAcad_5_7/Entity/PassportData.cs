﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainAcad_5_7.Entity
{
    public class PassportData
    {
        [Key]
        [ForeignKey("User")]
        public int UserId { get; set; }

        [Required]
        public string PassportSeries { get; set; }

        [Required]
        public string PassportNumber { get; set; }

        [Required]
        public string ProvidedBy { get; set; }

        [Required]
        public DateTime Date { get; set; }

        public virtual User User { get; set; }
    }
}
