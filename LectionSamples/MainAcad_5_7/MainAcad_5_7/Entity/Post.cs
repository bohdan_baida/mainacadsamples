﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainAcad_5_7.Entity
{
    public class Post
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [ForeignKey("Blog")]
        public int BlogId { get; set; }

        [ForeignKey("User")]
        public int CreatedBy { get; set; }

        public string PostHeader { get; set; }

        public string PostBody { get; set; }

        public DateTime OnDate { get; set; }


        public virtual User User { get; set; }
        public virtual Blog Blog { get; set; }

    }
}
