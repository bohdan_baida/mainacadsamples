﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MainAcad_5_7.Entity
{
    public class Blog
    {
        public Blog()
        {
            Posts = new HashSet<Post>();
            Users = new HashSet<User>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        public string BlogName { get; set; }

        public string Description { get; set; }

        public DateTime DateOfCreation { get; set; }

        public ICollection<Post> Posts { get; set; }

        public ICollection<User> Users { get; set; }

    }
}
