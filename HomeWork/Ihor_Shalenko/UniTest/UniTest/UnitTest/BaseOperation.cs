﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UniTest.UnitTest
{
    class BaseOperation
    {

        public decimal Divide(decimal a, decimal b)
        {
            if (b == 0)
            {
                throw new ArgumentException("Arg b can't be null");
            }
            return a / b;
        }

        public Countries GetCountries(Developers dev)
        {
            switch (dev)
            {
                case Developers.Audi:
                case Developers.BMW:
                    return Countries.Germani;

                case Developers.Ford:
                case Developers.Linkoln:
                    return Countries.USA;

                case Developers.AM:
                case Developers.Mini:
                    return Countries.UK;
                
                default:
                    throw new ArgumentException($"Value {dev} is not defined");
            }
        }

        public bool ValidateStudent(Student student)
        {
            if (student.Age < 5)
            {
                return false;
            }
            if (student.Age > 11 || student.Age < 1)
            {
                return false;
            }
            if (string.IsNullOrEmpty(student.FirstName))
            {
                return false;
            }

            if (string.IsNullOrEmpty(student.LastName))
            {
                return false;
            }

            return true;
        }
    }
}
