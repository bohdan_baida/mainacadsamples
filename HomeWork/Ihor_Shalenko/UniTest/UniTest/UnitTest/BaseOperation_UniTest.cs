﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using FluentAssertions;

namespace UniTest.UnitTest
{
    public class BaseOperation_UniTest
    {
        BaseOperation target;
        public BaseOperation_UniTest()
        {
            target = new BaseOperation();
        }

        [Fact]
        public void Divide_When_SecondArgIsZero_Then_ShouldThrowException()
        {
            // Arrange
            const decimal a = default(decimal);
            const decimal b = 0;

            // Act
            Action action = () => target.Divide(a, b);

            // Assert
            action.Should()
                .Throw<ArgumentException>()
                .WithMessage("Arg b can't be null");
        }

        [Fact]
        public void Divide_When_SecondArgIsNotZero_Then_ShouldThrowResult()
        {
            // Arrange
            const decimal a = 10;
            const decimal b = 5;
            const decimal expented = 2;

            // Act
            var result = target.Divide(a, b);

            // Assert
            result.Should().BeOfType(typeof(decimal));
            result.Should().Be(expented);
        }

        [Fact]
        public void GetCountries_WhenDevIsToyota_Then_ShouldThrowException()
        {
            const Developers toyota = Developers.Toyota;

            Action action = () => target.GetCountries(toyota);

            action.Should()
                .Throw<ArgumentException>()
                .WithMessage($"Value {toyota} is not defined");

        }

        [Theory]
        [InlineData(Developers.AM, Countries.UK)]
        [InlineData(Developers.Mini, Countries.UK)]
        [InlineData(Developers.Linkoln, Countries.USA)]
        [InlineData(Developers.Ford, Countries.USA)]
        [InlineData(Developers.Audi, Countries.Germani)]
        [InlineData(Developers.BMW, Countries.Germani)]
        public void GetCountry_When_DeveloperCorrect_Then_ShouldReturnCountry(Developers developer, Countries country)
        {
            var result = target.GetCountries(developer);

            // Assert
            result.Should().Be(country);

        }
    }
}
