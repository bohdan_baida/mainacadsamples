﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagerHelper
{
    class CategoryList : IEnumerable
    {

        private List<Category> categoryList;
        public List<Category> CatList { get { return categoryList; } }

        public CategoryList()
        {
            categoryList = new List<Category>();
        }

        public void AddCategory(Category category)
        {
            if (category.Id == 0)
            {
                Console.WriteLine("Id can't be 0!");
                return;
            }

            Category findCategory = ReturnNomByID(category.Id);
            if (findCategory is null)
                categoryList.Add(category);
        }

        public void RemoveCategory(Category category)
        {
            categoryList.Remove(category);
        }

        public Category ReturnNomByID(int id)
        {
            return categoryList.Find(c => c.Id == id); ;
        }

        public IEnumerator GetEnumerator()
        {
            return categoryList.GetEnumerator();
        }

    }
}
