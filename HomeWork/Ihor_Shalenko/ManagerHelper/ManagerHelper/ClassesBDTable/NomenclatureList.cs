﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagerHelper
{
    class NomenclatureList : IEnumerable
    {
        private List<Nomenclature> nomenclatureList;
        public List<Nomenclature> NomList {get { return nomenclatureList; } }

        public NomenclatureList()
        {
            nomenclatureList = new List<Nomenclature>();
        }

        public void AddNom(Nomenclature nom)
        {
            if (nom.Id == 0)
            {
                Console.WriteLine("Id can't be 0!");
                return;
            }

            Nomenclature findNom = ReturnNomById(nom.Id);
            if(findNom is null)
                nomenclatureList.Add(nom);
        }

        public void RemoveNom(Nomenclature nom)
        {
            nomenclatureList.Remove(nom);
        }

        public Nomenclature ReturnNomById(int id)
        {            
            return nomenclatureList.Find(c => c.Id == id); ;
        }

        public IEnumerator GetEnumerator()
        {
            return nomenclatureList.GetEnumerator();
        }
    }
}
