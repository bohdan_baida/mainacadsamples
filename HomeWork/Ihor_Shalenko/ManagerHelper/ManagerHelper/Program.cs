﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagerHelper
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello! It is Manager Helper!");
            Console.WriteLine("Write your name:");

            Manager iAm = new Manager(Console.ReadLine());

            while (true)
            {
                Console.WriteLine("Chose function:");
                Console.WriteLine("1 - ADD order");
                Console.WriteLine("2 - Remove order");
                Console.WriteLine("3 - Change order");
                Console.WriteLine("4 - Show Orders list");
                Console.WriteLine("5 - Exid");

                string choose = Console.ReadLine();
                Console.Clear();
                switch (choose)
                {
                    case "1":
                        iAm.CreateNewOrder();
                        break;
                    case "2":
                        Console.WriteLine("Write ID order to remove:");
                        iAm.RemoveOrder(int.Parse(Console.ReadLine()));
                        break;
                    case "3":
                        Console.WriteLine("Write ID order to change:");
                        iAm.ChangeOrder(int.Parse(Console.ReadLine()));
                        break;
                    case "4":
                        iAm.ShowManagerOrderList();
                        break;
                    case "5":
                        return;
                    default:
                        break;
                }


            }
        }
    }
}
