﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagerHelper
{
    class Nomenclature
    {
        public string Name { get; set; }
        public decimal Price { get; }
        public Category NomCategory { get; set; }
        public string Article { get; set; }
        public int Id { get; set; }


        public Nomenclature(string name, string article, int id)
        {
            Name = name;
            Article = article;
            Id = id;
        }
        public Nomenclature(string name, string article, int id, Category nomCategory) : this(name, article, id)
        {

            NomCategory = nomCategory;
        }
        public Nomenclature(string name, string article, int id, Category nomCategory, decimal price) : this(name, article, id, nomCategory)
        {
            Price = price;
        }
    }
}
