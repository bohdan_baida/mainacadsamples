﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagerHelper
{
    class Category
    {
        private Category categoryParent;
        public Category CategoryParent
        {
            get
            {
                return categoryParent;
            }

            set
            {
                if (value is null)
                {
                    categoryParent = value;
                    return;
                }

                if (this.GetHashCode() != value.GetHashCode())
                    categoryParent = value;
                else
                    throw new Exception("A category cannot be a parent to itself!");
            }

        }
        public string Name { get; set; }
        public int Id { get; set; }

        public Category(string name, int id)
        {
            Name = name;
            Id = id;
        }

        public Category(string name, int id, Category categoryParent) : this(name, id)
        {
            this.categoryParent = categoryParent;
        }
    }
}
