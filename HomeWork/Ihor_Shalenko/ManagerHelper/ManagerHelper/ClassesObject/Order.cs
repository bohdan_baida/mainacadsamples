﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Visualizer;

namespace ManagerHelper
{
    class Order
    {
        public string Counterparty { get; set; }
        public DateTime OrderDate { get; set; }
        public int OrderID { get; set; }


        public List<OrderLine> OrderLines { get; }

        public Order()
        {
            OrderLines = new List<OrderLine>();
        }

        public void AddLine(OrderLine orderLine)
        {
            OrderLines.Add(orderLine);
            upNumOrderLines();
        }

        public void RemoveLine(int numLine)
        {
            OrderLine thLine = ReturnNOrderLine(numLine);
            if (!(thLine is null))
                OrderLines.Remove(thLine);

            upNumOrderLines();
        }

        public void ShowOrderInfo()
        {
            Console.WriteLine($"Counterparty: {Counterparty}");
            Console.WriteLine($"OrderID:      {OrderID}");
            Console.WriteLine($"OrderDate:    {OrderDate}");

            var visualizer = new Visualizer<OrderLine>(OrderLines);
            visualizer.Visualize();
            Console.WriteLine();

        }

        public OrderLine ReturnNOrderLine(int numLine)
        {
            return OrderLines.Find(c => c.NumLine == numLine); ;
        }

        private void upNumOrderLines()
        {
            int con = 1;
            foreach (var item in OrderLines)
            {
                item.NumLine = con;
                con++;
            }
        }

        public class OrderLine
        {

            public int NumLine { get; set; }
            public Nomenclature Item { get; set; }
            public decimal Price { get; set; }
            public decimal Summ { get; set; }
            public int Count { get; set; }

            public OrderLine(List<OrderLine> OrderLines)
            {
                NumLine = OrderLines.Count + 1;
            }


        }

    }


}
