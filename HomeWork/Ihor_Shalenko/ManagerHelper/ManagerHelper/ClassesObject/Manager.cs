﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Visualizer;

namespace ManagerHelper
{
    class Manager
    {
        private List<Order> OrdersList { get;}
        public string Name { get; set; }

        public Manager(string name)
        {
            Name = name;
            OrdersList = new List<Order>();
        }

        public void ShowManagerOrderList()
        {
            var visualizerN = new Visualizer<Order>(OrdersList);
            visualizerN.Visualize();
            Console.WriteLine();

        }

        public void AddOrder(Order nOrder)
        {
            OrdersList.Add(nOrder);
            Console.WriteLine("Order Whas added");
        }
        public void RemoveOrder(int id)
        {
            Order thOrder = ReturnOrderById(id);
            if (thOrder != null)
            {
                OrdersList.Remove(thOrder);
                Console.WriteLine("Order Whas removed");
            }
        }
        public void ChangeOrder(int id)
        {
            Order thOrder = ReturnOrderById(id);
            if (thOrder != null)
            {
                WorkWithOrder(thOrder);
                thOrder.ShowOrderInfo();
                Console.WriteLine("Order Whas changed");
            }
        }
        public void CreateNewOrder()
        {
            Order newOrder = new Order();
            WorkWithOrder(newOrder);
            if (newOrder != null)
            {
                AddOrder(newOrder);
                newOrder.ShowOrderInfo();
                Console.WriteLine("Order Whas created");
            }


        }
        public Order ReturnOrderById(int id)
        {
            return OrdersList.Find(c => c.OrderID == id); ;
        }

        private static Order WorkWithOrder(Order thOrder)
        {
            Console.WriteLine("Write Counterparty");
            Console.WriteLine($"Counterparty: {thOrder.Counterparty}");
            string nameCounterparty = Console.ReadLine();
            if (nameCounterparty != string.Empty)
                thOrder.Counterparty = nameCounterparty;

            Console.WriteLine("Write OrderID");
            int OrderID = int.Parse(Console.ReadLine());
            if (OrderID != 0)
                thOrder.OrderID = OrderID;

            thOrder.OrderDate = DateTime.Now;

            while (true)
            {
                Console.Clear();
                thOrder.ShowOrderInfo();

                Console.WriteLine("1-ADD line");
                Console.WriteLine("2-Remove line");
                Console.WriteLine("3-Save Order");
                Console.WriteLine("4-break");


                string result = Console.ReadLine();

                switch (result)
                {
                    case "1":
                        Order.OrderLine nLine = new Order.OrderLine(thOrder.OrderLines);
                        nLine.Item = NomenclatureChooser.ReturnNomenclatureWithChoouse();
                        if (nLine.Item is null)
                            break;

                        Console.WriteLine("Price");
                        nLine.Price = int.Parse(Console.ReadLine());
                        Console.WriteLine("Count");
                        nLine.Count = int.Parse(Console.ReadLine());

                        nLine.Summ = nLine.Price * nLine.Count;

                        thOrder.AddLine(nLine);

                        break;

                    case "2":

                        Console.WriteLine("Write num line to remove:");
                        int numLine = int.Parse(Console.ReadLine());

                        thOrder.RemoveLine(numLine);

                        break;

                    case "3":

                        return thOrder;

                    case "4":

                        return null;

                    default:
                        break;
                }



            }
        }
    }
}
