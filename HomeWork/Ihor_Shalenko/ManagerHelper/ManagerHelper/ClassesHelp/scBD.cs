﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManagerHelper
{
     static class scBD
    {
        static NomenclatureList Nomenclatures { get; }
        static CategoryList Categorys { get; }

        static scBD()
        {
            Category c1 = new Category("Laptops", 1);
            Category c2 = new Category("Smartphones", 2);
            Category c3 = new Category("Accessories", 3);
            Category c4 = new Category("Laptops for Games", 4, c1);

            Category cTest1 = new Category("test1", 4);
            Category cTest2 = new Category("test2", 0);

            Categorys.AddCategory(c1);
            Categorys.AddCategory(c2);
            Categorys.AddCategory(c3);
            Categorys.AddCategory(c4);

            Categorys.AddCategory(cTest1);
            Categorys.AddCategory(cTest2);

            Nomenclature n1 = new Nomenclature("", "a1", 1);
            Nomenclature n2 = new Nomenclature("", "a2", 2, c1);
            Nomenclature n3 = new Nomenclature("", "a3", 3, c1);
            Nomenclature n4 = new Nomenclature("", "a4", 4, c4);
            Nomenclature n5 = new Nomenclature("", "a5", 5, c4);
            Nomenclature n6 = new Nomenclature("", "a6", 6, c2);

            Nomenclatures.AddNom(n1);
            Nomenclatures.AddNom(n2);
            Nomenclatures.AddNom(n3);
            Nomenclatures.AddNom(n4);
            Nomenclatures.AddNom(n5);
            Nomenclatures.AddNom(n6);

        }

        public static NomenclatureList GetNomenclatures()
        {
            return Nomenclatures;
        }
        public static CategoryList GetCategorys()
        {
            return Categorys;
        }
        static void BDConnect()
        {
            Console.WriteLine("BD Connerc....");
        }
    }
}
