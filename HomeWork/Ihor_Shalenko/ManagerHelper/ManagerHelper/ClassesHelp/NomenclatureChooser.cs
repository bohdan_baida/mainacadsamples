﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Visualizer;

namespace ManagerHelper
{
    static class NomenclatureChooser
    {
        static NomenclatureList Nomenclatures { get; }
        static CategoryList Categorys { get; }

        static NomenclatureChooser()
        {
            Nomenclatures = new NomenclatureList();
            Categorys = new CategoryList();
            scBD();
        }

        static void scBD()
        {
            Category c1 = new Category("Laptops", 1);
            Category c2 = new Category("Smartphones", 2);
            Category c3 = new Category("Accessories", 3);
            Category c4 = new Category("Laptops for Games", 4, c1);

            Category cTest1 = new Category("test1", 4);
            Category cTest2 = new Category("test2", 0);

            Categorys.AddCategory(c1);
            Categorys.AddCategory(c2);
            Categorys.AddCategory(c3);
            Categorys.AddCategory(c4);

            Categorys.AddCategory(cTest1);
            Categorys.AddCategory(cTest2);

            Nomenclature n1 = new Nomenclature("DD13", "a1", 1);
            Nomenclature n2 = new Nomenclature("S77", "a2", 2, c1);
            Nomenclature n3 = new Nomenclature("Fone2", "a3", 3, c1);
            Nomenclature n4 = new Nomenclature("Brrrr", "a4", 4, c4);
            Nomenclature n5 = new Nomenclature("BlbBlb", "a5", 5, c4);
            Nomenclature n6 = new Nomenclature("Samsung A11", "a6", 6, c2);

            Nomenclatures.AddNom(n1);
            Nomenclatures.AddNom(n2);
            Nomenclatures.AddNom(n3);
            Nomenclatures.AddNom(n4);
            Nomenclatures.AddNom(n5);
            Nomenclatures.AddNom(n6);

        }

        static public Nomenclature ReturnNomenclatureWithChoouse()
        {
            CategoryList cList = Categorys;
            NomenclatureList nList = Nomenclatures;

            Category thCategory = null;
            List<Category> thCategorys = new List<Category>();
            List<Nomenclature> thNomenclatures = new List<Nomenclature>();
            while (true)
            {
                Console.WriteLine("-----Select-----");

                thCategorys = cList.CatList.FindAll(c => c.CategoryParent == thCategory);
                Console.WriteLine("Categorys:");
                var visualizerC = new Visualizer<Category>(thCategorys);
                visualizerC.Visualize();
                Console.WriteLine();

                thNomenclatures = nList.NomList.FindAll(c => c.NomCategory == thCategory);
                Console.WriteLine("Nomenclature:");
                var visualizerN = new Visualizer<Nomenclature>(thNomenclatures);
                visualizerN.Visualize();
                Console.WriteLine();

                Console.WriteLine("1-Select Nomenclature");
                Console.WriteLine("2-Select Category");
                Console.WriteLine("3-Exit");

                string Choose = Console.ReadLine();

                switch (Choose)
                {
                    case "1":
                        Console.WriteLine("Write ID Nomenclature:");
                        return nList.ReturnNomById(int.Parse(Console.ReadLine()));
                    case "2":
                        Console.WriteLine("Write ID Category:");
                        thCategory = cList.ReturnNomByID(int.Parse(Console.ReadLine()));
                        Console.Clear();
                        break;
                    case "3":
                        return null;
                    default:
                        break;
                }

            }

            //return null;
        }
    }
}
