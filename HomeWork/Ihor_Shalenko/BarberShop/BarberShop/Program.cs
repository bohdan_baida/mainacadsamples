﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BarberShop
{
    class Program
    {
        static void Main(string[] args)
        {

            Barber barber = new Barber();
            List<Client> clients = new List<Client>()
            {
                new Client("Alex1", HaircutComplicationEnum.Esy),
                new Client("Alex2", HaircutComplicationEnum.Hard),
                new Client("Alex3", HaircutComplicationEnum.Hard),
                new Client("Alex4", HaircutComplicationEnum.Medium)
            };

            foreach (var item in clients)
            {
                Task.Run(() => barber.Cut(item));
            }

            Console.ReadLine();
        }
    }
}
