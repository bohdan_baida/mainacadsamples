﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BarberShop
{
    class Barber
    {
        public void Cut(Client client)
        {
            int cutingTime = EstimateTime(client.HaircutComplication);
            lock (this)
            {
                Task.Delay(cutingTime).Wait();
            }

            Console.WriteLine($"Client: {client.Name} cuting time: {cutingTime}");
        }

        public int EstimateTime(HaircutComplicationEnum haircutComplication)
        {
            if (haircutComplication == HaircutComplicationEnum.Esy)
                return 4000;
            if (haircutComplication == HaircutComplicationEnum.Medium)
                return 6000;
            if (haircutComplication == HaircutComplicationEnum.Hard)
                return 10000;
            else
                throw new Exception("");

        }
    }
}