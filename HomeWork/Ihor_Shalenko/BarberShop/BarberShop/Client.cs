﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace BarberShop
{
    public class Client
    {
        public string Name { get; set; }
        public HaircutComplicationEnum HaircutComplication { get; set; }

        public Client(string name, HaircutComplicationEnum haircutComplication)
        {
            Name = name;
            HaircutComplication = haircutComplication;
        }

    }

    public enum HaircutComplicationEnum
    {
        Esy,
        Medium,
        Hard
    }
}