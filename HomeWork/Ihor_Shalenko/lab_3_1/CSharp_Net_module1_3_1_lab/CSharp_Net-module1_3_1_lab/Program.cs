﻿using System;
using System.IO;

namespace CSharp_Net_module1_3_1_lab
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                CatchExceptionClass cec = new CatchExceptionClass();
                cec.CatchExceptionMethod();
            }
            catch (Exception ex)
            {

                // 11) Make some unhandled exception and study Visual Studio debugger report – 
                // read description and find the reason of exception
                Console.WriteLine(ex.Message);
            }

            Console.ReadLine();
        }
    }
}
