﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharp_Net_module1_3_1_lab
{
    class Car
    {
        public string Name { get; set; }

        public void CarVoice()
        {
            Console.WriteLine($"{Name} - brrr brrr!");
        }
    }
}
