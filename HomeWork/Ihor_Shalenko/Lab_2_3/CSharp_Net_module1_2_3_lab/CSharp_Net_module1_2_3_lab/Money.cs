﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharp_Net_module1_2_3_lab
{
    // 1) declare enumeration CurrencyTypes, values UAH, USD, EU
    enum CurrencyTypesEnum
    {
        UAH = 980,
        USD = 840,
        EUR = 978
    }



    class Money
    {
        // 2) declare 2 properties Amount, CurrencyType
        public CurrencyTypesEnum Currency { get; set; }
        public double Amount { get; set; }
        public int Course { get; set; }

        // 3) declare parameter constructor for properties initialization
        public Money()
        {
            Course = 1;
        }
        public Money(CurrencyTypesEnum currency, double amount, int course = 1) : this()
        {
            Amount = amount;
            Currency = currency;
            Course = Math.Max(course, 1);
        }

        // 4) declare overloading of operator + to add 2 objects of Money
        public static Money operator +(Money mFirst, Money mSecond)
        {
            return new Money(mFirst.Currency, mFirst.Amount + (mSecond.Amount * mSecond.Course) / mFirst.Course);
        }

        // 5) declare overloading of operator -- to decrease object of Money by 1
        public static Money operator --(Money thMoney)
        {
            thMoney.Amount = thMoney.Amount - 1;
            return thMoney;
        }
        public static Money operator ++(Money thMoney)
        {
            thMoney.Amount = thMoney.Amount + 1;
            return thMoney;
        }

        // 6) declare overloading of operator * to increase object of Money 3 times
        public static Money operator *(Money mFirst, int indexM)
        {
            return new Money(mFirst.Currency, mFirst.Amount * indexM);
        }

        // 7) declare overloading of operator > and < to compare 2 objects of Money
        public static bool operator >(Money mFirst, Money mSecond)
        {
            return mFirst.Amount > (mSecond.Amount * mSecond.Course) / mFirst.Course;
        }

        public static bool operator <(Money mFirst, Money mSecond)
        {
            return mFirst.Amount < (mSecond.Amount * mSecond.Course) / mFirst.Course;
        }


        // 8) declare overloading of operator true and false to check CurrencyType of object
        public static bool operator false(Money thMoney)
        {
            return thMoney.Currency == new CurrencyTypesEnum();
        }

        public static bool operator true(Money thMoney)
        {
            return thMoney.Currency != new CurrencyTypesEnum();
        }

        public static bool operator ==(Money mFirst, Money mSecond)
        {
            return mFirst.Currency == mSecond.Currency;
        }

        public static bool operator !=(Money mFirst, Money mSecond)
        {
            return mFirst.Currency != mSecond.Currency;
        }

        // 9) declare overloading of implicit/ explicit conversion  to convert Money to double, string and vice versa
        public static implicit operator Money(double amaunt)
        {
            return new Money(new CurrencyTypesEnum(), amaunt);
        }

        public static implicit operator double(Money thMoney)
        {
            return thMoney.Amount;
        }

        public static implicit operator Money(string numCurrency)
        {
            Money nMoney = new Money();

            if (numCurrency == "980")
            {
                nMoney.Currency = CurrencyTypesEnum.UAH;
            }
            else if (numCurrency == "840")
            {
                nMoney.Currency = CurrencyTypesEnum.UAH;
            }
            else if (numCurrency == "978")
            {
                nMoney.Currency = CurrencyTypesEnum.EUR;
            }
            else
            {
                nMoney.Currency = new CurrencyTypesEnum();
            }
            return nMoney;
        }

        public static implicit operator string(Money thMoney)
        {
            return thMoney.Currency.ToString();
        }

        public override string ToString()
        {
            return $"Currency {Currency} Course {Course} Amount {Amount}";
        }
    }
}
