﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharp_Net_module1_2_3_lab
{
    class Program
    {
        static void Main(string[] args)
        {
            // 10) declare 2 objects
            Money m1 = new Money(CurrencyTypesEnum.UAH, 100);
            Money m2 = new Money(CurrencyTypesEnum.USD, 5, 25);

            Console.WriteLine($"m1 = {m1}");
            Console.WriteLine($"m2 = {m2}");


            // 11) do operations
            // add 2 objects of Money
            Money m3 = m1 + m2;
            Console.WriteLine("ADD");
            Console.WriteLine(m3.ToString());

            // add 1st object of Money and double
            Console.WriteLine("add 1st object of Money and double");
            m1 = m1 + 3.3;
            Console.WriteLine($"m1 + 3.3 = {m1}");

            // decrease 2nd object of Money by 1 
            Console.WriteLine("decrease 2nd object of Money by 1");
            m2--;
            Console.WriteLine($"m2 -- = {m2}");

            // increase 1st object of Money
            Console.WriteLine("increase 1st object of Money");
            m1++;
            Console.WriteLine($"m1 ++ = {m1}");

            // compare 2 objects of Money
            Console.WriteLine("compare 2 objects of Money");
            Console.WriteLine($"m1 == m2 = {m1 == m2}");

            // compare 2nd object of Money and string
            // eror....

            // check CurrencyType of every object
            Console.WriteLine("ccheck CurrencyType of every object");

            if (m1)
            {
                Console.WriteLine($"m1");
            }

            if (m2)
            {
                Console.WriteLine($"m2");
            }

            // convert 1st object of Money to string
            Console.WriteLine("convert 1st object of Money to string");
            Console.WriteLine($"m1 to string {(string)m1}");


            Console.ReadLine();


        }
    }
}
