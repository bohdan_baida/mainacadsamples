﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharp_Net_module1_1_4_lab
{
    class Program
    {
        // 1) declare enum ComputerType
        public enum ComputerType
        {
            desktops,
            laptops,
            servers
        }


        // 2) declare struct Computer
        public struct Computer
        {
            public ComputerType TypeComp { get; set; }
            public int CPU_Cores { get; set; }
            public double CPU_Frequency { get; set; }
            public int Memory { get; set; }
            public int HDD { get; set; }

            public Computer(ComputerType type, int cores, double frequency, int memory, int hdd)
            {
                TypeComp = type;
                CPU_Cores = cores;
                CPU_Frequency = frequency;
                Memory = memory;
                HDD = hdd;
            }

            public void PrintParametrs()
            {
                Console.WriteLine($"Type: {TypeComp}, CPU: {CPU_Cores} cores, {CPU_Frequency}, HGz, memory: {Memory} GB, HDD: {HDD} GB");
            }

        }


        static void Main(string[] args)
        {
            // 3) declare jagged array of computers size 4 (4 departments)
            Computer[][] departmentsComp = new Computer[4][];

            // 4) set the size of every array in jagged array (number of computers)
            departmentsComp[0] = new Computer[5];
            departmentsComp[1] = new Computer[3];
            departmentsComp[2] = new Computer[5];
            departmentsComp[3] = new Computer[4];

            // 5) initialize array
            // Note: use loops and if-else statements
            FillDepartmentsComp(departmentsComp[0], 2, 2, 1);
            FillDepartmentsComp(departmentsComp[1], 0, 3, 0);
            FillDepartmentsComp(departmentsComp[2], 3, 2, 0);
            FillDepartmentsComp(departmentsComp[3], 1, 1, 2);

            // 6) count total number of every type of computers
            int[] countTC = { 0, 0, 0 };
            for (int i = 0; i < departmentsComp.Length; i++)
            {
                for (int j = 0; j < departmentsComp[i].Length; j++)
                {
                    ComputerType thisComputerType = departmentsComp[i][j].TypeComp;
                    if (thisComputerType == ComputerType.desktops)
                    {
                        countTC[0]++;
                    }
                    else if (thisComputerType == ComputerType.laptops)
                    {
                        countTC[1]++;
                    }
                    else if (thisComputerType == ComputerType.servers)
                    {
                        countTC[2]++;
                    }
                }
            }

            Console.WriteLine($"Count computers. desktops: {countTC[0]}; laptops: {countTC[1]}; servers: {countTC[2]};");

            // 7) count total number of all computers
            // Note: use loops and if-else statements
            // Note: use the same loop for 6) and 7)
            Console.WriteLine($"Count all computers: {countTC.Sum()}");

            //Computer[] ComputerType = {new Computer(Program.ComputerType.laptops, 4, 2.5, 6, 500) , new Computer(Program.ComputerType.laptops, 2, 1.7, 4, 250), new Computer(Program.ComputerType.servers, 8, 3, 16, 2000) };
            // 8) find computer with the largest storage (HDD) - 
            // compare HHD of every computer between each other; 
            // find position of this computer in array (indexes)
            // Note: use loops and if-else statements
            Computer computerWithMaxHDD = new Computer();
            for (int i = 0; i < departmentsComp.Length; i++)
            {
                for (int j = 0; j < departmentsComp[i].Length; j++)
                {
                    if (departmentsComp[i][j].HDD > computerWithMaxHDD.HDD)
                    {
                        computerWithMaxHDD = departmentsComp[i][j];
                    }
                }
            }

            Console.WriteLine($"MAX HDD = {computerWithMaxHDD.HDD} GB");
            computerWithMaxHDD.PrintParametrs();
            for (int i = 0; i < departmentsComp.Length; i++)
            {
                for (int j = 0; j < departmentsComp[i].Length; j++)
                {
                    if (departmentsComp[i][j].HDD == computerWithMaxHDD.HDD)
                    {
                        Console.WriteLine($"Сoordinates: i = {i}, j = {j}");
                    }
                }
            }


            // 9) find computer with the lowest productivity (CPU and memory) - 
            // compare CPU and memory of every computer between each other; 
            // find position of this computer in array (indexes)
            // Note: use loops and if-else statements
            // Note: use logical oerators in statement conditions
            Computer computerWithMinCPU_Memory = new Computer();
            bool itFirst = true;
            for (int i = 0; i < departmentsComp.Length; i++)
            {
                for (int j = 0; j < departmentsComp[i].Length; j++)
                {
                    if (itFirst == true)
                    {
                        computerWithMinCPU_Memory = departmentsComp[i][j];
                        itFirst = false;
                    }
                    else
                    {
                        if (departmentsComp[i][j].CPU_Cores < computerWithMinCPU_Memory.CPU_Cores
                            && departmentsComp[i][j].Memory < computerWithMinCPU_Memory.Memory)
                        {
                            computerWithMinCPU_Memory = departmentsComp[i][j];
                        }
                    }
                }
            }

            Console.WriteLine($"MIN CPU = {computerWithMinCPU_Memory.CPU_Cores} cores, Memory = {computerWithMinCPU_Memory.Memory} GB");
            computerWithMinCPU_Memory.PrintParametrs();
            for (int i = 0; i < departmentsComp.Length; i++)
            {
                for (int j = 0; j < departmentsComp[i].Length; j++)
                {
                    if (departmentsComp[i][j].CPU_Cores == computerWithMinCPU_Memory.CPU_Cores
                            && departmentsComp[i][j].Memory == computerWithMinCPU_Memory.Memory)
                    {
                        Console.WriteLine($"Сoordinates: i = {i}, j = {j}");
                    }
                }
            }

            // 10) make desktop upgrade: change memory up to 8
            // change value of memory to 8 for every desktop. Don't do it for other computers
            // Note: use loops and if-else statements
            for (int i = 0; i < departmentsComp.Length; i++)
            {
                for (int j = 0; j < departmentsComp[i].Length; j++)
                {
                    if (departmentsComp[i][j].TypeComp == ComputerType.desktops)
                    {
                        departmentsComp[i][j].Memory = 8;
                    }
                }
            }

            Console.ReadLine();
        }

        public static void FillDepartmentsComp(Computer[] compList, int countDesktops, int countLaptops, int countServers)
        {
            int count = 0;
            for (int i = 0; i < countDesktops; i++)
            {
                compList[count] = new Computer(ComputerType.desktops, 4, 2.5, 6, 500);
                count++;
            }
            for (int i = 0; i < countLaptops; i++)
            {
                compList[count] = new Computer(ComputerType.laptops, 2, 1.7, 4, 250);
                count++;
            }
            for (int i = 0; i < countServers; i++)
            {
                compList[count] = new Computer(ComputerType.servers, 8, 3, 16, 2000);
                count++;
            }
        }


    }
}
