
CREATE DATABASE QuestionsAnswers

USE QuestionsAnswers

CREATE TABLE Students
(
	Id INT IDENTITY(1,1),
	FirstName varchar(100),
	LastName varchar(150),
	Age int,

	CONSTRAINT PK_Students_Id PRIMARY KEY(Id),
	CONSTRAINT Age_limitation CHECK (Age>0 and Age<100)
)

CREATE TABLE Questions
(
	Id INT IDENTITY(1,1),
	TextQuestion varchar(max),

	CONSTRAINT PK_Questions_Id PRIMARY KEY(Id)
)

CREATE TABLE Answers
(
	Id INT IDENTITY(1,1),
	QuestionId INT,
	TextAnswer varchar(max),
	Correctly bit

	CONSTRAINT PK_Answers_Id PRIMARY KEY(Id),
	CONSTRAINT FK_Answers_Questions FOREIGN KEY(QuestionId) REFERENCES Questions(Id) ON DELETE CASCADE
	-- ON DELETE CASCADE how to change in the settings??? use Alter 
)

--ALTER TABLE Answers
--(
	
--)
 
CREATE TABLE StudentsAnswers
(
	Id INT IDENTITY(1,1),
	QuestionId INT,
	AnswerId INT,
	StudentID INT

	CONSTRAINT PK_StudentsAnswers_Id PRIMARY KEY(Id),
	CONSTRAINT FK_StudentsAnswers_Questions FOREIGN KEY(QuestionId) REFERENCES Questions(Id),
	CONSTRAINT FK_StudentsAnswers_Answers   FOREIGN KEY(AnswerId)   REFERENCES Answers(Id),
	CONSTRAINT FK_StudentsAnswers_Students  FOREIGN KEY(StudentID)  REFERENCES Students(Id)
)


USE QuestionsAnswers

INSERT INTO Students(FirstName, LastName, Age)
VALUES('FirstName', 'LastName', 22)

INSERT INTO Questions(TextQuestion)
VALUES('Are you a moron?')

INSERT INTO Answers(QuestionId, TextAnswer, Correctly)
VALUES(1, 'Yes', 0)
INSERT INTO Answers(QuestionId, TextAnswer, Correctly)
VALUES(1, 'No', 1)

INSERT INTO StudentsAnswers(QuestionId, AnswerId, StudentID)
VALUES(1, 2, 1)

Insert Into Students(FirstName, LastName, Age) VALUES('Ihor0', '', 1)

Select * From  Students

Select * From Answers Where QuestionId = 1

Delete From Students Where Id = 1