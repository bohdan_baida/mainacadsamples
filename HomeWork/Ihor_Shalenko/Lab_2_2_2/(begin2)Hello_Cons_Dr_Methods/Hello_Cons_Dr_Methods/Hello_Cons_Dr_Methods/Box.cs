﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hello_Cons_Dr_Methods
{
    class Box
    {
        //1.  Implement public  auto-implement properties for start position (point position)
        //auto-implement properties for width and height of the box
        //and auto-implement properties for a symbol of a given set of valid characters (*, + ,.) to be used for the border 
        //and a message inside the box
        public int HeightBox { get; set; }
        public int WidthBox { get; set; }
        public int StartX { get; set; }
        public int StartY { get; set; }
        public string Characters { get; set; }
        public string Massage { get; set; }

        //2.  Implement public Draw() method
        //to define start position, width and height, symbol, message  according to properties
        //Use Math.Min() and Math.Max() methods
        //Use draw() to draw the box with message
        public void Draw(int heightBox, int widthBox, int startX, int startY, string characters, string massage)
        {
            this.HeightBox = heightBox;
            this.WidthBox = widthBox;
            this.StartX = startX;
            this.StartY = startY;
            this.Characters = characters;
            this.Massage = massage;

            CheckParam();

            draw();
        }

        //3.  Implement private method draw() with parameters 
        //for start position, width and height, symbol, message
        //Change the message in the method to return the Box square
        //Use Console.SetCursorPosition() method
        //Trim the message if necessary
        private void draw()
        {
            char[] massageChar = Massage.ToArray();
            int finElement = massageChar.Length + StartX;


            for (int y = 0; y < HeightBox; y++)
            {
                for (int x = 0; x < WidthBox; x++)
                {
                    if (((x != 0 || x != WidthBox - 1) && (y == 0 || y == HeightBox -1))
                        || ((y != 0 || y != HeightBox - 1) && (x == 0 || x == WidthBox - 1)))
                    {
                        Console.Write(Characters);
                    }
                    else
                    {
                        if (y == StartY && x >= StartX  && x < finElement)
                        {
                            Console.Write(massageChar[x - StartX]);
                        }
                        else
                        Console.Write(" ");
                    }


                }
                Console.WriteLine();
            }
        }

        private void CheckParam()
        {
            if (StartY > HeightBox)
            {
                StartY = 1;
                Console.WriteLine("Eror! StartY > HeightBox, StartY = 1");
            }

            if (StartX > WidthBox)
            {
                StartX = 1;
                Console.WriteLine("Eror! StartX > WidthBox, StartX = 1");
            }

            if (StartY == HeightBox 
                || StartY == 0)
            {
                StartY = 1;
                Console.WriteLine("Eror! StartY == HeightBox or StartY == 0, StartY = 1");
            }

            if (StartX == WidthBox
                || StartX == 0)
            {
                StartX = 1;
                Console.WriteLine("Eror! StartX == WidthBox or StartX == 0, StartX = 1");
            }
        }

        public void InformTypeCharacters()
        {
            Console.WriteLine("In characters you can use *, + ,.");
        }

        bool CheckCharacters(string Characters)
        {
            bool thisResult = true;

            if (Characters != "*" 
                && Characters != "+"
                && Characters != ".")
            {
                thisResult = false;
                InformTypeCharacters();
            }

            return thisResult;
        }

    }
}
