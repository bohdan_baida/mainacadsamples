﻿using System;

namespace CSharp_Net_module1_1_2_lab
{
    class Program
    {
        static void Main(string[] args)
        {
            // Use "Debugging" and "Watch" to study variables and constants

            //1) declare variables of all simple types:
            //bool, char, byte, sbyte, short, ushort, int, uint, long, ulong, decimal, float, double
            // their names should be: 
            //boo, ch, b, sb, sh, ush, i, ui, l, ul, de, fl, d0
            // initialize them with 1, 100, 250.7, 150, 10000, -25, -223, 300, 100000.6, 8, -33.1

            bool boo = new bool();
            char ch = new char();
            byte b = 1;
            sbyte sb = 8;
            short sh = -25;
            ushort ush = 100;
            int i = -223;
            uint ui = 150;
            long l = 300;
            ulong ul = 10000;
            decimal de = -33.1m;
            float fl = 250.7f;
            double d0 = 100000.6;

            Console.WriteLine("declare variables of all simple types:");
            PrintResult(boo, ch, b, sb, sh, ush, i, ui, l, ul, de, fl, d0);
            Console.WriteLine("");


            // Check results (types and values). Is possible to do initialization?
            // Fix compilation errors (change values for impossible initialization)



            //2) declare constants of int and double. Try to change their values.
            //const int pIn2 = 777;
            //const double pDo2 = 333;

            Console.WriteLine("You can not cange const after declared");

            int pIn1 = 100, pIn2;
            double pDo1 = 100.111, pDo2;

            pIn2 = (int)pDo1;
            pDo2 = pIn1;
            Console.WriteLine("declare constants of int and double. Try to change their values.");
            Console.WriteLine("pIn1 = " + pIn1 + "(" + pIn1.GetType().Name + ")");
            Console.WriteLine("pDo1 = " + pDo1 + "(" + pDo1.GetType().Name + ")");
            Console.WriteLine("After swap--------------------------------------");
            Console.WriteLine("pIn2 = " + pIn2 + "(" + pIn2.GetType().Name + ")");
            Console.WriteLine("pDo2 = " + pDo2 + "(" + pDo2.GetType().Name + ")");
            Console.WriteLine("");


            //3) declare 2 variables with var. Initialize them 20 and 20.5. Check types. 
            // Try to reinitialize by 20.5 and 20 (change values). What results are there?

            var pV1 = 20;
            var pV2 = 20.5;
            Console.WriteLine("declare 2 variables with var. Initialize them 20 and 20.5. Check types");
            Console.WriteLine("pV1 = " + pV1 + "(" + pV1.GetType().Name + ")");
            Console.WriteLine("pV2 = " + pV2 + "(" + pV2.GetType().Name + ")");

            pV1 = (int)20.5;
            pV2 = 20;
            Console.WriteLine("After swap-----------------------------------");
            Console.WriteLine("pV1 = " + pV1 + "(" + pV1.GetType().Name + ")");
            Console.WriteLine("pV2 = " + pV2 + "(" + pV2.GetType().Name + ")");
            Console.WriteLine("");


            // 4) declare variables of System.Int32 and System.Double.
            // Initialize them by values of i and d0. Is it possible?
            int pIn3 = i;
            double pDo3 = d0;


            if (true)
            {
                // 5) declare variables of int, char, double 
                // with names i, ch, do
                // is it possible?

                Console.WriteLine("It is not possible because it was declare before...");


                // it is impossible because they are already initialized


                // 6) change values of variables from 1)
                boo = true;
                ch = 'a';
                b = 2;
                sb = 3;
                sh = -30;
                ush = 110;
                i = -225;
                ui = 140;
                l = 301;
                ul = 10001;
                de = -33.2m;
                fl = 250.8f;
                d0 = 100000.7;

            }

            // 7)check values of variables from 1). Are they changed? Think, why
            Console.WriteLine("check values of variables from 1). Are they changed? Think, why");
            Console.WriteLine("After changed--------------------------------");
            PrintResult(boo, ch, b, sb, sh, ush, i, ui, l, ul, de, fl, d0);
            Console.WriteLine("");




            // 8) use implicit/ explicit conversion to convert variables from 1). 
            // Is it possible? 

            // Fix compilation errors (in case of impossible conversion commemt that line).
            // int -> char
            ch = (char)i;

            // bool -> short 
            // it can't be realize
            // sh = (short)boo;

            // double -> long
            l = (long)d0;

            // float -> char 
            ch = (char)fl;

            // int to char
            ch = (char)i;

            // decimal -> double
            d0 = (double)de;

            // byte -> uint
            ui = b;

            // ulong -> sbyte
            sb = (sbyte)ul;

            Console.WriteLine("use implicit/ explicit conversion to convert variables from 1)");
            PrintResult(boo, ch, b, sb, sh, ush, i, ui, l, ul, de, fl, d0);
            Console.WriteLine("");

            // 9) and reverse conversion with fixing compilation errors.

            // char -> int
            i = ch;

            // short  ->  bool
            // it can't be realize
            // boo = (bool)sh;

            // long -> double
            d0 = l;

            // char  -> float 
            fl = ch;

            // char to int
            i = ch;

            // double -> decimal
            de = (decimal)d0;

            // uint -> byte
            b = (byte)ui;

            // sbyte -> ulong
            ul = (ulong)sb;

            Console.WriteLine("and reverse conversion with fixing compilation errors.");
            PrintResult(boo, ch, b, sb, sh, ush, i, ui, l, ul, de, fl, d0);
            Console.WriteLine("");

            // 10) declare int nullable value. Initialize it with 'null'. 
            // Try to initialize variable i with 'null'. Is it possible?
            int? iNull = null;


            Console.ReadLine();
        }

        private static void PrintResult(bool boo, char ch, byte b, sbyte sb, short sh, ushort ush, int i, uint ui, long l, ulong ul, decimal de, float fl, double d0)
        {
            Console.WriteLine("boo = " + boo + "(" + boo.GetType().Name + ")");
            Console.WriteLine("ch  = " + ch + "(" + ch.GetType().Name + ")");
            Console.WriteLine("b   = " + b + "(" + b.GetType().Name + ")");
            Console.WriteLine("sb  = " + sb + "(" + sb.GetType().Name + ")");
            Console.WriteLine("sh  = " + sh + "(" + sh.GetType().Name + ")");
            Console.WriteLine("ush = " + ush + "(" + ush.GetType().Name + ")");
            Console.WriteLine("i   = " + i + "(" + i.GetType().Name + ")");
            Console.WriteLine("ui  = " + ui + "(" + ui.GetType().Name + ")");
            Console.WriteLine("l   = " + l + "(" + l.GetType().Name + ")");
            Console.WriteLine("ul  = " + ul + "(" + ul.GetType().Name + ")");
            Console.WriteLine("de  = " + de + "(" + de.GetType().Name + ")");
            Console.WriteLine("fl  = " + fl + "(" + fl.GetType().Name + ")");
            Console.WriteLine("d0  = " + d0 + "(" + d0.GetType().Name + ")");
        }
    }
}
