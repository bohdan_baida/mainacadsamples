﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharp_Net_module1_4_1_lab
{
    class Program
    {
        static void Main(string[] args)
        {
            // 9) declare object of OnlineShop 
            OnlineShop myOnlineShop = new OnlineShop();

            // 10) declare several objects of Customer
            Customer customer1 = new Customer("customer1");
            Customer customer2 = new Customer("customer2");

            // 11) subscribe method GotNewGoods() of every Customer instance 
            // to event NewGoodsInfo of object of OnlineShop
            myOnlineShop.CreateGoods += customer1.GotNewGoods;
            myOnlineShop.CreateGoods += customer2.GotNewGoods;


            // 12) invoke method NewGoods() of object of OnlineShop
            // discuss results
            myOnlineShop.NewGoods("TV");
            Console.ReadLine();

        }
    }
}
