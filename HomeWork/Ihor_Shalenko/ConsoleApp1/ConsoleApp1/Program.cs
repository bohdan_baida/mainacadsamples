﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            var carType = typeof(Car);
            Car myCar = (Car)Activator.CreateInstance(carType, "BMW", (decimal)10000);

            var props = carType.GetProperties();

            foreach (var prop in props)
            {
                Console.WriteLine($"Name: {prop.Name}; value: {prop.GetValue(myCar)}"); 
            }

            var propMillage = carType.GetField("millage", BindingFlags.NonPublic | BindingFlags.Instance);
            propMillage.SetValue(myCar, 0);

            var constructors = carType.GetConstructors();

            MaxConstructor maxConstructor = new MaxConstructor() { ConstructorParapCount = 0, ConstructorName = ""};
            foreach (var constr in constructors)
            {
                int paramCount = constr.GetParameters().ToList().Count;
                if (paramCount > maxConstructor.ConstructorParapCount)
                {
                    maxConstructor.ConstructorParapCount = paramCount;
                    maxConstructor.ConstructorName = constr.Name;
                    maxConstructor.Constructor = constr;
                }
            }

            Console.WriteLine($" MAX count: {maxConstructor.ConstructorParapCount}; constructor name:{maxConstructor.ConstructorName}");

            maxConstructor.Constructor.Invoke(myCar, new object[]{ "BMW", (decimal)10000});

            Console.ReadLine();
        }

        struct MaxConstructor
        {
            public string ConstructorName { get; set; }
            public int ConstructorParapCount { get; set; }
            public ConstructorInfo Constructor { get; set; }
        }
    }
}
