﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace airport
{
    public class Flight : ICloneable
    {
        private DateTime CommanDataArrival;
        private DateTime CommanDataDeparture;


        public FlightTypesEnum FlightType { get; set; }
        public AirportTerminalsEnum AirportTerminal { get; set; }
        public FlightStatusEnum FlightStatus { get; set; }

        public string NumFlight { get; set; }
        public DateTime DataArrival { get; set; }
        public TimeSpan TimeArrival { get; set; }
        public DateTime DataDeparture { get; set; }
        public TimeSpan TimeDeparture { get; set; }
        public TimeSpan TimeStartBoarding { get; set; }
        public string CityDeparture { get; set; }
        public string CityArrival { get; set; }
        public string Airline { get; set; }
        public string Gate { get; set; }


        public Flight()
        { }
        public Flight(FlightTypesEnum flightType, AirportTerminalsEnum airportTerminal, FlightStatusEnum flightStatus,
            string numFlight, DateTime dataArrival, TimeSpan timeArrival, DateTime dataDeparture, TimeSpan timeDeparture,
            TimeSpan timeStartBoarding, string cityDeparture, string cityArrival, string airline, string gate)
        {
            this.FlightType = flightType;
            this.AirportTerminal = airportTerminal;
            this.FlightStatus = flightStatus;
            this.NumFlight = numFlight;
            this.DataArrival = dataArrival;
            this.TimeArrival = timeArrival;
            this.DataDeparture = dataDeparture;
            this.TimeDeparture = timeDeparture;
            this.TimeStartBoarding = timeStartBoarding;
            this.CityDeparture = cityDeparture;
            this.CityArrival = cityArrival;
            this.Airline = airline;
            this.Gate = gate;

        }

        private void SetCommanDataArrival()
        {
            CommanDataArrival = DataArrival.Date + TimeArrival;
        }
        private void SetCommanDataDeparture()
        {
            CommanDataDeparture = DataDeparture.Date + TimeDeparture;
        }

        public void InformationAboutFlight()
        {
            Console.WriteLine("Information About Flight");
            Console.WriteLine($"Flight Type         - {FlightType}");
            Console.WriteLine($"Airport Terminal    - {AirportTerminal}");
            Console.WriteLine($"Flight Status       - {FlightStatus}");
            Console.WriteLine($"Num Flight          - {NumFlight}");
            Console.WriteLine($"Data Arrival        - {DataArrival}");
            Console.WriteLine($"Data Departure      - {DataDeparture}");
            Console.WriteLine($"Time Departure      - {TimeDeparture}");
            Console.WriteLine($"Time Start Boarding - {TimeStartBoarding}");
            Console.WriteLine($"City Departure      - {CityDeparture}");
            Console.WriteLine($"City Arrival        - {CityArrival}");
            Console.WriteLine($"Airline             - {Airline}");
            Console.WriteLine($"Gate                - {Gate}");
            Console.WriteLine();

        }
        public void SetNewData(Flight newDataFlight)
        {
            this.FlightType = newDataFlight.FlightType;
            this.AirportTerminal = newDataFlight.AirportTerminal;
            this.FlightStatus = newDataFlight.FlightStatus;
            this.NumFlight = newDataFlight.NumFlight;
            this.DataArrival = newDataFlight.DataArrival;
            this.TimeArrival = newDataFlight.TimeArrival;
            this.DataDeparture = newDataFlight.DataDeparture;
            this.TimeDeparture = newDataFlight.TimeDeparture;
            this.TimeStartBoarding = newDataFlight.TimeStartBoarding;
            this.CityDeparture = newDataFlight.CityDeparture;
            this.CityArrival = newDataFlight.CityArrival;
            this.Airline = newDataFlight.Airline;
            this.Gate = newDataFlight.Gate;

        }
        public static void PrintInputOptionParam(string nameParam)
        {
            switch (nameParam)
            {
                case "FlightType":
                    Console.WriteLine("Select Flight Types");
                    Console.WriteLine("1 - InFlight");
                    Console.WriteLine("2 - OutFlight");
                    Console.WriteLine("# - Exit");
                    break;
                case "AirportTerminal":
                    Console.WriteLine("Select Airport Terminal");
                    Console.WriteLine("A1");
                    Console.WriteLine("A2");
                    Console.WriteLine("A3");
                    Console.WriteLine("B1");
                    Console.WriteLine("B2");
                    Console.WriteLine("# - Exit");
                    break;
                case "FlightStatus":
                    Console.WriteLine("Select Flight Status");
                    Console.WriteLine("1 - CheckIn");
                    Console.WriteLine("2 - GateClosed");
                    Console.WriteLine("3 - Arrived");
                    Console.WriteLine("4 - DepartedAt");
                    Console.WriteLine("5 - Unknown");
                    Console.WriteLine("6 - Canceled");
                    Console.WriteLine("7 - ExpectedAt");
                    Console.WriteLine("8 - Delayed");
                    Console.WriteLine("9 - InFlight");

                    Console.WriteLine("# - Exit");
                    break;
                case "NumFlight":
                    Console.WriteLine("Enter Num Flight");
                    Console.WriteLine("# - Exit");
                    break;
                case "DataArrival":
                    Console.WriteLine("Enter Data Arrival (format dd.mm.yyyy)");
                    Console.WriteLine("# - Exit");

                    break;
                case "TimeArrival":
                    Console.WriteLine("Enter Time Arrival (format hh:mm)");
                    Console.WriteLine("# - Exit");

                    break;
                case "DataDeparture":
                    Console.WriteLine("Enter Data Departure (format dd.mm.yyyy)");
                    Console.WriteLine("# - Exit");

                    break;
                case "TimeDeparture":
                    Console.WriteLine("Enter Time Departure (format hh:mm)");
                    Console.WriteLine("# - Exit");

                    break;
                case "TimeStartBoarding":
                    Console.WriteLine("Enter Time Start Boarding (format hh:mm)");
                    Console.WriteLine("# - Exit");

                    break;
                case "CityDeparture":
                    Console.WriteLine("Enter City Departure");
                    Console.WriteLine("# - Exit");

                    break;
                case "CityArrival":
                    Console.WriteLine("Enter City Arrival");
                    Console.WriteLine("# - Exit");

                    break;
                case "Airline":
                    Console.WriteLine("Enter Airline");
                    Console.WriteLine("# - Exit");

                    break;
                case "Gate":
                    Console.WriteLine("Enter Gate");
                    Console.WriteLine("# - Exit");

                    break;
                default:
                    break;
            }
        }

        //public Flight Clone()
        //{
        //    Flight clonFlight = new Flight();

        //    clonFlight.FlightType = this.FlightType;
        //    clonFlight.AirportTerminal = this.AirportTerminal;
        //    clonFlight.FlightStatus = this.FlightStatus;
        //    clonFlight.NumFlight = this.NumFlight;
        //    clonFlight.DataArrival = this.DataArrival;
        //    clonFlight.TimeArrival = this.TimeArrival;
        //    clonFlight.DataDeparture = this.DataDeparture;
        //    clonFlight.TimeDeparture = this.TimeDeparture;
        //    clonFlight.TimeStartBoarding = this.TimeStartBoarding;
        //    clonFlight.CityDeparture = this.CityDeparture;
        //    clonFlight.CityArrival = this.CityArrival;
        //    clonFlight.Airline = this.Airline;
        //    clonFlight.Gate = this.Gate;

        //    return clonFlight;
        //}

        object ICloneable.Clone()
        {
            return this.MemberwiseClone();
        }

        public Flight Clone()
        {
            return (Flight)this.MemberwiseClone();
        }
    }
}
