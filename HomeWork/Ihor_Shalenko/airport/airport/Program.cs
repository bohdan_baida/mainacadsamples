﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace airport
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello. Welcome to the airport data input terminal.");

            AirportSchedule ourAirportSchedule = ReturnAirportSchedule();

            while (true)
            {
                Console.WriteLine("Select a function:");
                Console.WriteLine("1. View  of the flight information about arrivals"); // Просмотр информации о рейсе прилета
                Console.WriteLine("2. View  of the flight information about departures");
                Console.WriteLine("3. Add new flight");
                Console.WriteLine("4. Deleting flight");
                Console.WriteLine("5. Edit flight");
                Console.WriteLine("6. Find flight");
                Console.WriteLine("7. Emergency information");
                Console.WriteLine("#. Exit");


                string function = Console.ReadLine();
                string strTitle;
                switch (function)
                {
                    case "1":
                        strTitle = "Incoming Flight Information";
                        ourAirportSchedule.PrintInformationAboutSchedule(ourAirportSchedule.AirportFlights, strTitle, FlightTypesEnum.InFlight);
                        break;
                    case "2":
                        strTitle = "Outbound Flight Information";
                        ourAirportSchedule.PrintInformationAboutSchedule(ourAirportSchedule.AirportFlights, strTitle, FlightTypesEnum.OutFlight);
                        break;
                    case "3":
                        Flight newFlight = new Flight();
                        bool isSuccess;
                        СreateFlightFromConsole(newFlight, out isSuccess);
                        if (isSuccess == true)
                        {
                            ourAirportSchedule.AddFlight(newFlight);
                        }
                        break;
                    case "4":
                        Console.WriteLine("Please write num flight to delete");
                        ourAirportSchedule.RemoveFlight(Console.ReadLine());
                        break;
                    case "5":
                        Console.WriteLine("Please write num flight to edit");
                        bool flightExist;
                        Flight ourFlight = ourAirportSchedule.ReternFlight(Console.ReadLine(), out flightExist);
                        if (flightExist == true)
                        {
                            bool isSuccessE;
                            Flight copyFlight = ourFlight.Clone();
                            ChangeFlightFromConsole(copyFlight, out isSuccessE); 

                            if (isSuccessE == true)
                            {
                                ourFlight.SetNewData(copyFlight);
                            }
                        }

                        break;
                    case "6":
                        SelectTypeOfFilter(ourAirportSchedule);
                        break;
                    case "7":
                        Console.WriteLine("Alarm!!!!!!!!!!!!!!!!!");
                        break;
                    case "#":
                        return;
                    default:
                        break;
                }

                Console.WriteLine();
                Console.WriteLine("Please press Enter to continue...");
                Console.ReadLine();
                Console.Clear();
            }
        }

        static void СreateFlightFromConsole(Flight cnFlight, out bool isSuccess)
        {
            isSuccess = true;
            string resoltCR = "";
            bool goNext;

            goNext = false;
            while (goNext == false)
            {

                Flight.PrintInputOptionParam(nameof(cnFlight.FlightType));

                resoltCR = Console.ReadLine();
                switch (resoltCR)
                {
                    case "1":
                        cnFlight.FlightType = FlightTypesEnum.InFlight;
                        goNext = true;
                        break;
                    case "2":
                        cnFlight.FlightType = FlightTypesEnum.OutFlight;
                        goNext = true;
                        break;
                    case "#":
                        isSuccess = false;
                        return;
                    default:
                        break;

                }
            }

            goNext = false;
            while (goNext == false)
            {

                Flight.PrintInputOptionParam(nameof(cnFlight.AirportTerminal));

                resoltCR = Console.ReadLine();
                switch (resoltCR)
                {
                    case "A1":
                        cnFlight.AirportTerminal = AirportTerminalsEnum.A1;
                        goNext = true;
                        break;
                    case "A2":
                        cnFlight.AirportTerminal = AirportTerminalsEnum.A2;
                        goNext = true;
                        break;
                    case "A3":
                        cnFlight.AirportTerminal = AirportTerminalsEnum.A3;
                        goNext = true;
                        break;
                    case "B1":
                        cnFlight.AirportTerminal = AirportTerminalsEnum.B1;
                        goNext = true;
                        break;
                    case "B2":
                        cnFlight.AirportTerminal = AirportTerminalsEnum.B2;
                        goNext = true;
                        break;
                    case "#":
                        isSuccess = false;
                        return;
                    default:
                        break;


                }
            }

            goNext = false;
            while (goNext == false)
            {

                Flight.PrintInputOptionParam(nameof(cnFlight.FlightStatus));

                resoltCR = Console.ReadLine();
                switch (resoltCR)
                {
                    case "1":
                        cnFlight.FlightStatus = FlightStatusEnum.Arrived;
                        goNext = true;
                        break;
                    case "2":
                        cnFlight.FlightStatus = FlightStatusEnum.GateClosed;
                        goNext = true;
                        break;
                    case "3":
                        cnFlight.FlightStatus = FlightStatusEnum.Arrived;
                        goNext = true;
                        break;
                    case "4":
                        cnFlight.FlightStatus = FlightStatusEnum.DepartedAt;
                        goNext = true;
                        break;
                    case "5":
                        cnFlight.FlightStatus = FlightStatusEnum.Unknown;
                        goNext = true;
                        break;
                    case "6":
                        cnFlight.FlightStatus = FlightStatusEnum.Canceled;
                        goNext = true;
                        break;
                    case "7":
                        cnFlight.FlightStatus = FlightStatusEnum.ExpectedAt;
                        goNext = true;
                        break;
                    case "8":
                        cnFlight.FlightStatus = FlightStatusEnum.Delayed;
                        goNext = true;
                        break;
                    case "9":
                        cnFlight.FlightStatus = FlightStatusEnum.InFlight;
                        goNext = true;
                        break;
                    case "#":
                        isSuccess = false;
                        return;
                    default:
                        break;


                }
            }

            goNext = false;
            while (goNext == false)
            {

                Flight.PrintInputOptionParam(nameof(cnFlight.NumFlight));

                resoltCR = Console.ReadLine();
                switch (resoltCR)
                {

                    case "#":
                        isSuccess = false;
                        return;
                    default:
                        cnFlight.NumFlight = resoltCR;
                        goNext = true;
                        break;


                }
            }

            goNext = false;
            while (goNext == false)
            {

                Flight.PrintInputOptionParam(nameof(cnFlight.DataArrival));

                resoltCR = Console.ReadLine();
                switch (resoltCR)
                {

                    case "#":
                        isSuccess = false;
                        return;
                    default:
                        DateTime thisDataArrival;
                        if (DateTime.TryParse(resoltCR, out thisDataArrival) == true)
                        {
                            cnFlight.DataArrival = thisDataArrival;
                            goNext = true;

                        }
                        else
                        {
                            Console.WriteLine("Incorrect ...");
                        }
                        break;

                }
            }

            goNext = false;
            while (goNext == false)
            {

                Flight.PrintInputOptionParam(nameof(cnFlight.TimeArrival));

                resoltCR = Console.ReadLine();
                switch (resoltCR)
                {

                    case "#":
                        isSuccess = false;
                        return;
                    default:
                        TimeSpan thisTimeArrival;
                        if (TimeSpan.TryParse(resoltCR, out thisTimeArrival) == true)
                        {
                            cnFlight.TimeArrival = thisTimeArrival;
                            goNext = true;

                        }
                        else
                        {
                            Console.WriteLine("Incorrect ...");
                        }
                        break;

                }
            }

            goNext = false;
            while (goNext == false)
            {

                Flight.PrintInputOptionParam(nameof(cnFlight.DataDeparture));

                resoltCR = Console.ReadLine();
                switch (resoltCR)
                {

                    case "#":
                        isSuccess = false;
                        return;
                    default:
                        DateTime thisDataDeparture;
                        if (DateTime.TryParse(resoltCR, out thisDataDeparture) == true)
                        {
                            cnFlight.DataDeparture = thisDataDeparture;
                            goNext = true;

                        }
                        else
                        {
                            Console.WriteLine("Incorrect ...");
                        }
                        break;

                }
            }

            goNext = false;
            while (goNext == false)
            {

                Flight.PrintInputOptionParam(nameof(cnFlight.TimeDeparture));

                resoltCR = Console.ReadLine();
                switch (resoltCR)
                {

                    case "#":
                        isSuccess = false;
                        return;
                    default:
                        TimeSpan thisTimeDeparture;
                        if (TimeSpan.TryParse(resoltCR, out thisTimeDeparture) == true)
                        {
                            cnFlight.TimeDeparture = thisTimeDeparture;
                            goNext = true;

                        }
                        else
                        {
                            Console.WriteLine("Incorrect ...");
                        }
                        break;

                }
            }

            goNext = false;
            while (goNext == false)
            {

                Flight.PrintInputOptionParam(nameof(cnFlight.TimeStartBoarding));

                resoltCR = Console.ReadLine();
                switch (resoltCR)
                {

                    case "#":
                        isSuccess = false;
                        return;
                    default:
                        TimeSpan thisTimeStartBoarding;
                        if (TimeSpan.TryParse(resoltCR, out thisTimeStartBoarding) == true)
                        {
                            cnFlight.TimeStartBoarding = thisTimeStartBoarding;
                            goNext = true;

                        }
                        else
                        {
                            Console.WriteLine("Incorrect ...");
                        }
                        break;

                }
            }

            goNext = false;
            while (goNext == false)
            {

                Flight.PrintInputOptionParam(nameof(cnFlight.CityDeparture));


                resoltCR = Console.ReadLine();
                switch (resoltCR)
                {

                    case "#":
                        isSuccess = false;
                        return;
                    default:
                        cnFlight.CityDeparture = resoltCR;
                        goNext = true;
                        break;


                }
            }

            goNext = false;
            while (goNext == false)
            {

                Flight.PrintInputOptionParam(nameof(cnFlight.CityArrival));


                resoltCR = Console.ReadLine();
                switch (resoltCR)
                {

                    case "#":
                        isSuccess = false;
                        return;
                    default:
                        cnFlight.CityArrival = resoltCR;
                        goNext = true;
                        break;


                }
            }

            goNext = false;
            while (goNext == false)
            {

                Flight.PrintInputOptionParam(nameof(cnFlight.Airline));

                resoltCR = Console.ReadLine();
                switch (resoltCR)
                {

                    case "#":
                        isSuccess = false;
                        return;
                    default:
                        cnFlight.Airline = resoltCR;
                        goNext = true;
                        break;


                }
            }

            goNext = false;
            while (goNext == false)
            {

                Flight.PrintInputOptionParam(nameof(cnFlight.Gate));

                resoltCR = Console.ReadLine();
                switch (resoltCR)
                {

                    case "#":
                        isSuccess = false;
                        return;
                    default:
                        cnFlight.Gate = resoltCR;
                        goNext = true;
                        break;


                }
            }

            cnFlight.InformationAboutFlight();
            Console.WriteLine();

            goNext = false;
            while (goNext == false)
            {
                Console.WriteLine("Everything correct?");
                Console.WriteLine("1 - Add new Flight");
                Console.WriteLine("# - Exit");

                resoltCR = Console.ReadLine();
                switch (resoltCR)
                {
                    case "1":
                        goNext = true;
                        break;
                    case "#":
                        isSuccess = false;
                        return;
                    default:
                        break;

                }
            }

        }
        static void ChangeFlightFromConsole(Flight cnFlight, out bool isSuccess)
        {
            isSuccess = true;
            string resoltCR = "";
            bool goNext;

            goNext = false;
            Console.WriteLine("If you want to skip a step, press Entre");
            while (goNext == false)
            {

                Flight.PrintInputOptionParam(nameof(cnFlight.FlightType));

                resoltCR = Console.ReadLine();
                switch (resoltCR)
                {
                    case "1":
                        cnFlight.FlightType = FlightTypesEnum.InFlight;
                        goNext = true;
                        break;
                    case "2":
                        cnFlight.FlightType = FlightTypesEnum.OutFlight;
                        goNext = true;
                        break;
                    case "#":
                        isSuccess = false;
                        return;
                    case "":
                        goNext = true;
                        break;
                    default:
                        break;

                }
            }

            goNext = false;
            while (goNext == false)
            {

                Flight.PrintInputOptionParam(nameof(cnFlight.AirportTerminal));

                resoltCR = Console.ReadLine();
                switch (resoltCR)
                {
                    case "A1":
                        cnFlight.AirportTerminal = AirportTerminalsEnum.A1;
                        goNext = true;
                        break;
                    case "A2":
                        cnFlight.AirportTerminal = AirportTerminalsEnum.A2;
                        goNext = true;
                        break;
                    case "A3":
                        cnFlight.AirportTerminal = AirportTerminalsEnum.A3;
                        goNext = true;
                        break;
                    case "B1":
                        cnFlight.AirportTerminal = AirportTerminalsEnum.B1;
                        goNext = true;
                        break;
                    case "B2":
                        cnFlight.AirportTerminal = AirportTerminalsEnum.B2;
                        goNext = true;
                        break;
                    case "#":
                        isSuccess = false;
                        return;
                    case "":
                        goNext = true;
                        break;

                    default:
                        break;


                }
            }

            goNext = false;
            while (goNext == false)
            {

                Flight.PrintInputOptionParam(nameof(cnFlight.FlightStatus));

                resoltCR = Console.ReadLine();
                switch (resoltCR)
                {
                    case "1":
                        cnFlight.FlightStatus = FlightStatusEnum.Arrived;
                        goNext = true;
                        break;
                    case "2":
                        cnFlight.FlightStatus = FlightStatusEnum.GateClosed;
                        goNext = true;
                        break;
                    case "3":
                        cnFlight.FlightStatus = FlightStatusEnum.Arrived;
                        goNext = true;
                        break;
                    case "4":
                        cnFlight.FlightStatus = FlightStatusEnum.DepartedAt;
                        goNext = true;
                        break;
                    case "5":
                        cnFlight.FlightStatus = FlightStatusEnum.Unknown;
                        goNext = true;
                        break;
                    case "6":
                        cnFlight.FlightStatus = FlightStatusEnum.Canceled;
                        goNext = true;
                        break;
                    case "7":
                        cnFlight.FlightStatus = FlightStatusEnum.ExpectedAt;
                        goNext = true;
                        break;
                    case "8":
                        cnFlight.FlightStatus = FlightStatusEnum.Delayed;
                        goNext = true;
                        break;
                    case "9":
                        cnFlight.FlightStatus = FlightStatusEnum.InFlight;
                        goNext = true;
                        break;
                    case "#":
                        isSuccess = false;
                        return;
                    case "":
                        goNext = true;
                        break;

                    default:
                        break;


                }
            }

            goNext = false;
            while (goNext == false)
            {

                Flight.PrintInputOptionParam(nameof(cnFlight.NumFlight));

                resoltCR = Console.ReadLine();
                switch (resoltCR)
                {

                    case "#":
                        isSuccess = false;
                        return;
                    case "":
                        goNext = true;
                        break;

                    default:
                        cnFlight.NumFlight = resoltCR;
                        goNext = true;
                        break;


                }
            }

            goNext = false;
            while (goNext == false)
            {

                Flight.PrintInputOptionParam(nameof(cnFlight.DataArrival));

                resoltCR = Console.ReadLine();
                switch (resoltCR)
                {

                    case "#":
                        isSuccess = false;
                        return;
                    case "":
                        goNext = true;
                        break;

                    default:
                        DateTime thisDataArrival;
                        if (DateTime.TryParse(resoltCR, out thisDataArrival) == true)
                        {
                            cnFlight.DataArrival = thisDataArrival;
                            goNext = true;

                        }
                        else
                        {
                            Console.WriteLine("Incorrect ...");
                        }
                        break;

                }
            }

            goNext = false;
            while (goNext == false)
            {

                Flight.PrintInputOptionParam(nameof(cnFlight.TimeArrival));

                resoltCR = Console.ReadLine();
                switch (resoltCR)
                {

                    case "#":
                        isSuccess = false;
                        return;
                    case "":
                        goNext = true;
                        break;

                    default:
                        TimeSpan thisTimeArrival;
                        if (TimeSpan.TryParse(resoltCR, out thisTimeArrival) == true)
                        {
                            cnFlight.TimeArrival = thisTimeArrival;
                            goNext = true;

                        }
                        else
                        {
                            Console.WriteLine("Incorrect ...");
                        }
                        break;

                }
            }

            goNext = false;
            while (goNext == false)
            {

                Flight.PrintInputOptionParam(nameof(cnFlight.DataDeparture));

                resoltCR = Console.ReadLine();
                switch (resoltCR)
                {

                    case "#":
                        isSuccess = false;
                        return;
                    case "":
                        goNext = true;
                        break;

                    default:
                        DateTime thisDataDeparture;
                        if (DateTime.TryParse(resoltCR, out thisDataDeparture) == true)
                        {
                            cnFlight.DataDeparture = thisDataDeparture;
                            goNext = true;

                        }
                        else
                        {
                            Console.WriteLine("Incorrect ...");
                        }
                        break;

                }
            }

            goNext = false;
            while (goNext == false)
            {

                Flight.PrintInputOptionParam(nameof(cnFlight.TimeDeparture));

                resoltCR = Console.ReadLine();
                switch (resoltCR)
                {

                    case "#":
                        isSuccess = false;
                        return;
                    case "":
                        goNext = true;
                        break;

                    default:
                        TimeSpan thisTimeDeparture;
                        if (TimeSpan.TryParse(resoltCR, out thisTimeDeparture) == true)
                        {
                            cnFlight.TimeDeparture = thisTimeDeparture;
                            goNext = true;

                        }
                        else
                        {
                            Console.WriteLine("Incorrect ...");
                        }
                        break;

                }
            }

            goNext = false;
            while (goNext == false)
            {

                Flight.PrintInputOptionParam(nameof(cnFlight.TimeStartBoarding));

                resoltCR = Console.ReadLine();
                switch (resoltCR)
                {

                    case "#":
                        isSuccess = false;
                        return;
                    case "":
                        goNext = true;
                        break;

                    default:
                        TimeSpan thisTimeStartBoarding;
                        if (TimeSpan.TryParse(resoltCR, out thisTimeStartBoarding) == true)
                        {
                            cnFlight.TimeStartBoarding = thisTimeStartBoarding;
                            goNext = true;

                        }
                        else
                        {
                            Console.WriteLine("Incorrect ...");
                        }
                        break;

                }
            }

            goNext = false;
            while (goNext == false)
            {

                Flight.PrintInputOptionParam(nameof(cnFlight.CityDeparture));


                resoltCR = Console.ReadLine();
                switch (resoltCR)
                {

                    case "#":
                        isSuccess = false;
                        return;
                    case "":
                        goNext = true;
                        break;

                    default:
                        cnFlight.CityDeparture = resoltCR;
                        goNext = true;
                        break;


                }
            }

            goNext = false;
            while (goNext == false)
            {

                Flight.PrintInputOptionParam(nameof(cnFlight.CityArrival));


                resoltCR = Console.ReadLine();
                switch (resoltCR)
                {

                    case "#":
                        isSuccess = false;
                        return;
                    case "":
                        goNext = true;
                        break;

                    default:
                        cnFlight.CityArrival = resoltCR;
                        goNext = true;
                        break;


                }
            }

            goNext = false;
            while (goNext == false)
            {

                Flight.PrintInputOptionParam(nameof(cnFlight.Airline));

                resoltCR = Console.ReadLine();
                switch (resoltCR)
                {

                    case "#":
                        isSuccess = false;
                        return;
                    case "":
                        goNext = true;
                        break;

                    default:
                        cnFlight.Airline = resoltCR;
                        goNext = true;
                        break;


                }
            }

            goNext = false;
            while (goNext == false)
            {

                Flight.PrintInputOptionParam(nameof(cnFlight.Gate));

                resoltCR = Console.ReadLine();
                switch (resoltCR)
                {

                    case "#":
                        isSuccess = false;
                        return;
                    case "":
                        goNext = true;
                        break;

                    default:
                        cnFlight.Gate = resoltCR;
                        goNext = true;
                        break;


                }
            }

            cnFlight.InformationAboutFlight();
            Console.WriteLine();

            goNext = false;
            while (goNext == false)
            {
                Console.WriteLine("Everything correct?");
                Console.WriteLine("1 - Change the Flight");
                Console.WriteLine("# - Exit");

                resoltCR = Console.ReadLine();
                switch (resoltCR)
                {
                    case "1":
                        goNext = true;
                        break;
                    case "#":
                        isSuccess = false;
                        return;
                    default:
                        break;

                }
            }

        }
        static void SelectTypeOfFilter(AirportSchedule ourAirportSchedule)
        {
            Console.WriteLine("Select a filter:");
            Console.WriteLine("1. Search by the flight number");
            Console.WriteLine("2. Search by the time of arrival");
            Console.WriteLine("3. Search by the arrival port");
            Console.WriteLine("4. Search by the departure port");
            Console.WriteLine("5. Search of the flight which is the nearest (1 hour) to the specified time to the specified port");
            Console.WriteLine("6. Search of the flight which is the nearest (1 hour) to the specified time from the specified port");
            Console.WriteLine("#. Exit");


            string function = Console.ReadLine();

            Flight ourFlight;
            List<Flight> thAirportFlights;
            switch (function)
            {
                case "1":
                    Console.WriteLine("Write number");
                    bool flightExist1;
                    ourFlight = ourAirportSchedule.ReternFlight(Console.ReadLine(), out flightExist1);
                    if (flightExist1 == true)
                    {
                        thAirportFlights = new List<Flight>();
                        thAirportFlights.Add(ourFlight);
                        ourAirportSchedule.PrintInformationAboutSchedule(thAirportFlights, "Search by the flight number");
                    }

                    break;
                case "2":

                    Flight.PrintInputOptionParam("DataArrival");
                    DateTime DataArrival2;
                    if (DateTime.TryParse(Console.ReadLine(), out DataArrival2) != true)
                    {
                        Console.WriteLine("Incorrect ...");
                        break;
                    }

                    Flight.PrintInputOptionParam("TimeArrival");
                    TimeSpan TimeArrival2;
                    if (TimeSpan.TryParse(Console.ReadLine(), out TimeArrival2) != true)
                    {
                        Console.WriteLine("Incorrect ...");
                        break;
                    }

                    bool flightsExist2;
                    thAirportFlights = ourAirportSchedule.ReternFlightByDataTime(DataArrival2, TimeArrival2, out flightsExist2);
                        if (flightsExist2 == true)
                            ourAirportSchedule.PrintInformationAboutSchedule(thAirportFlights, "Search by the time of arrival");
                    
                    break;
                case "3":
                    Flight.PrintInputOptionParam("CityArrival");

                    bool flightsExist3;
                    thAirportFlights = ourAirportSchedule.ReternFlightByCityArrival(Console.ReadLine(), out flightsExist3);
                    if (flightsExist3 == true)
                        ourAirportSchedule.PrintInformationAboutSchedule(thAirportFlights, "Search by the arrival port");

                    break;
                case "4":
                    Flight.PrintInputOptionParam("CityDeparture");

                    bool flightsExist4;
                    thAirportFlights = ourAirportSchedule.ReternFlightByCityArrival(Console.ReadLine(), out flightsExist4);
                    if (flightsExist4 == true)
                        ourAirportSchedule.PrintInformationAboutSchedule(thAirportFlights, "Search by the departure port");
                    break;
                case "5":

                    Flight.PrintInputOptionParam("DataArrival");
                    DateTime DataArrival5;
                    if (DateTime.TryParse(Console.ReadLine(), out DataArrival5) != true)
                    {
                        Console.WriteLine("Incorrect ...");
                        break;
                    }

                    Flight.PrintInputOptionParam("TimeArrival");
                    TimeSpan TimeArrival5;
                    if (TimeSpan.TryParse(Console.ReadLine(), out TimeArrival5) != true)
                    {
                        Console.WriteLine("Incorrect ...");
                        break;
                    }

                    Flight.PrintInputOptionParam("CityArrival");

                    bool flightsExist5;
                    thAirportFlights = ourAirportSchedule.ReternFlightByDataTimePMHourCityArrival(DataArrival5, TimeArrival5, 1, Console.ReadLine(), out flightsExist5);
                    if (flightsExist5 == true)
                        ourAirportSchedule.PrintInformationAboutSchedule(thAirportFlights, "Search of the flight which is the nearest (1 hour) to the specified time to the specified port");

                    break;
                case "6":
                    Flight.PrintInputOptionParam("DataDeparture");
                    DateTime DataDeparture6;
                    if (DateTime.TryParse(Console.ReadLine(), out DataDeparture6) != true)
                    {
                        Console.WriteLine("Incorrect ...");
                        break;
                    }

                    Flight.PrintInputOptionParam("TimeDeparture");
                    TimeSpan TimeDeparture6;
                    if (TimeSpan.TryParse(Console.ReadLine(), out TimeDeparture6) != true)
                    {
                        Console.WriteLine("Incorrect ...");
                        break;
                    }

                    Flight.PrintInputOptionParam("CityDeparture");

                    bool flightsExist6;
                    thAirportFlights = ourAirportSchedule.ReternFlightByDataTimePMHourCityDeparture(DataDeparture6, TimeDeparture6, 1, Console.ReadLine(), out flightsExist6);
                    if (flightsExist6 == true)
                        ourAirportSchedule.PrintInformationAboutSchedule(thAirportFlights, "Search of the flight which is the nearest (1 hour) to the specified time from the specified port");

                    break;
                case "#":
                    return;
                default:
                    break;
            }
        }
        static AirportSchedule ReturnAirportSchedule()
        {

            AirportSchedule thisAirportSchedule = new AirportSchedule();

            Flight testFlight = new Flight();
            testFlight.FlightType = FlightTypesEnum.InFlight;
            testFlight.AirportTerminal = AirportTerminalsEnum.A1;
            testFlight.FlightStatus = FlightStatusEnum.Arrived;
            testFlight.NumFlight = "F45512";
            testFlight.DataArrival = DateTime.Now;
            testFlight.TimeArrival = DateTime.Now.TimeOfDay;
            testFlight.DataDeparture = DateTime.Now;
            testFlight.TimeDeparture = DateTime.Now.TimeOfDay;
            testFlight.TimeStartBoarding = DateTime.Now.TimeOfDay;
            testFlight.CityDeparture = "Kyiv";
            testFlight.CityArrival = "not Kyiv";
            testFlight.Airline = "MUUUU123456789";
            testFlight.Gate = "21";
            // Прописать процедуру загрузки из файла
            thisAirportSchedule.AddFlight(testFlight, false);

            testFlight = new Flight();
            testFlight.FlightType = FlightTypesEnum.InFlight;
            testFlight.AirportTerminal = AirportTerminalsEnum.A2;
            testFlight.FlightStatus = FlightStatusEnum.Arrived;
            testFlight.NumFlight = "Y12";
            testFlight.DataArrival = DateTime.Now;
            testFlight.TimeArrival = DateTime.Now.TimeOfDay;
            testFlight.DataDeparture = DateTime.Now;
            testFlight.TimeDeparture = DateTime.Now.TimeOfDay;
            testFlight.TimeStartBoarding = DateTime.Now.TimeOfDay;
            testFlight.CityDeparture = "Kyiv";
            testFlight.CityArrival = "not Kyiv";
            testFlight.Airline = "MUUUU123456789";
            testFlight.Gate = "23";
            // Прописать процедуру загрузки из файла
            thisAirportSchedule.AddFlight(testFlight, false);

            testFlight = new Flight();
            testFlight.FlightType = FlightTypesEnum.OutFlight;
            testFlight.AirportTerminal = AirportTerminalsEnum.B1;
            testFlight.FlightStatus = FlightStatusEnum.Arrived;
            testFlight.NumFlight = "C66";
            testFlight.DataArrival = DateTime.Now;
            testFlight.TimeArrival = DateTime.Now.TimeOfDay;
            testFlight.DataDeparture = DateTime.Now;
            testFlight.TimeDeparture = DateTime.Now.TimeOfDay;
            testFlight.TimeStartBoarding = DateTime.Now.TimeOfDay;
            testFlight.CityDeparture = "Kyiv";
            testFlight.CityArrival = "not Kyiv";
            testFlight.Airline = "F2";
            testFlight.Gate = "24";
            // Прописать процедуру загрузки из файла
            thisAirportSchedule.AddFlight(testFlight, false);

            testFlight = new Flight();
            testFlight.FlightType = FlightTypesEnum.OutFlight;
            testFlight.AirportTerminal = AirportTerminalsEnum.A2;
            testFlight.FlightStatus = FlightStatusEnum.Arrived;
            testFlight.NumFlight = "L00";
            testFlight.DataArrival = DateTime.Now;
            testFlight.TimeArrival = DateTime.Now.TimeOfDay;
            testFlight.DataDeparture = DateTime.Now;
            testFlight.TimeDeparture = DateTime.Now.TimeOfDay;
            testFlight.TimeStartBoarding = DateTime.Now.TimeOfDay;
            testFlight.CityDeparture = "Kyiv";
            testFlight.CityArrival = "not Kyiv";
            testFlight.Airline = "F1";
            testFlight.Gate = "26";
            // Прописать процедуру загрузки из файла
            thisAirportSchedule.AddFlight(testFlight, false);


            return thisAirportSchedule;
        }
    }
}
