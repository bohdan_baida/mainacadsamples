﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace airport
{
    public enum FlightTypesEnum
    {
        InFlight,
        OutFlight
    }
    public enum AirportTerminalsEnum
    {
        A1,
        A2,
        A3,
        B1,
        B2
    }
    public enum FlightStatusEnum
    {
        CheckIn,
        GateClosed,
        Arrived,
        DepartedAt,
        Unknown,
        Canceled,
        ExpectedAt,
        Delayed, 
        InFlight
    }
}
