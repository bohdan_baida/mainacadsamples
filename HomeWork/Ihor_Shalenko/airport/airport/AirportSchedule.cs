﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace airport
{
    class AirportSchedule
    {
        public List<Flight> AirportFlights { get; }
        private int AirportTerminalSize, FlightStatusSize, NumFlightSize, DataArrivalSize,
            TimeArrivalSize, DataDepartureSize, TimeDepartureSize, TimeStartBoardingSize, 
            CityDepartureSize, CityArrivalSize, AirlineSize, GateSize, FlightTypeSize;

        public AirportSchedule()
        {
            AirportFlights = new List<Flight>();
        }

        public void AddFlight(Flight flight, bool vInfToADD = true)
        {
            bool flightExist;
            Flight oldFlight = ReternFlight(flight.NumFlight, out flightExist);

            if (flightExist == true)
            {
                Console.WriteLine($"Flight {flight.NumFlight} is exist!");
                oldFlight.InformationAboutFlight();
            }
            else
            {
                AirportFlights.Add(flight);
                if(vInfToADD == true)
                    Console.WriteLine($"Flight {flight.NumFlight} is add to Airport schedule!");
            }


        }
        public void RemoveFlight(string NumFlight)
        {
            bool flightExist;
            Flight ourFlight = ReternFlight(NumFlight, out flightExist);
            if (flightExist == true)
            {
                AirportFlights.Remove(ourFlight);
                Console.WriteLine($"Flight {ourFlight.NumFlight} is remowed!");
            }
            else
            {
                Console.WriteLine($"Flight {NumFlight} is not remowed. Flight {NumFlight} is not found in Airport schedule!");
            }
        }
        public void ChangeFlight(Flight flight)
        {

        }

        public void PrintInformationAboutSchedule(IList<Flight> thisAirportFlights, string strTitle, FlightTypesEnum? typeFlight = null)
        {
            FillUpSizeClassMembers();

            string strSeparator = "", strThisFlight = "", strThisParam = "", strTableTitle = "";

            CreateStrTableTitlt(ref strTableTitle);

            int allSize = GetAllSize();

            int sizeTitle = strTitle.Length + 2;
            if (sizeTitle > allSize)
            {
                allSize = sizeTitle;
            }

            CreateStrTitlt(ref strTitle, allSize, sizeTitle);
            CreateStrSeparator(ref strSeparator, allSize);

            Console.WriteLine(strSeparator);
            Console.WriteLine(strTitle);
            Console.WriteLine(strSeparator);
            Console.WriteLine();
            Console.WriteLine(strSeparator);
            Console.WriteLine(strTableTitle);

            Console.WriteLine(strSeparator);
            bool haveAPrint = false;
            foreach (var AirportFlight in thisAirportFlights)
            {
                if (typeFlight != null)
                {                
                if (AirportFlight.FlightType != typeFlight)
                    continue;
                }

                CreateStrThisFlight(ref strThisFlight, strThisParam, AirportFlight);
                    Console.WriteLine(strThisFlight);
                    Console.WriteLine(strSeparator);

                    haveAPrint = true;
            }

            if (thisAirportFlights == null || haveAPrint == false)
            {
                Console.WriteLine(strSeparator);
                Console.WriteLine("Sorry, schedule is empty...");
                return;
            }

            Console.WriteLine();

        }

        public void SortAirportSchedule() // написать сортировку
        {
            if (AirportFlights == null)
            {
                return;
            }

        }

        private void CreateStrTitlt(ref string strTitle, int allSize, int sizeTitle)
        {
  

            int addToSizeTitle = (allSize - sizeTitle - 2) / 2;
            for (int i = 0; i < addToSizeTitle; i++)
            {
                strTitle = " " + strTitle;
            }

            while (allSize - 2 > strTitle.Length)
            {
                strTitle = strTitle + " ";
            }

            strTitle = "|" + strTitle + "|";

        }
        private void CreateStrSeparator(ref string strSeparator, int allSize)
        {

            for (int i = 0; i < allSize; i++)
            {
                strSeparator = strSeparator + "*";
            }

        }
        private void CreateStrTableTitlt(ref string strTableTitle)
        {
            string strThisParam = "";
            strTableTitle = "|";

            //var properties = typeof(Flight).GetProperties().Select(c => c.Name);

            strThisParam = "Flight Type";
            CreateStrThisParam(ref strThisParam, FlightTypeSize);
            CheckSize(strThisParam.Length - 1, ref FlightTypeSize);
            strTableTitle = strTableTitle + strThisParam;

            strThisParam = "Airport Terminal";
            CreateStrThisParam(ref strThisParam, AirportTerminalSize);
            CheckSize(strThisParam.Length - 1, ref AirportTerminalSize);
            strTableTitle = strTableTitle + strThisParam;

            strThisParam = "Flight Status";
            CreateStrThisParam(ref strThisParam, FlightStatusSize);
            CheckSize(strThisParam.Length - 1, ref FlightStatusSize);
            strTableTitle = strTableTitle + strThisParam;

            strThisParam = "Num Flight";
            CreateStrThisParam(ref strThisParam, NumFlightSize);
            CheckSize(strThisParam.Length - 1, ref NumFlightSize);
            strTableTitle = strTableTitle + strThisParam;

            strThisParam = "DataArrival";
            CreateStrThisParam(ref strThisParam, DataArrivalSize);
            CheckSize(strThisParam.Length - 1, ref DataArrivalSize);
            strTableTitle = strTableTitle + strThisParam;

            strThisParam = "Time Arrival";
            CreateStrThisParam(ref strThisParam, TimeArrivalSize);
            CheckSize(strThisParam.Length - 1, ref TimeArrivalSize);
            strTableTitle = strTableTitle + strThisParam;

            strThisParam = "Data Departure";
            CreateStrThisParam(ref strThisParam, DataDepartureSize);
            CheckSize(strThisParam.Length - 1, ref DataDepartureSize);
            strTableTitle = strTableTitle + strThisParam;

            strThisParam = "Time Departure";
            CreateStrThisParam(ref strThisParam, TimeDepartureSize);
            CheckSize(strThisParam.Length - 1, ref TimeDepartureSize);
            strTableTitle = strTableTitle + strThisParam;

            strThisParam = "Time Start Boarding";
            CreateStrThisParam(ref strThisParam, TimeStartBoardingSize);
            CheckSize(strThisParam.Length - 1, ref TimeStartBoardingSize);
            strTableTitle = strTableTitle + strThisParam;

            strThisParam = "City Departure";
            CreateStrThisParam(ref strThisParam, CityDepartureSize);
            CheckSize(strThisParam.Length - 1, ref CityDepartureSize);
            strTableTitle = strTableTitle + strThisParam;

            strThisParam = "City Arrival";
            CreateStrThisParam(ref strThisParam, CityArrivalSize);
            CheckSize(strThisParam.Length - 1, ref CityArrivalSize);
            strTableTitle = strTableTitle + strThisParam;

            strThisParam = "Airline";
            CreateStrThisParam(ref strThisParam, AirlineSize);
            CheckSize(strThisParam.Length - 1, ref AirlineSize);
            strTableTitle = strTableTitle + strThisParam;

            strThisParam = "Gate";
            CreateStrThisParam(ref strThisParam, GateSize);
            CheckSize(strThisParam.Length - 1, ref GateSize);
            strTableTitle = strTableTitle + strThisParam;

        }

        private void CreateStrThisFlight(ref string strThisFlight, string strThisParam, Flight AirportFlight)
        {

            strThisFlight = "|";

            strThisParam = AirportFlight.FlightType.ToString();
            CreateStrThisParam(ref strThisParam, FlightTypeSize);
            strThisFlight = strThisFlight + strThisParam;

            strThisParam = AirportFlight.AirportTerminal.ToString();
            CreateStrThisParam(ref strThisParam, AirportTerminalSize);
            strThisFlight = strThisFlight + strThisParam;

            strThisParam = AirportFlight.FlightStatus.ToString();
            CreateStrThisParam(ref strThisParam, FlightStatusSize);
            strThisFlight = strThisFlight + strThisParam;

            strThisParam = AirportFlight.NumFlight.ToString();
            CreateStrThisParam(ref strThisParam, NumFlightSize);
            strThisFlight = strThisFlight + strThisParam;

            strThisParam = AirportFlight.DataArrival.ToString("d");
            CreateStrThisParam(ref strThisParam, DataArrivalSize);
            strThisFlight = strThisFlight + strThisParam;

            strThisParam = AirportFlight.TimeArrival.ToString(@"hh\:mm\:ss");
            CreateStrThisParam(ref strThisParam, TimeArrivalSize);
            strThisFlight = strThisFlight + strThisParam;

            strThisParam = AirportFlight.DataDeparture.ToString("d");
            CreateStrThisParam(ref strThisParam, DataDepartureSize);
            strThisFlight = strThisFlight + strThisParam;

            strThisParam = AirportFlight.TimeDeparture.ToString(@"hh\:mm\:ss");
            CreateStrThisParam(ref strThisParam, TimeDepartureSize);
            strThisFlight = strThisFlight + strThisParam;

            strThisParam = AirportFlight.TimeStartBoarding.ToString(@"hh\:mm\:ss");
            CreateStrThisParam(ref strThisParam, TimeStartBoardingSize);
            strThisFlight = strThisFlight + strThisParam;

            strThisParam = AirportFlight.CityDeparture.ToString();
            CreateStrThisParam(ref strThisParam, CityDepartureSize);
            strThisFlight = strThisFlight + strThisParam;

            strThisParam = AirportFlight.CityArrival.ToString();
            CreateStrThisParam(ref strThisParam, CityArrivalSize);
            strThisFlight = strThisFlight + strThisParam;

            strThisParam = AirportFlight.Airline.ToString();
            CreateStrThisParam(ref strThisParam, AirlineSize);
            strThisFlight = strThisFlight + strThisParam;

            strThisParam = AirportFlight.Gate.ToString();
            CreateStrThisParam(ref strThisParam, GateSize);
            strThisFlight = strThisFlight + strThisParam;

        }
        private void CreateStrThisParam(ref string strThisParam, int Size)
        {
            while (Size > strThisParam.Length)
            {
                strThisParam = strThisParam + " ";
            }

            strThisParam = strThisParam + "|";
        }

        private void FillUpSizeClassMembers()
        {
            if (AirportFlights == null)
            {
                return;
            }

            foreach (var AirportFlight in AirportFlights)
            {
                int thisSize = 0;

                thisSize = AirportFlight.FlightType.ToString().Length;
                CheckSize(thisSize, ref FlightTypeSize);

                thisSize = AirportFlight.AirportTerminal.ToString().Length;
                CheckSize(thisSize, ref FlightTypeSize);

                thisSize = AirportFlight.FlightStatus.ToString().Length;
                CheckSize(thisSize, ref FlightStatusSize);

                thisSize = AirportFlight.NumFlight.ToString().Length;
                CheckSize(thisSize, ref NumFlightSize);

                thisSize = AirportFlight.DataArrival.ToString("d").Length;
                CheckSize(thisSize, ref FlightTypeSize);

                thisSize = AirportFlight.TimeArrival.ToString(@"hh\:mm\:ss").Length;
                CheckSize(thisSize, ref TimeArrivalSize);

                thisSize = AirportFlight.DataDeparture.ToString("d").Length;
                CheckSize(thisSize, ref FlightTypeSize);

                thisSize = AirportFlight.TimeDeparture.ToString(@"hh\:mm\:ss").Length;
                CheckSize(thisSize, ref FlightTypeSize);

                thisSize = AirportFlight.TimeStartBoarding.ToString(@"hh\:mm\:ss").Length;
                CheckSize(thisSize, ref TimeStartBoardingSize);

                thisSize = AirportFlight.CityDeparture.ToString().Length;
                CheckSize(thisSize, ref CityDepartureSize);

                thisSize = AirportFlight.CityArrival.ToString().Length;
                CheckSize(thisSize, ref CityArrivalSize);

                thisSize = AirportFlight.Airline.ToString().Length;
                CheckSize(thisSize, ref AirlineSize);

                thisSize = AirportFlight.Gate.ToString().Length;
                CheckSize(thisSize, ref GateSize);

            }

        }

        private void CheckSize(int thisSize, ref int secondSize)
        {
            if (thisSize > secondSize)
            {
                secondSize = thisSize;
            }
        }

        private int GetAllSize()
        {
            int allSize = 0;

            allSize = FlightTypeSize + AirportTerminalSize + FlightStatusSize + NumFlightSize + DataArrivalSize +
            TimeArrivalSize + DataDepartureSize + TimeDepartureSize + TimeStartBoardingSize + 
            CityDepartureSize + CityArrivalSize + AirlineSize + GateSize + 14;

            return allSize;
        }
        public Flight ReternFlight(string NumFlight, out bool flightExist)
        {
            flightExist = false;

            if (AirportFlights == null)
            {
                return null;
            }

            foreach (Flight thisFlight in AirportFlights)
            {
                if (thisFlight.NumFlight == NumFlight)
                {
                    flightExist = true;
                    return thisFlight;
                }
            }

            return null;
        }
        public List<Flight> ReternFlightByDataTime(DateTime DataArrival, TimeSpan TimeArrival, out bool flightExist)
        {
            flightExist = false;

            if (AirportFlights == null)
            {
                return null;
            }

            List<Flight> thAirportFlights = new List<Flight>();
            foreach (Flight thisFlight in AirportFlights)
            {
                if (thisFlight.DataArrival == DataArrival && thisFlight.TimeArrival == TimeArrival)
                {
                    thAirportFlights.Add(thisFlight);
                    flightExist = true;
                }
            }

            return thAirportFlights;
        }
        public List<Flight> ReternFlightByDataTimePMHourCityArrival(DateTime DataArrival, TimeSpan TimeArrival, int countHour, string CityArrival, out bool flightExist)
        {
            flightExist = false;

            if (AirportFlights == null)
            {
                return null;
            }

            List<Flight> thAirportFlights = new List<Flight>();
            foreach (Flight thisFlight in AirportFlights)
            {
                if (thisFlight.DataArrival == DataArrival 
                    && thisFlight.CityArrival == CityArrival
                    && thisFlight.TimeArrival >= TimeArrival.Add(new TimeSpan(- countHour, 0, 0))
                    && thisFlight.TimeArrival >= TimeArrival.Add(new TimeSpan(countHour, 0, 0)))
                {
                    thAirportFlights.Add(thisFlight);
                    flightExist = true;
                }
            }

            return thAirportFlights;
        }
        public List<Flight> ReternFlightByDataTimePMHourCityDeparture(DateTime DataArrival, TimeSpan TimeArrival, int countHour, string CityDeparture, out bool flightExist)
        {
            flightExist = false;

            if (AirportFlights == null)
            {
                return null;
            }

            List<Flight> thAirportFlights = new List<Flight>();
            foreach (Flight thisFlight in AirportFlights)
            {
                if (thisFlight.DataArrival == DataArrival
                    && thisFlight.CityDeparture == CityDeparture
                    && thisFlight.TimeArrival >= TimeArrival.Add(new TimeSpan(-countHour, 0, 0))
                    && thisFlight.TimeArrival >= TimeArrival.Add(new TimeSpan(countHour, 0, 0)))
                {
                    thAirportFlights.Add(thisFlight);
                    flightExist = true;
                }
            }

            return thAirportFlights;
        }
        public List<Flight> ReternFlightByCityArrival(string CityArrival, out bool flightExist)
        {
            flightExist = false;

            if (AirportFlights == null)
            {
                return null;
            }

            List<Flight> thAirportFlights = new List<Flight>();
            foreach (Flight thisFlight in AirportFlights)
            {
                if (thisFlight.CityArrival == CityArrival)
                {
                    flightExist = true;
                    thAirportFlights.Add(thisFlight);
                }
            }

            return thAirportFlights;
        }
        public List<Flight> ReternFlightByCityDeparture(string CityDeparture, out bool flightExist)
        {
            flightExist = false;

            if (AirportFlights == null)
            {
                return null;
            }

            List<Flight> thAirportFlights = new List<Flight>();
            foreach (Flight thisFlight in AirportFlights)
            {
                if (thisFlight.CityDeparture == CityDeparture)
                {
                    flightExist = true;
                    thAirportFlights.Add(thisFlight);
                }
            }

            return thAirportFlights;
        }

    }

}
