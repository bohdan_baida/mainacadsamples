﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace TaxiDriver.Enums
{
    public enum OrdersStatusEnum
    {
        Created = 1,
        Added = 2,
        Assigned = 3,
        Completed = 4,
        Canceled = 5
    }
}