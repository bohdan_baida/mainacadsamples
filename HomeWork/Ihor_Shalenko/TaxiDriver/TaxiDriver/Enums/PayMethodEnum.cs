﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace TaxiDriver.Enums
{
    public enum PayMethodEnum
    {
        InCash = 1,
        PaymentCard = 2,
        Attachment = 3
    }
}