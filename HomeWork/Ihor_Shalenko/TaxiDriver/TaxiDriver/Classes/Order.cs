﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using TaxiDriver.Enums;

namespace TaxiDriver.Classes
{
    public class Order
    {
        public int Id { get; set; }
        public string Way { get; set; }
        public Customer OrderCustomer { get; set; }
        public Driver OrdersTaxiDriver { get; set; }
        public PayMethodEnum PayMethod { get; set; }
        public OrdersStatusEnum OrderStatus { get; set; }

        public Order()
        {
            PayMethod = PayMethodEnum.InCash;
            OrderStatus = OrdersStatusEnum.Created;
        }


    }
}