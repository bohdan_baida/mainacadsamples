﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace TaxiDriver.Classes
{
    public class Customer
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }

        public Customer() { }

        public Customer(int id, string firstName, string lastName, string phoneNumber) : this()
        {
            Id = id;
            FirstName = firstName;
            LastName = lastName;
            PhoneNumber = phoneNumber;
        }

        public Order CreateOrder()
        {
            return new Order() { OrderCustomer = this, OrderStatus = Enums.OrdersStatusEnum.Created };

        }

    }
}