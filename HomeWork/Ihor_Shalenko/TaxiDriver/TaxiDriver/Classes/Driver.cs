﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace TaxiDriver.Classes
{
    public class Driver
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public Car TaxiCar { get; set; }

        public Driver()
        {
            TaxiCar = new Car();
        }
        public Driver(int id, string firstName, string lastName, string phoneNumber) : this()
        {
            Id = id;
            FirstName = firstName;
            LastName = lastName;
            PhoneNumber = phoneNumber;
        }
        public void TakeTheOrder(Order order)
        {
            if (order.OrdersTaxiDriver == null)
            {
                order.OrdersTaxiDriver = this;
                order.OrderStatus = Enums.OrdersStatusEnum.Assigned;
                Console.WriteLine($"Driver: {Id} take the order: {order.Id}");
            }
            else
            {
                throw new Exception($"Order {order.Id} is adopted!");
            }
        }
        public void CompletedTheOrder(Order order)
        {
            order.OrderStatus = Enums.OrdersStatusEnum.Completed;
            Console.WriteLine($"Order: {order.Id} is completed");
        }

        public void InformTaxiDriverAboutNewOrder(string message)
        {
            Console.WriteLine($"Messege is: {message}");
        }
    }
}