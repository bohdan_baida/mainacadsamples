﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace TaxiDriver.Classes
{
    public class Car
    {
        public int Id { get; set; }
        public string CarModel { get; set; }
        public string Number { get; set; }

        public Car() { }

        public Car(int id, string carModel, string number) : this()
        {
            Id = id;
            CarModel = carModel;
            Number = number;
        }

    }
}