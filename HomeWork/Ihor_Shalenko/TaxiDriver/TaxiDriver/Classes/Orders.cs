﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace TaxiDriver.Classes
{
    public class Orders
    {
        private List<Order> ordersList;

        public IReadOnlyCollection<Order> OrdersList => ordersList;

        public event Action<string> Inform;

        public Orders()
        {
            ordersList = new List<Order>();
        }

        public void AddOrder(Order order)
        {
            order.OrderStatus = Enums.OrdersStatusEnum.Added;
            ordersList.Add(order);
            Inform?.Invoke($"order was added: {order.Id}");
        }

        public Order GetOrder(int orderId)
        {
            return ordersList.FirstOrDefault(c => c.Id == orderId);
        }
    }
}