﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaxiDriver.Classes;
using TaxiDriver.Enums;


namespace TaxiDriver
{
    class Program
    {
        static void Main(string[] args)
        {

            Customer customer = new Customer(1, "Ihor", "Sh", "0975552661");
            Driver driver = new Driver(1, "Max", "Du", "0502562064") { TaxiCar = new Car(1, "BMW", "AA222AA")};

            Order customerOrder = new Order()
            {
                Id = 1,
                OrderCustomer = customer,
                OrderStatus = OrdersStatusEnum.Created,
                PayMethod = PayMethodEnum.Attachment,
                Way = "Tupolevo - Golega"
            };

            Orders orders = new Orders();
            orders.Inform += driver.InformTaxiDriverAboutNewOrder;

            orders.AddOrder(customerOrder);
            try
            {
                driver.TakeTheOrder(customerOrder);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return;
            }

            driver.CompletedTheOrder(customerOrder);

            Console.ReadLine();

        }
    }
}
