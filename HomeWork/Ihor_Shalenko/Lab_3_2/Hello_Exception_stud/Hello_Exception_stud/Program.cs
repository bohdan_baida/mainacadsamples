﻿using System;

namespace Hello_Exception_stud
{
    class Program
    {
        static void Main( string[] args )
        {
            Console.WriteLine("Observation titmouse flight");
            Bird My_Bird = new Bird("Titmouse", 20);

            //1. Create the skeleton code with the  basic exception handling for the menu in the main method 
            //try - catch
            // 1. begin
            int rdk = 0;
            try
            {
                do
                {
                    Console.WriteLine("Write new speed");
                    rdk = int.Parse(Console.ReadLine());

                } while (rdk == 0);

                //2. Create code for the nested special exception handling in the main method
                //try - catch - catch - finally
                // 2. begin
                try
                {
                    Console.WriteLine("1 - NullReferenceException, 2 - OverflowException, Enter to pass");
                    string passf = Console.ReadLine();
                    switch (passf)
                    {
                        case "1":
                            Bird nullBird = null;
                            nullBird.BirdFlewAway = true;
                            break;
                        case "2":
                            Bird OverBird = new Bird();
                            OverBird.FlySpeed[10] = 1;
                            break;
                        default:
                            break;
                    }
                }
                catch (NullReferenceException nullEx)
                {
                    Console.WriteLine(nullEx.Message);
                }
                catch (OverflowException overEx)
                {
                    Console.WriteLine(overEx.Message);
                }
                finally
                {
                    Console.WriteLine("This is finally");
                }

                //3. Create the menu for three options in the inner try block  
                //In the second option throw the System.Exception
                // 3. begin

                //4. in case 1 code array overflow exception 
                //in case 2 code throw (new System.Exception("Oh! My system exception..."));

                //in case 3  code the sequentially incrementing of Bird speed until to the exception 
                My_Bird.FlyAway(rdk);

            }

            catch (BirdFlewAwayException BirdEx)
            {
                Console.WriteLine(BirdEx.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            Console.ReadLine();

        }

    }
}
