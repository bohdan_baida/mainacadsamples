﻿using System;

namespace Hello_Exception_stud
{
    //Create the BirdFlewAwayException class, derived from ApplicationException  with two properties  
    public class BirdFlewAwayException : ApplicationException
    { 
        //When
        public DateTime When { get; set; }
        //Why
        public string Why { get; set; }

        public BirdFlewAwayException()
        { }

        //Create constructors
        public BirdFlewAwayException(string message, string cause, DateTime time) : base(message)
        {
            Why = cause;
            When = time;
        }
    }
}
