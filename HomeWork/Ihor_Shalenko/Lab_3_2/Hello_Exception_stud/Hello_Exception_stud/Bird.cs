﻿using System;

namespace  Hello_Exception_stud
{
    //Create the Bird class with the fields, properties, constructors and the method
    public class Bird
    {
        //Create fields and properties

        public int[] FlySpeed = { 5, 15, 25, 35 };
        public int NormalSpeed { get; set; }
        public string Nick { get; set; }
        public bool BirdFlewAway { get; set; }

        //Create constructors
        public Bird()
        { }
        public Bird(string Name, int Speed)
        {
            Nick = Name;
            NormalSpeed = Speed;
        }

        //The public void FlyAway( int incrmnt ) method generates custom exception 
        public void FlyAway(int incrmnt)
        {
            if (BirdFlewAway == false)
            {
                NormalSpeed = incrmnt;
            }

            if (NormalSpeed >= FlySpeed[3])
            {
                BirdFlewAway = true;
                BirdFlewAwayException BFE = new BirdFlewAwayException(string.Format($"{Nick} flew with incredible speed!"), "Oh! Startle.", DateTime.Now);
                throw BFE;
            }
            else
                Console.WriteLine($"{Nick} speed = {NormalSpeed}");
        }

        //Implement Method public void FlyAway( int incrmnt ) which check Bird state by reading field  BirdFlewAway
        // check BirdFlewAway
        // if true 

        // write the message to console
        // else

        // increment the Bird speed by method argument
        // check the condition (NormalSpeed >= FlySpeed[3]) 
        // If it's true 

        // BirdFlewAway = true and we generate custom exception    BirdFlewAwayException(string.Format("{0} flew with incredible speed!", Nick), "Oh! Startle.", DateTime.Now)
        // with HelpLink = "http://en.wikipedia.org/wiki/Tufted_titmouse" else  console.writeline about Bird speed 
    }
}
