﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Transport
{
    public class City
    {
        public Random rand = new Random();
        public string Name { get; set; }

        public void Unload(Ship ship)
        {
            if (ship.Cargo == СargoTypeEnum.Food)
            {
                UploadFood(ship);
            }
            else
            {
                NewMethod(ship);
            }
        }

        private void NewMethod(Ship ship)
        {
            lock (this)
            {
                Task.Delay(rand.Next(1000, 10000)).Wait();
                Console.WriteLine($"Ship {ship.Id} is Unload");
            }
        }

        private void UploadFood(Ship ship)
        {
            lock (this)
            {
                Task.Delay(rand.Next(1000, 10000)).Wait();
                Console.WriteLine($"Ship {ship.Id} is Unload");
            }
        }
    }
}