﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Transport
{
    public class Port
    {
        public Semaphore Doc { get; set; }
        public Random rand = new Random();

        public Port()
        {
            Doc = new Semaphore(3, 3);
        }

        public void PrepereShip(Ship ship)
        {
            Doc.WaitOne();

            ship.City = new City() { Name = $"Kyiv {rand.Next(1, 5)}"};
            ship.Cargo = (СargoTypeEnum)rand.Next(1, 3);
            Task.Delay(rand.Next(1000, 10000)).Wait();

            Console.WriteLine($"ship: {ship.Id} City: {ship.City.Name} Cargo: {ship.Cargo}");

            Doc.Release();

        }
    }
}