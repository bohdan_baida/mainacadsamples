﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Transport
{
    public class Ship
    {
        public int Id { get; set; }
        public СargoTypeEnum Cargo { get; set; }
        public City City { get; set; }

        public Ship(int id)
        {
            Id = id;
        }
    }

    public enum СargoTypeEnum
    {
        Food = 1,
        Other = 2
    }
}