﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Transport
{
    class Program
    {
        static void Main(string[] args)
        {
            Port port = new Port();
            List<Ship> Ships = new List<Ship>()
            {
                new Ship(1),
                new Ship(2),
                new Ship(3),
                new Ship(4),
                new Ship(5),
                new Ship(6),
                new Ship(7)
            };

            foreach (var ship in Ships)
            {
                Task.Run(() => port.PrepereShip(ship))
                    .ContinueWith(t => Way(ship))
                    .ContinueWith(t => ship.City.Unload(ship));
            }

            Console.ReadLine();
        }
        static void Way(Ship ship)
        {
            Random rand = new Random();
            Task.Delay(rand.Next(1000, 10000)).Wait();
            Console.WriteLine($"Ship {ship.Id} and the Way");
        }
    }
}
