﻿using BlogsClasses.Composite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Repositories
{
    public class BlogsRepository : BaseRepository
    {
        public List<BlogWithPostsCount> GetBlogsForUser(int userId)
        {
            return context.GetBlogsForUser(userId);
        }
    }
}