﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlogsClasses;

namespace Repositories
{
    public class PostsRepositories : BaseRepository
    {
        public List<Comment> GetCommentsByPost(int idPost)
        {
            var commentsList = (
                from comment in context.Comments
                where comment.PostId == idPost
                select comment);

            return commentsList.ToList();
        }
        public List<Comment> GetCommentsByPostMR(int idPost)
        {
            var commentsList = context.Comments.Where(c => c.PostId == idPost);

            return commentsList.ToList();
        }
        public List<Post> GetTopFivePosts()
        {
            var postsLinq = (
                from post in context.Posts
                join comment in context.Comments on post.Id equals comment.PostId
                select new { post, commentsCount = post.Comments.Count() } 
                )
                .GroupBy(c => c.post.Id)
                .Select(gr => gr.FirstOrDefault())
                .OrderBy(c => c.commentsCount)
                .Select(c => c.post)
                .Take(5);

            return postsLinq.ToList();
        }
        public List<Post> GetPostsWhisComments()
        {
            return context.Posts.ToList();
        }
        public Post GetPostByID(int postId)
        {
            return context.Posts.Single(x => x.Id == postId);
        }
        public Comment GetCommentByID(int postId, int commentId)
        {
            return context.Comments.Single(x => x.PostId == postId && x.Id == commentId);
        }
        public void AddCommentToPost(Comment comment)
        {
            context.Comments.Add(comment);
            //context.Comments.Add(comment);
            context.SaveChanges();
        }

    }
}
