﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlogsClasses;
using System.Data.Entity;
using BlogsClasses.Composite;

namespace Repositories
{
    public class UserRepository : BaseRepository
    {
        public void AddUser(User thUser)
        {
            context.Users.Add(thUser);
            context.SaveChanges();
        }
        public void EditUser(User thUser)
        {
            EntityState uState = context.Entry(thUser).State;
            if (uState == EntityState.Modified)
            {
                context.SaveChanges();
            }
                
        }
        public void BlockUser(int userId)
        {
            User thUser = context.Users.Single(x => x.Id == userId);
            thUser.IsBlocked = true;
            context.SaveChanges();
        }
        public User GetUser(int userId)
        {
            return context.Users.Single(x => x.Id == userId);
        }
        public User GetUser(string userName)
        {
            return context.Users.SingleOrDefault(x => x.Username == userName);
        }
        public List<User> GetListOfUsers()
        {
           return context.Users.ToList();
        }

    }
}
