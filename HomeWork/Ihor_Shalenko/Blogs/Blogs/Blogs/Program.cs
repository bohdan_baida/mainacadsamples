﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BisinessLogic;
using BlogsClasses;
using Visualizer;
using BlogsClasses.Composite;

namespace Blogs
{
    class Program
    {
        public static User currentUser;
        static void Main(string[] args)
        {
            currentUser = new User();
            while (true)
            {
                UserBO userBO = new UserBO();
                Console.WriteLine("UserName:");
                try
                {
                    currentUser = userBO.GetUser(Console.ReadLine());
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    Console.WriteLine("press ENTER to continue");
                    Console.ReadLine();
                    Console.Clear();
                    continue;
                }

                Console.WriteLine("password:");
                if (!userBO.CheckUserPassword(currentUser, Console.ReadLine()))
                {
                    Console.WriteLine("password is not correct");
                    Console.WriteLine("press ENTER to continue");
                    Console.ReadLine();
                    Console.Clear();
                }
                else
                { 
                    Console.Clear();
                    break;
                }

            }

            while (true)
            {
                Console.WriteLine($"Hello {currentUser.Username}");
                Console.WriteLine("Select action:");
                Console.WriteLine("1 - print Users list;");
                Console.WriteLine("2 - create new User;");
                Console.WriteLine("3 - edit old User;");
                Console.WriteLine("4 - get blogs for user;");
                Console.WriteLine("5 - get posts comments;");
                Console.WriteLine("6 - get top five posts;");
                Console.WriteLine("7 - add comment;");

                switch (Console.ReadLine())
                {
                    case "1":
                        PrintListOfUsers();
                        break;
                    case "2":
                        CreateUser();
                        break;
                    case "3":
                        EditUser();
                        break;
                    case "4":
                        PrintBlogsForUser();
                        break;
                    case "5":
                        PrintPostsComments();
                        break;
                    case "6":
                        PrintTopFivePosts();
                        break;
                    case "7":
                        AddComment();
                        break;
                    case "#":
                        return;
                    default:
                        Console.Clear();
                        break;
                }

                Console.ReadLine();
                Console.Clear();

            }

        }

        private static void PrintBlogsForUser()
        {
            Console.WriteLine("UserID:");
            int userId = Convert.ToInt32(Console.ReadLine());
            BlogBO blogBO = new BlogBO();
            List<BlogWithPostsCount> blogWithPostsCount = blogBO.GetBlogsForUser(userId);

            Console.WriteLine("List:");
            var visualizer = new Visualizer<BlogWithPostsCount>(blogWithPostsCount);
            visualizer.Visualize();
        }
        private static void PrintPostsComments()
        {
            Console.WriteLine("Posts id:");
            int postID = Convert.ToInt32(Console.ReadLine());
            PostBO postBO = new PostBO();

            List<Comment> comments = postBO.GetCommentsByPost(postID);
            Console.WriteLine("Posts comments list:");
            var visualizer = new Visualizer<Comment>(comments);
            visualizer.Visualize();

            List<Comment> commentsMR = postBO.GetCommentsByPost(postID);
            Console.WriteLine("Posts comments list MR:");
            var visualizerMR = new Visualizer<Comment>(comments);
            visualizerMR.Visualize();

        }
        private static void PrintTopFivePosts()
        {
            PostBO postBO = new PostBO();

            List<Post> post = postBO.GetTopFivePosts();
            
            Console.WriteLine("Top 5 posts:");
            var visualizerMR = new Visualizer<Post>(post);
            visualizerMR.Visualize();

        }
        private static void CreateUser()
        {
            UserBO userBO = new UserBO();

            User nUser = new User();

            Console.WriteLine("FirstName:");
            nUser.FirstName = Console.ReadLine();
            Console.WriteLine("LastName:");
            nUser.LastName = Console.ReadLine();
            Console.WriteLine("Username:");
            nUser.Username = Console.ReadLine();
            Console.WriteLine("PasswordHash:");
            nUser.PasswordHash = Console.ReadLine();

            userBO.CreateUser(nUser);

            Console.WriteLine("User is created");
        }
        private static void EditUser()
        {
            Console.WriteLine("Write UserID");

            UserBO userBO = new UserBO();

            User thUser = userBO.GetUser(Convert.ToInt32(Console.ReadLine()));

            Console.WriteLine($"FirstName: {thUser.FirstName}");
            Console.Write("New:");
            string FirstName = Console.ReadLine();
            thUser.FirstName = FirstName == string.Empty ? thUser.FirstName : FirstName;

            Console.WriteLine($"LastName: {thUser.LastName}");
            Console.Write("New:");
            string LastName = Console.ReadLine();
            thUser.LastName = LastName == string.Empty ? thUser.LastName : LastName;

            Console.WriteLine($"Username: {thUser.Username}");
            Console.Write("New:");
            string Username = Console.ReadLine();
            thUser.Username = Username == string.Empty ? thUser.Username : Username;

            Console.WriteLine($"PasswordHash:");
            Console.Write("New:");
            string PasswordHash = Console.ReadLine();
            thUser.PasswordHash = PasswordHash == string.Empty ? thUser.PasswordHash : PasswordHash;

            userBO.EditUser(thUser);

            Console.WriteLine("User is edited");
        }
        private static void PrintListOfUsers()
        {
            UserBO userBO = new UserBO();
            List<User> users = userBO.GetUsers();

            Console.WriteLine("Users list:");
            var visualizer = new Visualizer<User>(users);
            visualizer.Visualize();

            //foreach (var user in users)
            //{
            //    Console.WriteLine($"ID = {user.Id}; FirstName = {user.FirstName}; LastName = {user.LastName}; Username = {user.Username}; PasswordHash = {user.PasswordHash};");
            //}
        }
        private static void AddComment()
        {
            while (true)
            {
                PrintPostsWhisComments();
                Console.WriteLine("1 - add comment by post;");
                Console.WriteLine("2 - add comment by comment;");

                switch (Console.ReadLine())
                {
                    case "1":
                        AddCommentToPost();
                        break;
                    case "2":
                        AddCommentToComment();
                        break;
                    case "#":
                        return;
                    default:
                        break;
                }
                Console.ReadLine();
                Console.Clear();
            }


        }
        private static void PrintPostsWhisComments()
        {
            PostBO postBO = new PostBO();
            List<Post> posts = postBO.GetPostsWhisComments();

            Console.WriteLine("posts:");
            foreach (var item in posts)
            {
                Console.WriteLine(item.ToString());
                var visualizer = new Visualizer<Comment>(item.Comments.ToList());
                visualizer.Visualize();
            }

        }
        private static void AddCommentToPost()
        {
            PostBO postBO = new PostBO();
            Console.WriteLine("ID post:");
            Post post = postBO.GetPostByID(Convert.ToInt32(Console.ReadLine()));

            Comment comment = new Comment();
            comment.PostId = post.Id;
            comment.UserId = currentUser.Id;
            Console.WriteLine("Write text");
            comment.CommentText = Console.ReadLine();

            postBO.AddCommentToPost(comment);

        }
        private static void AddCommentToComment()
        {
            PostBO postBO = new PostBO();
            Console.WriteLine("ID post and ID comment:");
            Comment commentParetn = postBO.GetCommentByID(Convert.ToInt32(Console.ReadLine()), Convert.ToInt32(Console.ReadLine()));

            Comment comment = new Comment();
            comment.PostId = commentParetn.Post.Id;
            comment.ParentCommentId = commentParetn.Id;
            comment.UserId = currentUser.Id;
            Console.WriteLine("Write text");
            comment.CommentText = Console.ReadLine();

            postBO.AddCommentToPost(comment);
        }

    }
}
