namespace BlogsClasses
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Post
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Post()
        {
            Comments = new HashSet<Comment>();
        }

        public int Id { get; set; }

        public int BlogId { get; set; }

        public int CreatedBy { get; set; }

        [Required]
        public string PostHeader { get; set; }

        [Required]
        public string PostBody { get; set; }

        [Required]
        public DateTime OnDate { get; set; }


        public virtual Blog Blog { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Comment> Comments { get; set; }

        public virtual User User { get; set; }
        public override string ToString()
        {
            return $"Id:{Id} BlogId:{BlogId} CreatedBy:{CreatedBy} PostHeader:{PostHeader} PostBody:{PostBody} OnDate:{OnDate}";
        }
    }
}
