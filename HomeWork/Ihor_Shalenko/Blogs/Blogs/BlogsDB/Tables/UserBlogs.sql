﻿create table UserBlogs(
UserId int not null,
BlogId int not null
primary key(UserId,BlogId),
foreign key (UserId) references dbo.Users(Id),
foreign Key (BlogId) references dbo.Blogs(Id)
)