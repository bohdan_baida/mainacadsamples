﻿CREATE TABLE [dbo].[UserRoles]
(
	[UserId] INT NOT NULL, 
    [RoleId] INT NOT NULL,
	primary key(UserId,RoleId),
	foreign key (UserId) references dbo.Users(Id),
	foreign Key (RoleId) references dbo.Roles(Id)
)
