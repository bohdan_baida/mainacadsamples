﻿create table Users(
Id int not null primary key identity(1,1),
FirstName varchar(max),
LastName varchar(Max),
Username varchar(100) not null,
PasswordHash varchar(max) not null, 
    [IsBlocked] BIT NOT NULL DEFAULT 0,
	CONSTRAINT MOD_UNIQUE UNIQUE (Username)
)
