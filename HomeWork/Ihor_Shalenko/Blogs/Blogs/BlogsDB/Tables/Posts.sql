﻿create table Posts(
Id int not null primary key identity(1,1),
BlogId int not null,
CreatedBy int not null,
PostHeader varchar(max) not null,
PostBody varchar(max) not null,
[OnDate] DATETIME NOT NULL, 
    foreign key (CreatedBy) references dbo.Users(Id),
foreign Key (BlogId) references dbo.Blogs(Id)
)