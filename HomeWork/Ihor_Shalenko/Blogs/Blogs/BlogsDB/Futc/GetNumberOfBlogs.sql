﻿CREATE FUNCTION [dbo].[GetNumberOfBlogs]
(
  @userId int
)
RETURNS INT
AS
BEGIN
  declare @result int=0

  select @result = count(*)
  from UserBlogs ub
  where ub.UserId = @userId

  RETURN @result
END