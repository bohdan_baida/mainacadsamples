﻿CREATE FUNCTION [dbo].[GetUserBlogs]
(
	@userId int
)
RETURNS @returntable TABLE
(
	BlogName varchar(max),
	PostCount int
)
AS
BEGIN
	INSERT @returntable
	SELECT Blogs.BlogName [BlogName], COUNT(Posts.Id) [PostCount]
	From UserBlogs 
	join Blogs ON Blogs.Id = UserBlogs.BlogId 
	left join Posts ON Posts.BlogId = Blogs.Id
			where UserBlogs.UserId = @userId
	group by Blogs.BlogName
	RETURN
END
