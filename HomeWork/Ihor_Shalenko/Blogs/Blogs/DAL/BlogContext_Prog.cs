﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BlogsClasses.Composite;

namespace DAL
{
    public partial class BloglsContext
    {
        public int GetNumberOfBlogs(int userId)
        {
            return this.Database.SqlQuery<int>(
                $"select [dbo].[GetNumberOfBlogs]({userId})"
                ).First();
        }

        public List<BlogWithPostsCount> GetBlogsForUser(int userId)
        {
            return this.Database.SqlQuery<BlogWithPostsCount>(
                $"select * from [dbo].[GetUserBlogs]({userId})"
                ).ToList();
        }
    }
}
