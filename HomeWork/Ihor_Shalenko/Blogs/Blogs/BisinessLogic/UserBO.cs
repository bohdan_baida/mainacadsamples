﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Repositories;
using BlogsClasses;
using BlogsClasses.Composite;

namespace BisinessLogic
{
    public class UserBO
    {
        UserRepository userRepository;

        public UserBO()
        {
            userRepository = new UserRepository();
        }
        public void CreateUser(User user)
        {
            userRepository.AddUser(user);
        }
        public void EditUser(User user)
        {
            userRepository.EditUser(user);
        }
        public User GetUser(int userId)
        {
            return userRepository.GetUser(userId);
        }
        public User GetUser(string userName)
        {
            return userRepository.GetUser(userName);
        }
        public void BlockUser(int userId)
        {
            userRepository.BlockUser(userId);
        }
        public List<User> GetUsers()
        {
            return userRepository.GetListOfUsers();
        }
        public bool CheckUserPassword(string userName, string password)
        {
            var user = userRepository.GetUser(userName);
            if (user == default(User))
                return false;

            return new PasswordHashManager().ValidatePassword(password, user.PasswordHash);
        }
        public string HashPassword(string password)
        {
            PasswordHashManager Hash = new PasswordHashManager();

            return Hash.HashPassword(password);
        }
    }
}
