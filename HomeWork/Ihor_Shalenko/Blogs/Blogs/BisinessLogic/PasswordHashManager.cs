﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace BisinessLogic
{
    class PasswordHashManager
    {

        private const string salt1 = "o7hfsd7fs";
        private const string salt2 = "a5d6s4g";

        public string HashPassword(string password)
        {
            var passwordWithSalt = salt1 + password + salt2;

            var sha256 = new SHA256Managed();
            var bytesHash = sha256.ComputeHash(
                Encoding.Unicode.GetBytes(passwordWithSalt));

            return Convert.ToBase64String(bytesHash);
        }

        public bool ValidatePassword(string password, string hashFromDb)
        {
            var passwordHash = HashPassword(password);
            return passwordHash == hashFromDb;
        }

    }
}