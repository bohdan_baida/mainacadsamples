﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Repositories;
using BlogsClasses;
using BlogsClasses.Composite;

namespace BisinessLogic
{
    public class PostBO
    {
        PostsRepositories postRepository;
        public PostBO()
        {
            postRepository = new PostsRepositories();
        }
        public List<Comment> GetCommentsByPost(int idPost)
        {
            return postRepository.GetCommentsByPost(idPost);
        }
        public List<Comment> GetCommentsByPostMR(int idPost)
        {
            return postRepository.GetCommentsByPostMR(idPost);
        }
        public List<Post> GetTopFivePosts()
        {
            return postRepository.GetTopFivePosts();
        }
        public List<Post> GetPostsWhisComments()
        {
            return postRepository.GetPostsWhisComments();
        }
        public Post GetPostByID(int postId)
        {
            return postRepository.GetPostByID(postId);
        }
        public Comment GetCommentByID(int postId, int commentId)
        {
            return postRepository.GetCommentByID(postId, commentId);
        }
        public void AddCommentToPost(Comment comment)
        {
            postRepository.AddCommentToPost(comment);
        }
    }
}
