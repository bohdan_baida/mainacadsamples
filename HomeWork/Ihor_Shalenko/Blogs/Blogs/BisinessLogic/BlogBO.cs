﻿using BlogsClasses.Composite;
using Repositories;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace BisinessLogic
{
    public class BlogBO
    {
        BlogsRepository blogRepository;

        public BlogBO()
        {
            blogRepository = new BlogsRepository();
        }
        public List<BlogWithPostsCount> GetBlogsForUser(int userId)
        {
            return blogRepository.GetBlogsForUser(userId);
        }
    }
}