﻿using MVVM.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MVVM
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = new MainWindowViewModel();
        }
        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.AddedItems.Count > 0)
            {
                var model = (MainWindowViewModel)DataContext;
                model.ListSelectionCommand.Execute(e.AddedItems[0]);
            }
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            var model = (MainWindowViewModel)DataContext;
            var tb = (TextBox)sender;
            model.Filter.Execute(tb.Text);
        }
    }
}
