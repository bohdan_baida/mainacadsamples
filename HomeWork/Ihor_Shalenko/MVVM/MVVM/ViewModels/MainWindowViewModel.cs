﻿using MVVM.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MVVM.ViewModels
{
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        private Cat currentCat;

        public Command.Command AddCommand { get; set; }
        public Command.Command RemoveCommand { get; set; }
        public Command.Command Filter { get; set; }


        public Command.Command ListSelectionCommand { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        public ObservableCollection<Cat> Cats { get; set; }
        public ObservableCollection<Cat> FilterCats { get; set; }
        public Cat CurrentCat
        {
            get
            {
                return currentCat;
            }
            set
            {
                currentCat = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("CurrentCat"));
            }
        }

        public MainWindowViewModel()
        {
            Cats = new ObservableCollection<Cat>();
            AddCommand = new Command.Command(obj =>
            {
                var newCat = new Cat()
                {
                    Species = new Species()
                };
                Cats.Add(newCat);
                CurrentCat = newCat;
                CatsFilterBase();
            });
            RemoveCommand = new Command.Command(obj =>
            {
                var newCat = Cats.Remove((Cat)obj);
                if (Cats.Count > 0)
                {
                    CurrentCat = Cats[Cats.Count-1];
                }
                CatsFilterBase();
            });

            Filter = new Command.Command(obj =>
            {
                string ourFilter = (string)obj;
                if (ourFilter == string.Empty)
                {
                    CatsFilterBase();
                }
                else
                {
                    FilterCats.Clear();
                    List<Cat> thCats = new List<Cat>(Cats.Where(c => c.Name.ToLower().Contains(ourFilter.ToLower()) || c.Species.Name.ToLower().Contains(ourFilter.ToLower())));
                    foreach (var item in thCats)
                    {
                        FilterCats.Add(item);
                    }
                }
            });
            ListSelectionCommand = new Command.Command(cat =>
            {
                CurrentCat = (Cat)cat;
            });

            Cats.Add(new Cat()
            {
                Name = "Tom",
                Age = 2,
                Price = 1000,
                ImagePath = @"Images/cat1.jpg",
                Species = new Species()
                {
                    Name = "British",
                    Description = "Super rare british cat. Ultra expensive"
                }
            });
            Cats.Add(new Cat()
            {
                Name = "Barsik",
                Age = 10,
                Price = 2000,
                ImagePath = @"Images/cat2.jpg",
                Species = new Species()
                {
                    Name = "Persian",
                    Description = "Super rare persian cat. Ultra expensive"
                }
            });
            Cats.Add(new Cat()
            {
                Name = "Puffy",
                Age = 2,
                Price = 500,
                ImagePath = @"Images/cat3.jpg",
                Species = new Species()
                {
                    Name = "Prosto kot",
                    Description = "Super rare prosto cat. Ultra expensive"
                }
            });

            FilterCats = new ObservableCollection<Cat>(Cats);

        }

        private void CatsFilterBase()
        {
            FilterCats.Clear();
            foreach (var item in Cats)
            {
                FilterCats.Add(item);
            }
        }
    }
}
