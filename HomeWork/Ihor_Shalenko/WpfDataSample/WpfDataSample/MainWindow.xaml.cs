﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfDataSample.Class;

namespace WpfDataSample
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public CatsModel CatsModel { get; set; }

        public MainWindow()
        {
            InitializeComponent();

            var Cats = new List<Cat>();
            Cats.Add(new Cat()
            {
                Id = 1,
                Name = "Tom",
                Age = 2,
                Price = 1000,
                ImagePath = @"Images/cat1.jpg",
                Species = new Species()
                {
                    Name = "British",
                    Description = "Super rare british cat. Ultra expensive"
                }
            });
            Cats.Add(new Cat()
            {
                Id = 2,
                Name = "Barsik",
                Age = 10,
                Price = 2000,
                ImagePath = @"Images/cat2.jpg",
                Species = new Species()
                {
                    Name = "Persian",
                    Description = "Super rare persian cat. Ultra expensive"
                }
            });
            Cats.Add(new Cat()
            {
                Id = 3,
                Name = "Puffy",
                Age = 2,
                Price = 500,
                ImagePath = @"Images/cat3.jpg",
                Species = new Species()
                {
                    Name = "Prosto kot",
                    Description = "Super rare prosto cat. Ultra expensive"
                }
            });

            CatsModel = new CatsModel()
            {
                Cats = Cats,
                CurrentCat = null
            };
            DataContext = CatsModel;
        }



        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Cat copyCurrentCat = (Cat)e.AddedItems[0];
            CatsModel.CurrentCat = (Cat)copyCurrentCat.Clone();
        }

        private void SaveCatsData_Click(object sender, RoutedEventArgs e)
        {
            Cat currentCatForSave = CatsModel.Cats.ToList().Find(x => x.Id == CatsModel.CurrentCat.Id);

            currentCatForSave.Age = CatsModel.CurrentCat.Age;
            currentCatForSave.ImagePath = CatsModel.CurrentCat.ImagePath;
            currentCatForSave.Name = CatsModel.CurrentCat.Name;
            currentCatForSave.Price = CatsModel.CurrentCat.Price;
            currentCatForSave.Species.Description = CatsModel.CurrentCat.Species.Description;
            currentCatForSave.Species.Name = CatsModel.CurrentCat.Species.Name;

        }
    }
    public class CatsModel : INotifyPropertyChanged
    {
        private Cat currentCat;
        public IEnumerable<Cat> Cats { get; set; }
        public Cat CurrentCat
        {
            get { return currentCat; }
            set
            {
                currentCat = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("CurrentCat"));
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
