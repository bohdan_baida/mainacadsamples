﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfDataSample.Class
{
    public class Cat : INotifyPropertyChanged, ICloneable
    {
        private string name;
        private int age;
        private decimal price;
        private Species species;


        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                if (PropertyChanged!=null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("Name"));
                }
            }
        }

        public int Age
        {
            get
            {
                return age;
            }
            set
            {
                age = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("Age"));
                }
            }
        }

        public decimal Price
        {
            get
            {
                return price;
            }
            set
            {
                price = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("Price"));
                }
            }
        }

        public int Id { get; set; }

        public string ImagePath { get; set; }

        public Species Species
        {
            get
            {
                return species;
            }
            set
            {
                species = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("species"));
                }
            }
        }



        public event PropertyChangedEventHandler PropertyChanged;

        public object Clone()
        {
            Cat CloneCat = new Cat()
            {
                Id = this.Id,
                Name = this.Name,
                Age = this.Age,
                Price = this.Price,
                ImagePath = this.ImagePath,
                Species = new Species()
                {
                    Name = this.Species.Name,
                    Description = this.Species.Description
                }
            };
            return CloneCat;
        }
    }

    public class Species : INotifyPropertyChanged
    {
        private string name;
        private string description;

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("name"));
                }
            }
        }

        public string Description
        {
            get
            {
                return description;
            }
            set
            {
                description = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("description"));
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
