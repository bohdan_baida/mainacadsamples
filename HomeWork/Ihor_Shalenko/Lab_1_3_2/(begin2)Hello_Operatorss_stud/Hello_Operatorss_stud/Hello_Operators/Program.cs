﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloOperators_stud
{
    class Program
    {
        static void Main(string[] args)
        {
            long a;
            Console.WriteLine(@"Please,  type the number:
            1. Farmer, wolf, goat and cabbage puzzle
            2. Console calculator
            3. Factirial calculation
            ");
            
            a = long.Parse(Console.ReadLine());
            switch (a)
            {
                case 1:
                    Farmer_puzzle();
                    Console.WriteLine("");
                    break;
                case 2:
                    Calculator();
                    Console.WriteLine("");
                    break;
                case 3:
                    Factorial_calculation();
                    Console.WriteLine("");
                    break;
                default:
                    Console.WriteLine("Exit");
                    break;
            }
            Console.WriteLine("Press any key");
            Console.ReadLine();
        }
        #region farmer
        static void Farmer_puzzle()
        {
            //Key sequence: 3817283 or 3827183
            // Declare 7 int variables for the input numbers and other variables
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(@"From one bank to another should carry a wolf, goat and cabbage. 
At the same time can neither carry nor leave together on the banks of a wolf and a goat, 
a goat and cabbage. You can only carry a wolf with cabbage or as each passenger separately. 
You can do whatever how many flights. How to transport the wolf, goat and cabbage that all went well?");
            Console.WriteLine("There: farmer and wolf - 1");
            Console.WriteLine("There: farmer and cabbage - 2");
            Console.WriteLine("There: farmer and goat - 3");
            Console.WriteLine("There: farmer  - 4");
            Console.WriteLine("Back: farmer and wolf - 5");
            Console.WriteLine("Back: farmer and cabbage - 6");
            Console.WriteLine("Back: farmer and goat - 7");
            Console.WriteLine("Back: farmer  - 8");
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Please,  type numbers by step ");
            Console.ForegroundColor = ConsoleColor.DarkGray;
            Console.WriteLine("Please,  write a # if ypu want to Exit");

            // Implement input and checking of the 7 numbers in the nested if-else blocks with the  Console.ForegroundColor changing
            // farmer  = 0;
            // wolf    = 1; 
            // cabbage = 2; 
            // goat    = 3;

            int[] rightBank = { 0, 0, 0, 0 };
            int[] newStep = { 0, 0, 0, 0 };

            bool correctStep;
            while (true)
            {
                Console.WriteLine();
                var thisNum = Console.ReadLine();
                correctStep = false;
                switch (thisNum)
                {
                    case "1":
                        newStep[0] = 1;
                        newStep[1] = 1;

                        if (CheckObjectPosition(newStep[0], rightBank[0]) == false)
                        {
                            Console.WriteLine("This action cannot be performed because the farmer is on the other side!");
                            continue;
                        }
                        if (CheckObjectPosition(newStep[1], rightBank[1]) == false)
                        {
                            Console.WriteLine("This action cannot be performed because the wolf is on the other side!");
                            continue;
                        }

                        break;
                    case "2":
                        newStep[0] = 1;
                        newStep[2] = 1;

                        if (CheckObjectPosition(newStep[0], rightBank[0]) == false)
                        {
                            Console.WriteLine("This action cannot be performed because the farmer is on the other side!");
                            continue;
                        }
                        if (CheckObjectPosition(newStep[2], rightBank[2]) == false)
                        {
                            Console.WriteLine("This action cannot be performed because the cabbage is on the other side!");
                            continue;
                        }

                        break;
                    case "3":
                        newStep[0] = 1;
                        newStep[3] = 1;

                        if (CheckObjectPosition(newStep[0], rightBank[0]) == false)
                        {
                            Console.WriteLine("This action cannot be performed because the farmer is on the other side!");
                            continue;
                        }
                        if (CheckObjectPosition(newStep[3], rightBank[3]) == false)
                        {
                            Console.WriteLine("This action cannot be performed because the goat is on the other side!");
                            continue;
                        }

                        break;
                    case "4":
                        newStep[0] = 1;

                        if (CheckObjectPosition(newStep[0], rightBank[0]) == false)
                        {
                            Console.WriteLine("This action cannot be performed because the farmer is on the other side!");
                            continue;
                        }

                        break;
                    case "5":
                        newStep[0] = 0;
                        newStep[1] = 0;

                        if (CheckObjectPosition(newStep[0], rightBank[0]) == false)
                        {
                            Console.WriteLine("This action cannot be performed because the farmer is on the other side!");
                            continue;
                        }
                        if (CheckObjectPosition(newStep[1], rightBank[1]) == false)
                        {
                            Console.WriteLine("This action cannot be performed because the wolf is on the other side!");
                            continue;
                        }

                        break;
                    case "6":
                        newStep[0] = 0;
                        newStep[2] = 0;

                        if (CheckObjectPosition(newStep[0], rightBank[0]) == false)
                        {
                            Console.WriteLine("This action cannot be performed because the farmer is on the other side!");
                            continue;
                        }
                        if (CheckObjectPosition(newStep[2], rightBank[2]) == false)
                        {
                            Console.WriteLine("This action cannot be performed because the cabbage is on the other side!");
                            continue;
                        }

                        break;
                    case "7":
                        newStep[0] = 0;
                        newStep[3] = 0;

                        if (CheckObjectPosition(newStep[0], rightBank[0]) == false)
                        {
                            Console.WriteLine("This action cannot be performed because the farmer is on the other side!");
                            continue;
                        }
                        if (CheckObjectPosition(newStep[3], rightBank[3]) == false)
                        {
                            Console.WriteLine("This action cannot be performed because the goat is on the other side!");
                            continue;
                        }

                        break;
                    case "8":
                        newStep[0] = 0;

                        if (CheckObjectPosition(newStep[0], rightBank[0]) == false)
                        {
                            Console.WriteLine("This action cannot be performed because the farmer is on the other side!");
                            continue;
                        }

                        break;
                    case "#":
                        return;
                    default:
                        Console.WriteLine("Input error, enter a character from the proposed");
                        break;
                }

                CheckThisStep(ref correctStep, newStep);
                if (correctStep == false)
                {
                    Console.WriteLine("This action cannot be performed, because on the same bank incompatible objects!");
                    continue;
                }

                for (int i = 0; i < rightBank.Length; i++)
                {
                    rightBank[i] = newStep[i];
                }

                PrintState(rightBank);

                if (rightBank.Min() == 1)
                {
                    Console.WriteLine("Exactly! You answer correct!");
                    return;
                }
            }

        }

        static bool CheckObjectPosition(int newPosition, int thisPosition)
        {
            if (newPosition == thisPosition)
                return false;
            else
                return true;
        }

        static void CheckThisStep(ref bool correctStep, int[] newStep)
        {
            correctStep = true;

            if (newStep[0] == 0)
            {
                if (newStep[1] == 1 && newStep[3] == 1)
                {
                    correctStep = false;
                }

                if (newStep[2] == 1 && newStep[3] == 1)
                {
                    correctStep = false;
                }
            }
            else if (newStep[0] == 1)
            {
                if (newStep[1] == 0 && newStep[3] == 0)
                {
                    correctStep = false;
                }

                if (newStep[2] == 0 && newStep[3] == 0)
                {
                    correctStep = false;
                }
            }
        }

        static void PrintState(int[] rightBank)
        {
            Console.WriteLine();
            Console.Write("Left bank: ");
            if (rightBank[0] == 0)
            {
                Console.Write("farmer ");
            }
            if (rightBank[1] == 0)
            {
                Console.Write("wolf ");
            }
            if (rightBank[2] == 0)
            {
                Console.Write("cabbage ");
            }
            if (rightBank[3] == 0)
            {
                Console.Write("goat ");
            }

            Console.WriteLine();
            Console.Write("Right bank: ");
            if (rightBank[0] == 1)
            {
                Console.Write("farmer ");
            }
            if (rightBank[1] == 1)
            {
                Console.Write("wolf ");
            }
            if (rightBank[2] == 1)
            {
                Console.Write("cabbage ");
            }
            if (rightBank[3] == 1)
            {
                Console.Write("goat ");
            }

            Console.WriteLine();

        }
        #endregion

        #region calculator
        static void Calculator()
        {
            // Set Console.ForegroundColor  value
            Console.WriteLine(@"Select the arithmetic operation:
            1. Multiplication 
            2. Divide 
            3. Addition 
            4. Subtraction
            5. Exponentiation ");
            // Implement option input (1,2,3,4,5)
            //     and input of  two or one numbers
            //  Perform calculations and output the result 
            long a = long.Parse(Console.ReadLine());

            Console.WriteLine("Write first number");
            double N1 = double.Parse(Console.ReadLine());

            double N2 = 0;
            if (a < 5)
            {
                Console.WriteLine("Write second number");
                N2 = double.Parse(Console.ReadLine());
            }


            switch (a)
            {
                case 1:
                    Console.WriteLine($"result {N1} * {N2} = {N1 * N2}");
                    break;
                case 2:
                    Console.WriteLine($"result {N1} / {N2} = {N1 / N2}");
                    break;
                case 3:
                    Console.WriteLine($"result {N1} + {N2} = {N1 + N2}");
                    break;
                case 4:
                    Console.WriteLine($"result {N1} - {N2} = {N1 - N2}");
                    break;
                case 5:
                    Console.WriteLine($"result Exp in {N1} = {Math.Exp(N1)}");
                    break;
                default:
                    Console.WriteLine("Exit");
                    break;
            }
        }
        #endregion

        #region Factorial
        static void Factorial_calculation()
        {
            // Implement input of the number
            Console.WriteLine(@"Write number:");
            var a = Console.ReadLine();

            int myNum;
            if (int.TryParse(a, out myNum))
            {
                // Implement input the for circle to calculate factorial of the number
                int result = 1;
                for (int i = 1; i <= myNum; i++)
                {
                    result = result * i;
                }
                // Output the result
                Console.WriteLine($"{myNum}! = {result}");
            }
            else
            {
                Console.WriteLine("Can't trunsorm to int...");
                return;
            }



        }
        #endregion
    }
}
