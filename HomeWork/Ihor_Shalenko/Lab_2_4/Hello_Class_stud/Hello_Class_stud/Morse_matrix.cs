﻿using System;

namespace Hello_Class_stud
{
    //Implement class Morse_matrix derived from String_matrix, which realize IMorse_crypt
    class Morse_matrix : String_matrix, IMorse_crypt
    {
        public const int Size_2 = Alphabet.Size;
        private int offset_key = 0;


        //Implement Morse_matrix constructor with the int parameter for offset
        //Use fd(Alphabet.Dictionary_arr) and sd() methods
        public Morse_matrix()
        { }
        public Morse_matrix(int offset_key)
        {
            this.offset_key = offset_key;
            fd(Alphabet.Dictionary_arr);
            sd();
        }
        //Implement Morse_matrix constructor with the string [,] Dict_arr and int parameter for offset
        //Use fd(Dict_arr) and sd() methods
        public Morse_matrix(string[,] Dict_arr, int offset_key)
        {
            this.offset_key = offset_key;
            fd(Dict_arr);
            sd();
        }

        private void fd(string[,] Dict_arr)
        {
            for (int ii = 0; ii < Size1; ii++)
                for (int jj = 0; jj < Size_2; jj++)
                    this[ii, jj] = Dict_arr[ii, jj];
        }


        private void sd()
        {
            int off = Size_2 - offset_key;
            for (int jj = 0; jj < off; jj++)
                this[1, jj] = this[1, jj + offset_key];
            for (int jj = off; jj < Size_2; jj++)
                this[1, jj] = this[1, jj - off];
        }

        //Implement Morse_matrix operator +
        public static Morse_matrix operator +(Morse_matrix tMorse_matrix, int offset_key)
        {
            tMorse_matrix.offset_key = offset_key;
            tMorse_matrix.fd(Alphabet.Dictionary_arr);
            tMorse_matrix.sd();
            return tMorse_matrix;
        }

        //Realize crypt() with string parameter
        //Use indexers

        public string crypt(string word)
        {
            char[] aWord = word.ToCharArray();
            string mCode = "";
            for (int i = 0; i < aWord.Length; i++)
            {
                for (int j = 0; j < Alphabet.Dictionary_arr.GetLength(1); j++)
                {
                    if (Alphabet.Dictionary_arr[0, j] == aWord[i].ToString())
                    {
                        mCode = mCode + Alphabet.Dictionary_arr[1, j];
                    }
                }
            }

            return mCode;
        }

        //Realize decrypt() with string array parameter
        //Use indexers
        public string decrypt(string[] mCode)
        {
            string dCode = "";
            for (int i = 0; i < mCode.Length; i++)
            {
                for (int j = 0; j < Alphabet.Dictionary_arr.GetLength(1); j++)
                {
                    if (Alphabet.Dictionary_arr[1, j] == mCode[i].ToString())
                    {
                        dCode = dCode + Alphabet.Dictionary_arr[0, j];
                    }
                }
            }

            return dCode;
        }



        //Implement Res_beep() method with string parameter to beep the string
        public void Res_beep(string cWord)
        {
            char[] acWord = cWord.ToCharArray();
            string ts = "";

            for (int i = 0; i < acWord.Length; i++)
            {
                ts = acWord[i].ToString();
                if (ts == "-")
                {
                    Console.Beep(1000, 1000);
                }
                else if (ts == ".")
                {
                    Console.Beep(1000, 100);
                }

            }          

        }

    }
}

