﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hello_fact_advstud
{
    class Program
    {
        static void Main(string[] args)
        {
            //Define parameters to calculate the factorial of
            while (true)
            {
                Console.WriteLine("Please press the number");
                var resultCRL = Console.ReadLine();
                int numForFact;

                if (resultCRL == "#")
                {
                    return;
                }

                if (int.TryParse(resultCRL, out numForFact) == true)
                {
                    //Call fact() method to calculate
                    Fact(numForFact);
                    break;
                }
                else
                {
                    Console.WriteLine("Try again or press # for exit");
                }

            }

            Console.ReadLine();
        }

        //Create fact() method  with parameter to calculate factorial
        //Use ternary operator
        static void Fact(int num)
        {
            double factRsult = 1;
            for (int i = 1; i <= num; i++)
            {
                factRsult *= i;
            }

            Console.WriteLine($"{num}! = {factRsult}");
        }

    }

    

}
