﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;

namespace WpfL_1
{
    public class Cat : INotifyPropertyChanged
    {
        private string name;
        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Name"));
            }
        }
        public int Age { get; set; }
        public string ImagePath { get; set; }
        public decimal Price { get; set; }
        public Species Species { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
    }
    public class Species
    {
        public string Name { get; set; }
        public string Descripteon { get; set; }
    }
}