﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfL_1
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public List<Cat> cats = new List<Cat>();
        public Cat CurrentCat;
        public MainWindow()
        {
            InitializeComponent();

            cats.Add(
            new Cat()
            {
                Name = "tom",
                Age = 2,
                ImagePath = @"img\Cat1.jpg",
                Price = 1000,
                Species = new Species()
                {
                    Name = "British",
                    Descripteon = "Descripteon"
                }
            });

            cats.Add(
            new Cat()
            {
                Name = "Bublik",
                Age = 2,
                ImagePath = @"img\Cat2.jpg",
                Price = 1000,
                Species = new Species()
                {
                    Name = "British",
                    Descripteon = "Descripteon"
                }
            });

            cats.Add(
            new Cat()
            {
                Name = "WIscers",
                Age = 2,
                ImagePath = @"img\Cat3.jpg",
                Price = 1000,
                Species = new Species()
                {
                    Name = "British",
                    Descripteon = "Descripteon"
                }
            });



            DataContext = new CatsModel()
            {
                Cats = cats,
                CurrentCat = CurrentCat
            };

    }

        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            CurrentCat = (Cat)e.AddedItems[0];

        }

        class CatsModel
        {
            public IEnumerable<Cat> Cats { get; set; }
            public Cat CurrentCat { get; set; }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //string texCatName = tbCatName.Text;
            //cat.Name = texCatName;
        }
    }
}
