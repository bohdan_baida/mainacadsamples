﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public class Flight
    {
        public int index { get; set; }
        public string thisCity { get; set; }
        public string arriveCity { get; set; }
        public DateTime arriveDate { get; set; }
        public string airplanes { get; set; }
        public string airlines { get; set; }
        public string terminals { get; set; }
        public string status { get; set; }
    }
}
