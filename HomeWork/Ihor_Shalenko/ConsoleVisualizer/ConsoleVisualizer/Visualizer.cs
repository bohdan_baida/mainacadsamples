﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

namespace Visualizer
{
    public class Visualizer<T> : IVisualizer
    {
        List<T> flights;
        Dictionary<string, int> lenghts;

        public Visualizer(List<T> flights)
        {
            this.flights = flights;
            if (this.flights.Count != 0)
                ComputePropertiesLenghts();
        }

        public void Visualize()
        {
            if (this.flights.Count == 0)
                return;

            VisualizeEmptyLine();
            VisualizeHeaders();
            foreach (var flight in flights)
            {
                VisualizeEmptyLine();
                VisualizeFlight(flight);
            }
            VisualizeEmptyLine();
        }
        private void VisualizeEmptyLine()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("+ ");

            foreach (var property in lenghts)
            {
                sb.Append('-', property.Value);
                sb.Append(" + ");

            }
            Console.WriteLine(sb.ToString());



        }
        private void VisualizeHeaders()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("| ");

            foreach (var property in lenghts)
            {
                sb.Append(property.Key);
                sb.Append(' ', property.Value - property.Key.Length);
                sb.Append(" | ");
            }

            Console.WriteLine(sb.ToString());
        }
        private void VisualizeFlight(T flight)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("| ");
            foreach (var property in flight.GetType().GetProperties())
            {
                if (property.PropertyType.IsPrimitive == false
                    && property.PropertyType != typeof(string)
                    && property.PropertyType != typeof(decimal))
                    continue;

                var value = property.GetValue(flight).ToString();
                sb.Append(value);
                sb.Append(' ', lenghts.Single(len => len.Key == property.Name).Value - value.Length);
                sb.Append(" | ");
            }

            Console.WriteLine(sb.ToString());

        }

        private void ComputePropertiesLenghts()
        {
            Dictionary<string, int> result = new Dictionary<string, int>();

            var flightProperties = flights.First().GetType().GetProperties();

            foreach (var property in flightProperties)
            {
                if (property.PropertyType.IsPrimitive == false
                    && property.PropertyType != typeof(string)
                    && property.PropertyType != typeof(decimal))
                    continue;

                var name = property.Name;
                var maxLenght = flights.
                    Select(flight => flight.
                           GetType().
                           GetProperty(property.Name).
                           GetValue(flight).
                           ToString().
                           Length
                     ).
                     Max();
                result.Add(name, Math.Max(maxLenght, property.Name.Length));

            }

            lenghts = result;
        }
    }
}
