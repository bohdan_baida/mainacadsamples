﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;


namespace StudrntsTest
{
    class Program
    {
        const string pass = "12345";
        static void Main(string[] args)
        {
            using (DataExchange dbCon = new DataExchange())
            {
                while (true)
                {
                    Console.WriteLine("Select mode");
                    Console.WriteLine("1 - admin");
                    Console.WriteLine("2 - student");
                    Console.WriteLine("# - close app");


                    switch (Console.ReadLine())
                    {
                        case "1":
                            AdminWork(dbCon);
                            break;
                        case "2":
                            StudentWork(dbCon);
                            break;
                        case "#":
                            return;
                        default:
                            break;
                    }

                    Console.Clear();
                }
            }

        }
        static void StudentWork(DataExchange dbCon)
        {
            while (true)
            {

                Console.WriteLine("Write your ID:");
                Student stud = dbCon.ReturtnStudentFromBD(Convert.ToInt32(Console.ReadLine()));

                if (stud is null)
                {
                    Console.WriteLine("ID incorrect! Contact the teacher");
                    Console.WriteLine("1 - continue");
                    Console.WriteLine("2 - Exit");

                    switch (Console.ReadLine())
                    {
                        case "1":
                            Console.Clear();
                            continue;
                        case "2":
                            Console.Clear();
                            return;
                        default:
                            break;
                    }
                }

                Console.WriteLine($"Hi {stud.StudName}");
                Console.WriteLine("Press Enter to run the test!");

            }
        }

        static void AdminWork(DataExchange dbCon)
        {
            while (true)
            {
                Console.WriteLine("1 - ADD stud");
                Console.WriteLine("2 - DEL stud");

                Console.WriteLine("3 - ADD Question");
                Console.WriteLine("4 - DEL Question");

                Console.WriteLine("5 - print students list");
                Console.WriteLine("6 - print questions list");
                Console.WriteLine("7 - print students questions list");

                Console.WriteLine("# - Exit");


                switch (Console.ReadLine())
                {
                    case "1":
                        Console.WriteLine("StudName =");
                        Student stud = new Student { StudName = Console.ReadLine() };
                        dbCon.CreateNewStudent(stud);
                        break;
                    case "2":
                        Console.WriteLine("StudID =");
                        dbCon.DeleteStudent(Convert.ToInt32(Console.ReadLine()));
                        break;
                    case "3":
                        Question quest = new Question();
                        Console.WriteLine("Question text = ");
                        quest.TextQuestion = Console.ReadLine();

                        while (true)
                        {
                            Question.Answer answ = new Question.Answer();
                            Console.WriteLine("Answer text = ");
                            answ.TextAnswer = Console.ReadLine();
                            answ.Correctly = Convert.ToBoolean(Console.ReadLine());
                            if (answ.TextAnswer == "")
                            {
                                break;
                            }
                            quest.Answers.Add(answ);
                        }

                        dbCon.CreateNewQuestion(quest);

                        break;
                    case "4":
                        Console.WriteLine("Question ID =");
                        dbCon.DeleteQuestion(Convert.ToInt32(Console.ReadLine()));
                        break;
                    case "5":
                        List<Student> studList = dbCon.GetAllStudentFromBD();
                        foreach (var item in studList)
                        {
                            item.PrintStudent();
                        }
                        break;
                    case "6":
                        List<Question> questList = dbCon.GetAllQuestionFromBD();
                        foreach (var item in questList)
                        {
                            item.PrintQuestion();
                        }
                        break;
                    case "7":
                        break;
                    case "#":
                        break;
                    default:
                        break;
                }
                Console.ReadLine();
                Console.WriteLine("Press Enter to continue!");
                Console.Clear();

            }

        }
    }
}
