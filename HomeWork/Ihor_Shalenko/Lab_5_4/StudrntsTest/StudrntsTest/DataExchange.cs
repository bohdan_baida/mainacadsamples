﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace StudrntsTest
{
    public class DataExchange : IDisposable
    {
        public SqlConnection cn { get; set; }

        public DataExchange()
        {
            string cnStr = @"Data Source=elephant-ПК\SQLEXPRESS;Integrated Security=SSPI;Initial Catalog=QuestionsAnswers";

            DbProviderFactory dF = DbProviderFactories.GetFactory("System.Data.SqlClient");

            cn = (SqlConnection)dF.CreateConnection();

            cn.ConnectionString = cnStr;
            cn.Open();
            Console.WriteLine($"Data base is conected! Name: {cn.Database}");
        }


        public void CreateNewStudent(Student stud)
        {
            string sql = $"Insert Into Students(FirstName, LastName, Age) VALUES('{stud.StudName}', '', 20)";
            using (SqlCommand cmd = new SqlCommand(sql, cn))
            {
                cmd.ExecuteNonQuery();
            }

        }
        public void DeleteStudent(int id)
        {
            string sql = $"Delete From Students Where Id = {id}";
            using (SqlCommand cmd = new SqlCommand(sql, cn))
            {
                cmd.ExecuteNonQuery();
            }

        }
        public Student ReturtnStudentFromBD(int id)
        {
            string sql = $"Select * From Students Where Id = {id}";
            using (SqlCommand cmd = new SqlCommand(sql, cn))
            {
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    return new Student((int)dr["Id"], (string)dr["FirstName"]);
                }
                dr.Close();
            }
            return null;
        }
        public void CreateNewQuestion(Question quest)
        {
            string sql = $"Insert Into Questions (TextQuestion) VALUES('{quest.TextQuestion}')";
            using (SqlCommand cmd = new SqlCommand(sql, cn))
            {
                cmd.ExecuteNonQuery();
            }

            sql = $"Select * From Questions Where TextQuestion = {quest.TextQuestion}";
            using (SqlCommand cmd = new SqlCommand(sql, cn))
            {
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    quest.ID = (int)dr["Id"];
                }
                dr.Close();
            }

            foreach (var answ in quest.Answers)
            {
                sql = $"Insert Into Answers(QuestionId, TextAnswer, Correctly) VALUES('{quest.ID}', '{answ.TextAnswer}', '{answ.Correctly}')";
                using (SqlCommand cmd = new SqlCommand(sql, cn))
                {
                    cmd.ExecuteNonQuery();
                }
            }

        }
        public void DeleteQuestion(int id)
        {
            string sql = $"Delete From Questions Where Id = {id}";
            using (SqlCommand cmd = new SqlCommand(sql, cn))
            {
                cmd.ExecuteNonQuery();
            }

        }

        public List<Student> GetAllStudentFromBD()
        {
            List<Student> studList = new List<Student>();

            string sql = $"Select * From Students";
            using (SqlCommand cmd = new SqlCommand(sql, cn))
            {
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    studList.Add(new Student((int)dr["Id"], (string)dr["FirstName"]));
                }
                dr.Close();
            }

            return studList;
        }
        public List<Question> GetAllQuestionFromBD()
        {
            List<Question> questionList = new List<Question>();

            string sql = $"Select * From Questions";
            using (SqlCommand cmd = new SqlCommand(sql, cn))
            {
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Question quest = new Question{ID = (int)dr["Id"], TextQuestion = (string)dr["TextQuestion"]};

                    questionList.Add(quest);
                }
                dr.Close();
            }

            foreach (var quest in questionList)
            {
                sql = $"Select * From Answers Where QuestionId = {quest.ID}";
                using (SqlCommand cmd = new SqlCommand(sql, cn))
                {
                    SqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        quest.Answers.Add(new Question.Answer { ID = (int)dr["Id"], TextAnswer = (string)dr["TextAnswer"], Correctly = (bool)dr["Correctly"]});
                    }
                    dr.Close();
                }
            }

            return questionList;
        }
        public List<StudentsAnswers> GetAllStudentsAnswersFromBD()
        {
            List<StudentsAnswers> studAnsList = new List<StudentsAnswers>();

            string sql = $"Select * From StudentsAnswers";
            using (SqlCommand cmd = new SqlCommand(sql, cn))
            {
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    //studAnsList.Add(new StudentsAnswers {Student = (int)dr["Id"], (string)dr["FirstName"] });
                }
                dr.Close();
            }

            return studAnsList;
        }

        public void Dispose()
        {
            cn.Close();
        }
    }
}
