﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudrntsTest
{
    public class StudentsAnswers
    {
        public Student Student { get; set; }
        public Dictionary<Question, Question.Answer> Answers { get; set; }
    }
}
