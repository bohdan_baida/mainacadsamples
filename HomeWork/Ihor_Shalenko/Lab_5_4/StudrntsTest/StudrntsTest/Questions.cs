﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudrntsTest
{
    public class Question
    {
        public int ID { get; set; }
        public string TextQuestion { get; set; }
        public List<Answer> Answers { get; set; }

        public Question()
        {
            Answers = new List<Answer>();
        }
        public class Answer
        {
            public int ID { get; set; }
            public string TextAnswer { get; set; }
            public bool Correctly { get; set; }
        }

        public void PrintQuestion()
        {
            Console.WriteLine($"Question: {TextQuestion}");
            for (int i = 0; i < Answers.Count; i++)
            {
                if (Answers[i].Correctly == true)
                    Console.WriteLine($"      Answer №{i +1 }: {Answers[i].TextAnswer} (Correct)");
                else
                    Console.WriteLine($"      Answer №{i + 1}: {Answers[i].TextAnswer}");
            }

        }
    }
}
