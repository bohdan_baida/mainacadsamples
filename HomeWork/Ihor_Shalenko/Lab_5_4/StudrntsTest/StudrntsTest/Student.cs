﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudrntsTest
{
    public class Student
    {
        public int ID { get; set; }
        public string StudName { get; set; }

        public Student()
        { }
        public Student(int id, string studName)
        {
            ID = id;
            StudName = studName;
        }

        public void PrintStudent()
        {
            Console.WriteLine(ToString());
        }

        public override string ToString()
        {
            return $"ID: {ID} Name: {StudName}";
        }
    }
}
