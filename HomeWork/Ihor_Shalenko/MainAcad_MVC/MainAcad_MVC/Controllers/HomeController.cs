﻿using MainAcad_MVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MainAcad_MVC.Controllers
{
    public class HomeController : Controller
    {
        public List<UserModel> users { get; }

        public HomeController()
        {

            users = new List<UserModel>()
            {
                new UserModel()
                {
                    Id=1,
                    FirstName ="Bohdan",
                    LastName = "Baida"
                },
                new UserModel()
                {
                    Id=2,
                    FirstName ="Ivan",
                    LastName = "Petrov"
                }
            };
        }

        public ActionResult Index()
        {
            return View(users);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}