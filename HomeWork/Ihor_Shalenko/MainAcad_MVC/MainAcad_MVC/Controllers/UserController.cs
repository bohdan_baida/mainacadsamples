﻿using MainAcad_MVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MainAcad_MVC.Controllers
{
    public class UserController : Controller
    {
        // GET: User
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Edit(int id)
        {
            UserModel user = new HomeController()
                .users.Find(x => x.Id == id);

            return View(user);
        }
    }
}