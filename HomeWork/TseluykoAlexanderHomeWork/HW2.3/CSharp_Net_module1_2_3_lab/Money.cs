﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharp_Net_module1_2_3_lab
{
    // 1) declare enumeration CurrencyTypes, values UAH, USD, EU
    enum CurrencyTypes
    {
        UAH = 1,
        USD,
        EU
    }


    class Money
    {
        // 2) declare 2 properties Amount, CurrencyType
        public int Amount { get; set; }
         public CurrencyTypes CurrencyType { get; set; }
        // 3) declare parameter constructor for properties initialization
        public Money()
        {

        }
        public Money(CurrencyTypes cType ,int am)
        {
            Amount = am;
            CurrencyType = cType;
        }
        public Money(CurrencyTypes cType)
        {
            CurrencyType = cType;
        }
        // 4) declare overloading of operator + to add 2 objects of Money
        public static Money operator +(Money m1, Money m2)
        {
            //return new Money(m1.CurrencyType){ Amount = m1.Amount + m2.Amount };
            return new Money(m1.CurrencyType, m1.Amount + m2.Amount);
        }
        public static Money operator --(Money m1)
        {
            //return new Money(m1.CurrencyType){ Amount = m1.Amount + m2.Amount };
            return new Money(m1.CurrencyType, m1.Amount-- );
        }
        public static Money operator *(Money m1, int x)
        {
            //return new Money(m1.CurrencyType){ Amount = m1.Amount + m2.Amount };
            return new Money(m1.CurrencyType, m1.Amount * x);
        }
        public static bool operator >(Money m1, Money m2)
        {
            return m1.Amount > m2.Amount;
        }
        public static bool operator <(Money m1, Money m2)
        {
            return m1.Amount < m2.Amount;
        }
        public static bool operator true(Money m1)
        {
            return m1.CurrencyType ==CurrencyTypes.UAH;
        }
        public static bool operator false(Money m1)
        {
            return m1.CurrencyType != CurrencyTypes.UAH;
        }
        //public static explisit operator double(Money mn)
        //{
        //    return mn.Amount;

        //}
        public static implicit operator double(Money mn)
        {
            return mn.Amount;

        }
        public static explicit operator string(Money mn)
        {
            return mn.Amount.ToString() + " " + mn.CurrencyType;
        }


    }
    // 5) declare overloading of operator -- to decrease object of Money by 1

    // 6) declare overloading of operator * to increase object of Money 3 times

    // 7) declare overloading of operator > and < to compare 2 objects of Money

    // 8) declare overloading of operator true and false to check CurrencyType of object

    // 9) declare overloading of implicit/ explicit conversion  to convert Money to double, string and vice versa


}

