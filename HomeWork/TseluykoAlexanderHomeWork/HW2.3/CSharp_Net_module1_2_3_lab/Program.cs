﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharp_Net_module1_2_3_lab
{
    class Program
    {
        static void Main(string[] args)
        {
            // 10) declare 2 objects
            // 11) do operations
            // add 2 objects of Money
            var m1 = new Money(CurrencyTypes.EU, 300);
            var m2 = new Money(CurrencyTypes.EU, 600);
           
            var mn = m1 + m2;
            Console.WriteLine(mn);

            // add 1st object of Money and double
            var mpn = m1 + 2;
            Console.WriteLine(mpn);

            // decrease 2nd object of Money by 1 
            var mnp = m2 - 1;
            Console.WriteLine(mnp);

            // increase 1st object of Money
            mn = m1 * 3;
            Console.WriteLine(mn);

            // compare 2 objects of Money
            if (m2 > m1)
                Console.WriteLine($"{m2.Amount}>{m1.Amount}");

            // check CurrencyType of every object
            m2.CurrencyType = CurrencyTypes.UAH;
            if (m1)
                Console.WriteLine("National Currency");
            else
                Console.WriteLine("Foreign Currency");
            if (m2)
                Console.WriteLine("National Currency");
            else
                Console.WriteLine("Foreign Currency");
            double dbl = m1 + 0;
            Console.WriteLine($"double: {dbl}");

            // convert 1st object of Money to string
            string str = (string)m1;
            Console.WriteLine($"string : {str}");

            // compare 2nd object of Money and string
            if ((string)m2 == str)
                Console.WriteLine("true");
            else
                Console.WriteLine("false");
            Console.Read();
            
            

           

            

            

            

            

            

           

        }
    }
}
