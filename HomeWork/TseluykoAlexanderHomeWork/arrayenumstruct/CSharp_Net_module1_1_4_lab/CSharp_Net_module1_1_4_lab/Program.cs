﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharp_Net_module1_1_4_lab
{
    class Program
    {
       

        static void Main(string[] args)
        {

            var desktop = new Computer(ComputerType.Desktop, 4, 6, 500);
            var laptop = new Computer(ComputerType.Laptop, 2, 4, 250);
            var server = new Computer(ComputerType.Server, 8, 16, 2048);
            Computer[][] arr_dep = new Computer[4][];

            arr_dep[0] = new Computer[] { desktop, desktop, laptop, laptop, server };
            arr_dep[1] = new Computer[] { laptop, laptop, laptop };
            arr_dep[2] = new Computer[] { desktop, desktop, desktop, laptop, laptop };
            arr_dep[3] = new Computer[] { desktop, laptop, server, server };

            var total = 0;
            var totDesktop = 0;
            var totLaptop = 0;
            var totServer = 0;
            for (int i = 0; i < arr_dep.Length; i++)
            {
                total += arr_dep[i].Count();
                for (int j = 0; j < arr_dep[i].Length; j++)
                {
                    switch (arr_dep[i][j].CType)
                    {
                        case ComputerType.Desktop:
                            totDesktop++;
                            break;
                        case ComputerType.Laptop:
                            totLaptop++;
                            break;
                        case ComputerType.Server:
                            totServer++;
                            break;
                        default:
                            break;
                    }
                }
            }
            Console.WriteLine($"Total: {total}, Desktops={totDesktop}, Laptops={totLaptop}, Servers={totServer}");

            Computer[] arr_hd = new Computer[3] { desktop, server, laptop };

            //var maxElement = arr_hd[0];
            //int ind = 0;
            //for (int i = 0; i < arr_hd.Length; i++)
            //{
            //    var item = arr_hd[i];
            //    if (item.HDD > maxElement.HDD)
            //    {
            //        ind = i;
            //        maxElement = item;
            //    }
            //}
            var maxElement = arr_hd[0];
            foreach (var item in arr_hd)
            {
                if (item.HDD > maxElement.HDD)
                {
                    maxElement = item;
                }
            }
            Console.WriteLine($"Max HDD = {maxElement.HDD}, {maxElement.CType}, {Array.IndexOf(arr_hd, maxElement)} ");

            Computer minElement = arr_hd[0];
            int ind = 0;
            for (int i = 0; i < arr_hd.Length; i++)
            {
                var item = arr_hd[i];
                if (item.CPU < minElement.CPU & item.Memory < minElement.Memory)
                {
                    ind = i;
                    minElement = item;
                }
            }
            Console.WriteLine($"Min CPU = {minElement.CPU}, Memory = {minElement.Memory}, {minElement.CType}, {ind} ");

            for (int i = 0; i < arr_dep.Length; i++)
            {
                for (int j = 0; j < arr_dep[i].Length; j++)
                {
                    if ((arr_dep[i][j]).CType == ComputerType.Desktop)
                    {
                        arr_dep[i][j].Memory = 8;                
                    }

                }

            }
                

            Console.Read();
            // 3) declare jagged array of computers size 4 (4 departments)


            // 4) set the size of every array in jagged array (number of computers)


            // 5) initialize array
            // Note: use loops and if-else statements


            // 6) count total number of every type of computers
            // 7) count total number of all computers
            // Note: use loops and if-else statements
            // Note: use the same loop for 6) and 7)



            // 8) find computer with the largest storage (HDD) - 
            // compare HHD of every computer between each other; 
            // find position of this computer in array (indexes)
            // Note: use loops and if-else statements


            // 9) find computer with the lowest productivity (CPU and memory) - 
            // compare CPU and memory of every computer between each other; 
            // find position of this computer in array (indexes)
            // Note: use loops and if-else statements
            // Note: use logical oerators in statement conditions


            // 10) make desktop upgrade: change memory up to 8
            // change value of memory to 8 for every desktop. Don't do it for other computers
            // Note: use loops and if-else statements

        }
 
    }
}
