﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharp_Net_module1_1_4_lab
{

    // 1) declare enum ComputerType
    public enum ComputerType : byte
    {
        Desktop = 1,
        Laptop,
        Server
    }

    // 2) declare struct Computer
    struct Computer
    {
        public int CPU { get; }
        public int Memory { get; set; }
        public int HDD { get; }
        public ComputerType CType { get; }

        //constructor
        public Computer(ComputerType ctype, int cpu, int memory, int hdd)
        {
            CPU = cpu;
            Memory = memory;
            HDD = hdd;
            CType = ctype;
        }
    }
}

