﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hello_Cons_Dr_Methods
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                //Implement start position, width and height, symbol, message input
                Console.Write("Enter Position X: ");
                var x = (int)uint.Parse(Console.ReadLine());
                Console.Write("Enter Position Y: ");
                var y = (int)uint.Parse(Console.ReadLine());
                Console.Write("Enter Width: ");
                var width = (int)uint.Parse(Console.ReadLine());
                Console.Write("Enter Height: ");
                var height = (int)uint.Parse(Console.ReadLine());
                Console.Write("Enter Symbol: ");
                var symbol = Convert.ToChar((Console.ReadLine()));

                //Create Box class instance
                var bx = new Box(x, y, width, height, symbol);
                //Use  Box.Draw() method
                bx.Draw();
               
               
                

                

                Console.WriteLine("Press any key...");
            Console.ReadLine();
            }
            catch (Exception)
            {
                Console.WriteLine("Error!");
            }
            
        }
    }
}
