﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WpfDataSample.Class;

namespace WpfDataSample
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public CatsModel CatsModel { get; set; }
        //Зробити так, щоб кіт при редагуванні мінявся при натисканні кнопки Зберегти
        //Ввести Id для котів
        //При виборі кота зі списку копіювати його(new Cat(){....})
        //При збереженні переприсвоїти свойства оригінального кота свойствами копії
        public MainWindow()
        {
            InitializeComponent();

            var Cats = new List<Cat>();
            Cats.Add(new Cat()
            {   
                Id = 1,
                Name = "Tom",
                Age = 2,
                Price = 1000,
                ImagePath = @"Images/cat1.jpg",
                Species = new Species()
                {
                    Name = "British",
                    Description = "Super rare british cat. Ultra expensive"
                }
            });
            Cats.Add(new Cat()
            {
                Id = 2,
                Name = "Barsik",
                Age = 10,
                Price = 2000,
                ImagePath = @"Images/cat2.jpg",
                Species = new Species()
                {
                    Name = "Persian",
                    Description = "Super rare persian cat. Ultra expensive"
                }
            });
            Cats.Add(new Cat()
            {
                Id = 3,
                Name = "Puffy",
                Age = 2,
                Price = 500,
                ImagePath = @"Images/cat3.jpg",
                Species = new Species()
                {
                    Name = "Prosto kot",
                    Description = "Super rare prosto cat. Ultra expensive"
                }
            });

            CatsModel = new CatsModel()
            {
                Cats = Cats,
                CurrentCat = null
            };
            DataContext = CatsModel;
        }



        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Cat currentCat = (Cat)e.AddedItems[0];
            CatsModel.CurrentCat = new Cat()
            {
                Id = currentCat.Id,
                Name = currentCat.Name,
                Age = currentCat.Age,
                Price = currentCat.Price,
                Species = new Species()
                {
                    Name = currentCat.Species.Name,
                    Description = currentCat.Species.Description
                }
            };
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
           var cat = CatsModel.Cats.Where(c => c.Id == CatsModel.CurrentCat.Id).ToList<Cat>();
            cat[0].Name = CatsModel.CurrentCat.Name;
            cat[0].Age = CatsModel.CurrentCat.Age;
            cat[0].Price = CatsModel.CurrentCat.Price;
            cat[0].Species.Name = CatsModel.CurrentCat.Species.Name;
            cat[0].Species.Description = CatsModel.CurrentCat.Species.Description;
            
        }
    }

    public class CatsModel : INotifyPropertyChanged
    {
        private Cat currentCat;
        public IEnumerable<Cat> Cats { get; set; }
        public Cat CurrentCat
        {
            get { return currentCat; }
            set
            {
                currentCat = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("CurrentCat"));
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
