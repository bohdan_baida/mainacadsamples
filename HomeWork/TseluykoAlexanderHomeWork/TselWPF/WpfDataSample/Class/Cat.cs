﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfDataSample.Class
{
    public class Cat : INotifyPropertyChanged
    {
        private string name;
        public int Id;
        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                if (PropertyChanged!=null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("Name"));
                }
            }
        }
        
        public int Age { get; set; }

        public string ImagePath { get; set; }

        public Species Species { get; set; }

        public decimal Price { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;
    }

    public class Species
    {
        public string Name { get; set; }

        public string Description { get; set; }
    }
}
