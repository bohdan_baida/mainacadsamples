﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainAcad_Async
{
    public class Barber
    {
        public void Cut(Client client)
        {
            lock (this)
            {
                var time = EstimateTime(client.HaircutComplication);
                Task.Delay(time).Wait();
                Console.WriteLine($"Client {client.Name} was cutted");
            }
        }

        public int EstimateTime(HaircutComplicationEnum haircutComplication)
        {
            switch (haircutComplication)
            {
                case HaircutComplicationEnum.Easy:
                    return 4000;
                case HaircutComplicationEnum.Medium:
                    return 6000;
                case HaircutComplicationEnum.Hard:
                    return 10000;
                default:
                    throw new Exception("No enum element in switch");
            }
        }
    }
}
