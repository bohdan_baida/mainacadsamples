﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainAcad_Async
{
    public class Client
    {
        public Client(string name, HaircutComplicationEnum haircutComplication)
        {
            Name = name;
            HaircutComplication = haircutComplication;
        }
        public string Name { get; set; }
        public HaircutComplicationEnum HaircutComplication { get; set; }
    }

    public enum HaircutComplicationEnum
    {
        Easy,
        Medium,
        Hard
    }
}
