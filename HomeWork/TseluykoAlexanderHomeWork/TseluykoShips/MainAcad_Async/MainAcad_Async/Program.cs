﻿using MainAcad_Async.Port;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MainAcad_Async
{
    class Program
    {
        static void Main(string[] args)
        {
            //var result = Task.Run(() => Fib(5));
            //var task = new Task<int>(() => Fib(5));
            //task.ContinueWith(t => Console.WriteLine(t.Result));
            //task.Start();
            //task.Wait();
            //var task1 = new Task<int>(A1);

            //Console.WriteLine("End");
            ////task.Start();

            ////task.Wait();
            ////Console.WriteLine("Task finished");
            //Task.Factory.StartNew(() => Fib1(5));
            //Console.Read();

            //List<Task<int>> tasks = new List<Task<int>>();
            //for (int i = 4; i <= 10; i++)
            //{
            //    tasks.Add(new Task<int>(() => Fib(i)));
            //}
            //tasks.ForEach(c => c.Start());

            /////
            ///

            //var result = Task.WhenAll<int>(tasks).Result;

            //var task = Task.Run(() => A());
            //task.Wait();

            //Console.WriteLine("End");

            //CancellationTokenSource cancellationToken = new CancellationTokenSource();

            //var task1 = Task.Run(() => Fib(10, cancellationToken.Token));
            //Task.Delay(2000).Wait();
            //if (task1.Status!= TaskStatus.RanToCompletion)
            //{
            //    cancellationToken.Cancel();
            //}
            //task1.Wait();
            ////Cancellation tokens
            //Console.Read();

            //Task.WhenAll(task, task1);

            //var result = Parallel.For(3, 10, (n) => Fib1(n));
            //var result = await FibAsync(10);
            ////async/await
            //var barber = new Barber();
            //var clients = new List<Client>()
            //{
            //    new Client("Alex", HaircutComplicationEnum.Hard),
            //    new Client("Ivan", HaircutComplicationEnum.Easy),
            //    new Client("Misha", HaircutComplicationEnum.Medium),
            //    new Client("Bohdan", HaircutComplicationEnum.Medium)
            //};
            //foreach (var client in clients)
            //{
            //    Task.Run(() => barber.Cut(client));
            //}
            
            var port = new Port.Port();
            var ships = new List<Ship>()
            {
                new Ship(1),
                new Ship(2),
                new Ship(3),
                new Ship(4),
                new Ship(5),
                new Ship(6),
            };
            foreach (var ship in ships)
            {
                Task.Run(() => port.PrepareShip(ship)).ContinueWith(t => Trips(ship)).ContinueWith(t => Unloads(ship));
                    //.ContinueWith(bla bla) for path time
                    //.ContinueWith(bla bla) for un

            }
            Console.Read();
        }
        public static void Trips(Ship ship)
        {
            var path = new Path();
            path.Trip(ship);
        }
        public static void Unloads(Ship ship)
        {
            var delPort = new DeliveryPort();
            delPort.Unload(ship);
        }















        //ValueTask
        public async static Task<int> FibAsync(int n)
        {
            Console.WriteLine(2312);
            return await Task.Run(() => Fib(n));
        }

        public static void Fib1(int n)
        {
            int number = n - 1; //Need to decrement by 1 since we are starting from 0  
            int[] Fib = new int[number + 1];
            Fib[0] = 0;
            Fib[1] = 1;
            for (int i = 2; i <= number; i++)
            {
                Fib[i] = Fib[i - 2] + Fib[i - 1];
            }
            Console.WriteLine(Fib[number]);
            Task task = new Task(() =>
            {
                //var result = Program.Fib(3);
                Task.Delay(n * 400);
                //Console.WriteLine(result);
            }, TaskCreationOptions.AttachedToParent);
            task.Start();
            //return Fib[number];
        }
        public static int Fib(int n)
        {

            int number = n - 1; //Need to decrement by 1 since we are starting from 0  
            int[] Fib = new int[number + 1];
            Fib[0] = 0;
            Fib[1] = 1;
            for (int i = 2; i <= number; i++)
            {
                Task.Delay(n * 400).Wait();
                Fib[i] = Fib[i - 2] + Fib[i - 1];
            }
            return Fib[number];
        }
        public static int Fib(int n, CancellationToken token)
        {

            int number = n - 1; //Need to decrement by 1 since we are starting from 0  
            int[] Fib = new int[number + 1];
            Fib[0] = 0;
            Fib[1] = 1;
            for (int i = 2; i <= number; i++)
            {
                if (token.IsCancellationRequested)
                {
                    throw new Exception("Task is cancelled");
                }
                Task.Delay(n * 400).Wait();
                Fib[i] = Fib[i - 2] + Fib[i - 1];
            }
            return Fib[number];
        }
    }
}
