﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MainAcad_Async.Port
{
    public class Port
    {
        //Linear conguent method
        Random rand = new Random(DateTime.Now.Millisecond);
        Semaphore semaphore = new Semaphore(3, 3);

        public void PrepareShip(Ship ship)
        {
            semaphore.WaitOne();

            Task.Delay(rand.Next(2000, 6000)).Wait();
            ship.City = GetCity();
            ship.LaggageType = GetLaggageType();

            Console.WriteLine($"Ship {ship.Id} preapared to city {ship.City} with laggage {ship.LaggageType}");

            semaphore.Release();
        }

        public Cities GetCity()
        {
            var randNumber = rand.Next(1, 4);
            switch (randNumber)
            {
                case 1:
                    return Cities.Boston;
                case 2:
                    return Cities.NY;
                case 3:
                    return Cities.Singapure;
                default:
                    throw new Exception();
            }

        }

        public LaggageType GetLaggageType()
        {
            //LaggageType a = (LaggageType)1;
            var randNumber = rand.Next(1,3);
            switch (randNumber)
            {
                case 1:
                    return LaggageType.Food;
                case 2:
                    return LaggageType.Others;
                default:
                    throw new Exception();
            }
        }
    }
}
