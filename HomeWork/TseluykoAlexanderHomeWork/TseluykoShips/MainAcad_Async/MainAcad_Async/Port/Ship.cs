﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainAcad_Async.Port
{
    public class Ship
    {
        public int Id { get; set; }

        public LaggageType LaggageType { get; set; }

        public Cities City { get; set; }

        public Ship(int id)
        {
            Id = id;
        }
    }

    public enum LaggageType
    {
        Food = 1,
        Others
    }

    public enum Cities
    {
        NY,
        Boston,
        Singapure
    }
    
}
