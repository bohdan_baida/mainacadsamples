﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainAcad_Async.Port
{
    public class DeliveryPort
    {
        public void Unload(Ship ship)
        {
            if (ship.LaggageType == LaggageType.Food )
            {
                UnloadFood(ship);
            }
            else
            {
                UnloadGood(ship);
            }
        }
        private void UnloadFood(Ship ship)
        {
            lock (this)
            {
                Console.WriteLine($"Ship {ship.Id} is unloaded in Food Dock");
            }
        }
        private void UnloadGood(Ship ship)
        {
            lock (this)
            {
                Console.WriteLine($"Ship {ship.Id} is unloaded in Good Dock");
            }
        }


    }
}
