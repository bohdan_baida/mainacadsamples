﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainAcad_Async.Port
{
    public class Path
    {
        public int EstimateTime(Cities city)
        {
            switch (city)
            {
                case Cities.Boston:
                    return 4000;
                case Cities.NY:
                    return 6000;
                case Cities.Singapure:
                    return 10000;
                default:
                    throw new Exception("No enum element in switch");
            }
        }
        public void Trip(Ship ship)
        {
            lock (this)
            {
                var time = EstimateTime(ship.City);
                Task.Delay(time).Wait();
                Console.WriteLine($"Ship {ship.Id} on its way");
            }
        }
    }
}
