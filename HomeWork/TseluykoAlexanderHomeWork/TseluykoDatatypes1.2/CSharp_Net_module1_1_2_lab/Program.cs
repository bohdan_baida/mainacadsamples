﻿using System;

namespace CSharp_Net_module1_1_2_lab
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Datatypes");
            bool boo = true;
            char ch = '2' ;
            byte b = 3;
            sbyte sb = 4;
            short sh = 10000;
            ushort ush = 25;
            int i = -223;
            uint ui = 300;
            long l = 100000;
            ulong ul = 8;
            decimal de = -33;
            float fl = 2.2F;
            double d0 = 54;
            Console.WriteLine("{0} , {1} , {2} , {3} , {4} , {5} , {6} , {7} , {8} , {9} ,{10} , {11} , {12} ", boo, ch, b, sb, sh, ush, i, ui, l, ul, de , fl , d0);
            Console.Read();
            const int cint = 2;
            const double cdbl = 4;
            Console.WriteLine(byte.MinValue);
            Console.WriteLine(byte.MaxValue);
            Console.WriteLine(char.MinValue);
            Console.WriteLine(char.MaxValue);
            Console.WriteLine(sbyte.MinValue);
            Console.WriteLine(sbyte.MaxValue);
            Console.WriteLine(short.MinValue);
            Console.WriteLine(short.MaxValue);
            Console.WriteLine(ushort.MinValue);
            Console.WriteLine(ushort.MaxValue);
            Console.WriteLine(int.MinValue);
            Console.WriteLine(int.MaxValue);
            Console.WriteLine(uint.MinValue);
            Console.WriteLine(uint.MaxValue);
            Console.WriteLine(long.MinValue);
            Console.WriteLine(ulong.MaxValue);
            Console.WriteLine(decimal.MinValue);
            Console.WriteLine(decimal.MaxValue);
            Console.WriteLine(float.MinValue);
            Console.WriteLine(float.MaxValue);
            Console.WriteLine(double.MinValue);
            Console.WriteLine(double.MaxValue);


            // Use "Debugging" and "Watch" to study variables and constants

            //1) declare variables of all simple types:
            //bool, char, byte, sbyte, short, ushort, int, uint, long, ulong, decimal, float, double
            // their names should be: 
            //boo, ch, b, sb, sh, ush, i, ui, l, ul, de, fl, d0
            // initialize them with 1, 100, 250.7, 150, 10000, -25, -223, 300, 100000.6, 8, -33.1
            // Check results (types and values). Is possible to do initialization?
            // Fix compilation errors (change values for impossible initialization)I



            //2) declare constants of int and double. Try to change their values.

            boo = false;
            //3) declare 2 variables with var. Initialize them 20 and 20.5. Check types. 
            // Try to reinitialize by 20.5 and 20 (change values). What results are there?
            var p1 = 20.50;
            var p2 = 25;



            // 4) declare variables of System.Int32 and System.Double.
            // Initialize them by values of i and d0. Is it possible?
            System.Int32 z1 = i;
            System.Double z2 = d0;

            if (true)
            {
                // 5) declare variables of int, char, double 
                // with names i, ch, do
                // is it possible?
                int b1 = i;
                char b2 = ch;
                double b3 = d0;

                // 6) change values of variables from 1)

                i = (int)d0;
                long b5 = sb;

            }

            // 7)check values of variables from 1). Are they changed? Think, why


            // 8) use implicit/ explicit conversion to convert variables from 1). 
            // Is it possible? 

            // Fix compilation errors (in case of impossible conversion commemt that line).
           int ih = (int)ch;

            //bool pik = (bool)sh;

            double ji = (double)l;

            float fff = (float)ch;

            // int to char

            decimal dec = (decimal)d0;

            byte bsss = (byte)ui;

            ulong ulon = (ulong)sb;

            sbyte c = (sbyte)ul;

            uint bdc = (uint)b;

            double dd = (double)de;

            char cr = (char)fl;

            long lng = (long)d0;

            char chr = (char)i;


            // 9) and reverse conversion with fixing compilation errors.


            // 10) declare int nullable value. Initialize it with 'null'. 
            // Try to initialize variable i with 'null'. Is it possible?

            int? nl;
            nl = 7;
            nl = null;
        }
    }
}
