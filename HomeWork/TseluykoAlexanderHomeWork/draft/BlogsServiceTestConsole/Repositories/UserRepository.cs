﻿using DAL;
using Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositories
{
    public class UserRepository : BaseRepository , IUserRepository
    {

        public void AddUser(User user)
        {
            context.Users.Add(user);
            context.SaveChanges();
        }

        public void BlockUser(int userId)
        {
            var user = context.Users.Single(c => c.Id == userId);
            user.IsBlocked = true;
            context.SaveChanges();
        }
        
        public User GetUser(int userId)
        {
            return context.Users.Where(c => c.Id == userId).First();
        }
    }
}
