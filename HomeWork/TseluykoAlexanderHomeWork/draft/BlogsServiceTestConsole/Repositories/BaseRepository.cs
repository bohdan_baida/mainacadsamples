﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositories
{
    public class BaseRepository
    {
        protected BlogsContext context;

        public BaseRepository()
        {
            context = new BlogsContext();
        }
    }
}
