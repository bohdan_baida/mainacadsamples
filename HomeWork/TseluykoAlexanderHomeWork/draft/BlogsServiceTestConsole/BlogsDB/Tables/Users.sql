﻿create table Users(
Id int not null primary key identity(1,1),
FirstName varchar(max),
LastName varchar(Max),
Username varchar(max) not null,
PasswordHash varchar(max) not null, 
[IsBlocked] BIT NOT NULL DEFAULT 0
)