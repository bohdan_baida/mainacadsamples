﻿
create table Comments(
Id int not null primary key identity(1,1),
PostId int not null,
UserId int not null,
CommentText varchar(max) not null,
[ParentCommentId] int
foreign key (UserId) references dbo.Users(Id),
foreign Key (PostId) references dbo.Posts(Id),
foreign key ([ParentCommentId]) references Comments(Id)

)