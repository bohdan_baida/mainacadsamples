﻿create table UserRoles(
UserId int not null,
RoleId int not null
primary key(UserId,RoleId),
foreign key (UserId) references dbo.Users(Id),
foreign Key (RoleId) references dbo.Roles(RoleId)
)