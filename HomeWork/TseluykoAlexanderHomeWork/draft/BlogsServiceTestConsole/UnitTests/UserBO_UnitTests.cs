﻿using BusinessLogic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Moq;
using Repositories.Interfaces;
using BusinessLogic.Services;
using DAL;
using FluentAssertions;

namespace UnitTests
{
    public class UserBO_UnitTests
    {
        UserBO target;
        Mock<IUserRepository> repository = new Mock<IUserRepository>();
        Mock<ISmsService> smsService = new Mock<ISmsService>();
        public UserBO_UnitTests()
        {
            target = new UserBO(repository.Object, smsService.Object);
        }

        [Fact]
        public void BlockUser_When_UserHasBlogs_Then_ShouldThrowException()
        {
            // Arrange
            var user = new User();
            user.Id = 1;
            user.Posts = new List<Post>();

            repository.Setup(c => c.GetUser(1))
                .Returns(user);

            // Act
            Action action = () => target.BlockUser(1);

            // Assert
            action.Should().Throw<ApplicationException>();
        }

        [Fact]
        public void BlockUser_When_UserHasNotBlogs_Then_Should_BlockUser()
        {
            // Arrange
            var user = new User();
            user.Id = 1;
            user.Posts = new List<Post>();
            user.Posts.Add(new Post());

            repository.Setup(c => c.GetUser(It.IsAny<int>()))
                .Returns(user);

            // Act
            target.BlockUser(1);

            // Assert
            repository.Verify(c => c.BlockUser(1), Times.AtLeastOnce);
            smsService.Verify(c => c.Notify(1, "Your user account is blocked"));

        }
    }
}
