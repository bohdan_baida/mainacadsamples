﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogsServiceTestConsole
{
    public static class Extensions
    {
        public static void AddPost(this IEnumerable<User> users , Blog blog)
        {
            foreach (var user in users)
            {
                user.Blogs.Add(blog);
            }
        }
    }
}
