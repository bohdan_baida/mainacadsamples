﻿using BusinessLogic;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogsServiceTestConsole
{
    class Program
    {
        static void Main(string[] args)
        {

            UserBO userBO = new UserBO();
            
            var user = new User();
            //Console.WriteLine("Type FirstName:");
            //user.FirstName = Console.ReadLine();

            //Console.WriteLine("Type LastName:");
            //user.LastName = Console.ReadLine();

            Console.WriteLine("Type Username:");
            user.Username = Console.ReadLine();

            Console.WriteLine("Type Password:");
            user.PasswordHash = Console.ReadLine();
            var auth = userBO.GetUserAuth(user.Username, user.PasswordHash);
            if(auth == null || auth.Count()  != 1)
            {
                Console.WriteLine("Invalid Credential");
            }
            else
            {
                foreach (var item in userBO.GetUserBlogs(auth[0].Id))
                {
                    Console.WriteLine($"{item.BlogName} - {item.PostsCount}");
                }
            }


            //userBO.CreateUser(user);

            //var properties = typeof(User)
            //    .GetProperties(System.Reflection.BindingFlags.Instance
            //        | System.Reflection.BindingFlags.Public)
            //    .Where(c => c.PropertyType.IsPrimitive 
            //        || c.PropertyType.GUID == typeof(string).GUID);
            //foreach (var property in properties)
            //{
            //    Console.WriteLine(property.Name);
            //}
            //foreach (var user in userBO.GetUsers())
            //{
            //    foreach (var property in properties)
            //    {
            //        Console.WriteLine(property.GetValue(user));
            //    }
            //}


            //Console.WriteLine("Type Username:");
            //user.Username = Console.ReadLine();

            //Console.WriteLine("Type Password:");
            //user.PasswordHash = Console.ReadLine();




            //foreach (var item in userBO.ReadUsers())
            //{
            //    Console.WriteLine($"{item.FirstName} {item.LastName} {item.Username}");
            //}

            //foreach (var usr in userBO.GetUsers())
            //{
            //    Console.WriteLine($"{usr.Username} - {userBO.GetNumberOfBlogs(usr.Id)}");
            //}




            Console.Read();
        }
    }
    //1. Add changes to database(if needed)
    //2. Add changes to DAL(if needed)
    //3. Add changes to repository
    //4. Add changes to business logic
    //5. Add changes to UI

    //HW!
    //1. Move classes User,Post,Blog,Comment to separated library(dll)
    //2. Add references for newly created library to all layers of application
    //3. Visualize list of user in console
    //4. Add options to create new user and edit old user in console.
}
