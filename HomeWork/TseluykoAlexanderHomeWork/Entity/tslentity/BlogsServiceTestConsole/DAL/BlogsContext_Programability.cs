﻿using Entities.Composite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public partial class BlogsContext
    {
        public int GetNumberOfBlogs(int userId)
        {
            return this.Database.SqlQuery<int>(
                $"select [dbo].[GetNumberOfBlogs]({userId})"
                ).First();
        }
        public List<BlogWithPostsCount> GetBlogsForUser(int userId)
        {
            return this.Database.SqlQuery<BlogWithPostsCount>(
                $"select * FROM [dbo].[GetUserBlogs]({userId})"
                ).ToList();
        }
    }
}
