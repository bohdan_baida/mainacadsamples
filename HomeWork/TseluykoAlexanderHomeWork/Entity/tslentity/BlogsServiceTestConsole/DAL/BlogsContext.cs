namespace DAL
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using Entities.Composite;

    public partial class BlogsContext : DbContext
    {
        public BlogsContext()
            : base("name=BlogsContext1")
        {
        }

        public virtual DbSet<Blog> Blogs { get; set; }
        public virtual DbSet<Comment> Comments { get; set; }
        public virtual DbSet<Post> Posts { get; set; }
        public virtual DbSet<User> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //FluentAPI
            modelBuilder.Entity<Blog>()
                .Property(e => e.BlogName)
                .IsUnicode(false);

            modelBuilder.Entity<Blog>()
                .Property(e => e.BlogDescription)
                .IsUnicode(false);

            modelBuilder.Entity<Blog>()
                .HasMany(e => e.Posts)
                .WithRequired(e => e.Blog)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Blog>()
                .HasMany(e => e.Users)
                .WithMany(e => e.Blogs)
                .Map(m => m.ToTable("UserBlogs").MapLeftKey("BlogId").MapRightKey("UserId"));

            modelBuilder.Entity<Comment>()
                .Property(e => e.CommentText)
                .IsUnicode(false);

            modelBuilder.Entity<Comment>()
                .HasMany(e => e.Comments1)
                .WithOptional(e => e.Comment1)
                .HasForeignKey(e => e.ParentCommentId);

            modelBuilder.Entity<Post>()
                .Property(e => e.PostHeader)
                .IsUnicode(false);

            modelBuilder.Entity<Post>()
                .Property(e => e.PostBody)
                .IsUnicode(false);

            modelBuilder.Entity<Post>()
                .HasMany(e => e.Comments)
                .WithRequired(e => e.Post)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .Property(e => e.FirstName)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.LastName)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.Username)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.PasswordHash)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Comments)
                .WithRequired(e => e.User)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Posts)
                .WithRequired(e => e.User)
                .HasForeignKey(e => e.CreatedBy)
                .WillCascadeOnDelete(false);
            //modelBuilder.Entity<BlogWithPostsCount>()
            //   .Property(e => e.BlogName)
            //   .IsUnicode(false);

            //modelBuilder.Entity<BlogWithPostsCount>()
            //    .Property(e => e.PostsCount);

        }
    }
}
