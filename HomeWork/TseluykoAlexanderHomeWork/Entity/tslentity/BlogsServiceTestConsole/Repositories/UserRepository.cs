﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities.Composite;

namespace Repositories
{
    public class UserRepository : BaseRepository
    {

        public void AddUser(User user)
        {
            context.Users.Add(user);
            context.SaveChanges();
        }

        public void BlockUser(int userId)
        {
            var user = context.Users.Single(c => c.Id == userId);
            user.IsBlocked = true;
            context.SaveChanges();
        }

        public List<User> ReadUsers()
        {
            List<User> users = context.Users.ToList<User>();
            return users;
        }
        public IReadOnlyCollection<User> GetUsers()
        {
            return context.Users.AsNoTracking().ToList();
        }
        public List<BlogWithPostsCount> GetUserBlogs(int userid)
        {
            return context.GetBlogsForUser(userid);
        }
        public int GetNumberOfBlogs(int userId)
        {
            return context.GetNumberOfBlogs(userId);
        }
        public List<User> GetUserAuth(string userName, string password)
        {
            return context.Users.Where(c => c.Username == userName && c.PasswordHash == password).ToList();

        }
    }
}
