﻿using DAL;
using Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities.Composite;

namespace BusinessLogic
{
    public class UserBO
    {
        UserRepository userRepository;

        public UserBO()
        {
            userRepository = new UserRepository();
        }

        public void CreateUser(User user)
        {
            userRepository.AddUser(user);
        }

        public void BlockUser(int userid)
        {
            userRepository.BlockUser(userid);
        }

        public List<User> ReadUsers()
        {
            return userRepository.ReadUsers();
        }
        public IReadOnlyCollection<User> GetUsers()
        {
            return userRepository.GetUsers();
        }
        public List<BlogWithPostsCount> GetUserBlogs(int userid)
        {
            return userRepository.GetUserBlogs(userid);
        }

        public int GetNumberOfBlogs(int userId)
        {
            return userRepository.GetNumberOfBlogs(userId);
        }
        public List<User> GetUserAuth(string userName, string password)
        {

            return userRepository.GetUserAuth(userName, password);
        }

    }
}
