﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.Composite
{
    public class BlogWithPostsCount
    {
        public string BlogName { get; set; }

        public int PostsCount { get; set; }
    }
}
