﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharp_Net_module1_4_1_lab
{

    public class OnlineShop
    {
        public delegate void Handler(object obj, GoodsInfoEventArgs good);
        // 4) declare event of type EventHandler<GoodsInfoEventArgs>
        public event Handler EventHandler;
        // 5) declare method NewGoods for event initiation
        // use parameter string to get GoodsName
        // don't forget to check if event is not null
        // in true case intiate the event
        // use next line
        // your_event_name(this, new GoodsInfoEventArgs(GoodsName));

        public void NewGoods(string goodsname)
        {
            EventHandler?.Invoke(this, new GoodsInfoEventArgs(goodsname));
        }
        
    }

}
