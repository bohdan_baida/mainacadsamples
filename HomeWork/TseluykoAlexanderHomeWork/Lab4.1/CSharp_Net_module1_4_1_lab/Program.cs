﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharp_Net_module1_4_1_lab
{
    class Program
    {
        static void Main(string[] args)
        {
            // 9) declare object of OnlineShop
            var o1 = new OnlineShop();

            // 10) declare several objects of Customer
            var cust1 = new Customer("Alex");
            var cust2 = new Customer("Al");
            var cust3 = new Customer("Michael");
            // 11) subscribe method GotNewGoods() of every Customer instance
            // to event NewGoodsInfo of object of OnlineShop
            o1.EventHandler += cust1.GotNewGoods;
            o1.EventHandler += cust2.GotNewGoods;
            o1.EventHandler += cust3.GotNewGoods;



            // 12) invoke method NewGoods() of object of OnlineShop
            // discuss results
            o1.NewGoods("Computer");
            Console.Read();

        }
    }
}
