﻿using System;

namespace CSharp_Net_module1_2_1_lab
{
    interface ILibraryUser
    {
        void AddBook(string bookName);
        void RemoveBook(string bookName);
        string BookInfo(int index);
        int BooksCount();
        
    }

    // 1) declare interface ILibraryUser
    // declare method's signature for methods of class LibraryUser
    // 2) declare class LibraryUser, it implements ILibraryUser
    public class LibraryUser : ILibraryUser
    {
        public string FirstName { get; }
        public string LastName { get; }
        public int Id { get; }
        public string Phone { get; set; }
        public int BookLimit { get; set; }
        private string[] bookList = new string[0];
        public string this[int index]
        {
            get { return bookList[index]; }
            set { bookList[index] = value; }

        }
        public LibraryUser()
        {

        }
        public LibraryUser(string firstn, string lastn, string phone, int id)
        {
            FirstName = firstn;
            LastName = lastn;
            Id = id;
            Phone = phone;
        }
        public void AddBook(string bookName)
        {
            bookList = Add(bookList, bookName);

        }
        public void RemoveBook(string bookName)
        {
            bookList = Remove(bookList, bookName);

        }
        public string BookInfo(int index)
        {
            return bookList[index];
        }
        public int BooksCount()
        {
            return bookList.Length;
        }

        string[] Add(string[] array, string newValue)
        {
            int newLength = array.Length + 1;

            string[] result = new string[newLength];

            for (int i = 0; i < array.Length; i++)
                result[i] = array[i];

            result[newLength - 1] = newValue;

            return result;
        }

        string[] Remove(string[] array, string oldValue)
        {
            int newLength = array.Length - 1;

            if (newLength < 1)
            {
                return array;
            }

            string[] result = new string[newLength];
            int newCounter = 0;
            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] == oldValue)
                {
                    continue;
                }
                result[newCounter] = array[i];
                newCounter++;
            }

            return result;
        }
    }





        // 7) declare methods: 

        //AddBook() – add new book to array bookList

        //RemoveBook() – remove book from array bookList

        //BookInfo() – returns book info by index

        //BooksCout() – returns current count of books

    }
