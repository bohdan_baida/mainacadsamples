﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OrderService.Entities;

namespace OrderService
{
    public static class GoodMenu
    {
        public static void CategoryMenu(List<Good> goods)
        {
            Dictionary<string, decimal> bucket = new Dictionary<string, decimal>();
            int x = -1;
            int parentid = 0;
            bool exit = false;
            var categories = CategoriesHepler.categories;
            int mode = 0;
           


            try
            {
                do
                {
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.Clear();
                    //var list = categories.Select(c => (c.ParentCategory.Id == parentid) || (parentid == 0 && c.ParentCategory == null));

                    var lcat = from c in categories
                               where (parentid == 0 && c.ParentCategory == null) || (c.ParentCategory?.Id == parentid)
                               select new { Name = c.CategoryName, Id = c.Id };
                    var lgod = from g in goods
                              where (g.Category.Id == parentid)
                              select new { g.Name, g.Price };
                    int ind = 0;
                    foreach (var item in lcat)
                    {
                        ind++;
                        Console.WriteLine($"{ind}. {item.Name}");
                        mode = 0;
                    }
                    if(ind == 0)
                    {
                       
                       
                        foreach (var item in lgod)
                        {
                            ind++;
                            Console.WriteLine($"{ind}. {item.Name}, Price = {item.Price}");
                            
                        }
                        mode = 1;
                    }
                    
                    Console.WriteLine("0. Exit       <");
                    Console.WriteLine(" ");

                    Console.WriteLine("Selected items : ");
                    foreach (var item in bucket)
                    {
                        Console.WriteLine($"{item.Key}, {item.Value}");
                    }
                    try
                    {
                        Console.Write("Enter menu: ");
                        x = (int)uint.Parse(Console.ReadLine());
                        Console.WriteLine();
                        if (x >= 1 && x <= ind && mode == 0)
                        {
                            var cat = CategoriesHepler.GetCategory(lcat.ElementAt(x - 1).Id);
                            parentid = cat.Id;
                        }
                        else if (x >= 1 && x <= ind && mode == 1)
                        {
                            var good = GetGood(lgod.ElementAt(x - 1).Name, goods);
                            bucket[good.Name] = good.Price;
                        }
                        else if (x == 0 && parentid != 0)
                        {
                            var cat = CategoriesHepler.GetCategory(parentid);
                            if (cat.ParentCategory != null)
                            {
                                parentid = cat.ParentCategory.Id;
                            }
                            else
                            {
                                parentid = 0;
                            }
                        }
                        else if (x == 0 && parentid == 0)
                        {
                            exit = true;
                        }
                        
                    }

                    catch (Exception e)
                    {
                        Console.WriteLine("Error" + e.Message);
                    }
                    
                    Console.ForegroundColor = ConsoleColor.DarkYellow;
                    //Console.WriteLine("Press Spacebar to exit; press any key to continue");
                    Console.ForegroundColor = ConsoleColor.White;
                }
                while (!exit);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
           
        }
        public static Good GetGood(string name, List<Good> goods)
        {
            //categories.Where(c => c.ParentCategory == parCateg);
            return goods.FirstOrDefault(c => c.Name == name);
        }
    }
}
