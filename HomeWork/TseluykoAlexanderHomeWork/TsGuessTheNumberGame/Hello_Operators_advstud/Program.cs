﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hello_Operators_advstud
{
    class Program
    {
        static void Main(string[] args)
        {
            const int MyMax = 200;

            Random random = new Random();
            // random.Next(MaxValue) returns a 32-bit signed integer that is greater than or equal to 0 and less than MaxValue
            int Guess_number = random.Next(MyMax) + 1;
            // implement input of number and comparison result message in the while circle with  comparison condition
            Console.WriteLine("Guess the number !");
            int gs = 0;
            Console.WriteLine("The number is between 1 and {0}.", MyMax);

            while (gs != Guess_number)
            {
                Console.Write("Enter your guess: ");
                gs = Convert.ToInt32(Console.ReadLine());
                if (gs > Guess_number)
                {
                    Console.WriteLine("{0} too high!", gs);
                }
                else if (gs < Guess_number)
                {
                    Console.WriteLine("{0} too low!", gs);
                }
                else
                {
                    Console.WriteLine("{0} is right! Congratulations.", gs);
                    Console.ReadLine();
                }
            }
        }
    }
}
