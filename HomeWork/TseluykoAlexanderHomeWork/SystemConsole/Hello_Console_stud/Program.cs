﻿using System;
using System.Linq;
using System.Threading;

namespace Hello_Console_stud
{
    class Program
    {
        static void Main(string[] args)
        {
            int a;
            try
            {
                do
                {
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine(@"Please,  type the number:
                    1.  f(a,b) = |a-b| (unary)
                    2.  f(a) = a (binary)
                    3.  music
                    4.  morse sos
          
                    ");
                    try
                    {
                        a = (int)uint.Parse(Console.ReadLine());
                        switch (a)
                        {
                            case 1:
                                My_strings();
                                Console.WriteLine("");
                                break;
                            case 2:
                                My_Binary();
                                Console.WriteLine("");
                                break;
                            case 3:
                                My_music();
                                Console.WriteLine("");
                                break;
                            case 4:
                                Morse_code();
                                Console.WriteLine("");
                                break;
                            default:
                                Console.WriteLine("Exit");
                                break;
                        }

                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Error" + e.Message);
                    }
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("Press Spacebar to exit; press any key to continue");
                    Console.ForegroundColor = ConsoleColor.White;
                }
                while (Console.ReadKey().Key != ConsoleKey.Spacebar);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        #region ToFromBinary
        static void My_Binary()
        {
            //Implement positive integer variable input
            int a;
            Console.Write("Input (a): ");

            a = (int)uint.Parse(Console.ReadLine());

            //Present it like binary string
            //   For example, 4 as 100
            Console.WriteLine("Binary String: " + Convert.ToString(a, 2));

            //Use modulus operator to obtain the remainder  (n % 2) 
            //and divide variable by 2 in the loop
            Console.Write("Modulus (n % 2) = ");
            foreach (char symbol in a.ToString())
            {
                int b = Convert.ToInt32(symbol.ToString());
                Console.Write(Convert.ToString(b % 2));
            }
            Console.WriteLine();

            //Use the ToCharArray() method to transform string to chararray
            //and Array.Reverse() method
            char[] chars = a.ToString().ToCharArray();
            Array.Reverse(chars);
            Console.WriteLine("Reverse: " + string.Join("", chars));
        }
        #endregion

        #region ToFromUnary
        static void My_strings()
        {
            //UNFINISHED!!!!!!!!!!
            //Declare int and string variables for decimal and binary presentations
            int dec;
            string bin;

            //Implement two positive integer variables input
            int a = (int)uint.Parse(Console.ReadLine());
            int b = (int)uint.Parse(Console.ReadLine());

            //To present each of them in the form of unary string use for loop
            string s1 = "";
            foreach (char symbol in a.ToString())
            {
                int n = int.Parse(symbol.ToString());
                s1 += new String('1', n) + '#';
            }
            Console.WriteLine($"Unary String 1: {s1}");

            string s2 = "";
            foreach (var symbol in b.ToString())
            {
                s2 += new String('1', int.Parse(symbol.ToString()));
            }
            Console.WriteLine($"Unary String 2: {s2}");

            
            var s3 = string.Join("", s1.Split(new char[] { '#' }));

            var s4 = "";
            for (int i = 0; i < s1.Length; i++)
            {
                if (s1[i] != '#')
                {
                    s4 += s1[i];
                }
            }

            var s5 =string.Join("",s1.Where(c => c != '#'));
            
            //Use concatenation of these two strings 
            //Note it is necessary to use some symbol ( for example “#”) to separate

            //Check the numbers on the equality 0
            //Realize the  algorithm for replacing '1#1' to '#' by using the for loop 
            //Delete the '#' from algorithm result

            //Output the result 
        }
        #endregion

        #region My_music
        static void My_music()
        {
            //HappyBirthday
            Thread.Sleep(2000);
            Console.Beep(264, 125);
            Thread.Sleep(250);
            Console.Beep(264, 125);
            Thread.Sleep(125);
            Console.Beep(297, 500);
            Thread.Sleep(125);
            Console.Beep(264, 500);
            Thread.Sleep(125);
            Console.Beep(352, 500);
            Thread.Sleep(125);
            Console.Beep(330, 1000);
            Thread.Sleep(250);
            Console.Beep(264, 125);
            Thread.Sleep(250);
            Console.Beep(264, 125);
            Thread.Sleep(125);
            Console.Beep(297, 500);
            Thread.Sleep(125);
            Console.Beep(264, 500);
            Thread.Sleep(125);
            Console.Beep(396, 500);
            Thread.Sleep(125);
            Console.Beep(352, 1000);
            Thread.Sleep(250);
            Console.Beep(264, 125);
            Thread.Sleep(250);
            Console.Beep(264, 125);
            Thread.Sleep(125);
            Console.Beep(2642, 500);
            Thread.Sleep(125);
            Console.Beep(440, 500);
            Thread.Sleep(125);
            Console.Beep(352, 250);
            Thread.Sleep(125);
            Console.Beep(352, 125);
            Thread.Sleep(125);
            Console.Beep(330, 500);
            Thread.Sleep(125);
            Console.Beep(297, 1000);
            Thread.Sleep(250);
            Console.Beep(466, 125);
            Thread.Sleep(250);
            Console.Beep(466, 125);
            Thread.Sleep(125);
            Console.Beep(440, 500);
            Thread.Sleep(125);
            Console.Beep(352, 500);
            Thread.Sleep(125);
            Console.Beep(396, 500);
            Thread.Sleep(125);
            Console.Beep(352, 1000);
        }
        #endregion

        #region Morse
        static void Morse_code()
        {
            //Create string variable for 'sos'      
            string sos = "sos";
            //Use string array for Morse code
            string[,] Dictionary_arr = new string[,] { { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" },
            { ".-   ", "-... ", "-.-. ", "-..  ", ".    ", "..-. ", "--.  ", ".... ", "..   ", ".--- ", "-.-  ", ".-.. ", "--   ", "-.   ", "---  ", ".--. ", "--.- ", ".-.  ", "...  ", "-    ", "..-  ", "...- ", ".--  ", "-..- ", "-.-- ", "--.. ", "-----", ".----", "..---", "...--", "....-", ".....", "-....", "--...", "---..", "----." }};
            //Use ToCharArray() method for string to copy charecters to Unicode character array
            char[] arrch = sos.ToCharArray();

            //Use foreach loop for character array in which

            //Implement Console.Beep(1000, 250) for '.'
            // and Console.Beep(1000, 750) for '-'

            //Use Thread.Sleep(50) to separate sounds
            //   
            foreach (var symbol in arrch)
            {
                for (int i = 0; i < Dictionary_arr.Length / 2; i++)
                {
                    if (Dictionary_arr[0, i] == symbol.ToString())
                    {
                        string smrs = Dictionary_arr[1, i];

                        foreach (var sm in smrs)
                        {
                            if (sm == '.')
                                Console.Beep(1000, 250);
                            else if (sm == '-')
                                Console.Beep(1000, 750);
                            Thread.Sleep(50);
                        }
                    }
                }
            }

        }

        #endregion
    }
}
