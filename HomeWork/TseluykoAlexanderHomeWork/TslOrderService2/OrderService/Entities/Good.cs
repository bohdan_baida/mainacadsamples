﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderService.Entities
{
    public class Good
    {
        public string Name { get; set; }
        public decimal Price { get; set; }
        //public CategoriesEnum Category { get; set; }
        public Category Category { get; set; }
    }
}
