﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderService.Entities
{
    public class Category
    {
        public int Id { get; set; }
        public string CategoryName { get; set; }
        public Category ParentCategory { get; set; }

        public Category(int id, string categoryName, Category parentCategory)
        {
            Id = id;
            CategoryName = categoryName;
            ParentCategory = parentCategory;
        }
    }
}
