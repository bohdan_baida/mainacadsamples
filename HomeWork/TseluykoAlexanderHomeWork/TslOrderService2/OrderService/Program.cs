﻿using OrderService.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderService
{
    public class Program
    {
        static void Main(string[] args)
        {
            //1. Вибір товарів(навігація по категоріях + пошук)
            List<Good> goods = new List<Good>(); 
            var good1 = new Good()
            {
                Name = "Asus ZenBook 1",
                Price = 30000,
                Category = CategoriesHepler.GetCategory("Asus")
            };
            var good2 = new Good()
            {
                Name = "Apple MacBook",
                Price = 500000,
                Category = CategoriesHepler.GetCategory("Apple")
            };
            goods.Add(good1);
            goods.Add(good2);
            GoodMenu.CategoryMenu(goods);
            Console.Read();
        }
    }
}
