﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OrderService.Entities;
 
namespace OrderService
{
    public delegate void SelectedItems(object obj, List<Good> goods);
     
    public enum MenuMode
    {
        Root = 0,
        Categories,
        Goods

    }
    public class MenuProcessor
    {
        public event SelectedItems ApplyEvent;
        private MenuMode mode = 0;
        private Category lastcategory;
        private List<Good> goods;
        private Dictionary<int, string> menuList = new Dictionary<int, string>();
        List<Good> bucket = new List<Good>();
        public MenuProcessor(List<Good> goods)
        {
            this.goods = goods;
        }
        public  Good GetGoods(string name, List<Good> goods)
        {
            //categories.Where(c => c.ParentCategory == parCateg);
            return goods.FirstOrDefault(c => c.Name == name);
        }
        public void DisplayMenu()
        {
            if (mode == MenuMode.Root)
            {
                DisplayRoot();
            }
            else
                DisplayTree();
            Console.WriteLine();
            Console.WriteLine("Selected items : ");
            foreach (var item in bucket)
            {
                Console.WriteLine($"{item.Name}, {item.Price}");
            }

        }
        private void DisplayRoot()
        {
            Console.WriteLine("1. Search for the goods ");
            Console.WriteLine("2. Confirm your order ");
            Console.WriteLine("0. Exit    ");
           
        }
        private void DisplayTree()
        {
            var categories = CategoriesHepler.categories;
            var parentid = lastcategory == null?0:lastcategory.Id;
            menuList.Clear();

            var lcat = from c in categories
                       where (parentid == 0 && c.ParentCategory == null) || (c.ParentCategory?.Id == parentid)
                       select new { MenuName = c.CategoryName, Entid = c.Id};
            var lgod = from g in goods
                       where (g.Category.Id == parentid)
                       select new {MenuName = g.Name + "Price: " + g.Price.ToString(), Entid = g.Name };
            mode = MenuMode.Categories;
            int ind = 0;
            foreach (var item in lcat)
            {
                ind++;
                Console.WriteLine($"{ind}. {item.MenuName}");
                menuList[ind] = item.Entid.ToString();
            }
            if (ind == 0)
            {


                foreach (var item in lgod)
                {
                    ind++;
                    Console.WriteLine($"{ind}. {item.MenuName}");
                    menuList[ind] = item.Entid.ToString();
                }
                mode = MenuMode.Goods;
                
            }
            Console.WriteLine("0. Back    ");
        }
        public int HandleChoice(int menuitem)
        {
            int res = 0;
            if(mode == MenuMode.Root)
            {
                if(menuitem == 0)
                {
                    res = 1;
                }
                else if (menuitem == 1)
                {
                    mode = MenuMode.Categories;
                }
                else if (menuitem == 2)
                {
                    AcceptOrder();
                }
            }
            else if (mode == MenuMode.Categories)
            {
                if (menuitem == 0)
                {

                    lastcategory = lastcategory?.ParentCategory;
                    if (lastcategory == null)
                    {
                        mode = MenuMode.Root;
                    }
                    
                }
                else if (menuitem >= 1 && menuitem <= menuList.Count)
                {
                    var id = menuList[menuitem];
                    var cat = CategoriesHepler.GetCategory(Convert.ToInt32(id));
                    
                    lastcategory = cat;
                }
                
                
            }
            else if (mode == MenuMode.Goods)
            {
                if (menuitem == 0)
                {
                    lastcategory = lastcategory?.ParentCategory;
                }
                else if (menuitem >= 1 && menuitem <= menuList.Count)
                {
                    var id = menuList[menuitem];
                    var good = GetGoods(id, goods);
                    bucket.Add(good);
                }
                
            }
            
            
            return res;
        }
        public void AcceptOrder()
        {
            Console.Clear();
            foreach (var item in bucket)
            {
                Console.WriteLine($"{item.Name}, {item.Price}");
            }
            Console.WriteLine("Are you sure ? (y/n) ");
            var stash = Console.ReadLine();
            if (stash == "y")
            {
                ApplyEvent?.Invoke(this, bucket);
                bucket.Clear();
            }




        }
    }
}
