﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Airline_info_proj
{
    struct AirportDeparture
    {
        public int FligtNumber { get; set; }
        public string DeptTo { get; set; }
        public DateTime DeptTime { get; set; }
        public string Airline { get; set; }
        public Gates Gate { get; set; }
        public Terminal Terminal { get; set; }
        public FlStatus FlightStatus { get; set; }

        public AirportDeparture(int flightnumber, string deptTo, DateTime deptTime, string airline, Gates gate, Terminal terminal, FlStatus flightstatus)
        {
            FligtNumber = flightnumber;
            DeptTo = deptTo;
            DeptTime = deptTime;
            Airline = airline;
            Gate = gate;
            Terminal = terminal;
            FlightStatus = flightstatus;
        }

        public void Display()
        {
            Console.WriteLine($"{FligtNumber} | {DeptTo.PadRight(8)} | {DeptTime:dd.MM.yyy HH:mm} | {Airline} | {Gate} | {Terminal} | {FlightStatus}");
        }

    }
}
