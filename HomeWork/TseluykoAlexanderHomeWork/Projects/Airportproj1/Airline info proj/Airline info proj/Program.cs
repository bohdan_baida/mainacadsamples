﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Airline_info_proj
{
    class Program
    {
      
        static void Main(string[] args)
        {
            int x;
            int dcnt = 0;
            int acnt = 0;
            try
            {
                do
                {
                    Console.ForegroundColor = ConsoleColor.Blue;
                    Console.WriteLine("1. Departures ");
                    Console.WriteLine("2. Arrivals");
                    Console.WriteLine("3. Emergency");
                    try
                    {
                        x = (int)uint.Parse(Console.ReadLine());
                        switch (x)
                        {
                            case 1:
                                Departures(dcnt);
                                Console.WriteLine("");
                                dcnt++;
                                break;
                            case 2:
                                Arrivals(acnt);
                                Console.WriteLine("");
                                acnt++;
                                break;
                            case 3:
                                Emergency_info();
                                Console.WriteLine("");
                                break;
                            default:
                                Console.WriteLine("Exit");
                                break;
                        }
                        
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Error" + e.Message);
                    }
                    Console.ForegroundColor = ConsoleColor.DarkYellow;
                    Console.WriteLine("Press Spacebar to exit; press any key to continue");
                    Console.ForegroundColor = ConsoleColor.White;
                }
                while (Console.ReadKey().Key != ConsoleKey.Spacebar);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        #region Departures
        static void Departures(int dcnt)
        {
            var dept = new AirportDeparture[] {
                new AirportDeparture(208, "Odessa", DateTime.Now.AddHours(3), "Turkish", Gates.Gate2, Terminal.Terminal6, FlStatus.Departured),
                new AirportDeparture(211, "Odessa", DateTime.Now, "Turkish", Gates.Gate3, Terminal.Terminal1, FlStatus.Delayed),
                new AirportDeparture(905, "Odessa", DateTime.Now.AddMinutes(30), "Turkish", Gates.Gate1, Terminal.Terminal3, FlStatus.InFly),
                new AirportDeparture(301, "Kharkov", DateTime.Now.AddMinutes(12), "Turkish", Gates.Gate4, Terminal.Terminal5, FlStatus.InFly)
            };

            //dept = dept.OrderBy(a => a.DeptTime).ToArray();

            for (int i = dept.Length-1; i > 0; i--)
            {
                // Console.WriteLine(i);
                for (int j = 0; j < i; j++)
                {
                    // Console.WriteLine(j + " " + (j+1));
                    if (dept[j].DeptTime > dept[j+1].DeptTime)
                    {
                        AirportDeparture tmp = dept[j];
                        dept[j] = dept[j + 1];
                        dept[j + 1] = tmp;

                    }
                }
            }
    

            if (dcnt > 0)
            {
                Random rand = new Random();
                dept[2].FlightStatus = (FlStatus)rand.Next(1, 4); 
                dept[3].FlightStatus = (FlStatus)rand.Next(1, 4);
            }

            foreach (var item in dept)
            {
                item.Display();
            }
        }
        #endregion
        #region Arrivals
        static void Arrivals(int acnt)
        {
            var arrv = new AirportArrival[]
            {
            new AirportArrival(120,"Kyiv",DateTime.Now.AddHours(3),"Turkish",Gates.Gate3,Terminal.Terminal4,FlStatus.InFly),
            new AirportArrival(112,"Kyiv",DateTime.Now.AddHours(1),"Turkish",Gates.Gate2,Terminal.Terminal1,FlStatus.Delayed),
            new AirportArrival(180,"Lvov",DateTime.Now.AddHours(2),"Ukraine",Gates.Gate1,Terminal.Terminal5,FlStatus.Cancelled),
            new AirportArrival(203,"Krakow",DateTime.Now.AddMinutes(1),"Turkish",Gates.Gate4,Terminal.Terminal7,FlStatus.Arrived),
            new AirportArrival(260,"Lvov",DateTime.Now.AddMinutes(50),"Turkish",Gates.Gate2,Terminal.Terminal3,FlStatus.InFly)
            };
            arrv = arrv.OrderBy(a => a.ArrTime).ToArray();
            if (acnt > 0)
            {
                Random rand = new Random();
                arrv[1].FlightStatus = (FlStatus)rand.Next(1, 3);
                arrv[2].FlightStatus = (FlStatus)rand.Next(1, 4);
                arrv[3].Terminal = (Terminal)rand.Next(1, 3);
            }
            foreach (var item in arrv)
            {
                item.Display();
            }

        }
        #endregion
        #region Emergency_info
        static void Emergency_info()
        {
            Console.WriteLine(@"Briefly about Emergency.");
            Console.WriteLine("In case of fire, specially trained people are involved in the evacuation.");
            Console.WriteLine("Please follow their directions and do not do anything alone.");
            Console.WriteLine("At the airport itself, evacuation passes through the main exits.");
            Console.WriteLine("The main thing - do not panic!");
        }
        #endregion
    }
}
