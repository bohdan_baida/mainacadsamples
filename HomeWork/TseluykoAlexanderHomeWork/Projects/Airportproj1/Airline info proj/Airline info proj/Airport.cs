﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Airline_info_proj
{

    enum Gates : int
    {
        Gate1 = 1,
        Gate2,
        Gate3,
        Gate4,
        Gate5
    }

    enum Terminal : int
    {
        Terminal1,
        Terminal2,
        Terminal3,
        Terminal4,
        Terminal5,
        Terminal6,
        Terminal7
    }

    enum FlStatus : int
    {
        Arrived,
        Departured,
        CheckIn,
        Cancelled,
        InFly,
        Delayed
    }

}
