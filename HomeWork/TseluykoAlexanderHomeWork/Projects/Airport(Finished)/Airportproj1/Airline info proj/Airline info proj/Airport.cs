﻿namespace Airline_info_proj
{

    enum GatesEnum : byte
    {
        Gate1 = 1,
        Gate2,
        Gate3,
        Gate4,
        Gate5
    }

    enum TerminalsEnum : byte
    {
        Terminal1,
        Terminal2,
        Terminal3,
        Terminal4,
        Terminal5,
        Terminal6,
        Terminal7
    }

    enum FlStatus  : byte
    {
        Arrived,
        Departured,
        CheckIn,
        Cancelled,
        InFly,
        Delayed
    }

}
