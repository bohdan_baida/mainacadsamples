﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Airline_info_proj
{
    class Program
    {
      
        static void Main(string[] args)
        {
            int x = -1;
            int menu = 0;
            int[] pres = { 3, 11, 12, 13, 21, 22, 23 };
            try
            {
                do
                {
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.Clear();
                    if (menu == 0)
                    {
                        Console.WriteLine("+++++++++++++++++++++++++++++");
                        Console.WriteLine("+         Main menu         +");
                        Console.WriteLine("+++++++++++++++++++++++++++++");
                        Console.WriteLine("1. Departures >");
                        Console.WriteLine("2. Arrivals   > ");
                        Console.WriteLine("3. Emergency");
                        Console.WriteLine();
                        Console.WriteLine("0. Exit       <");
                        Console.WriteLine("+++++++++++++++++++++++++++++");
                    }
                    else if (menu == 1)
                    {
                        Console.WriteLine("+++++++++ Departures ++++++++");
                        Console.WriteLine("1. Show All");
                        Console.WriteLine("2. Serach by FlightNumber...");
                        Console.WriteLine("3. Search by nearest hour...");
                        Console.WriteLine();
                        Console.WriteLine("0. Go Back                 <");
                        Console.WriteLine("+++++++++++++++++++++++++++++");
                    }
                    else if (menu == 2)
                    {
                        Console.WriteLine("+++++++++ Arrivals +++++++++");
                        Console.WriteLine("1. Show All");
                        Console.WriteLine("2. Serach by FlightNumber...");
                        Console.WriteLine("3. Search by nearest hour...");
                        Console.WriteLine();
                        Console.WriteLine("0. Go Back                 <");
                        Console.WriteLine("+++++++++++++++++++++++++++++");
                    }

                    try
                    {
                        Console.Write("Enter menu: ");
                        x = 10*menu + (int)uint.Parse(Console.ReadLine());
                        Console.WriteLine();
                        switch (x)
                        {
                            case 1:
                                menu = 1;
                                break;
                            case 2:
                                menu = 2;
                                break;
                            case 3:
                                Emergency_info();
                                break;
                            case 0:
                                break;
                            case 11:
                                Departures(x, 0);
                                break;
                            case 12:
                                Console.Write("Enter Flight Number: ");
                                var fltnum = (int)uint.Parse(Console.ReadLine());
                                Departures(x, fltnum);
                                break;
                            case 13:
                                Departures(x, 0);
                                break;
                            case 10:
                                menu = 0;
                                break;
                            case 21:
                                Arrivals(x,0);
                                break;
                            case 22:
                                Console.Write("Enter Flight Number: ");
                                var flnum = (int)uint.Parse(Console.ReadLine());
                                Arrivals(x, flnum);
                                break;
                            case 23:
                                Arrivals(x, 0);
                                break;
                            case 20:
                                menu = 0;
                                break;
                        }
                        
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Error" + e.Message);
                    }
                    Console.ForegroundColor = ConsoleColor.DarkYellow;
                    //Console.WriteLine("Press Spacebar to exit; press any key to continue");
                    Console.ForegroundColor = ConsoleColor.White;
                    if (pres.Contains(x))
                    {
                        Console.Write("Please enter any key...");
                        Console.ReadKey();
                    }
                }
                while (x != 0);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        #region Departures
        static void Departures(int opt, int fltnum)
        {
            var dept = new AirportDeparture[] {
                new AirportDeparture(208, "Odessa", DateTime.Now.AddHours(3), "Turkish", GatesEnum.Gate2, TerminalsEnum.Terminal6, FlStatus.Departured),
                new AirportDeparture(211, "Odessa", DateTime.Now, "Turkish", GatesEnum.Gate3, TerminalsEnum.Terminal1, FlStatus.Delayed),
                new AirportDeparture(905, "Odessa", DateTime.Now.AddMinutes(30), "Turkish", GatesEnum.Gate1, TerminalsEnum.Terminal3, FlStatus.InFly),
                new AirportDeparture(301, "Kharkov", DateTime.Now.AddMinutes(12), "Turkish", GatesEnum.Gate4, TerminalsEnum.Terminal5, FlStatus.InFly)
            };

            Random rand = new Random();
            if (rand.Next(0, 2) > 0)
            {
                dept[2].FlightStatus = (FlStatus)rand.Next(1, 4);
                dept[3].FlightStatus = (FlStatus)rand.Next(1, 4);
            }


            //dept = dept.OrderBy(a => a.DeptTime).ToArray();

            for (int i = dept.Length-1; i > 0; i--)
            {
                // Console.WriteLine(i);
                for (int j = 0; j < i; j++)
                {
                    // Console.WriteLine(j + " " + (j+1));
                    if (dept[j].DeptTime > dept[j+1].DeptTime)
                    {
                        AirportDeparture tmp = dept[j];
                        dept[j] = dept[j + 1];
                        dept[j + 1] = tmp;

                    }
                }
            }

             foreach (var item in dept)
            {
                var additem = false;
                if (opt == 11)
                {
                    additem = true;
                }
                else if (opt == 13 && item.DeptTime <= DateTime.Now.AddHours(1))
                {
                    additem = true;
                }
                else if (opt == 12 && item.FligtNumber == fltnum)
                {
                    additem = true;
                }
                if (additem)
                    item.Display();
            }
        }
        #endregion
        #region Arrivals
        static void Arrivals(int ard, int flnum)
        {
            var arrv = new AirportArrival[]
            {
            new AirportArrival(120,"Kyiv",DateTime.Now.AddHours(3),"Turkish",GatesEnum.Gate3,TerminalsEnum.Terminal4,FlStatus.InFly),
            new AirportArrival(112,"Kyiv",DateTime.Now.AddHours(1),"Turkish",GatesEnum.Gate2,TerminalsEnum.Terminal1,FlStatus.Delayed),
            new AirportArrival(180,"Lvov",DateTime.Now.AddHours(2),"Ukraine",GatesEnum.Gate1,TerminalsEnum.Terminal5,FlStatus.Cancelled),
            new AirportArrival(203,"Krakow",DateTime.Now.AddMinutes(1),"Turkish",GatesEnum.Gate4,TerminalsEnum.Terminal7,FlStatus.Arrived),
            new AirportArrival(260,"Lvov",DateTime.Now.AddMinutes(50),"Turkish",GatesEnum.Gate2,TerminalsEnum.Terminal3,FlStatus.InFly)
            };

            Random rand = new Random();
            if (rand.Next(0, 2) > 0)
            {
                arrv[1].FlightStatus = (FlStatus)rand.Next(1, 3);
                arrv[2].FlightStatus = (FlStatus)rand.Next(1, 4);
                arrv[3].Terminal = (TerminalsEnum)rand.Next(1, 3);
            }

            arrv = arrv.OrderBy(a => a.ArrTime).ToArray();

            foreach (var item in arrv)
            {
                var additem = false;
                if (ard == 21)
                {
                    additem = true;
                }
                else if (ard == 23 && item.ArrTime <= DateTime.Now.AddHours(1))
                {
                    additem = true;
                }
                else if (ard == 22 && item.FligtNumber == flnum)
                {
                    additem = true;
                }
                if (additem)
                    item.Display();
            }
            }
        #endregion
        #region Emergency_info
        static void Emergency_info()
        {
            Console.WriteLine(@"Briefly about Emergency.");
            Console.WriteLine("In case of fire, specially trained people are involved in the evacuation.");
            Console.WriteLine("Please follow their directions and do not do anything alone.");
            Console.WriteLine("At the airport itself, evacuation passes through the main exits.");
            Console.WriteLine("The main thing - do not panic!");
        }
        #endregion
    }
}
