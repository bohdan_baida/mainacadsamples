﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Airline_info_proj
{
    struct AirportArrival
    {
        public int FligtNumber { get; set; }
        public string ArrFrom { get; set; }
        public DateTime ArrTime { get; set; }
        public string Airline { get; set; }
        public GatesEnum Gate { get; set; }
        public TerminalsEnum Terminal { get; set; }
        public FlStatus FlightStatus { get; set; }

        public AirportArrival(
            int flightnumber,
            string arrFrom,
            DateTime arrTime,
            string airline,
            GatesEnum gate,
            TerminalsEnum terminal,
            FlStatus flightStatus)
        {
            FligtNumber = flightnumber;
            ArrFrom = arrFrom;
            ArrTime = arrTime;
            Airline = airline;
            Gate = gate;
            Terminal = terminal;
            FlightStatus = flightStatus;
        }
        public void Display()
        {
            Console.WriteLine($"{FligtNumber} | {ArrFrom.PadRight(8)} | {ArrTime:dd.MM.yyy HH:mm} | {Airline} | {Gate} | {Terminal} | {FlightStatus}");
        }
    }
}

