﻿using System;

namespace Hello_Exception_stud
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Observation titmouse flight");
            //Bird My_Bird = new Bird("Titmouse", 20);
            Bird myBird = new Bird(20,"Titmouse");


            //1. Create the skeleton code with the  basic exception handling for the menu in the main method 
            //try - catch
            // 1. begin
            char rdk;
            try
            {
                do
                {
                    Console.WriteLine("Monitoring in try block");
                    Console.WriteLine(@"Please, type the number
                             1. Array overflow 
                             2. Throw exception
                             3. User exception ");
                    uint i = uint.Parse(Console.ReadLine());
                    try
                    {

                        switch (i)
                        {
                            case 1:
                                myBird.FlySpeed[4] = 7;
                                
                                break;
                            case 2:
                                throw (new System.Exception("Oh! My System Exception..."));
                            case 3:
                                for (int x = 0; x < 20; x+=5)
                                {
                                    myBird.FlyAway(x);
                                }
                                break;
                            default:
                                break;
                        }
                    }
                    catch (BirdFlewAwayException e)
                    {
                        Console.WriteLine("CLS Exception: Message: " + e.Message + " Source: " + e.Source);
                    }

                    finally
                    {
                        Console.WriteLine("For the next step...");
                    }
                    rdk = Console.ReadKey().KeyChar;
                }
                while (rdk != ' ');
            }

            catch (Exception mn)
            {
                Console.WriteLine(mn.Message);
            }


            //2. Create code for the nested special exception handling in the main method
            //try - catch - catch - finally
            // 2. begin
            //try
            //{

            //}
            //catch (BirdFlewAwayException e)
            //{
            //    Console.WriteLine("CLS Exception: Message -" + e.Message + "Source" + e.Source);
            //}

            //finally
            //{
            //    Console.WriteLine("For the next step...");
            //}
            //Console.WriteLine("Monitoring in try block");
            //Console.WriteLine(@"Please, type the number
            //                 1. Array overflow 
            //                 2. Throw exception
            //                 3. User exception ");
            //uint i = uint.Parse(Console.ReadLine());
            //switch (i)
            //{
            //    case 1:
            //        break;
            //    case 2:
            //        throw (new System.Exception("Oh! My System Exception..."));
            //    case 3:
            //        break;
            //    default:
            //        break;
            //}
            Console.Read();
            //3. Create the menu for three options in the inner try block  
            //In the second option throw the System.Exception
            // 3. begin

            //4. in case 1 code array overflow exception 
            //in case 2 code throw (new System.Exception("Oh! My system exception..."));
            //in case 3  code the sequentially incrementing of Bird speed until to the exception 

            // 3. end

            // 2. end

            // 1. end

        }

    }
}
