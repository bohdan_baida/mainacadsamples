﻿using System;

namespace Hello_Exception_stud
{
    //Create the BirdFlewAwayException class, derived from ApplicationException  with two properties 
    public class BirdFlewAwayException : ApplicationException
    {
        public DateTime When { get; set; }
        public string Why { get; set; }
        //When
        //Why

        //Create constructors
        public BirdFlewAwayException()
        {
        }
        public BirdFlewAwayException(string message, DateTime time, string why)
            :base(message)
        {
            Why = why;
            When = time;

        }

    }

        

}
