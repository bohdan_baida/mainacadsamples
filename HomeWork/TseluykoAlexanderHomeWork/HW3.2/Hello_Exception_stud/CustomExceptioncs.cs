﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hello_Exception_stud
{
    public class CustomExceptions : ApplicationException
    {
        
        public  CustomExceptions(string Message) : base(Message)
        {
            this.HelpLink = "http://en.wikipedia.org/wiki/Tufted_titmouse";
        }
    }
}
