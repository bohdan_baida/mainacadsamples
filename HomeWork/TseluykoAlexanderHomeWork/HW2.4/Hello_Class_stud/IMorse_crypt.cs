﻿namespace Hello_Class_stud
{
    public interface IMorse_crypt
    {
        string crypt(string str);
        string decrypt(string[] arr);
        
    }
    //Define interface IMorse_crypt wicth two methods  
    //crypt - to crypt the string
    //decrypt - to decrypt array of strings

}
