﻿using System;
using System.Threading;

namespace Hello_Class_stud
{
    //Implement class Morse_matrix derived from String_matrix, which realize IMorse_crypt
    class Morse_matrix : String_matrix, IMorse_crypt
    {
        public const int Size_2 = Alphabet.Size;
        private int offset_key = 0;



        //Implement Morse_matrix constructor with the int parameter for offset
        //Use fd(Alphabet.Dictionary_arr) and sd() methods
        public Morse_matrix()
        {
            fd(Alphabet.Dictionary_arr);
            sd();
        }

        public Morse_matrix(int offset)
        {
            offset_key = offset;
            fd(Alphabet.Dictionary_arr);
            sd();
        }

        //Implement Morse_matrix constructor with the string [,] Dict_arr and int parameter for offset
        //Use fd(Dict_arr) and sd() methods
        public Morse_matrix(string [,] dict_arr, int offset)
        {
            offset_key = offset;
            fd(dict_arr);
            sd();
        }


        private void fd(string[,] Dict_arr)
        {
            for (int ii = 0; ii < Size1; ii++)
                for (int jj = 0; jj < Size_2; jj++)
                    this[ii, jj] = Dict_arr[ii, jj];
        }


        private  void sd()
        {
            string[,] temp = new string[Size1, Size2];
            for (int ii = 0; ii < Size1; ii++)
                for (int jj = 0; jj < Size_2; jj++)
                    temp[ii, jj] = this[ii, jj];

            int off = Size_2 - offset_key;
            for (int jj = 0; jj < off; jj++)
                this[1, jj] = temp[1, jj + offset_key];
            for (int jj = off; jj < Size_2; jj++)
                this[1, jj] = temp[1, jj - off];
        }

        //Implement Morse_matrix operator +
        public static Morse_matrix operator +(Morse_matrix m1 , int offset)
        {
            return new Morse_matrix(offset);
        }

        //Realize crypt() with string parameter
        //Use indexers
        public string crypt(string str)
        {
            var res = "";
            foreach (var sm in str)
            {

                for (int i = 0; i < Size2; i++)
                {
                    if(this[0,i] == sm.ToString())
                    {
                        res +=  this[1, i];
                    }
                }
            }
            return res ;
            
        }

        //Realize decrypt() with string array parameter
        //Use indexers
        public string decrypt(string[] arr)
        {
            var res = "";
            foreach(var item in arr)
            {
                for(int i = 0; i < Size2; i++)
                {
                    if (this[1, i] == item)
                    {
                        res += this[0, i];
                    }
                }
            }
            return res;
        }

        //Implement Res_beep() method with string parameter to beep the string
        public void Res_beep(string str)
        {
            foreach (var sm in str)
            {

                
                    if (sm == '.')
                        Console.Beep(1000, 250);
                    else if (sm == '-')
                        Console.Beep(1000, 750);
                    Thread.Sleep(50);
                

            }
        }

    }
}

