﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hello_fact_advstud
{
    class Program
    {
        public static int Fact(int num)
        {
            Console.WriteLine(num);
            return (num == 1) ? 1 : num * Fact(num - 1); 
                
        }
        static void Main(string[] args)
        {
            Console.Write("Enter the number = ");
            int num = (int)uint.Parse(Console.ReadLine());

            var factorial = Fact(num);
            Console.WriteLine($"Factorial = {factorial}");
            Console.Read();
            //Define parameters to calculate the factorial of
            //Call fact() method to calculate
            //var emp = new Empl()
            //{
            //    Name = "string", LastName = "Ts"
            //};
            //emp.Name = "str";
            //emp.FirstName = "Sasj";

            //var emp1 = new Empl("firstname", "lastname");
            //Console.WriteLine(emp1.GetFullName(0));
            //Console.Read();
        }

        //Create fact() method  with parameter to calculate factorial
        //Use ternary operator

    }

    

}
