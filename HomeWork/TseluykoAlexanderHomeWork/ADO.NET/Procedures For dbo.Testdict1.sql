USE [Testdict1]
GO
/****** Object:  StoredProcedure [dbo].[sp_AddQuestion]    Script Date: 10/24/2019 9:39:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[sp_AddQuestion] (@TEST_ID INT, @QUEST_NAME VARCHAR(255), @QUEST_TYPE VARCHAR(5))
AS
BEGIN
	INSERT INTO QUESTIONS (TEST_ID, QUEST_NAME, QUEST_TYPE) 
	VALUES (@TEST_ID, @QUEST_NAME, @QUEST_TYPE)
END



USE [Testdict1]
GO
/****** Object:  StoredProcedure [dbo].[sp_DelQuestion]    Script Date: 10/24/2019 9:39:28 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[sp_DelQuestion] (@QUEST_ID INT, @TEST_ID INT)
AS
BEGIN
	DELETE dbo.QUESTIONS 
	 WHERE TEST_ID  = @TEST_ID AND
		   QUEST_ID = @QUEST_ID
END


USE [Testdict1]
GO
/****** Object:  StoredProcedure [dbo].[sp_GetAllTables]    Script Date: 10/24/2019 9:39:50 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[sp_GetAllTables] (@TEST_ID int)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT TEST_ID,TEST_NAME 
	FROM dbo.TEST
	WHERE TEST_ID = @TEST_ID

	SELECT QUEST_ID, TEST_ID, QUEST_NAME, QUEST_TYPE 
	FROM dbo.QUESTIONS
	WHERE TEST_ID = @TEST_ID

	--SELECT QUEST_ID,OPT_NUMB,OPT_DESC,OPT_RT
	--FROM dbo.QUESTOPT
END





USE [Testdict1]
GO
/****** Object:  StoredProcedure [dbo].[sp_UpdQuestion]    Script Date: 10/24/2019 9:40:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[sp_UpdQuestion] (@QUEST_ID INT, @TEST_ID INT, @QUEST_NAME VARCHAR(255), @QUEST_TYPE VARCHAR(5))
AS
BEGIN
	UPDATE dbo.QUESTIONS 
	   SET QUEST_NAME = @QUEST_NAME, 
		   QUEST_TYPE = @QUEST_TYPE
	 WHERE TEST_ID  = @TEST_ID AND
		   QUEST_ID = @QUEST_ID
END
