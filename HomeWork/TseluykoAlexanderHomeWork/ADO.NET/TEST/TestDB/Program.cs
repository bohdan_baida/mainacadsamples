﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDB
{
    class Program
    {
        static void Main(string[] args)
        {
            var testId = 1;
            string connectionString = @"Data Source=LAPTOP-D8MCP9H7;Initial Catalog=Testdict1;Integrated Security=True";
            DBHelper dBHelper = new DBHelper(connectionString);
            dBHelper.Init(testId);

            MenuHelper menuHelper = new MenuHelper(dBHelper);
            menuHelper.LaunchMenu();

            dBHelper.Close();
        }
    }
}
