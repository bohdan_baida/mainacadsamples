﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDB
{
    public class DBHelper
    {
        private DataSet dataSet;
        private SqlConnection connection;
        private string connString;

        SqlDataAdapter adapter = new SqlDataAdapter();

        public int TestId { get; private set; }
        public DBHelper(string connectionString)
        {
            dataSet = new DataSet();
            connString = connectionString;
        }
        public void Init(int testId)
        {
            TestId = testId;
            connection = new SqlConnection(connString);
            try
            {

                connection.Open();
                Console.WriteLine("Connection opened");

                SqlCommand selectCommand = new SqlCommand("sp_GetAllTables", connection);
                selectCommand.CommandType = CommandType.StoredProcedure;
                selectCommand.Parameters.Add(new SqlParameter("@TEST_ID", testId));
                adapter.SelectCommand = selectCommand;

                SqlCommand insCommand = new SqlCommand("sp_AddQuestion", connection);
                insCommand.CommandType = CommandType.StoredProcedure;
                insCommand.Parameters.Add(new SqlParameter("@TEST_ID", SqlDbType.Int)).SourceColumn = "TEST_ID";
                insCommand.Parameters.Add(new SqlParameter("@QUEST_NAME", SqlDbType.VarChar, 255)).SourceColumn = "QUEST_NAME";
                insCommand.Parameters.Add(new SqlParameter("@QUEST_TYPE", SqlDbType.VarChar, 1)).SourceColumn = "QUEST_TYPE";
                adapter.InsertCommand = insCommand;

                SqlCommand updCommand = new SqlCommand("sp_UpdQuestion", connection);
                updCommand.CommandType = CommandType.StoredProcedure;
                updCommand.Parameters.Add(new SqlParameter("@QUEST_ID", SqlDbType.VarChar, 255)).SourceColumn = "QUEST_ID";
                updCommand.Parameters.Add(new SqlParameter("@TEST_ID", SqlDbType.Int)).SourceColumn = "TEST_ID";
                updCommand.Parameters.Add(new SqlParameter("@QUEST_NAME", SqlDbType.VarChar, 255)).SourceColumn = "QUEST_NAME";
                updCommand.Parameters.Add(new SqlParameter("@QUEST_TYPE", SqlDbType.VarChar, 1)).SourceColumn = "QUEST_TYPE";
                adapter.UpdateCommand = updCommand;

                SqlCommand delCommand = new SqlCommand("sp_DelQuestion", connection);
                delCommand.CommandType = CommandType.StoredProcedure;
                delCommand.Parameters.Add(new SqlParameter("@QUEST_ID", SqlDbType.VarChar, 255)).SourceColumn = "QUEST_ID";
                delCommand.Parameters.Add(new SqlParameter("@TEST_ID", SqlDbType.Int)).SourceColumn = "TEST_ID";
                adapter.DeleteCommand = delCommand;
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        public void InitDataSet()
        {
            dataSet.Clear();
            adapter.Fill(dataSet);
        }
        public void Close()
        {
            dataSet.AcceptChanges();
            dataSet = null;
            connection.Close();
        }
        public DataSet GetDataSet()
        {
            return dataSet;
        }
        public bool SaveData(out string errmsg)
        {
            bool res = true;
            errmsg = null;
            try
            {
                adapter.Update(dataSet.Tables[1]);
                InitDataSet();
            }
            catch (Exception ex)
            {
                res = false;
                errmsg = ex.Message;
                dataSet.Tables[1].RejectChanges();
            }
            return res;
        }
    }
}
