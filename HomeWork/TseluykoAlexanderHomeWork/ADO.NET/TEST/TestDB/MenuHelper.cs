﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDB
{
    public class MenuHelper
    {
        private DBHelper dbhelp;

        public MenuHelper(DBHelper dbhelp)
        {
            dbhelp.InitDataSet();
            this.dbhelp = dbhelp;
        }

        public void LaunchMenu()
        {
            int react = 0;
           
            do
            {
                Console.Clear();
                Console.WriteLine("1. Show Questions");
                Console.WriteLine("2. Add new Question");
                Console.WriteLine("3. Modify Question");
                Console.WriteLine("4. Delete Question");
                Console.WriteLine("0. Exit");

                Console.Write("Enter your choice: ");
                try
                {
                    react = (int)uint.Parse(Console.ReadLine());
                }
                catch
                {
                    react = -1;
                }
                switch (react)
                {
                    case 1:
                        ShowQuestions();
                        break;
                    case 2:
                        AddQuestions();
                        break;
                    case 3:
                        UpdQuestions();
                        break;
                    case 4:
                        DelQuestions();
                        break;
                    case 0:
                        break;
                }
            }
            while (react != 0);
        }

        public void ShowQuestions()
        {
            //dbhelp.InitDataSet();
            DataSet Ds = dbhelp.GetDataSet();
            var tests = Ds.Tables[0];
            var questions = Ds.Tables[1];
            foreach (DataRow item in questions.Rows)
            {
                Console.WriteLine(item["QUEST_ID"].ToString() + ". " + item["QUEST_NAME"].ToString());
            }
            Console.WriteLine(Environment.NewLine + "Press any key...");
            Console.ReadKey();
        }

        public void AddQuestions()
        {
            DataSet Ds = dbhelp.GetDataSet();

            Console.WriteLine("Enter new question: ");
            var quest = Console.ReadLine();
            Console.WriteLine("Question Type: ");
            var qtype = Console.ReadLine();
            DataTable dt = Ds.Tables[1];
            DataRow newRow = dt.NewRow();
            newRow["QUEST_NAME"] = quest;
            newRow["QUEST_TYPE"] = qtype;
            newRow["TEST_ID"] = dbhelp.TestId;
            dt.Rows.Add(newRow);

            SaveData();
        }

        public void UpdQuestions()
        {
            DataSet Ds = dbhelp.GetDataSet();
            DataTable dt = Ds.Tables[1];

            Console.WriteLine("Enter ID of question: ");
            var qid = Console.ReadLine();
            DataRow[] drcol = dt.Select($"QUEST_ID = {qid} AND TEST_ID = {dbhelp.TestId.ToString()}", 
                              null, DataViewRowState.CurrentRows);

            if (drcol.Length == 1)
            {
                DataRow dr = drcol[0];
                Console.WriteLine("Current question: " + dr["QUEST_NAME"].ToString());
                
                Console.WriteLine("Enter new question: ");
                var quest = Console.ReadLine();
                Console.WriteLine("Question Type: ");
                var qtype = Console.ReadLine();
                if (quest.Length > 0)
                {
                    dr["QUEST_NAME"] = quest;
                }
                if (qtype.Length > 0)
                {
                    dr["QUEST_TYPE"] = qtype.ToUpper();
                }
                dr["TEST_ID"] = dbhelp.TestId;
            }

            SaveData();
        }

        public void DelQuestions()
        {
            DataSet Ds = dbhelp.GetDataSet();
            DataTable dt = Ds.Tables[1];

            Console.WriteLine("Enter ID of question: ");
            var qid = Console.ReadLine();
            DataRow[] drcol = dt.Select($"QUEST_ID = {qid} AND TEST_ID = {dbhelp.TestId.ToString()}",
                              null, DataViewRowState.CurrentRows);

            if (drcol.Length == 1)
            {
                DataRow dr = drcol[0];
                dr.Delete();
            }

            SaveData();
        }

        private void SaveData()
        {
            string errmsg = null; ;
            Boolean res = dbhelp.SaveData(out errmsg);
            if (!res)
            {
                Console.WriteLine($"Error: {errmsg}");
                Console.WriteLine(Environment.NewLine + "Press any key...");
                Console.ReadKey();
            }
        }
    }
}
