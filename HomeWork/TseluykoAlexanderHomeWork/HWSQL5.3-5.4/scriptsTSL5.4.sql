SELECT a.ShopId,
	a.ShopName,
	COUNT(DISTINCT sps.SupplierId)
FROM Shops a
	INNER JOIN Supplier_Port_Shop sps ON sps.ShopId = a.ShopId
GROUP BY a.ShopId,a.ShopName
HAVING COUNT(DISTINCT sps.SupplierId) = (SELECT COUNT(*) 
FROM Suppliers)

SELECT s.SupplierId,
	   s.SupplierName,
	COUNT(DISTINCT sps.ShopId)
from Suppliers s
	INNER JOIN Supplier_Port_Shop sps ON sps.SupplierId = s.SupplierId
GROUP BY s.SupplierId, s.SupplierName


SELECT distinct PortName
FROM Ports INNER JOIN Supplier_Port_Shop ON  Ports.PortId = Supplier_Port_Shop.PortId
WHERE Supplier_Port_Shop.SupplierId = 2

CREATE FUNCTION [dbo].[Supplier_Port_Shop]
(
	@ShopId int
)
RETURNS decimal
AS
BEGIN
	declare @cnt decimal(18,2) = 0

	select @cnt = COUNT(DISTINCT SupplierId)
	from Supplier_Port_Shop
	where Supplier_Port_Shop.ShopId = @ShopId 

	RETURN @cnt
END
GO

DECLARE @cnt decimal
set @cnt = [dbo].[Supplier_Port_Shop2](2)
print @cnt

CREATE FUNCTION [dbo].[Supplier_Port_Shop2]
(
	@ShopId int
)
RETURNS decimal
AS
BEGIN
	declare @cnt decimal(18,2) = 0

	select @cnt = COUNT(DISTINCT SupplierId)
	from Supplier_Port_Shop
	where Supplier_Port_Shop.ShopId = @ShopId

	RETURN @cnt
END
GO



USE [Contraband]
GO
DECLARE @cnt decimal
set @cnt = [dbo].[Supplier_Port_Shop2](2)
print @cnt



SELECT [dbo].[Supplier_Port_Shop2] (
   <@ShopId, int,>)
GO



-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE dbo.Procedure1
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   SELECT [PortId],
   COUNT(* )
  FROM [Contraband].[dbo].[Supplier_Port_Shop]
  GROUP BY [PortId]
END
GO

dbo.Procedure1
















-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE dbo.Procedure1
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   SELECT [PortId],
   COUNT(* ) PathPort
  FROM [Contraband].[dbo].[Supplier_Port_Shop]
  GROUP BY [PortId]
END
GO

dbo.Procedure1










USE [Contraband]
GO
/****** Object:  Trigger [dbo].[trg_Supplier_Port_Shop]    Script Date: 15.10.2019 20:32:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER TRIGGER [dbo].[trg_Supplier_Port_Shop]
   ON  [dbo].[Supplier_Port_Shop]
   AFTER INSERT,UPDATE 
AS 
BEGIN
	SET NOCOUNT ON;

	DECLARE @cnt integer
	SELECT @cnt = COUNT(DISTINCT s.PortId)
	FROM dbo.Supplier_Port_Shop s INNER JOIN inserted i on s.SupplierId = i.SupplierId and s.ShopId = i.ShopId
	GROUP BY s.ShopId,s.SupplierId
	HAVING COUNT(DISTINCT s.PortId) > 3

IF @cnt is not null
	RAISERROR('U entered more than 3 ports', 11, 1)

END