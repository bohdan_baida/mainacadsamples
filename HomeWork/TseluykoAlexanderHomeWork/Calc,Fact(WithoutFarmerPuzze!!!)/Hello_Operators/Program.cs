﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloOperators_stud
{
    class Program
    {
        static void Main(string[] args)
        {
            long a;
            Console.WriteLine(@"Please,  type the number:
            1. Farmer, wolf, goat and cabbage puzzle
            2. Console calculator
            3. Factirial calculation
            ");

            a = long.Parse(Console.ReadLine());
            switch (a)
            {
                case 1:
                    Farmer_puzzle();
                    Console.WriteLine("");
                    break;
                case 2:
                    Calculator();
                    Console.WriteLine("");
                    break;
                case 3:
                    Factorial_calculation();
                    Console.WriteLine("");
                    break;
                default:
                    Console.WriteLine("Exit");
                    break;
            }
            Console.WriteLine("Press any key");
            Console.ReadLine();
        }
        #region farmer
        static void Farmer_puzzle()
        {
            int a;
            int b;
            int c;
            int d;
            int e;
            int f;
            int g;

            //Key sequence: 3817283 or 3827183
            // Declare 7 int variables for the input numbers and other variables
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(@"From one bank to another should carry a wolf, goat and cabbage. 
At the same time can neither carry nor leave together on the banks of a wolf and a goat, 
a goat and cabbage. You can only carry a wolf with cabbage or as each passenger separately. 
You can do whatever how many flights. How to transport the wolf, goat and cabbage that all went well?");
            Console.WriteLine("There: farmer and wolf - 1");
            Console.WriteLine("There: farmer and cabbage - 2");
            Console.WriteLine("There: farmer and goat - 3");
            Console.WriteLine("There: farmer  - 4");
            Console.WriteLine("Back: farmer and wolf - 5");
            Console.WriteLine("Back: farmer and cabbage - 6");
            Console.WriteLine("Back: farmer and goat - 7");
            Console.WriteLine("Back: farmer  - 8");
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Please,  type numbers by step ");
            // Implement input and checking of the 7 numbers in the nested if-else blocks with the  Console.ForegroundColor changing

        }
        #endregion

        #region calculator
        static void Calculator()
        {
            // Set Console.ForegroundColor  value
            int numb1 = 0;
            int numb2 = 0;
            Console.WriteLine("Type the first number");
            numb1 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Type the second number");
            numb2 = Convert.ToInt32(Console.ReadLine());
            Console.ReadLine();
            Console.WriteLine(@"Select the arithmetic operation:");
            Console.WriteLine("1 - Multiplication");
            Console.WriteLine("2 - Division");
            Console.WriteLine("3 - Addition");
            Console.WriteLine("4 - Subtraction");
            Console.WriteLine("5 - Exponentiation");
            // Implement option input (1,2,3,4,5)
            //     and input of  two or one numbers
            //  Perform calculations and output the result 
            switch (Console.ReadLine())
            {
                case "1":
                    Console.WriteLine($"The result of your Multiplication is : {numb1} * {numb2} = " + (numb1 * numb2));
                        break;
                case "2":
                    Console.WriteLine($"The result of your Division is : {numb1} / {numb2} = " + (numb1 / numb2));
                    break;
                case "3":
                    Console.WriteLine($"The result of your Addition is : {numb1} + {numb2} = " + (numb1 + numb2));
                    break;
                case "4":
                    Console.WriteLine($"The result of your Substraction is : {numb1} - {numb2} = " + (numb1 - numb2));
                    break;
                case "5":
                    Console.WriteLine($"The result of your Exponentiation is : {numb1} % {numb2} = " + (numb1 % numb2));
                    break;
            }
        }
        #endregion
        
        #region Factorial
        static void Factorial_calculation()
        {
            int i;
            int number;
            int factorial;
            Console.WriteLine("Enter the Number");
            number = Convert.ToInt32(Console.ReadLine());
            factorial = number;
            for (i = number - 1; i >= 1; i--)
            {
                factorial = factorial * i;
            }
            Console.WriteLine("Factorial of your number is: " + factorial);
            Console.ReadLine();
            // Implement input of the number
            // Implement input the for circle to calculate factorial of the number
            // Output the result
        }
        #endregion
    }
}
