using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.DirectoryServices.AccountManagement;

namespace ConsoleApp3
{
    class Program
    {

        static void Main(string[] args)
        {
            var AD = new PrincipalContext(ContextType.Domain);
            var u = new UserPrincipal(AD);
            Console.WriteLine(u.Surname = Console.ReadLine());
            Console.WriteLine(u.GivenName = Console.ReadLine()); 
            u.Name = u.Surname + " " + u.GivenName;
            u.SamAccountName = u.Surname.ToLower() + "." + u.GivenName.ToLower();
            u.EmailAddress = u.SamAccountName + "@dev-pro.net";
            u.SetPassword("Password007");
            u.Enabled = true;
            u.DisplayName = u.Name;
            u.ExpirePasswordNow();
            u.UserPrincipalName = u.SamAccountName + "@do.local";
            u.Save();


        }
    }
}
