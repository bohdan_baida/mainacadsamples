﻿using System;

namespace Hello_Class_stud
{
    //Implement class Morse_matrix derived from String_matrix, which realize IMorse_crypt
    public class Morse_matrix : String_matrix, IMorse_crypt
    {
        public const int Size_2 = Alphabet.Size;
        private int offset_key = 0;

        //Implement Morse_matrix constructor with the int parameter for offset
        //Use fd(Alphabet.Dictionary_arr) and sd() methods
        public Morse_matrix()
        {

        }
        public Morse_matrix(int offset_key)
        {
            this.offset_key = offset_key;
        }

        //Implement Morse_matrix constructor with the string [,] Dict_arr and int parameter for offset
        //Use fd(Dict_arr) and sd() methods
        public Morse_matrix(string[,] Dict_arr, int offset_key)
        {
            this.offset_key = offset_key;
            fd(Dict_arr);
            sd();
        }

        private void fd(string[,] Dict_arr)
        {
            for (int ii = 0; ii < Size1; ii++)
                for (int jj = 0; jj < Size_2; jj++)
                    this[ii, jj] = Dict_arr[ii, jj];
        }


        private void sd()
        {
            int off = Size_2 - offset_key;
            for (int jj = 0; jj < off; jj++)
                this[1, jj] = this[1, jj + offset_key];
            for (int jj = off; jj < Size_2; jj++)
                this[1, jj] = this[1, jj - off];
        }

        //Implement Morse_matrix operator +
        public static Morse_matrix operator +(Morse_matrix morse_Matrix, int offset_key)
        {
            morse_Matrix.offset_key = offset_key;
            morse_Matrix.fd(Alphabet.Dictionary_arr);
            morse_Matrix.sd();
            return morse_Matrix;
        }

        //Realize crypt() with string parameter
        //Use indexers
        public string crypt(string cryptedWord)
        {
            char[] word = cryptedWord.ToCharArray();
            string cryptedCode = " ";
            for (int i = 0; i < word.Length; i++)
            {
                for (int j = 0; i < Alphabet.Dictionary_arr.Length; j++)
                {
                    if (Alphabet.Dictionary_arr[0, j] == word[i].ToString())
                    {
                        cryptedCode = cryptedCode + Alphabet.Dictionary_arr[0, j];
                    }
                }
            }

            return cryptedCode;
        }
        //Realize decrypt() with string array parameter
        //Use indexers
        public string decrypt(string[] decryptedWord)
        {
            string decryptedCode = " ";
            for (int i = 0; i < decryptedWord.Length; i++)
            {
                for (int j = 0; j < Alphabet.Dictionary_arr.Length; j++)
                {
                    if (Alphabet.Dictionary_arr[0,j]== decryptedWord[i].ToString())
                    {
                        decryptedCode = decryptedCode + Alphabet.Dictionary_arr[0, j];
                    }
                }
            }
            return decryptedCode;
        }

        //Implement Res_beep() method with string parameter to beep the string
        public void Res_beep(string beepWord)
        {
            char[] tempCharArr = beepWord.ToCharArray();
            string beep = " ";
            for (int i = 0; i < tempCharArr.Length; i++)
            {
                beep = tempCharArr[i].ToString();
                if(beep=="-")
                {
                    Console.Beep(1000,1000);
                }
                if (beep == ".")
                {
                    Console.Beep(1000,100);
                }
            }
        }
    }
}

