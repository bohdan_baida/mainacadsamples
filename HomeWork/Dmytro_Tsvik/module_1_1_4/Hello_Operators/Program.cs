﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloOperators_stud
{
    class Program
    {
        static void Main(string[] args)
        {
            long a;
            Console.WriteLine(@"Please,  type the number:
            1. Farmer, wolf, goat and cabbage puzzle
            2. Console calculator
            3. Factorial calculation

            ");

            a = long.Parse(Console.ReadLine());
            switch (a)
            {
                case 1:
                    Farmer_puzzle();
                    Console.WriteLine("");
                    break;
                case 2:
                    Calculator();
                    Console.WriteLine("");
                    break;
                case 3:
                    Factorial_calculation();
                    Console.WriteLine("");
                    break;
                default:
                    Console.WriteLine("Exit");
                    break;
            }

            Console.ReadLine();
        }
        #region farmer
        static void Farmer_puzzle()
        {
            //Key sequence: 3817283 or 3827183
            // Declare 7 int variables for the input numbers and other variables

            bool rightSideFull = false;
            bool goatRightSide = false;
            bool cabbageRightSide = false;
            bool wolfRightSide = false;
            bool farmerRightSide = false;
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(@"From one bank to another should carry a wolf, goat and cabbage. 
At the same time can neither carry nor leave together on the banks of a wolf and a goat, 
a goat and cabbage. You can only carry a wolf with cabbage or as each passenger separately. 
You can do whatever how many flights. How to transport the wolf, goat and cabbage that all went well?");
            Console.WriteLine("There: farmer and wolf - 1");
            Console.WriteLine("There: farmer and cabbage - 2");
            Console.WriteLine("There: farmer and goat - 3");
            Console.WriteLine("There: farmer  - 4");
            Console.WriteLine("Back: farmer and wolf - 5");
            Console.WriteLine("Back: farmer and cabbage - 6");
            Console.WriteLine("Back: farmer and goat - 7");
            Console.WriteLine("Back: farmer  - 8");
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("Please,  type numbers by step ");

            while (rightSideFull != true)
            {
                switch (Console.ReadLine())
                {
                    case "1":
                        if (wolfRightSide == true)
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("Wolf already moved to Right Side");
                            break;

                        }
                        if (farmerRightSide == true)
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("Farmer already moved to Right Side");
                            break;
                        }
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine("Farmer moves a Wolf to Right Side");
                        if (goatRightSide == false && cabbageRightSide == false)
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("A Goat ate a Cabbage!");
                            break;
                        }
                        wolfRightSide = true;
                        farmerRightSide = true;
                        break;

                    case "2":
                        if (cabbageRightSide == true)
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("Cabbage already moved to Right Side");
                            break;

                        }
                        if (farmerRightSide == true)
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("Farmer already moved to Right Side");
                            break;
                        }
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine("Farmer moves a Cabbage to Right Side");
                        if (goatRightSide == false && wolfRightSide == false)
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("A Wolf ate a Goat!");
                            break;
                        }
                        cabbageRightSide = true;
                        farmerRightSide = true;
                        break;

                    case "3":
                        if (goatRightSide == true)
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("Goat already moved to Right Side");
                            break;

                        }
                        if (farmerRightSide == true)
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("Farmer already moved to Right Side");
                            break;
                        }
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine("Farmer moves a Goat to Right Side");
                        goatRightSide = true;
                        farmerRightSide = true;
                        break;
                    case "4":
                        if (farmerRightSide == true)
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("Farmer already moved to Right Side");
                            break;
                        }
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine("Farmer moves to Right Side");
                        if ((goatRightSide == true && cabbageRightSide == true) || (goatRightSide == false && cabbageRightSide == false))
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("A Goat ate a Cabbage!");
                            break;

                        }
                        if ((wolfRightSide == true && goatRightSide == true) || (wolfRightSide == false && goatRightSide == false))
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("A Wolf ate a Goat!");
                            break;
                        }
                        farmerRightSide = true;
                        break;
                    case "5":
                        if (farmerRightSide == false)
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("Farmer already moved to Left Side");
                            break;
                        }
                        if (wolfRightSide == false)
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("Wolf already moved to Left Side");
                            break;
                        }
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine("Farmer moves a Wolf to Left Side");
                        if (goatRightSide == true && cabbageRightSide == true)
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("A Goat ate a Cabbage!");
                            break;
                        }
                        wolfRightSide = false;
                        farmerRightSide = false;
                        break;

                    case "6":
                        if (cabbageRightSide == false)
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("Cabbage already moved to Left Side");
                            break;

                        }
                        if (farmerRightSide == false)
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("Farmer already moved to Left Side");
                            break;
                        }
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine("Farmer moves a Cabbage to Left Side");
                        if (goatRightSide == true && wolfRightSide == true)
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("A Wolf ate a Goat!");
                            break;
                        }
                        cabbageRightSide = false;
                        farmerRightSide = false;
                        break;
                    case "7":
                        if (goatRightSide == false)
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("Goat already moved to Left Side");
                            break;

                        }
                        if (farmerRightSide == false)
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("Farmer already moved to Left Side");
                            break;
                        }
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine("Farmer moves a Cabbage to Left Side");
                        goatRightSide = false;
                        farmerRightSide = false;
                        break;
                    case "8":
                        if (farmerRightSide == false)
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("Farmer already moved to Left Side");
                            break;
                        }
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine("Farmer moves to Left Side");
                        if ((goatRightSide == true && cabbageRightSide == true) || (goatRightSide == false && cabbageRightSide == false))
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("A Goat ate a Cabbage!");
                            break;

                        }
                        if ((wolfRightSide == true && goatRightSide == true) || (wolfRightSide == false && goatRightSide == false))
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine("A Wolf ate a Goat!");
                            break;
                        }
                        farmerRightSide = false;
                        break;
                    case "9":
                        break;

                }
                if (wolfRightSide == true && cabbageRightSide == true && goatRightSide == true)
                {
                    rightSideFull = true;
                }
            }
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Well done!");
            // Implement input and checking of the 7 numbers in the nested if-else blocks with the  Console.ForegroundColor changing

        }
        #endregion

        #region calculator
        static void Calculator()
        {
            string end = "a";
            // Set Console.ForegroundColor  value
            // Implement option input (1,2,3,4,5)
            //     and input of  two or one numbers
            //  Perform calculations and output the result 
            while (end != "exit")
            {
                Console.WriteLine(@"
Select the arithmetic operation:
1. Multiplication 
2. Divide 
3. Addition 
4. Subtraction
5. Exponentiation
6. All-in-one
Type exit for Exit
");
                switch (Console.ReadLine())
                {
                    case "1":
                        Console.WriteLine("Insert int A to Multiplication");
                        var multA = Convert.ToInt64(Console.ReadLine());
                        Console.WriteLine("Insert int B to Multiplication");
                        var multB = Convert.ToInt64(Console.ReadLine());
                        Console.WriteLine($"The result of Multiplication A and B is {multA * multB}");
                        break;
                    case "2":
                        Console.WriteLine("Insert int A to Divide");
                        var divA = Convert.ToInt64(Console.ReadLine());
                        Console.WriteLine("Insert int B to Divide");
                        var divB = Convert.ToInt64(Console.ReadLine());
                        Console.WriteLine($"The result of Divide A and B is {divA / divB}");
                        break;
                    case "3":
                        Console.WriteLine("Insert int A to Addition");
                        var addA = Convert.ToInt64(Console.ReadLine());
                        Console.WriteLine("Insert int B to Addition");
                        var addB = Convert.ToInt64(Console.ReadLine());
                        Console.WriteLine($"The result of Addition A and B is {addA + addB}");
                        break;
                    case "4":
                        Console.WriteLine("Insert int A to Subtraction");
                        var subA = Convert.ToInt64(Console.ReadLine());
                        Console.WriteLine("Insert int B to Subtraction");
                        var subB = Convert.ToInt64(Console.ReadLine());
                        Console.WriteLine($"The result of Subtraction A and B is {subA - subB}");
                        break;
                    case "5":
                        Console.WriteLine("Insert int A to Exponentiation");
                        var expA = Convert.ToInt64(Console.ReadLine());
                        Console.WriteLine("Insert int B to Exponentiation");
                        var expB = Convert.ToInt64(Console.ReadLine());
                        Console.WriteLine($"The result of Exponentiation A and B is {expA ^ expB}");
                        break;
                    case "6":
                        Console.WriteLine("Insert numbers and arithmetic operation in to line");
                        string math = Console.ReadLine();
                        string multi = new DataTable().Compute(math, null).ToString();
                        Console.WriteLine($"The result is {multi}");
                        break;
                    case "exit":
                        end = "exit";
                        break;

                }
            }
        }


        #endregion

        #region Factorial
        static void Factorial_calculation()
        {
            Console.WriteLine("Let's calculate a factorial! Please, insern a number");

            int i, fact;
            int num = Convert.ToInt32(Console.ReadLine());
            fact = num;
            for (i = num - 1; i >= 1; i--)
            {
                fact = fact * i;
            }
            Console.WriteLine("\nFactorial of number is: " + fact);
        }


        // Implement input of the number
        // Implement input the for circle to calculate factorial of the number
        // Output the result
    }
    #endregion
}

