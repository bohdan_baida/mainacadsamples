﻿using System;
namespace CSharp_Net_module1_1_2_lab
{
    class Program
    {
        static void Main(string[] args)
        {
            // Use "Debugging" and "Watch" to study variables and constants

            //1) declare variables of all simple types:
            //bool, char, byte, sbyte, short, ushort, int, uint, long, ulong, decimal, float, double
            // their names should be: 
            //boo, ch, b, sb, sh, ush, i, ui, l, ul, de, fl, d0
            // initialize them with 1, 100, 250.7, 150, 10000, -25, -223, 300, 100000.6, 8, -33.1
            // Check results (types and values). Is possible to do initialization?
            // Fix compilation errors (change values for impossible initialization)
            object[] arr = new object[14];
            bool boo = true;
            char ch = char.MaxValue;
            byte b = byte.MaxValue;
            sbyte sb = sbyte.MaxValue;
            short sh = short.MaxValue;
            ushort ush = ushort.MaxValue;
            int i = int.MaxValue;
            uint ui = uint.MaxValue;
            long l = long.MaxValue;
            ulong ul = ulong.MaxValue;
            decimal de = decimal.MaxValue;
            float fl = float.MaxValue;
            double d0 = double.MaxValue;
            Console.WriteLine(d0);
            // for (int j = 0; j < arr.Length; j++)
            //  {
            arr[0] = boo;
            arr[1] = ch;
            arr[2] = b;
            arr[3] = sb;
            arr[4] = sh;
            arr[5] = ush;
            arr[6] = i;
            arr[7] = ui;
            arr[8] = l;
            arr[9] = ul;
            arr[10] = de;
            arr[11] = fl;
            arr[12] = d0;
            foreach (var item in arr) Console.WriteLine(item);
            // }





            //2) declare constants of int and double. Try to change their values.
            //const int a = 1;
            //const double b1 = 1.5;
            //double c =a + b;
            //System.Console.WriteLine(c);


            //3) declare 2 variables with var. Initialize them 20 and 20.5. Check types. 
            // Try to reinitialize by 20.5 and 20 (change values). What results are there?
            var var1 = 20;
            var var2 = 20.5;
            Console.WriteLine(var1 + " " + var2);
            var1 = 1;
            var2 = 18446744073709551615;
            Console.WriteLine(var1 + " " + var2);


            // 4) declare variables of System.Int32 and System.Double.
            // Initialize them by values of i and d0. Is it possible?
            var var3 = i;
            var var4 = d0;


            if (true)
            {
                // 5) declare variables of int, char, double 
                // with names i, ch, do
                // is it possible? 
                int i1 = 1;
                char chq = 'q';
                double d01 = 0.5;

                // 6) change values of variables from 1)
                boo = false;
                l = long.MinValue;
                sb = sbyte.MinValue;

            }

            // 7)check values of variables from 1). Are they changed? Think, why
            Console.WriteLine("{0} and {1} and {2}", boo, ch, sb);

            // 8) use implicit/ explicit conversion to convert variables from 1). 
            // Is it possible? 

            // Fix compilation errors (in case of impossible conversion commemt that line).
            // int -> char

            // bool -> short

            // double -> long

            // float -> char 

            // int to char

            // decimal -> double

            // byte -> uint

            // ulong -> sbyte
            int int1 = (int)ch;
            double dobl1 = (double)l;
            float flo = (float)ch;
            decimal dec = (decimal)d0;
            byte bte = (byte)ui;
            ulong ulo = (ulong)sb;
            sbyte sby = (sbyte)ul;
            uint uin = (uint)b;
            double dobl = (double)de;
            char cha = (char)fl;
            long long1 = (long)d0;
            char chart = (char)i;
            // 9) and reverse conversion with fixing compilation errors.


            // 10) declare int nullable value. Initialize it with 'null'. 
            // Try to initialize variable i with 'null'. Is it possible?
            int? a = null;
            int? ii = null;
            Nullable<int> c1 = null;
            Console.WriteLine(a);
            Console.WriteLine(a != b);
            const int constA = 10;
        }
    }
}