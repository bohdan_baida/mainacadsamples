﻿using System;
using System.Linq;
namespace CSharp_Net_module1_2_1_lab
{

    // 1) declare interface ILibraryUser
    // declare method's signature for methods of class LibraryUser
    public interface ILibraryUser
    {

    }


    // 2) declare class LibraryUser, it implements ILibraryUser
    public class LibraryUser : ILibraryUser
    {
        // 3) declare properties: FirstName (read only), LastName (read only), 
        // Id (read only), Phone (get and set), BookLimit (read only)
        public string FirstName { get; }
        public string LastName { get; }
        public int Id { get; }
        public string Phone { get; set; }
        public int BookLimit { get; }


        // 4) declare field (bookList) as a string array
        public string[] bookList;

        // 5) declare indexer BookList for array bookList
        public string this[int index]
        {
            get
            {
                return bookList[index];
            }
            set
            {
                bookList[index] = value;
            }

        }
        // 6) declare constructors: default and parameter

        public LibraryUser()
        {
            bookList = new string[0];
        }

        public LibraryUser(string firstName, string lastName, int id, string phone, int bookLimit)
        {
            FirstName = firstName;
            LastName = lastName;
            Id = id;
            Phone = phone;
            BookLimit = bookLimit;
            bookList = new string[0];
        }

        // 7) declare methods: 


        //AddBook() – add new book to array bookList
        public void AddBook(string book)
        {
            if (bookList.Length == BookLimit)
            {
                return;
            }
            string[] tmp = new string[bookList.Length + 1];

            for (int i = 0; i < bookList.Length; i++)
            {
                tmp[i] = bookList[i];
            }

            tmp[tmp.Length - 1] = book;

            bookList = tmp;

        }
        //RemoveBook() – remove book from array bookList
        public void RemoveBook(string book)
        {
            if (bookList.Contains(book))
            {
                int index = Array.IndexOf(bookList, book);
                string[] tmp = new string[bookList.Length - 1];
                for (int i = 0; i < bookList.Length; i++)
                {
 
                    if (index == i)
                    {
                        continue;
                    }
                    if (index > i)
                    {
                        tmp[i - 1] = bookList[i];
                    }

                }
                bookList = tmp;

            }

            else
            {
                Console.WriteLine("Can't fint this book in List of Books of this user");
            }
        }
        //BookInfo() – returns book info by index
        public string BookInfo(int index)
        {
            return bookList[index];
        }
        //BooksCout() – returns current count of books
        public int BooksCount()
        {
            return bookList.Count();
        }
    }
}
