﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharp_Net_module1_1_4_lab
{
    public class Program
    {


        // 1) declare enum ComputerType
        enum ComputerType
        {
            Laptop = 1,
            Desktop,
            Monoblock,
            Server,
        }

        // 2) declare struct Computer
        struct Computer
        {
            public ComputerType PC { get; set; }
            public int CPU { get; set; }
            public int RAM { get; set; }
            public int HDD { get; set; }
            public string SSD { get; set; }
            public string Display { get; set; }

            public Computer(ComputerType type, int cpu, int ram, int hdd, string ssd, string monitor)
            {
                PC = type;
                CPU = cpu;
                RAM = ram;
                HDD = hdd;
                SSD = ssd;
                Display = monitor;
            }
            public void ShowAssembly()
            {
                Console.WriteLine($"Specs of PC {PC}: {CPU}, {RAM}, {HDD}, {SSD}, {Display}");
            }
        }

        static void Main(string[] args)
        {
            var desktop = new Computer(ComputerType.Desktop, 6, 16, 500, "256", "24");
            var laptop = new Computer(ComputerType.Laptop, 2, 8, 320, "128", "17");
            var monoblock = new Computer(ComputerType.Monoblock, 4, 16, 1000, "256", "27");
            var server = new Computer(ComputerType.Server, 16, 32, 2000, "512", "19");

            // 3) declare jagged array of computers size 4 (4 departments)
            Computer[][] departments = new Computer[4][];

            // 4) set the size of every array in jagged array (number of computers)
            // 5) initialize array
            departments[0] = new Computer[] { desktop, desktop, desktop, desktop, desktop, server };
            departments[1] = new Computer[] { server, server, server, server, server, monoblock };
            departments[2] = new Computer[] { laptop, laptop, laptop, laptop, server };
            departments[3] = new Computer[] { monoblock, monoblock, monoblock, monoblock, server };

            //string[][] computers = new string[5][];
            //computers[0] = new string[] { "intel", "ryzen" };
            //computers[1] = new string[] { "4", "8", "16" };
            //computers[2] = new string[] { "250", "320", "500", "1000" };
            //computers[3] = new string[] { "128", "256", "512" };
            //computers[4] = new string[] { "19", "21", "24", "27" };
            //departments[i][j] = new Computer();
            // Note: use loops and if-else statements
            // 6) count total number of every type of computers

            int[] totalNumberComps = new int[4] { 0, 0, 0, 0 };
            for (int i = 0; i < departments.Length; i++)
            {
                for (int j = 0; j < departments[i].Length; j++)
                {
                    ComputerType testPCType = departments[i][j].PC;
                    if (testPCType == ComputerType.Desktop)
                    {
                        totalNumberComps[0]++;
                    }
                    if (testPCType == ComputerType.Server)
                    {
                        totalNumberComps[1]++;
                    }
                    if (testPCType == ComputerType.Laptop)
                    {
                        totalNumberComps[2]++;
                    }
                    if (testPCType == ComputerType.Monoblock)
                    {
                        totalNumberComps[3]++;
                    }

                }
            }
            Console.WriteLine($"All count of Desktops {totalNumberComps[0]}");
            Console.WriteLine($"All count of Servers {totalNumberComps[1]}");
            Console.WriteLine($"All count of Laptops {totalNumberComps[2]}");
            Console.WriteLine($"All count of Monoblocks {totalNumberComps[3]}");


            // 7) count total number of all computers
            Console.WriteLine($"Number of all PCs {totalNumberComps.Sum()}");
            // Note: use loops and if-else statements
            // Note: use the same loop for 6) and 7)
            for (int i = 0; i < departments.Length; i++)
            {
                for (int j = 0; j < departments[i].Length; j++)
                {
                    departments[i][j].ShowAssembly();
                }
            }


            // 8) find computer with the largest storage (HDD) - 
            Computer maxHDD = new Computer();
            for (int i = 0; i < departments.Length; i++)
            {
                for (int j = 0; j < departments[i].Length; j++)
                {
                    if (departments[i][j].HDD > maxHDD.HDD)
                        maxHDD = departments[i][j];
                }
            }

            Console.WriteLine($"Show biggest HDD: {maxHDD.HDD} Gb");
            maxHDD.ShowAssembly();

            // compare HHD of every computer between each other; 

            // find position of this computer in array (indexes)
            // Note: use loops and if-else statements
            for (int i = 0; i < departments.Length; i++)
            {
                for (int j = 0; j < departments[i].Length; j++)
                {
                    if (departments[i][j].HDD == maxHDD.HDD)
                    {
                        Console.WriteLine($"Here is an indexes i={i}  j={j}");
                    }
                }
            }



            // 9) find computer with the lowest productivity (CPU and memory) - 
            // compare CPU and memory of every computer between each other; 
            // find position of this computer in array (indexes)
            // Note: use loops and if-else statements
            // Note: use logical oerators in statement conditions
            Computer lowMemCpu = new Computer();
            int notzero = 0;
            for (int i = 0; i < departments.Length; i++)
            {
                for (int j = 0; j < departments[i].Length; j++)
                {
                    if (notzero == 0)
                    {
                        lowMemCpu = departments[i][j];
                        notzero = 1;
                    }
                    else
                    {
                        if (departments[i][j].CPU < lowMemCpu.CPU && departments[i][j].RAM < lowMemCpu.RAM)
                        {
                            lowMemCpu = departments[i][j];
                        }
                    }

                    //if (lowMemCpu.RAM == 0 || lowMemCpu.CPU == 0)
                    //{

                    //}
                }
            }

            Console.WriteLine($"Show lowest CPU and RAM: {lowMemCpu.PC}, it has {lowMemCpu.CPU} cores and {lowMemCpu.RAM} ram.");
            
            // 10) make desktop upgrade: change memory up to 8
            // change value of memory to 8 for every desktop. Don't do it for other computers
            // Note: use loops and if-else statements
            for (int i = 0; i < departments.Length; i++)
            {
                for (int j = 0; j < departments[i].Length; j++)
                {
                   if(departments[i][j].PC == ComputerType.Desktop)
                    {
                        departments[i][j].RAM = 8;
                        
                    }
                }
            }
            for (int i = 0; i < departments.Length; i++)
            {
                for (int j = 0; j < departments[i].Length; j++)
                {
                    departments[i][j].ShowAssembly(); } }

        }

    }
}


