﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharp_Net_module1_2_3_lab
{
    // 1) declare enumeration CurrencyTypes, values UAH, USD, EU
    public enum CurrencyTypes
    {
        UAH = 1,
        USD,
        EU
    }


    class Money
    {

        // 2) declare 2 properties Amount, CurrencyType

        public CurrencyTypes CurrencyType { get; set; }
        public int Amount { get; set; }

        // 3) declare parameter constructor for properties initialization
        public Money()
        {

        }
        public Money(CurrencyTypes curType)
        {
            CurrencyType = curType;
        }
        public Money(CurrencyTypes curType, int amount)
        {
            CurrencyType = curType;
            Amount = amount;
        }

        // 4) declare overloading of operator + to add 2 objects of Money
        public static Money operator +(Money money1, Money money2)
        {
            return new Money(money1.CurrencyType, money1.Amount + money2.Amount);
        }

        // 5) declare overloading of operator -- to decrease object of Money by 1
        public static Money operator --(Money money1)
        {
            return new Money(money1.CurrencyType, money1.Amount--);
        }

        // 6) declare overloading of operator * to increase object of Money 3 times
        public static Money operator *(Money money1, int x)
        {
            return new Money(money1.CurrencyType, money1.Amount * 3);
        }
        // 7) declare overloading of operator > and < to compare 2 objects of Money
        public static bool operator <(Money money1, Money money2)
        {
            return money1.Amount < money2.Amount;
        }
        public static bool operator >(Money money1, Money money2)
        {
            return money1.Amount > money2.Amount;
        }
        // 8) declare overloading of operator true and false to check CurrencyType of object
        public static bool operator ==(Money money1, Money money2)
        {
            return money1.CurrencyType.Equals(money2.CurrencyType);
        }
        public static bool operator !=(Money money1, Money money2)
        {
            return !money1.CurrencyType.Equals(money2.CurrencyType);
        }
        // 9) declare overloading of implicit/ explicit conversion  to convert Money to double, string and vice versa
        public static implicit operator double(Money money1)
        {
            return money1.Amount;
        }
        public static explicit operator int(Money money1)
        {
            return money1.Amount;
        }
       
     
        public static implicit operator string(Money money1)
        {
            return money1.CurrencyType + " " + money1.Amount;
        }
    }
}
