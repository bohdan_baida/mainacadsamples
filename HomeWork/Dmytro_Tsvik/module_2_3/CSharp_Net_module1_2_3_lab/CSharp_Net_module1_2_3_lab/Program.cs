﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSharp_Net_module1_2_3_lab
{
    class Program
    {
        static void Main(string[] args)
        {
            // 10) declare 2 objects
            Money money3 = new Money(CurrencyTypes.UAH);
            Money money4 = new Money(CurrencyTypes.EU, 28);
            Money money5 = new Money(CurrencyTypes.USD, 25);
            // 11) do operations
            // add 2 objects of Money
            var money4and5Sum = money4 + money5;

            // add 1st object of Money and double
            var money4andDouble = money4 + 1.15;
            Console.WriteLine(money4andDouble);
            // decrease 2nd object of Money by 1 
            var money5minus1 = money5--;
            Console.WriteLine(money5minus1.Amount);

            // increase 1st object of Money
            var money4add = money4.Amount++;
            Console.WriteLine(money4add);
            // compare 2 objects of Money
            if (money4 > money5)
            {
                Console.WriteLine($"{money4.Amount} > {money5.Amount}");
            }
            if (money4 < money5)
            {
                Console.WriteLine($"{money4.Amount} < {money5.Amount}");
            }
            if (money4 == money5)
            {
                Console.WriteLine($"{money4.Amount} equals {money5.Amount}");
            }

            // compare 2nd object of Money and string
            var comparemoney5 = money5.CurrencyType.ToString();
            if (comparemoney5 == "UAH")
            {
                Console.WriteLine($"{comparemoney5} is UAH");
            }
            if (comparemoney5 == "USD")
            {
                Console.WriteLine($"{comparemoney5} is USD");
            }
            if (comparemoney5 == "EU")
            {
                Console.WriteLine($"{comparemoney5} is EU");
            }
            // check CurrencyType of every object
            
            // convert 1st object of Money to string
       
            Console.ReadKey();
        }
    }
}
