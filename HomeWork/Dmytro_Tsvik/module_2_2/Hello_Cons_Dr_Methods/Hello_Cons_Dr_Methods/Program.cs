﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hello_Cons_Dr_Methods
{
    class Program
    {

        static void Main(string[] args)
        {
            try
            {
                Box box1 = new Box(10,10,1,"*","test");//Implement start position, width and height, symbol, message input
                box1.Draw();
                //Create Box class instance

                //Use  Box.Draw() method

                Console.WriteLine("Press any key...");
            Console.ReadLine();
            }
            catch (Exception)
            {
                Console.WriteLine("Error!");
            }
            
        }
    }
}
