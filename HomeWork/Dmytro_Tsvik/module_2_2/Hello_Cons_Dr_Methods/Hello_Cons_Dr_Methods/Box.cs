﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hello_Cons_Dr_Methods
{
    public class Box
    {
        //1.  Implement public  auto-implement properties for start position (point position)
        //auto-implement properties for width and height of the box
        //and auto-implement properties for a symbol of a given set of valid characters (*, + ,.) to be used for the border 
        //and a message inside the box
        public int Width { get; set; }
        public int Height { get; set; }
        public int StartPosition { get; set; }
        public string Border { get; set; }
        public string Message { get; set; }
        
        
        public Box(int width, int height, int start,string border,string message)
        {
            Width = width;
            Height = height;
            StartPosition = start;
            Border = border;
            Message = message;
        }


        //2.  Implement public Draw() method
        //to define start position, width and height, symbol, message  according to properties
        //Use Math.Min() and Math.Max() methods
        //Use draw() to draw the box with message
        public void Draw()
        {
            Console.WriteLine($"Width: {Width}, Heigth: {Height}, StartPosition: {StartPosition}, Border: {Border}, Message: {Message} ");
            
            string[] borderUpDown = new string[Width];
 
            for (int i = 0; i < Width; i++)
            {
                borderUpDown[i] = Border;
                Console.WriteLine(borderUpDown[i]);
            }
            string[] borderLeftRigth = new string[Height];
            for (int j = 0; j < Height; j++)
            {
                borderLeftRigth[j]= Border;
                Console.Write(borderLeftRigth[j]); 
            }


           
            
           
            

        }
        //3.  Implement private method draw() with parameters 
        //for start position, width and height, symbol, message
        //Change the message in the method to return the Box square
        //Use Console.SetCursorPosition() method
        //Trim the message if necessary


    }
}
