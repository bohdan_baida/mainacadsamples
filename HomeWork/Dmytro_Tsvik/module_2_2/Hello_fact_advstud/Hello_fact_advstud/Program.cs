﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hello_fact_advstud
{
    class Program
    {
        static void Main(string[] args)
        {
            //Define parameters to calculate the factorial of
            //Call fact() method to calculate
            Console.WriteLine("Insert the number to calculate a factorial");
            int a = int.Parse(Console.ReadLine());

            Console.WriteLine($"The factorial of {a} equals {Fact(a)}");

        }

        static int Fact(int x)
        {
            int z = x == 0 ? x = 1 : x * Fact(x - 1);
            return z;
        }
        //Create fact() method  with parameter to calculate factorial
        //Use ternary operator

    }



}
