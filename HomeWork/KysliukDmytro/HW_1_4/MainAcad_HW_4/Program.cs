﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainAcad_HW_4
{
    class Program
    {
        static void Main(string[] args)
        {

            Computer desktop = new Computer(4, 2.5, 6, 500, ComputerType.Desktop);
            Computer laptop = new Computer(2, 1.7, 4, 250, ComputerType.Laptop);
            Computer server = new Computer(8, 3, 16,2000, ComputerType.Server);
            Computer[][] departs = new Computer[4][];
            departs[0] = new Computer[] { desktop, desktop, laptop, laptop, server };
            departs[1] = new Computer[] { laptop, laptop, laptop };
            departs[2] = new Computer[] { desktop, desktop, desktop, laptop, laptop };
            departs[3] = new Computer[] { desktop, laptop, server, server };


            var totalDesktops = 0;
            var totalLaptops = 0;
            var totalServers = 0;
            var totalComputers = 0;

            for (int i = 0; i < departs.Length; i++)
            {
                totalComputers += departs[i].Count();
                for (int j = 0; j < departs[i].Length; j++)
                {
                    if ((departs[i][j]).compType == ComputerType.Desktop)
                    {
                        totalDesktops++;
                    }
                    else if ((departs[i][j]).compType == ComputerType.Laptop)
                    {
                        totalLaptops++;
                    }
                    else if ((departs[i][j]).compType == ComputerType.Server)
                    {
                        totalServers++;
                    }
                }
            }

            Console.WriteLine($"Total devices: {totalComputers}, Total desktops: {totalDesktops}, Total laptops: {totalLaptops}, Total servers: {totalServers}");
            Computer[] config = new Computer[] {desktop, laptop, server};


            var configElement = config[0];
            int index = 0;
            for (int i = 0; i < config.Length; i++)
            {
                var item = config[i];
                if (item.storage > configElement.storage)
                {
                    index = i;
                    configElement = item;
                }
            }
            Console.WriteLine($"Max storage device: {configElement.compType}, {configElement.storage} GB, Index: {index}" );

            index = 0;
            for (int i = 0; i < config.Length; i++)
            {
                var item = config[i];
                if (item.proccesor < configElement.proccesor & item.opMemory < configElement.opMemory)
                {
                    index = i;
                    configElement = item;
                }
            }

            Console.WriteLine($"Lowest productivity device: {configElement.compType}, {configElement.proccesor} cores, Memory: {configElement.opMemory} GB, Index: {index}");

            Console.WriteLine($"Before update {desktop}");
            //for (int i = 0; i < departs.Length; i++)
            //{
            //    for (int j = 0; j < departs[i].Length; j++)
            //    {
            //        if ((departs[i][j]).compType == ComputerType.Desktop) ///////НЕПРАЦЮЄ :(((
            //        {
            //            desktop.opMemory = 8;
            //        }

            //    }
            //}

            desktop.opMemory = 8;
            Console.WriteLine($"After upgrade {desktop}");
            Console.ReadLine();
        }
    }
}
