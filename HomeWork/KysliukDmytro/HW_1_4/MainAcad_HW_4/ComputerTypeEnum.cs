﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MainAcad_HW_4
{
    public enum ComputerType:byte
    {
        Desktop = 1,
        Laptop,
        Server,
    }


    public struct Computer
    {

        public byte proccesor { get; set; }
        public double herz { get; set; }
        public byte opMemory { get; set; }
        public int storage { get; set; }
        public ComputerType compType { get; set; }


        public Computer(byte core, double ghz, byte memory, int hdd, ComputerType type)
        {
            proccesor = core;
            herz = ghz;
            opMemory = memory;
            storage = hdd;
            compType = type;
        }

        public override string ToString()
        {
            return $"{compType}: CPU - {proccesor} cores {herz} GHz, Memory - {opMemory}, HDD - {storage} GB";
        }
    }


}
