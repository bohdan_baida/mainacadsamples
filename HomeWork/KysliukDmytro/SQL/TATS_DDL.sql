CREATE DATABASE MainAcadTaskAnsterTestSystem

USE MainAcadTaskAnsterTestSystem


CREATE TABLE Task
(
	Id INT IDENTITY(1,1),
	Variant INT,
	Task VARCHAR(500),
	CONSTRAINT PK_Task_Id PRIMARY KEY(Id), 
	CONSTRAINT Task_Variant_Max_Two CHECK (Variant>0 and Variant<3),
)

CREATE TABLE Answer
(
	Id INT IDENTITY(1,1),
	IdTask INT,
	FirstAnswer VARCHAR(100),
	SecondAnswer VARCHAR(100),
	ThirdAnswer VARCHAR(100),
	FourthAnswer VARCHAR(100),
	RightAnster INT,
	CONSTRAINT PK_Answer_Id PRIMARY KEY(Id),
	CONSTRAINT FK_Answer_Taks FOREIGN KEY (IdTask) REFERENCES Task(Id),
	CONSTRAINT Answer_Max_Four CHECK (RightAnster>0 and RightAnster<5)
)

CREATE TABLE Student
(
	Id INT IDENTITY(1,1),
	FirstName VARCHAR(30),
	SecondName VARCHAR(40),
	Variant INT,
	TestScore INT,
	CONSTRAINT PK_Student_Id PRIMARY KEY(Id),
	CONSTRAINT Student_Variant_Max_Two CHECK (Variant>0 and Variant<3)
)
