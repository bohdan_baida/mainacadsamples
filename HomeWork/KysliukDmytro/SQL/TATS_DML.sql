﻿USE MainAcadTaskAnsterTestSystem

---------------------------------------------------------------------------------------------

INSERT INTO Task(Variant,Task)
VALUES(1, 'How to figure out the number one thousand six hundred six ?')

INSERT INTO Task(Variant, Task)
VALUES(1, 'What is the number of digit additions: 1000 + 10 +2 ?')

INSERT INTO Task(Variant, Task)
VALUES(2, '12+12:1 ?')

INSERT INTO Task(Variant, Task)
VALUES(2, 'What number should be added to 500 to get 800?')

---------------------------------------------------------------------------------------------

INSERT INTO Answer(IdTask, FirstAnswer, SecondAnswer, ThirdAnswer, FourthAnswer, RightAnster)
VALUES(1, '1060', '1666', '1660', '1606', 4)

INSERT INTO Answer(IdTask, FirstAnswer, SecondAnswer, ThirdAnswer, FourthAnswer, RightAnster)
VALUES(2, '112', '1002', '1012', '1120', 3)

INSERT INTO Answer(IdTask, FirstAnswer, SecondAnswer, ThirdAnswer, FourthAnswer, RightAnster)
VALUES(3, '24', '32', '48', '74', 1)

INSERT INTO Answer(IdTask, FirstAnswer, SecondAnswer, ThirdAnswer, FourthAnswer, RightAnster)
VALUES(4, '400', '300', '200', '800', 2)

---------------------------------------------------------------------------------------------

INSERT INTO Student(FirstName, SecondName, Variant, TestScore)
VALUES('Vasya', 'Vasiliev', 1, 100)

INSERT INTO Student(FirstName, SecondName, Variant, TestScore)
VALUES('Peter', 'Petrov', 2, 50)

INSERT INTO Student(FirstName, SecondName, Variant, TestScore)
VALUES('Alex', 'Alexeev', 1, 50)

INSERT INTO Student(FirstName, SecondName, Variant, TestScore)
VALUES('Kiril', 'Kirilov', 2, 0)

---------------------------------------------------------------------------------------------

SELECT * FROM Task

SELECT * FROM Answer

SELECT * FROM Student
