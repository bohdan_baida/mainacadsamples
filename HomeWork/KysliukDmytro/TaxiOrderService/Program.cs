﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaxiOrderService.Classes;
using TaxiOrderService.BusinessLogic;

namespace TaxiOrderService
{
    class Program
    {
        static void Main()
        {

            CustomerBO customerBO = new CustomerBO();
            OrderBO orderBO = new OrderBO();
            TaxiDriverBO taxiDriverBO = new TaxiDriverBO();
            CarBO carBO = new CarBO();


            customerBO.CreateCustomer(1, 1, "First Name", "Last Naki");
            //orderBO.CreateOrder(1, 1, 1, 100.50m, Enums.PayMethodEnum.ByCard, Enums.OrderStatusEnum.Active);
            taxiDriverBO.CreateTaxiDriver(1, 0, "Taxist FirstName", "Taxist LastName");
            carBO.CreateCar(1, "Tesla", "Model X", Enums.CarClassEnum.BusinessGreen, "Yarik");
            //------MainAcad-4-1-----//Triggers!!!!
            //customerBO.Subscribe(orderBO.CreateOrder);
            customerBO.OrderTaxi(1, 1, 1, 100.50m, Enums.PayMethodEnum.ByCard, Enums.OrderStatusEnum.Active, orderBO.CreateOrder);

            var order = orderBO.GetOrders();



            Console.Read();
        }
    }
}
