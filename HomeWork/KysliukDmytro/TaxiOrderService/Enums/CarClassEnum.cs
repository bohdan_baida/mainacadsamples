﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxiOrderService.Enums
{
    public enum CarClassEnum
    {
        Eco = 1,
        Standart,
        Business,
        Green,
        BusinessGreen,
    }
}
