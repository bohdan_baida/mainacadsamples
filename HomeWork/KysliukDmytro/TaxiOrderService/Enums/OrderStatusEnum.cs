﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxiOrderService.Enums
{
    public enum OrderStatusEnum
    {
        Active = 1,
        Booked,
        InDrive,
        WaitingForPayment,
        Completed,
    }
}
