﻿using TaxiOrderService.Enums;


namespace TaxiOrderService.Classes
{
    public class Order
    {
        public int Id { get; set; }
        public int DriverId { get; set; }
        public int CustomerId { get; set; }
        public decimal Payment { get; set; }
        public PayMethodEnum PayMethod { get; set; }
        //public string StartPoint { get; set; }
        //public string FinishPoint { get; set; }
        public OrderStatusEnum Status { get; set; }
    }
}
