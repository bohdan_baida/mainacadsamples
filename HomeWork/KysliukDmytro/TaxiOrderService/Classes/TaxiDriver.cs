﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaxiOrderService.Classes
{
    public class TaxiDriver
    {
        public int Id { get; set; }
        public int NumberOfTrips { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }

    }
}
