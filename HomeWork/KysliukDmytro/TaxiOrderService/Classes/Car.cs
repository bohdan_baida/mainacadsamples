﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaxiOrderService.Enums;

namespace TaxiOrderService.Classes
{
    public class Car
    {
        public int DriverId { get; set; }
        public string MadeMark { get; set; }
        public string Model { get; set; }
        public CarClassEnum Class { get; set; }
        public string Number { get; set; }

    }
}
