﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaxiOrderService.Classes;

namespace TaxiOrderService.BusinessLogic
{
    public class TaxiDriverBO
    {
        TaxiDriver taxiDriver;
        List<TaxiDriver> taxiDrivers;

        public TaxiDriverBO()
        {
            taxiDriver = new TaxiDriver();
            taxiDrivers = new List<TaxiDriver>();
        }

        public void CreateTaxiDriver(int id, int numberOfTrips, string firstName, string secondName)
        {
            taxiDriver.Id = id;
            taxiDriver.NumberOfTrips = numberOfTrips;
            taxiDriver.FirstName = firstName;
            taxiDriver.SecondName = secondName;

            taxiDrivers.Add(taxiDriver);
        }

        public List<TaxiDriver> GetTaxiDrivers()
        {
            return taxiDrivers;
        }

        public void TakeCar(int carId)
        {

        }
    }
}
