﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaxiOrderService.Classes;
using TaxiOrderService.Enums;

namespace TaxiOrderService.BusinessLogic
{
    //public delegate void OrderCreated(int id, int driverId, int customerId, decimal payment, PayMethodEnum payMethod, OrderStatusEnum orderStatus);
    public class OrderBO
    {
        Order order;
        List<Order> orders;
        //public delegate void OnOrderTaxi(int id, int driverId, int customerId, decimal payment, PayMethodEnum payMethod, OrderStatusEnum orderStatus);
        //public event OnOrderTaxi subcribers;


        public OrderBO()
        {
            order = new Order();
            orders = new List<Order>();
        }

        public void CreateOrder(int id, int driverId, int customerId, decimal payment, PayMethodEnum payMethod, OrderStatusEnum orderStatus)
        {
            order.Id = id;
            order.DriverId = driverId;
            order.CustomerId = customerId;
            order.Payment = payment;
            order.Status = orderStatus;
            order.PayMethod = payMethod;

            orders.Add(order);
            //subcribers?.Invoke(id, driverId, customerId, payment, payMethod, orderStatus);
            
        }

        //public void Subscribe(OnOrderTaxi ordercreated)
        //{
        //    subcribers += ordercreated;
        //}

        //public void Unubscribe(OnOrderTaxi ordercreated)
        //{
        //    subcribers -= ordercreated;
        //}



        public List<Order> GetOrders()
        {
            return orders.ToList();
        }
    }
}
