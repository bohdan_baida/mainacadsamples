﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaxiOrderService.Classes;
using TaxiOrderService.Enums;

namespace TaxiOrderService.BusinessLogic
{
    public class CarBO
    {
        Car car;
        List<Car> cars;

        public CarBO()
        {
            car = new Car();
            cars = new List<Car>();
        }

        public void CreateCar(int driverId, string madeMark, string model, CarClassEnum _class, string number)
        {
            car.DriverId = driverId;
            car.MadeMark = madeMark;
            car.Model = model;
            car.Class = _class;
            car.Number = number;

            cars.Add(car);
        }

        public List<Car> GetCars()
        {
            return cars;
        }
    }
}
