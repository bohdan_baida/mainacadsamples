﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaxiOrderService.Classes;
using TaxiOrderService.Enums;

namespace TaxiOrderService.BusinessLogic
{

    public class CustomerBO
    {
        public delegate void OnOrderTaxi(int id, int driverId, int customerId, decimal payment, PayMethodEnum payMethod, OrderStatusEnum orderStatus);
        public event OnOrderTaxi subcribers;


        Customer customer;
        OrderBO orderBO;
        List<Customer> customers;

        public CustomerBO()
        {
            customer = new Customer();
            orderBO = new OrderBO();
            customers = new List<Customer>();
        }

        public void CreateCustomer(int id, int orderId, string firstName, string lastName)
        {
            customer.Id = id;
            customer.OrderId = orderId;
            customer.FirstName = firstName;
            customer.LastName = lastName;
            customers.Add(customer);
        }

        

        public List<Customer> GetCustomers()
        {
            return customers.ToList();
        }


        public void Subscribe(OnOrderTaxi ordercreated)
        {
            subcribers += ordercreated;
        }

        public void Unubscribe(OnOrderTaxi ordercreated)
        {
            subcribers -= ordercreated;
        }

        public void OrderTaxi(int id, int driverId, int customerId, decimal payment, PayMethodEnum payMethod, OrderStatusEnum orderStatus, OnOrderTaxi orderTaxi)
        {
            subcribers += orderTaxi;
            subcribers?.Invoke(id, driverId, customerId, payment, payMethod, orderStatus);
        }

        //public void OrderTaxi(int id, int driverId, int customerId, decimal payment, PayMethodEnum payMethod, OrderStatusEnum orderStatus)
        //{
        //    orderBO.Subscribe(orderBO.CreateOrder(id, driverId, customerId, payment, payMethod, orderStatus));
        //}

    }
}
