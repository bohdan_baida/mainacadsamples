﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloOperators_stud
{
    class Program
    {
        static void Main(string[] args)
        {
          
                long a;
                Console.WriteLine(@"Please,  type the number:
            1. Farmer, wolf, goat and cabbage puzzle
            2. Console calculator
            3. Factirial calculation
            4. Randomiser
            ");

                a = long.Parse(Console.ReadLine());
                switch (a)
                {
                    case 1:
                        Farmer_puzzle();
                        Console.WriteLine("");
                        break;
                    case 2:
                        Calculator();
                        Console.WriteLine("");
                        break;
                    case 3:
                        Factorial_calculation();
                        Console.WriteLine("");
                        break;
                    case 4:
                        Randomiser();
                        Console.WriteLine("");
                        break;
                    default:
                        Console.WriteLine("Exit");
                        break;
                }
                Console.WriteLine("Press any key");
            
                Console.ReadLine();
            
        }
            #region farmer
            static void Farmer_puzzle()
            {
                //Key sequence: 3817283 or 3827183
                // Declare 7 int variables for the input numbers and other variables
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine(@"From one bank to another should carry a wolf, goat and cabbage. 
At the same time can neither carry nor leave together on the banks of a wolf and a goat, 
a goat and cabbage. You can only carry a wolf with cabbage or as each passenger separately. 
You can do whatever how many flights. How to transport the wolf, goat and cabbage that all went well?");
                Console.WriteLine("There: farmer and wolf - 1");
                Console.WriteLine("There: farmer and cabbage - 2");
                Console.WriteLine("There: farmer and goat - 3");
                Console.WriteLine("There: farmer  - 4");
                Console.WriteLine("Back: farmer and wolf - 5");
                Console.WriteLine("Back: farmer and cabbage - 6");
                Console.WriteLine("Back: farmer and goat - 7");
                Console.WriteLine("Back: farmer  - 8");
                Console.WriteLine("Please,  type numbers by step ");

                int isWolfRightSide = 0;    //Можна через bool, но я художник я так бачу :D
                int isCabbegeRightSide = 0;
                int isGoatRightSide = 0;
                int isFarmerRightSide = 0;
                int isAllOnRightSide = 0;

                while (isAllOnRightSide != 3)
                {
                    switch (Console.ReadLine())
                    {
                        case "1":
                            if (isWolfRightSide == 1)
                            {
                                Console.ForegroundColor = ConsoleColor.DarkCyan;
                                Console.WriteLine("WOLF already on RIGHT side!");
                                Console.ForegroundColor = ConsoleColor.Yellow;
                                break;
                            } else if (isFarmerRightSide == 1)
                            {
                                Console.ForegroundColor = ConsoleColor.DarkGreen;
                                Console.WriteLine("FARMER on RIGHT side! Move him back! [Press 8]");
                                Console.ForegroundColor = ConsoleColor.Yellow;
                                break;
                            }
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.WriteLine("Farmer move WOLF to RIGHT side!");
                            Console.ForegroundColor = ConsoleColor.Yellow;
                            if (isCabbegeRightSide == 0 && isGoatRightSide == 0)
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("Goat ate cabbage! Try again...");
                                Console.ForegroundColor = ConsoleColor.Yellow;
                                break;
                            }
                            isWolfRightSide++;
                            isFarmerRightSide++;
                            isAllOnRightSide++;
                            break;
                        case "2":
                            if (isCabbegeRightSide == 1)
                            {
                                Console.ForegroundColor = ConsoleColor.DarkCyan;
                                Console.WriteLine("CABBAGE already on RIGHT side!");
                                Console.ForegroundColor = ConsoleColor.Yellow;
                                break;
                            } else if (isFarmerRightSide == 1)
                            {
                                Console.ForegroundColor = ConsoleColor.DarkGreen;
                                Console.WriteLine("FARMER on RIGHT side! Move him back! [Press 8]");
                                Console.ForegroundColor = ConsoleColor.Yellow;
                                break;
                            }
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.WriteLine("Farmer move CABBAGE to RIGHT side!");
                            Console.ForegroundColor = ConsoleColor.Yellow;
                            if (isWolfRightSide == 0 && isGoatRightSide == 0)
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("Wolf ate goat! Try again...");
                                Console.ForegroundColor = ConsoleColor.Yellow;
                                break;
                            }
                            isCabbegeRightSide++;
                            isFarmerRightSide++;
                            isAllOnRightSide++;
                            break;
                        case "3":
                            if (isGoatRightSide == 1)
                            {
                                Console.ForegroundColor = ConsoleColor.DarkCyan;
                                Console.WriteLine("GOAT already on RIGHT side!");
                                Console.ForegroundColor = ConsoleColor.Yellow;
                                break;
                            } else if (isFarmerRightSide == 1) {
                                Console.ForegroundColor = ConsoleColor.DarkGreen;
                                Console.WriteLine("FARMER already on RIGHT side!");
                                Console.ForegroundColor = ConsoleColor.Yellow;
                                break;
                            }
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.WriteLine("Farmer move GOAT to RIGHT side!");
                            Console.ForegroundColor = ConsoleColor.Yellow;
                            isGoatRightSide++;
                            isFarmerRightSide++;
                            isAllOnRightSide++;
                            break;
                        case "4":
                            if (isFarmerRightSide == 1)
                            {
                                Console.ForegroundColor = ConsoleColor.DarkGreen;
                                Console.WriteLine("Farmer already on RIGHT side! Move him back! [Press 8]");
                                Console.ForegroundColor = ConsoleColor.Yellow;
                                break;
                            }
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.WriteLine("Farmer move to RIGHT side alone!");
                            Console.ForegroundColor = ConsoleColor.Yellow;
                            if (isGoatRightSide == 0 && isWolfRightSide == 0)
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("Wolf ate goat! Try again...");
                                Console.ForegroundColor = ConsoleColor.Yellow;
                                break;
                            } else if (isCabbegeRightSide == 0 && isGoatRightSide == 0)
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("Goat ate cabbage! Try again...");
                                Console.ForegroundColor = ConsoleColor.Yellow;
                                break;
                            }
                            isFarmerRightSide++;
                            break;
                        case "5":
                            if (isWolfRightSide == 0)
                            {
                                Console.ForegroundColor = ConsoleColor.DarkCyan;
                                Console.WriteLine("WOLF already on LEFT side!");
                                Console.ForegroundColor = ConsoleColor.Yellow;
                                break;
                            } else if (isFarmerRightSide == 0)
                            {
                                Console.ForegroundColor = ConsoleColor.DarkGreen;
                                Console.WriteLine("Farmer already on LEFT side! Move him! [Press 4]");
                                Console.ForegroundColor = ConsoleColor.Yellow;
                                break;
                            }
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.WriteLine("Farmer move WOLF to LEFT side!");
                            Console.ForegroundColor = ConsoleColor.Yellow;
                            if (isGoatRightSide == 1 && isCabbegeRightSide == 1)
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("Goat ate cabbage! Try again...");
                                Console.ForegroundColor = ConsoleColor.Yellow;
                                break;
                            }
                            isFarmerRightSide--;
                            isWolfRightSide--;
                            isAllOnRightSide--;
                            break;
                        case "6":
                            if (isCabbegeRightSide == 0)
                            {
                                Console.ForegroundColor = ConsoleColor.DarkCyan;
                                Console.WriteLine("CABBAGE already on LEFT side!");
                                Console.ForegroundColor = ConsoleColor.Yellow;
                                break;
                            }
                            else if (isFarmerRightSide == 0)
                            {
                                Console.ForegroundColor = ConsoleColor.DarkGreen;
                                Console.WriteLine("Farmer already on LEFT side! Move him! [Press 4]");
                                Console.ForegroundColor = ConsoleColor.Yellow;
                                break;
                            }
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.WriteLine("Farmer move CABBAGE to LEFT side!");
                            Console.ForegroundColor = ConsoleColor.Yellow;
                            if (isGoatRightSide == 1 && isWolfRightSide == 1)
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("Goat ate cabbage! Try again...");
                                Console.ForegroundColor = ConsoleColor.Yellow;
                                break;
                            }
                            isCabbegeRightSide--;
                            isFarmerRightSide--;
                            isAllOnRightSide--;
                            break;
                        case "7":
                            if (isGoatRightSide == 0)
                            {
                                Console.ForegroundColor = ConsoleColor.DarkCyan;
                                Console.WriteLine("GOAT already on LEFT side!");
                                Console.ForegroundColor = ConsoleColor.Yellow;
                                break;
                            }
                            else if (isFarmerRightSide == 0)
                            {
                                Console.ForegroundColor = ConsoleColor.DarkGreen;
                                Console.WriteLine("Farmer already on LEFT side! Move him! [Press 4]");
                                Console.ForegroundColor = ConsoleColor.Yellow;
                                break;
                            }
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.WriteLine("Farmer move GOAT to LEFT side!");
                            Console.ForegroundColor = ConsoleColor.Yellow;
                            isFarmerRightSide--;
                            isGoatRightSide--;
                            isAllOnRightSide--;
                            break;
                        case "8":
                            if (isFarmerRightSide == 0)
                            {
                                Console.ForegroundColor = ConsoleColor.DarkGreen;
                                Console.WriteLine("Farmer already on LEFT side! Move him! [Press 4]");
                                Console.ForegroundColor = ConsoleColor.Yellow;
                                break;
                            }
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.WriteLine("Farmer move to LEFT side alone!");
                            Console.ForegroundColor = ConsoleColor.Yellow;
                            if (isGoatRightSide == 1 && isWolfRightSide == 1)
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("Wolf ate goat! Try again...");
                                Console.ForegroundColor = ConsoleColor.Yellow;
                                break;
                            }
                            else if (isCabbegeRightSide == 1 && isGoatRightSide == 1)
                            {
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine("Goat ate cabbage! Try again...");
                                Console.ForegroundColor = ConsoleColor.Yellow;
                            }
                            isFarmerRightSide--;
                            break;
                    }
                }
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Congratulations!!!!! You moved them all and nobody was eaten!!!!");

                // Implement input and checking of the 7 numbers in the nested if-else blocks with the  Console.ForegroundColor changing

            }
            #endregion

            #region calculator
            static void Calculator()
            {
                Console.ForegroundColor = ConsoleColor.Green;
                var calFst = 0;
                var calSnd = 0;
                var calAnsw = 0;
                while (true)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("Type first number: ");
                    if (int.TryParse(Console.ReadLine(), out calFst))
                    {
                        Console.WriteLine(calFst + " is accepted!");
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Wrong symbol! Try again...");
                        continue;
                    }
                    Console.WriteLine("Type second number: ");
                    if (int.TryParse(Console.ReadLine(), out calSnd))
                    {
                        Console.WriteLine(calSnd + " is accepted!");
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Wrong symbol! Try again...");
                        continue;
                    }
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine(@"Select the arithmetic operation:
1. Multiplication 
2. Divide 
3. Addition 
4. Subtraction:
5. Exponentiation ");
                    int calcul = int.Parse(Console.ReadLine());
                    switch (calcul)
                    {
                        case 1:
                            Console.WriteLine("Multilpication");
                            calAnsw = calFst * calSnd;
                            Console.WriteLine("Answer is - " + calAnsw);
                            break;
                        case 2:
                            Console.WriteLine("Divide");
                            calAnsw = calFst / calSnd;
                            Console.WriteLine("Answer is - " + calAnsw);
                            break;
                        case 3:
                            Console.WriteLine("Addition");
                            calAnsw = calFst + calSnd;
                            Console.WriteLine("Answer is - " + calAnsw);
                            break;
                        case 4:
                            Console.WriteLine("Subtraction");
                            calAnsw = calFst - calSnd;
                            Console.WriteLine("Answer is - " + calAnsw);
                            break;
                        case 5:
                            Console.WriteLine("Exponentiation");
                            calAnsw = calFst * calFst;
                            Console.WriteLine("Answer is - " + calAnsw);
                            break;

                    }

                }

                // Implement option input (1,2,3,4,5)
                //     and input of  two or one numbers
                //  Perform calculations and output the result 
            }
            #endregion

            #region Factorial
            static void Factorial_calculation()
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Type number to factorial it:");
                long number = 0;


                while (true) {
                    Console.ForegroundColor = ConsoleColor.Cyan;
                    if (long.TryParse(Console.ReadLine(), out number))
                    {
                        long factAnsw = 1;
                        for (long i = 1; i <= number; i++)
                            factAnsw *= i;
                        Console.WriteLine("Factorial of " + number + " is " + factAnsw);
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine("Type number to factorial it:");
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Wrong symbol! Try again...");
                        continue;
                    }
                }


                // Implement input of the number
                // Implement input the for circle to calculate factorial of the number
                // Output the result
            }
        #endregion

        #region Randomiser
        static void Randomiser()
        {
            int a = 0;
            const int MyMax = 10;
            while (true)
            {
                Random random = new Random();
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("Enter your value:");
                int guessNumber = random.Next(1, MyMax); //зробив від 1 до 10 щоб було зручніше тестити
                if (int.TryParse(Console.ReadLine(), out a))
                {
                    if (a == guessNumber)
                    {
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine("CONGRATULATIONS You GUESSED a random value!!!!");
                        Console.ForegroundColor = ConsoleColor.Yellow;
                    }
                    else if (a <= guessNumber)
                    {
                        Console.ForegroundColor = ConsoleColor.Cyan;
                        Console.WriteLine("Your number is LOWER then random value!");
                        Console.ForegroundColor = ConsoleColor.Yellow;
                    }
                    else if (a >= guessNumber)
                    {
                        Console.ForegroundColor = ConsoleColor.Cyan;
                        Console.WriteLine("Your number is HIGHER then random value!");
                        Console.ForegroundColor = ConsoleColor.Yellow;
                    }
                } else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("You type a wrong symbol! Try again...");
                    continue;
                }

            }
        }
        #endregion
    }
}


