﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
namespace Main_Acad_AirportPanel
{

    public enum Cities : byte
    {
        Washington = 1,
        Paris,
        Milan,
        Madrid,
        Dortmund,
        Barcelona,
        Moscow,
        Rivne,
        Amsterdam,
        London,
        Ldsfsdfsfsdfsdfsdfdsfsdfsdfsdfsd
    }

    public enum FlightStatus : byte
    {
        CheckIn = 1,
        Departed,
        Unknown,
        Canceled,
        Delayed,
    }

    public enum Airplane : byte
    {
        Superjet100 = 1,
        Airbus310,
        Boeing737,
    }

    public enum AirLine : byte
    {
        AnexTour = 1,
        LiveCell,
        CurvaTravel,
    }

    public enum Terminals:byte
    {
        A1 = 1,
        A2,
        A3,
        A4,
        B1,
        B2,
        B3,
        B4,
        C1,
        C2,
        C3,
        C4,
        D1,
        D2,
        D3,
        D4,
    }


    public class AirPanel
    {
        public int index { get; set; }
        public string thisCity { get; set; }
        public Cities arriveCity { get; set; }
        public DateTime arriveDate { get; set; }
        public Airplane airplanes { get; set; }
        public AirLine airlines { get; set; }
        public Terminals terminals { get; set; }
        public FlightStatus status { get; set; }


        public AirPanel(string tcity, Cities city, DateTime date, Airplane airp, AirLine airl, Terminals termin, FlightStatus stat, int id)
        {
            thisCity = tcity;
            arriveCity = city;
            arriveDate = date;
            airplanes = airp;
            airlines = airl;
            terminals = termin;
            status = stat;
            index = id;
        }

        public override string ToString()
        {
            return $"|  {thisCity}\t|  {arriveCity}\t| {arriveDate}\t| {airplanes}\t| {airlines}\t|     {terminals} \t| {status}\t|   {index}\t |";
        }

    }
       



    


}

