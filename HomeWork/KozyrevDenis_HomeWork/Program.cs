﻿using System;

namespace Homework_GuessTheNumber
{
    class Program
    {
        static void Main(string[] args)
        {
            const int MyVar = 10;
            Random random = new Random();
            int GuessNum = random.Next(MyVar);

            Console.WriteLine("Guess the number between 0 and 10 ");
            int gs = 0;
            

            while (gs != GuessNum)
            {
                Console.Write("Guess your num: ");
                gs = Convert.ToInt32(Console.ReadLine());

                if (gs > GuessNum)
                {
                    Console.WriteLine( gs + " is so far");
                } else if (gs < GuessNum)
                {
                    Console.WriteLine(gs + " is so low");
                } else
                {
                    Console.WriteLine(gs + "! It is!");
                }
            }
            Console.ReadLine();

        }
    }
}
